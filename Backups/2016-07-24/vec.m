function vector = vec(array)

vector = reshape(array,numel(array),1);