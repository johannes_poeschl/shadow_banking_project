% Calibration of BCR

clear all
clc

% Data ragets
R_D = 1.008;
R_B = 1.0093;
R_K = 1.0151;

K_H_ratio = 0.29; 
K_R_ratio = 0.45;
% S_leverage = 0.7;
S_leverage = 0.9; 

% Parameter group 1
p_alpha = 0.36;
p_delta = 0.025;
p_gamma_R = 0.08;
p_omega_B = 0.5;
p_omega_K = 1;

% Parameter group 2 from Steady state conditions
p_beta_H   = 1/R_D;
p_beta_R   = p_gamma_R*p_omega_B/(R_B-R_D*(1-p_gamma_R*p_omega_B));
K          = ((R_K-1+p_delta)/p_alpha)^(1/(p_alpha-1));
K_H        = K_H_ratio*K;
p_alpha_H  = (p_beta_H*R_K-1)/K_H;
K_R        = K_R_ratio*K;
p_alpha_R  = (p_beta_R*(R_K-R_D*(1-p_gamma_R*p_omega_K))-p_gamma_R*p_omega_K)/K_R;
K_S        = K - K_H - K_R;
p_theta_S  = 1 - S_leverage * R_B /R_K;
p_beta_S   = (R_B/R_K-1+p_theta_S)/p_theta_S/R_B;





