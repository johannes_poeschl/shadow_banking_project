%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBurnin = 250;
nSimulations = nBurnin + 100;  % number of simulated periods 

%- (1) random sequence of Z -
% seq_Z_transpose = simulateMarkovChain(1,Z_grid,Z_prob,nSimulations);
% seq_Z = seq_Z_transpose';

% - (2) impulse response function -
seq_Z = mean(Z_grid)*ones(1,nSimulations);
seq_Z(1,nBurnin + 30) = Z_grid(1,1); 

% -- Interpolate policy function -- ////
funs_NoRun = struct;
funs_NoRun.K_H_int = base_poly\vec_K_H_prime;
funs_NoRun.K_R_int = base_poly\vec_K_R_prime;
funs_NoRun.K_S_int = base_poly\vec_K_S_prime;
funs_NoRun.D_int = base_poly\vec_D_prime;
funs_NoRun.B_int = base_poly\vec_B_prime;
funs_NoRun.R_D_int = base_poly\vec_R_D_prime;
funs_NoRun.R_B_int = base_poly\vec_R_B_prime;
funs_NoRun.Q_int = base_poly\vec_Q;

% states, no run
seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state //////
seq_K   = ones(1, nSimulations)*K_SS;    % state

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice %////
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice %////
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price  %////
seq_R_K_prime = ones(1, nSimulations);  % price
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);  % price, no run
seq_Q_old = seq_Q; % price, no run, guess

qError = 10;
 
while qError > 1e-6;
    for iii = 1: nSimulations-1
        
        eval_point = [seq_N_H(1, iii) seq_N_R(1, iii),...
                          seq_N_S(1, iii) seq_K(1, iii)];
        eval_point_rescaled = Rescale(eval_point,funs.unit_min,funs.unit_max, ...
            funs.orig_min,funs.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);
        
        % Policy Functions
        seq_K_H_prime(1, iii) = eval_poly*funs_NoRun.K_H_int;
        seq_K_R_prime(1, iii) = eval_poly*funs_NoRun.K_R_int;
        seq_K_S_prime(1, iii) = eval_poly*funs_NoRun.K_S_int;              
        seq_R_D_prime(1, iii) = eval_poly*funs_NoRun.R_D_int;
        seq_R_B_prime(1, iii) = eval_poly*funs_NoRun.R_B_int;
        seq_D_prime(1, iii)   = eval_poly*funs_NoRun.D_int;
        seq_B_prime(1, iii)   = eval_poly*funs_NoRun.B_int;
        seq_Q(1,iii)    = eval_poly*funs_NoRun.Q_int; 
        
        % Auxiliary Variables
        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  
        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);
        

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;
        
        % State Variables
        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
              
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
end

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime;  % /////
seq_Y   = seq_Z .* seq_K.^p.alpha;

leverage_R = seq_D_prime./(seq_K_R_prime.*seq_Q + seq_B_prime);
leverage_S = seq_B_prime./(seq_K_S_prime.*seq_Q);
grossLeverageBoth = (seq_B_prime + seq_D_prime)./((seq_K_R_prime + seq_K_S_prime).*seq_Q + seq_B_prime);

%%
% compare bounds

fprintf('Minimum, Simulation \n')
display([min(seq_N_H(1:end-1)) min(seq_N_R(1:end-1)) min(seq_N_S(1:end-1)) min(seq_K(1:end-1))])

fprintf('Minimum, Solution Algorithm \n')
display(orig_min)

fprintf('Maximum, Simulation \n')
display([max(seq_N_H(1:end-1)) max(seq_N_R(1:end-1)) max(seq_N_S(1:end-1)) max(seq_K(1:end-1))])

fprintf('Maximum, Solution Algorithm \n')
display(orig_max)

%%
% plots

vals = [log(seq_K_H_prime(nBurnin:end-1)) - log(K_H_SS);
    log(seq_K_R_prime(nBurnin:end-1)) - log(K_R_SS);
    log(seq_K_S_prime(nBurnin:end-1)) - log(K_S_SS);
    log(seq_K(nBurnin+1:end)) - log(K_SS)];

figure
for ii = 1:size(vals,1)
    subplot(2,2,ii)
    plot(vals(ii,:))
end
    