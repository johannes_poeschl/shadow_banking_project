%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

tic
nBurnin = 200;
nSimulations = nBurnin + 1000;  % number of simulated periods 
nPanels = 1;

gammaVals = [0.05 0.08 0.15];
dateVal = date;

bankRunProb = zeros(nPanels,length(gammaVals));
cons_eq_H = zeros(nPanels,length(gammaVals));
cons_eq_R = zeros(nPanels,length(gammaVals));
cons_eq_S = zeros(nPanels,length(gammaVals));

% -- Simulate a sequence of Z: choose between (1) & (2) --

for kk = 1:length(gammaVals)
%     if kk <= length(gammaVals) - 2
%         dateVal = date;
%     else
%         dateVal = '17-May-2016';
%     end
load(strcat('Result_NoRun_',dateVal,'_Gamma_',num2str(gammaVals(kk)),'.mat'));

% -- Interpolate policy function -- ////
funs_NoRun = struct;
funs_NoRun.K_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_H_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.K_R_int = griddedInterpolant({N_H_grid, N_R_grid ,N_S_grid, K_grid},...
    reshape(vec_K_R_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.K_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_S_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_D_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.R_D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_D_prime, n_N_H, n_N_R , n_N_S, n_K),'linear');
funs_NoRun.R_B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');

load(strcat('Result_Run_',dateVal,'_Gamma_',num2str(gammaVals(kk)),'.mat'));

funs_Run = struct;
for tt = 1:2
funs_Run(tt).K_H_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(K_H_prime_run(:,tt), n_N_H, n_N_R, n_K),'linear');
funs_Run(tt).K_R_int = griddedInterpolant({N_H_grid, N_R_grid , K_grid},...
    reshape(K_R_prime_run(:,tt), n_N_H, n_N_R, n_K),'linear');
funs_Run(tt).D_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(D_prime_run(:,tt), n_N_H, n_N_R, n_K),'linear');
funs_Run(tt).R_D_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(R_D_prime_run(:,tt), n_N_H, n_N_R , n_K),'linear');
funs_Run(tt).Q_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(Q_run(:,tt), n_N_H, n_N_R, n_K),'linear');
end

for jj = 1:nPanels
%- (1) random sequence of Z -
seq_Z_transpose = simulateMarkovChain(1,Z_grid,Z_prob,nSimulations);
seq_Z = seq_Z_transpose';

% states, no run
seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state //////
seq_K   = ones(1, nSimulations)*K_SS;    % state

% states, run
seq_N_H_star = ones(1, nSimulations)*N_H_SS; 
seq_N_R_star = ones(1, nSimulations)*N_R_SS;
seq_N_S_star = p.exoInc*ones(1, nSimulations);

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice %////
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice %////
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price  %////
seq_R_K_prime = ones(1, nSimulations);  % price
seq_R_K_prime_star = ones(1, nSimulations);
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);  % price, no run
seq_Q_old = seq_Q; % price, no run, guess
seq_Q_star = ones(1, nSimulations); % price, run
seq_Q_star_old = ones(1, nSimulations); % price, run, guess

seq_run = zeros(1, nSimulations);

qError = 10;
 
while qError > 1e-5;
    for iii = 1: nSimulations-1
        
        if iii < 10
            seq_run(1,iii) = 0;
        else
            seq_run(1,iii) = ((1-p.phi)*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii) < ...
                     seq_R_B_prime(1, iii)*seq_B_prime(1, iii)) && seq_run(1,iii-1) == 0;
        end
        
        if seq_run(1,iii) == 1
            if seq_run(1,iii) == 1
                tt = 1;
            elseif seq_run(1,iii-1) == 1
                tt = 2;
            end           
            seq_N_H(1, iii) = seq_N_H_star(1,iii);
            seq_N_R(1 ,iii) = seq_N_R_star(1,iii);
            
            seq_K_H_prime(1, iii) = max(funs_Run(tt).K_H_int(seq_N_H(1, iii), seq_N_R(1, iii), ...
                seq_K(1, iii)),0);
            seq_K_R_prime(1, iii) = max(funs_Run(tt).K_R_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_K(1, iii)),0);
            seq_K_S_prime(1, iii) = 0;              
            seq_R_D_prime(1, iii) = funs_Run(tt).R_D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_K(1, iii));
            seq_R_B_prime(1, iii) = 0;
  
            seq_D_prime(1, iii)   = funs_Run(tt).D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_K(1, iii));
            seq_B_prime(1, iii)   = 0;
            seq_Q(1,iii)    = funs_Run(tt).Q_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                            seq_K(1, iii));
        else 
            seq_K_H_prime(1, iii) = funs_NoRun.K_H_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii), seq_K(1, iii));
            seq_K_R_prime(1, iii) = funs_NoRun.K_R_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii),seq_K(1, iii));
            seq_K_S_prime(1, iii) = funs_NoRun.K_S_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii),seq_K(1, iii));
            seq_D_prime(1, iii)   = funs_NoRun.D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii), seq_K(1, iii));

        	seq_B_prime(1, iii)   = funs_NoRun.B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii), seq_K(1, iii));
            seq_R_D_prime(1, iii) = funs_NoRun.R_D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii),seq_K(1, iii));
            seq_R_B_prime(1, iii) = funs_NoRun.R_B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii),seq_K(1, iii));
                          
            seq_Q(1,iii)    = funs_NoRun.Q_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                              seq_N_S(1, iii),seq_K(1, iii));
        end             
       
        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  
        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);
                        
        seq_Q_star(1,iii) = funs_Run(1).Q_int(seq_N_H_star(1,iii),seq_N_R_star(1,iii), ...
            seq_K(1,iii));

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);
                      
        seq_R_K_prime_star(1,iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
                        
        seq_N_H_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_H_prime(1, iii) + ...
                 seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;
        seq_N_S_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii) -...
                     seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                    + p.exoInc;
              
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);
end

fprintf('\n Panel %d, Simulation %d finished \n \n',kk,jj)

bankRunProb(jj,kk) = 100*mean(seq_run(nBurnin+1:end));

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime;  % /////

util_H = log(seq_C_H(nBurnin+1:end));
util_R = log(seq_C_R(nBurnin+1:end));
util_S = log(seq_C_R(nBurnin+1:end));

tt = 0:1:nSimulations-nBurnin-1;

seq_V_H = sum(p.beta_H.^tt.*util_H);
seq_V_R = sum(p.beta_R.^tt.*util_R);
seq_V_S = sum(p.beta_S.^tt.*util_S);

cons_eq_H(jj,kk) = exp((1-p.beta_H)*seq_V_H);
cons_eq_R(jj,kk) = exp((1-p.beta_R)*seq_V_R);
cons_eq_S(jj,kk) = exp((1-p.beta_S)*seq_V_S);
end

end

save('WelfarePlots.mat')

toc

%%

fontOptions = {'FontSize',15};

gammaVals= [0.05 0.08 0.15];

figure
plot(gammaVals,mean(bankRunProb,1),'LineWidth',2)
grid on
title('Realized Bank Run Probability',fontOptions{:})
xlabel('\Gamma^R',fontOptions{:})
ylabel('Bank Runs Per 100 Periods',fontOptions{:})

saveas(gcf,'./Figures/BankRunProb.fig')

yVals = [mean(cons_eq_H,1); mean(cons_eq_R,1); mean(cons_eq_S,1); ];

names = {'Cons Eq HH','Cons Eq R Banks','Cons Eq S Banks','Cons Eq All'};

figure
for ii = 1:size(yVals,1)
    subplot(1,3,ii)
    plot(gammaVals,yVals(ii,:),'LineWidth',2)
    grid on
    title(names{ii},fontOptions{:})
    xlabel('\Gamma^R',fontOptions{:})
    ylabel('Perm Cons Equiv',fontOptions{:})
end
saveas(gcf,'./Figures/Welfare.fig')