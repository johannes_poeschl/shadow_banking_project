%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
close all

addpath('./Smolyak_Anisotropic_JMMV_2014')

% Parameter
mu_H = 0.5;
mu_R = 0.25;
mu_S = 0.25;
nu = 0.08; %%%%% probability that bankrun happens in case bankrun condition is met, i.e. coordination failure


tic
nBurnin = 200; 
nSimulations = nBurnin + 800;  % number of simulated periods 
nPanels = 1000; %%%%% number of simulations for each capital requirement level

gammaVals = [0.05, 0.08, 0.12, 0.15, 0.2];  %%%%%

dateVal = '26-Aug-2016'; %date; %%%%%

bankRunProb = zeros(nPanels,length(gammaVals));
cons_eq_H = zeros(nPanels,length(gammaVals));
cons_eq_R = zeros(nPanels,length(gammaVals));
cons_eq_S = zeros(nPanels,length(gammaVals));


for kk = 1:length(gammaVals) %%%%% for each capital requirement level
    
load(strcat('Result_NoRun_',dateVal,'_Gamma_',num2str(gammaVals(kk)),'.mat'));
load(strcat('Result_Run_',dateVal,'_Gamma_',num2str(gammaVals(kk)),'.mat'));

for jj = 1:nPanels  %%%%% for each simulation 
% --  random sequence of Z --
seq_Z_transpose = simulateMarkovChain(Z_grid(5),Z_grid,Z_prob,nSimulations);
seq_Z = seq_Z_transpose';

% -- Interpolate policy function -- 

% no run case
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

funs.K_H_int = No_Run_Poly\pols.K_H_prime;
funs.K_R_int = No_Run_Poly\pols.K_R_prime;
funs.K_S_int = No_Run_Poly\pols.K_S_prime;
funs.D_int   = No_Run_Poly\pols.D_prime;
funs.B_int   = No_Run_Poly\pols.B_prime;
funs.R_D_int = No_Run_Poly\pols.R_D_prime;
funs.R_B_int = No_Run_Poly\pols.R_B_prime;

% run case 

Run_Grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
Run_Poly = Smolyak_Polynomial(Run_Grid,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

funs_run.K_H_int = Run_Poly\pols_run.K_H_prime;
funs_run.K_R_int = Run_Poly\pols_run.K_R_prime;
funs_run.D_int   = Run_Poly\pols_run.D_prime;
funs_run.R_D_int = Run_Poly\pols_run.R_D_prime;

% states, no run
seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state 
seq_K   = ones(1, nSimulations)*K_SS;    % state

% states, run
seq_N_H_star = ones(1, nSimulations)*N_H_SS;    % state
seq_N_R_star = ones(1, nSimulations)*N_R_SS;    % state
seq_N_S_star = ones(1, nSimulations)*p.exoInc;  % state

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice 
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price 
seq_R_K_prime = ones(1, nSimulations);  % price
seq_W_prime   = ones(1, nSimulations);  % price

seq_Q          = ones(1, nSimulations);  % price, no run
seq_Q_old      = ones(1, nSimulations);  % price, no run, guess
seq_Q_star     = ones(1, nSimulations);  % price, run
seq_Q_star_old = ones(1, nSimulations);  % price, run, guess

seq_R_K_prime_star = ones(1, nSimulations);

seq_run           = zeros(1, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_happened  = zeros(1, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(1, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(1, nSimulations);   %%%%% whether a run starts in period iii

seq_random = normrnd(0,1,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 

qError = 10;
while qError > 1e-6;
    
    for iii = 1: nSimulations-1
                      
            if iii > 1
            seq_run(1,iii) = (1-p.phi)*seq_R_K_prime_star(1,iii-1)*seq_K_S_prime(1,iii-1) < ...
                seq_R_B_prime(1,iii-1)*seq_B_prime(1,iii-1);
            end
            
        if (seq_run(1,iii) == 1  && seq_random(iii) < norminv(nu)) || seq_run_persist(1, iii) == 1; %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
            
            seq_run_happened(1,iii) = 1;  %%%%% bankrun 
            
            if seq_run_persist(1, iii) ~= 1 %%%%% eliminate the cases where bankrun continues from last period
                seq_run_triggered(1,iii) = 1;
            end
       
            seq_N_H(1, iii) = seq_N_H_star(1,iii);
            seq_N_R(1 ,iii) = seq_N_R_star(1,iii);
            seq_N_S(1, iii) = seq_N_S_star(1,iii);

            eval_point = [seq_N_H(1,iii) seq_N_R(1,iii) seq_K(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
            
            seq_K_H_prime(1, iii) = eval_poly*funs_run.K_H_int;
            seq_K_R_prime(1, iii) = eval_poly*funs_run.K_R_int;
            seq_K_S_prime(1, iii) = 0;              
            seq_R_D_prime(1, iii) = eval_poly*funs_run.R_D_int;
            seq_R_B_prime(1, iii) = 0;          
            seq_D_prime(1, iii)   = eval_poly*funs_run.D_int;          
            seq_B_prime(1, iii)   = 0;
            seq_Q(1, iii)         = eval_poly*funs_run.vec_Q_int;
            seq_Q_star(1, iii)    = seq_Q(1,iii);
            
            if seq_random(iii) < norminv(7/8)
            seq_run_persist(1, iii+1) = 1;  %%%%% whether bankrun persists to the next period
            end
            
        else
            
            eval_point = [seq_N_H(1,iii) seq_N_R(1,iii) seq_N_S(1, iii) seq_K(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);           
            
            seq_K_H_prime(1, iii) = eval_poly*funs.K_H_int;
            seq_K_R_prime(1, iii) = eval_poly*funs.K_R_int;
            seq_K_S_prime(1, iii) = eval_poly*funs.K_S_int;              
            seq_R_D_prime(1, iii) = eval_poly*funs.R_D_int;
            seq_R_B_prime(1, iii) = eval_poly*funs.R_B_int;          
            seq_D_prime(1, iii)   = eval_poly*funs.D_int;          
            seq_B_prime(1, iii)   = eval_poly*funs.B_int;
            seq_Q(1,iii)          = eval_poly*funs.vec_Q_int;
            
            eval_point = [seq_N_H_star(1,iii) seq_N_R_star(1,iii) seq_K(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
            
            seq_Q_star(1,iii) = eval_poly*funs_run.vec_Q_int;
        end
        
        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  

        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);  

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);
                      
        seq_R_K_prime_star(1,iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
        
        seq_N_H_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_H_prime(1, iii) + ...
                 seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
             
        seq_N_R_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_R_prime(1, iii) + ...
                         (1-p.phi)*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii)...
                          - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                          + p.exoInc;
                      
        %seq_N_S_star(1, iii+1) = p.exoInc + p.phi*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii);            
        seq_N_S_star(1, iii+1) = p.exoInc;   
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);

end

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime; 

util_H = log(seq_C_H(nBurnin+1:end));
util_R = log(seq_C_R(nBurnin+1:end));
util_S = log(seq_C_R(nBurnin+1:end));

tt = 0:1:nSimulations-nBurnin-1;

seq_V_H = sum(p.beta_H.^tt.*util_H);
seq_V_R = sum(p.beta_R.^tt.*util_R);
seq_V_S = sum(p.beta_S.^tt.*util_S);

cons_eq_H(jj,kk) = exp((1-p.beta_H)*seq_V_H);
cons_eq_R(jj,kk) = exp((1-p.beta_R)*seq_V_R);
cons_eq_S(jj,kk) = exp((1-p.beta_S)*seq_V_S);
cons_eq(jj,kk) = mu_H*cons_eq_H(jj,kk) + mu_R*cons_eq_R(jj,kk) + mu_S*cons_eq_S(jj,kk);

bankRunProb(jj,kk) = mean(seq_run_triggered(nBurnin+1:end));

end

end

save('WelfarePlots.mat')

elapsedTime = toc

%% Plot

fontOptions = {'FontSize',15};

figure
plot(gammaVals,mean(bankRunProb,1),'LineWidth',2);
xlabel('Capital requirement');
ylabel('Probability of bankrun per quarter');
title('Probability of Bankrun as a Function of Capital Requirement');
grid on
saveas(gcf,strcat('./Figures/Bankrun_Prob.fig'))

% figure
% plot(gammaVals,mean(bankRunProb,1),'LineWidth',2)
% grid on
% title('Realized Bank Run Probability',fontOptions{:})
% xlabel('\Gamma^R',fontOptions{:})
% ylabel('Bank Runs Per 100 Periods',fontOptions{:})
% 
% saveas(gcf,'./Figures/BankRunProb.fig')

yVals = [mean(cons_eq_H,1); mean(cons_eq_R,1); mean(cons_eq_S,1); mean(cons_eq,1) ];

names = {'Cons Eq HH','Cons Eq R Banks','Cons Eq S Banks','Cons Eq All'};

figure
for ii = 1:size(yVals,1)
    subplot(2,2,ii)
    plot(gammaVals,yVals(ii,:),'LineWidth',2)
    grid on
    title(names{ii},fontOptions{:})
    xlabel('\Gamma^R',fontOptions{:})
    ylabel('Perm Cons Equiv',fontOptions{:})
end
saveas(gcf,'./Figures/Welfare.fig')