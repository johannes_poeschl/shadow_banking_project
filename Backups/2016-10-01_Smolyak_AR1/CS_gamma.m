%                  Bank Capital Regulation 
%                Macro seminar presentation
%                   Comparative statics  (gamma)
%                  
 
clear all
close all
clc


% Define parameter values



beta_H  = 0.9921;         % Discount factor of HH
beta_R  = 0.9763;         % Discount factor of Retail Banker  
beta_S  = 0.9369;         % Discount factor of Shadow Banker
alpha   = 0.36;           % Capital share in production function
alpha_H = 7.8721e-4;      % Household managerial cost   % GKP = 0.03; 
alpha_R = 4.5356e-4;       % Retail banks managerial cost  % GKP = 0.0074; 
W_R     = 2.5;            % Bankers of retail banks endowment
W_S     = 2.5;           % Bankers of shadow banks endowment
Z       = 1;              % Steady state productivity
pphi = 0.1051;         % Financial friction in shadow banking 
w_K     = 0.5;            % Risk weighting of retail banks' capital holding
w_B     = 1;              % Risk weighting of retail banks' lending to shadow banking
delta = 0.025;            % Discount rate

 

gamma_R = linspace(0.08,0.1,1000);   % Minimum capital requirement on retail banks


% SS equilibrium conditions:

R_D = 1/beta_H + 0*gamma_R;  % 1)
R_B = 1/beta_R.*gamma_R.*w_B+R_D.*(1-gamma_R.*w_B) + 0*gamma_R;  % 2)
R_K = 1./(beta_S*pphi + (1-pphi)./R_B); % 3) 
K_H = ( beta_H*R_K -1 ) / alpha_H + 0*gamma_R;  % 4)
K_R = max(1/alpha_R.*(beta_R.*(R_K-R_D.*(1-gamma_R.*w_K))- gamma_R.*w_K) + 0*gamma_R,0);  % 5)
%K_R = max(K_R,0);
r_K = R_K - (1-delta);
K   = (r_K/alpha/Z).^(1/(alpha-1));
W_H = (1-alpha)*Z*K.^alpha;
K_S = K - K_H - K_R + 0*gamma_R;  % 6)

B   = R_K.*K_S*(1-pphi)./R_B + 0*gamma_R;  % 7)&8)
E_S = K_S-B + 0*gamma_R;  % 9)

E_R = gamma_R.*(w_K*K_R+w_B*B) + 0*gamma_R;  % 10)
D   = K_R + B - E_R + 0*gamma_R;  % 11)&12)

C_H = W_H + (R_D-1).*D + (R_K-1).*K_H - alpha_H/2*K_H.^2 + 0*gamma_R;  % 13)
C_R = W_R +(1-R_D).*D + (R_B-1).*B + (R_K-1).* K_R - alpha_R/2*K_R.^2 + 0*gamma_R;  % 14)
C_S = W_S +(1-R_B).*B + (R_K-1).*K_S + 0*gamma_R;  % 15)
C   = C_H+C_R+C_S;

Y = Z*K.^alpha + W_S + W_R;

%Z_R_hat=(R_D.*D-R_B.*B)./K_R-Q_star;

% p.R_B = (R_B - R_B(1))/R_B(1);
% plot(gamma_R, p.R_B);


% %% Plot
% 
% figure
% 
% % prices
% 
% subplot(3,6,1);
% plot(gamma_R,R_D,'LineWidth',1);
% title('R^D_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,2);
% plot(gamma_R,R_B,'LineWidth',1);
% title('R^B_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,3);
% plot(gamma_R,R_K,'LineWidth',1);
% title('R^K_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,4);
% plot(gamma_R,R_K-R_B,'LineWidth',1);
% title('R^K_{SS} - R^B_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% 
% % capital holdings
% 
% subplot(3,6,5);
% plot(gamma_R,K_H,'LineWidth',1);
% title('K^H_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% 
% subplot(3,6,6);
% plot(gamma_R,K_R,'LineWidth',1);
% title('K^R_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,7);
% plot(gamma_R,K_S,'LineWidth',1);
% title('K^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,8);
% plot(gamma_R,K,'LineWidth',1);
% title('K_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% % financial intermediation
% 
% subplot(3,6,9);
% plot(gamma_R,D,'LineWidth',1);
% title('D_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,10);
% plot(gamma_R,B,'LineWidth',1);
% title('B_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% % bank equity
% 
% subplot(3,6,11);
% plot(gamma_R,E_R,'LineWidth',1);
% title('E^R_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,12);
% plot(gamma_R,E_S,'LineWidth',1);
% title('E^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,13);
% plot(gamma_R,E_S./K_S,'LineWidth',1);
% title('E^S_{SS}/A^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% % subplot(3,6,11);
% % plot(gamma_R,(R_D.*D-R_B.*B)./K_R-Q_star,'LineWidth',1);
% % title('Z_R^{hat}');
% % xlabel('\Gamma^R','FontSize',10);
% % 
% % subplot(3,6,12);
% % plot(gamma_R,R_B.*B./K_S-Q_star,'LineWidth',1);
% % title('Z_S^{hat}');
% % xlabel('\Gamma^R','FontSize',10);
% 
% % comsumptions
% 
% subplot(3,6,14);
% plot(gamma_R,C_H,'LineWidth',1);
% title('C^H_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% 
% subplot(3,6,15);
% plot(gamma_R,C_R,'LineWidth',1);
% title('C^R_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,16);
% plot(gamma_R,C_S,'LineWidth',1);
% title('C^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% 
% subplot(3,6,17);
% plot(gamma_R,C,'LineWidth',1);
% title('C_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(3,6,18);
% plot(gamma_R,K,'LineWidth',1);
% title('Y_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% 
% % subplot(3,6,17);
% % plot(gamma_R,alpha_H./2.*K_H.^2+alpha_R./2.*K_R.^2,'LineWidth',1);
% % title('cost');
% % xlabel('\Gamma^R','FontSize',10);
% 
% %%
% figure 
% 
% subplot(2,4,1);
% plot(gamma_R,K_H,'LineWidth',1);
% title('K^H_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% ylabel('Level','FontSize',12);
% 
% 
% subplot(2,4,2);
% plot(gamma_R,K_R,'LineWidth',1);
% title('K^R_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(2,4,3);
% plot(gamma_R,K_S,'LineWidth',1);
% title('K^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(2,4,4);
% plot(gamma_R,K,'LineWidth',1);
% title('K_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(2,4,5);
% plot(gamma_R,C_H,'LineWidth',1);
% title('C^H_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% ylabel('Level','FontSize',12);
% 
% 
% subplot(2,4,6);
% plot(gamma_R,C_R,'LineWidth',1);
% title('C^R_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(2,4,7);
% plot(gamma_R,C_S,'LineWidth',1);
% title('C^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% subplot(2,4,8);
% plot(gamma_R,K,'LineWidth',1);
% title('Y_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% %%
% figure 
% 
% plot(gamma_R,E_S./K_S,'LineWidth',1);
% title('Capital ratio of shadow banks: E^S_{SS}/A^S_{SS}');
% xlabel('\Gamma^R','FontSize',10);
% 
% %%
% figure 
% 
% subplot(2,5,1);
% plot(gamma_R,K_H,'LineWidth',1);
% title('K^H_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% ylabel('Level','FontSize',15);
% 
% 
% subplot(2,5,2);
% plot(gamma_R,K_R,'LineWidth',1);
% title('K^R_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,3);
% plot(gamma_R,K_S,'LineWidth',1);
% title('K^S_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,4);
% plot(gamma_R,K,'LineWidth',1);
% title('K_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,5);
% plot(gamma_R,K,'LineWidth',1);
% title('Y_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,6);
% plot(gamma_R,C_H,'LineWidth',1);
% title('C^H_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% ylabel('Level','FontSize',15);
% 
% subplot(2,5,7);
% plot(gamma_R,C_R,'LineWidth',1);
% title('C^R_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,8);
% plot(gamma_R,C_S,'LineWidth',1);
% title('C^S_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,9);
% plot(gamma_R,R_B,'LineWidth',1);
% title('R^B','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);
% 
% subplot(2,5,10);
% plot(gamma_R,E_S./K_S,'LineWidth',1);
% title('E^S_{SS}/A^S_{SS}','FontSize',15);
% xlabel('\Gamma^R','FontSize',15);

%%
figure
fontOptions1 = {'FontSize',12};
fontOptions2 = {'FontSize',11};
limit = [0 0.2];

subplot(3,4,1);
plot(gamma_R,D,'LineWidth',1);
title('D_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
ylabel('LEVEL');
xlim(limit);

subplot(3,4,2);
plot(gamma_R,B,'LineWidth',1);
title('B_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,3);
plot(gamma_R,E_S,'LineWidth',1);
title('E^S_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,4);
plot(gamma_R,E_S./K_S,'LineWidth',1);
title('E^S_{SS}/K^S_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,5);
plot(gamma_R,K_H,'LineWidth',1);
title('K^H_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
ylabel('LEVEL');
xlim(limit);

subplot(3,4,6);
plot(gamma_R,K_R,'LineWidth',1);
title('K^R_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,7);
plot(gamma_R,K_S,'LineWidth',1);
title('K^S_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,8);
plot(gamma_R,K,'LineWidth',1);
title('K_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,9);
plot(gamma_R,C_H,'LineWidth',1);
title('C^H_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
ylabel('LEVEL');
xlim(limit);

subplot(3,4,10);
plot(gamma_R,C_R,'LineWidth',1);
title('C^R_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,11);
plot(gamma_R,C_S,'LineWidth',1);
title('C^S_{SS}',fontOptions1{:});
xlabel('\Gamma^R',fontOptions2{:});
xlim(limit);

subplot(3,4,12);
plot(gamma_R,Y,'LineWidth',1);
title('Y_{SS}',fontOptions1{:});
xlim(limit);
xlabel('\Gamma^R',fontOptions2{:});