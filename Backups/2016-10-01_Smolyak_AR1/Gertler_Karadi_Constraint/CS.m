clear
clc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Low capital requirement  gamma_R = 0.05
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;

p.alpha = 0.3;
p.beta_H = 1.03^(-0.25);
p.beta_R = 1.035^(-0.25);
p.delta = 0.02;
p.sigma = 1;
p.rho = 0;
p.sigma_Z = 0.23;
p.alpha_H = 0.03;  %GKP 
p.alpha_R = 0.0074;  %GKP
p.gamma_R = 0.05;
p.sigma_R = 1;
p.sigma_S = 1;
p.iota = 0.1;

p.theta = 0.25;  % need to keep it small for solutions, 
                % model cannot solve for solutions if big
p.phi = 0.1;    % level of financial friction (moral hazard) in S-B

% new parameters when inclduing SB //////
p.omega_K = 1;  % risk weights 
p.omega_B = 1;  % risk weights 
p.beta_S  = 1.04^(-0.25);

p.exoInc = 2; % either add this exogenous income or make banks linear in net worth.

% -- Calculate SS -- //////
R_D_SS = 1/p.beta_H;
R_B_SS = p.gamma_R * p.omega_B / p.beta_R + R_D_SS * (1-p.gamma_R * p.omega_B);
R_K_SS = 1 / (p.beta_S * p.phi + 1/R_B_SS*(1 - p.phi));
r_K_SS = R_K_SS - 1 + p.delta;
K_H_SS = 1/ p.alpha_H * (p.beta_H * R_K_SS - 1);
K_R_SS = 1/ p.alpha_R * (p.beta_R *(R_K_SS-R_D_SS*(1-p.gamma_R*p.omega_K))-p.gamma_R*p.omega_K);
K_SS = (r_K_SS/p.alpha)^(1/(p.alpha-1));
K_S_SS = K_SS - K_H_SS - K_R_SS;
B_SS = (1-p.phi) * R_K_SS * K_S_SS / R_B_SS;
D_SS = K_R_SS + B_SS - p.gamma_R * (p.omega_K*K_R_SS + p.omega_B*B_SS);
W_SS = (1-p.alpha)*K_SS^p.alpha;
N_H_SS = R_D_SS*D_SS + R_K_SS * K_H_SS + W_SS;
N_R_SS = R_K_SS*K_R_SS + R_B_SS*B_SS - R_D_SS*D_SS + p.exoInc;
N_S_SS = R_K_SS*K_S_SS - R_B_SS*B_SS + p.exoInc;


% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_N_H = 5;
n_N_R = 5;
n_N_S = 5;  % ///////////
n_K = 5;
n_Z = 5;
n_total = n_N_H*n_N_R*n_N_S*n_K; % /////

K_l = 0.75 * K_SS;
K_u = 1.25 * K_SS;

N_H_l = 0.75*N_H_SS;
N_H_u = 1.25*N_H_SS;

N_R_l = 0.75*N_R_SS;
N_R_u = 1.25*N_R_SS;

N_S_l = 0.75*N_S_SS;
N_S_u = 1.25*N_S_SS;  % /////

K_grid = linspace(K_l,K_u,n_K);
N_H_grid = linspace(N_H_l, N_H_u, n_N_H);
N_R_grid = linspace(N_R_l, N_R_u, n_N_R);
N_S_grid = linspace(N_S_l, N_S_u, n_N_S);

[logZ_grid, Z_prob] = tauchen(n_Z, 0, p.rho, p.sigma_Z, 3);
Z_prob_ext = kron(Z_prob,ones(n_total,1));
Z_grid = exp(logZ_grid);

[nd_N_H_grid, nd_N_R_grid, nd_N_S_grid, nd_K_grid] = ndgrid(N_H_grid, N_R_grid, N_S_grid, K_grid); % /////

vec_N_H = vec(nd_N_H_grid);
vec_N_R = vec(nd_N_R_grid);
vec_N_S = vec(nd_N_S_grid);  % /////
vec_K   = vec(nd_K_grid);

states = [vec_N_H, vec_N_R, vec_N_S, vec_K];  % ////


% Initial Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_K_H_prime = zeros(n_total,1);
vec_K_R_prime = zeros(n_total,1);
vec_K_S_prime = zeros(n_total,1); % /////
vec_D_prime = zeros(n_total,1);
vec_B_prime = zeros(n_total,1); % /////

vec_Q = ones(n_total,1);
vec_Q_old = vec_Q;

vec_C_H = vec_N_H - vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
vec_C_R = vec_N_R - vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
vec_C_S = vec_N_S - vec_K_S_prime + vec_B_prime; % //////

V_N_H = vec_C_H .^(-p.sigma);
V_N_R = vec_C_R .^(-p.sigma_R);
V_N_S = vec_C_S .^(-p.sigma_S);  % /////
% V_N_R = 1 - p.iota*vec_C_R.*(vec_C_R < 0);
% V_N_S = 1 - p.iota*vec_C_S.*(vec_C_S < 0);  % /////
V_Q = zeros(n_total,1);

V_N_H_old = V_N_H;
V_N_R_old = V_N_R;
V_N_S_old = V_N_S; % /////
V_Q_old = V_Q;

funs = struct;
funs.v_N_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(V_N_H, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.v_N_R_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(V_N_R, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.v_N_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(V_N_S, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.vec_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.v_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////



% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_H_prime_new = repmat(vec_N_H, 1,n_Z);
N_R_prime_new = repmat(vec_N_R, 1,n_Z);
N_S_prime_new = repmat(vec_N_S, 1,n_Z); % /////

N_H_prime_old = N_H_prime_new;
N_R_prime_old = N_R_prime_new;
N_S_prime_old = N_S_prime_new; % /////

vError = 10;
iter = 1;
maxIter =  200;
initialIter = 1;

vec_K_prime = ones(n_total,1);
vec_K_H_prime = ones(n_total,1);
vec_K_R_prime = ones(n_total,1);
vec_K_S_prime = ones(n_total,1); % /////
vec_R_D_prime = R_D_SS*ones(n_total,1);  
vec_R_B_prime = R_B_SS*ones(n_total,1); 
lambda_H = zeros(n_total,1);
lambda_R = zeros(n_total,1);

J = [1 1 1 1 1 1 0;
    1 1 1 1 1 0 0;
    1 1 1 1 1 0 1;
    1 1 1 1 1 0 0;
    1 1 1 1 1 0 0;
    1 0 0 0 0 1 0;
    0 1 0 0 0 0 1];

options = optimset('Display','off', ...
    'JacobPattern',J, ...
    'FinDiffType','forward', ...
    'TolFun',1e-6);

vec_I         = ones(n_total,1);

while vError > 1e-4 && iter <= maxIter
    tic
    parfor ii = 1:length(vec_K)
        
        ind = 1;
        
        handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
        
        x0 = [vec_K_H_prime(ii,1);
            vec_K_R_prime(ii,1);
            vec_K_S_prime(ii,1);
            vec_R_D_prime(ii,1);
            vec_R_B_prime(ii,1);
            0;
            0;
            vec_Q(ii,1)];

        [x_prime_new,~,fval] = fsolve(handle,x0,options);
                
        if x_prime_new(1) < 0 && x_prime_new(2) >= 0
            ind = 2;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,~,fval] = fsolve(handle,x0,options);
                        
            if x_prime_new(2) < 0
                ind = 4;
            
                handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
                states(ii,:),Z_grid',Z_prob_ext(ii,:),...
                funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
                N_S_prime_old(ii,:),ind);

                [x_prime_new,~,fval] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) >= 0 && x_prime_new(2) < 0
            ind = 3;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,~,fval] = fsolve(handle,x0,options);
                                    
            if x_prime_new(2) < 0
                ind = 4;
            
                handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
                states(ii,:),Z_grid',Z_prob_ext(ii,:),...
                funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
                N_S_prime_old(ii,:),ind);

                [x_prime_new,~,fval] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) < 0 && x_prime_new(2) < 0
            ind = 4;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,~,fval] = fsolve(handle,x0,options);
        end

        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_K_S_prime(ii,1) = x_prime_new(3);
        vec_R_D_prime(ii,1) = x_prime_new(4);
        vec_R_B_prime(ii,1) = x_prime_new(5);
        lambda_H(ii,1) = x_prime_new(6);
        lambda_R(ii,1) = x_prime_new(7);
        vec_Q(ii,1) = x_prime_new(8);
        
        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
            + vec_K_S_prime(ii,1);
        vec_I(ii,1) = vec_K_prime(ii,1) - (1-p.delta)*vec_K(ii,1);

        [N_H_prime_new(ii,:),N_R_prime_new(ii,:),N_S_prime_new(ii,:),...
         vec_D_prime(ii,:), vec_B_prime(ii,:)] = HH_RB_SB_compute_N_prime...
         (p, x_prime_new, states(ii,:), Z_grid', Z_prob_ext(ii,:), funs, ...
         N_H_prime_old(ii,:),N_R_prime_old(ii,:), N_S_prime_old(ii,:));

    end
    toc
    
    % -- balance sheet constraint of retail bank: --  
    vec_C_H = vec_N_H - vec_Q.*vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_Q.*vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
    vec_C_S = vec_N_S - vec_Q.*vec_K_S_prime + vec_B_prime; % ////// 

    V_N_H = vec_C_H .^(-p.sigma);
    V_N_R = vec_C_R .^(-p.sigma_R);
    V_N_S = vec_C_S .^(-p.sigma_S);  % /////
%     V_N_R = 1 - p.iota*vec_C_R.*(vec_C_R < 0);
%     V_N_S = 1 - p.iota*vec_C_S.*(vec_C_S < 0);  % /////
    V_Q =  (p.theta*(vec_I./vec_K - p.delta).*vec_I./vec_K - ...
        p.theta/2*(vec_I./vec_K - p.delta).^2);
    
    vError = norm([V_N_H; V_N_R; V_N_S] - [V_N_H_old; V_N_R_old; V_N_S_old],'inf');
    fprintf('Iteration %d: Value Function Error %6.6f \n \n',iter,vError) 

    weight = 0.5;
    N_H_prime_old = weight*N_H_prime_new + (1-weight)*N_H_prime_old;
    N_R_prime_old = weight*N_R_prime_new + (1-weight)*N_R_prime_old;
    N_S_prime_old = weight*N_S_prime_new + (1-weight)*N_S_prime_old;
    
    weight = 1;
    vec_Q = weight*vec_Q + (1-weight)*vec_Q_old;
    V_N_H = weight*V_N_H + (1-weight)*V_N_H_old;
    V_N_R = weight*V_N_R + (1-weight)*V_N_R_old;
    V_N_S = weight*V_N_S + (1-weight)*V_N_S_old;
 
    funs = struct;
    funs.v_N_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_N_H, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.v_N_R_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_N_R, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.v_N_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid,  K_grid}, ...
        reshape(V_N_S, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.vec_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.v_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

    V_N_H_old = V_N_H;
    V_N_R_old = V_N_R;
    V_N_S_old = V_N_S;
    vec_Q_old = vec_Q;
    V_Q_old = V_Q;
    
    iter = iter + 1;
end

save(strcat('Result_NoRun_',date))

% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBurnin = 1000;
nSimulations = nBurnin + 250;  % number of simulated periods 

% -- Simulate a sequence of Z: choose between (1) & (2) --

%- (1) random sequence of Z -
% seq_Z_transpose = simulateMarkovChain(1,Z_grid,Z_prob,nSimulations);
% seq_Z = seq_Z_transpose';

% - (2) impulse response function -
seq_Z = Z_grid(3,1)*ones(1,nSimulations);
seq_Z(1,nBurnin + 30) = Z_grid(1,1);  % productivity shock in the 30th period

% -- Interpolate policy function -- ////
funs.K_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_H_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.K_R_int = griddedInterpolant({N_H_grid, N_R_grid ,N_S_grid, K_grid},...
    reshape(vec_K_R_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.K_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_S_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_D_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.R_D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_D_prime, n_N_H, n_N_R , n_N_S, n_K),'linear');
funs.R_B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');

seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state //////
seq_K   = ones(1, nSimulations)*K_SS;    % state

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice %////
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice %////
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price  %////
seq_R_K_prime = ones(1, nSimulations);  % price
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);  % price
seq_Q_old = seq_Q;

qError = 10;
 
while qError > 1e-6;
    for iii = 1: nSimulations-1;
        seq_K_H_prime(1, iii) = funs.K_H_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));
        seq_K_R_prime(1, iii) = funs.K_R_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));
        seq_K_S_prime(1, iii) = funs.K_S_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));              
        seq_R_D_prime(1, iii) = funs.R_D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));
        seq_R_B_prime(1, iii) = funs.R_B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));

        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  
        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);
        seq_Q(1,iii)    = funs.Q_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                            seq_N_S(1, iii),seq_K(1, iii));

        seq_D_prime(1, iii)   = funs.D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));

        seq_B_prime(1, iii)   = funs.B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
end

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime;  % /////
seq_Y   = seq_Z .* seq_K.^p.alpha;


% redefine sequence for plot

seq_K_l   = seq_K(1,nBurnin + 1:end-1);
seq_N_H_l = seq_N_H(1,nBurnin + 1:end-1);
seq_N_R_l = seq_N_R(1,nBurnin + 1:end-1);
seq_N_S_l = seq_N_S(1,nBurnin + 1:end-1);

seq_K_H_prime_l = seq_K_H_prime(1,nBurnin + 1:end-1);
seq_K_R_prime_l = seq_K_R_prime(1,nBurnin + 1:end-1);
seq_K_S_prime_l = seq_K_S_prime(1,nBurnin + 1:end-1);

seq_D_prime_l   = seq_D_prime(1,nBurnin + 1:end-1);
seq_B_prime_l   = seq_B_prime(1,nBurnin + 1:end-1);

seq_R_D_prime_l = seq_R_D_prime(1,nBurnin + 1:end-1);
seq_R_B_prime_l = seq_R_B_prime(1,nBurnin + 1:end-1);
seq_R_K_prime_l = seq_R_K_prime(1,nBurnin + 1:end-1);

seq_C_H_l = seq_C_H(1,nBurnin + 1:end-1);
seq_C_R_l = seq_C_R(1,nBurnin + 1:end-1);
seq_C_S_l = seq_C_S(1,nBurnin + 1:end-1);
seq_Y_l   = seq_Y(1,nBurnin + 1:end-1);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% High capital requirement  gamma_R = 0.08
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;

p.alpha = 0.3;
p.beta_H = 1.03^(-0.25);
p.beta_R = 1.035^(-0.25);
p.delta = 0.02;
p.sigma = 1;
p.rho = 0;
p.sigma_Z = 0.23;
p.alpha_H = 0.03;  %GKP 
p.alpha_R = 0.0074;  %GKP
p.gamma_R = 0.08;
p.sigma_R = 1;
p.sigma_S = 1;
p.iota = 0.1;

p.theta = 0.25;  % need to keep it small for solutions, 
                % model cannot solve for solutions if big
p.phi = 0.1;    % level of financial friction (moral hazard) in S-B

% new parameters when inclduing SB //////
p.omega_K = 1;  % risk weights 
p.omega_B = 1;  % risk weights 
p.beta_S  = 1.04^(-0.25);

p.exoInc = 5; % either add this exogenous income or make banks linear in net worth.

% -- Calculate SS -- //////
R_D_SS = 1/p.beta_H;
R_B_SS = p.gamma_R * p.omega_B / p.beta_R + R_D_SS * (1-p.gamma_R * p.omega_B);
R_K_SS = 1 / (p.beta_S * p.phi + 1/R_B_SS*(1 - p.phi));
r_K_SS = R_K_SS - 1 + p.delta;
K_H_SS = 1/ p.alpha_H * (p.beta_H * R_K_SS - 1);
K_R_SS = 1/ p.alpha_R * (p.beta_R *(R_K_SS-R_D_SS*(1-p.gamma_R*p.omega_K))-p.gamma_R*p.omega_K);
K_SS = (r_K_SS/p.alpha)^(1/(p.alpha-1));
K_S_SS = K_SS - K_H_SS - K_R_SS;
B_SS = (1-p.phi) * R_K_SS * K_S_SS / R_B_SS;
D_SS = K_R_SS + B_SS - p.gamma_R * (p.omega_K*K_R_SS + p.omega_B*B_SS);
W_SS = (1-p.alpha)*K_SS^p.alpha;
N_H_SS = R_D_SS*D_SS + R_K_SS * K_H_SS + W_SS;
N_R_SS = R_K_SS*K_R_SS + R_B_SS*B_SS - R_D_SS*D_SS + p.exoInc;
N_S_SS = R_K_SS*K_S_SS - R_B_SS*B_SS + p.exoInc;


% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_N_H = 5;
n_N_R = 5;
n_N_S = 5;  % ///////////
n_K = 5;
n_Z = 5;
n_total = n_N_H*n_N_R*n_N_S*n_K; % /////

K_l = 0.75 * K_SS;
K_u = 1.25 * K_SS;

N_H_l = 0.75*N_H_SS;
N_H_u = 1.25*N_H_SS;

N_R_l = 0.75*N_R_SS;
N_R_u = 1.25*N_R_SS;

N_S_l = 0.75*N_S_SS;
N_S_u = 1.25*N_S_SS;  % /////

K_grid = linspace(K_l,K_u,n_K);
N_H_grid = linspace(N_H_l, N_H_u, n_N_H);
N_R_grid = linspace(N_R_l, N_R_u, n_N_R);
N_S_grid = linspace(N_S_l, N_S_u, n_N_S);

[logZ_grid, Z_prob] = tauchen(n_Z, 0, p.rho, p.sigma_Z, 3);
Z_prob_ext = kron(Z_prob,ones(n_total,1));
Z_grid = exp(logZ_grid);

[nd_N_H_grid, nd_N_R_grid, nd_N_S_grid, nd_K_grid] = ndgrid(N_H_grid, N_R_grid, N_S_grid, K_grid); % /////

vec_N_H = vec(nd_N_H_grid);
vec_N_R = vec(nd_N_R_grid);
vec_N_S = vec(nd_N_S_grid);  % /////
vec_K   = vec(nd_K_grid);

states = [vec_N_H, vec_N_R, vec_N_S, vec_K];  % ////


% Initial Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_K_H_prime = zeros(n_total,1);
vec_K_R_prime = zeros(n_total,1);
vec_K_S_prime = zeros(n_total,1); % /////
vec_D_prime = zeros(n_total,1);
vec_B_prime = zeros(n_total,1); % /////

vec_Q = ones(n_total,1);
vec_Q_old = vec_Q;

vec_C_H = vec_N_H - vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
vec_C_R = vec_N_R - vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
vec_C_S = vec_N_S - vec_K_S_prime + vec_B_prime; % //////

V_N_H = vec_C_H .^(-p.sigma);
V_N_R = vec_C_R .^(-p.sigma_R);
V_N_S = vec_C_S .^(-p.sigma_S);  % /////
% V_N_R = 1 - p.iota*vec_C_R.*(vec_C_R < 0);
% V_N_S = 1 - p.iota*vec_C_S.*(vec_C_S < 0);  % /////
V_Q = zeros(n_total,1);

V_N_H_old = V_N_H;
V_N_R_old = V_N_R;
V_N_S_old = V_N_S; % /////
V_Q_old = V_Q;

funs = struct;
funs.v_N_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(V_N_H, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.v_N_R_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(V_N_R, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.v_N_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(V_N_S, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.vec_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
    reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

funs.v_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////



% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_H_prime_new = repmat(vec_N_H, 1,n_Z);
N_R_prime_new = repmat(vec_N_R, 1,n_Z);
N_S_prime_new = repmat(vec_N_S, 1,n_Z); % /////

N_H_prime_old = N_H_prime_new;
N_R_prime_old = N_R_prime_new;
N_S_prime_old = N_S_prime_new; % /////

vError = 10;
iter = 1;
maxIter =  200;
initialIter = 1;

vec_K_prime = ones(n_total,1);
vec_K_H_prime = ones(n_total,1);
vec_K_R_prime = ones(n_total,1);
vec_K_S_prime = ones(n_total,1); % /////
vec_R_D_prime = R_D_SS*ones(n_total,1);  
vec_R_B_prime = R_B_SS*ones(n_total,1); 
lambda_H = zeros(n_total,1);
lambda_R = zeros(n_total,1);

J = [1 1 1 1 1 1 0;
    1 1 1 1 1 0 0;
    1 1 1 1 1 0 1;
    1 1 1 1 1 0 0;
    1 1 1 1 1 0 0;
    1 0 0 0 0 1 0;
    0 1 0 0 0 0 1];

options = optimset('Display','off', ...
    'JacobPattern',J, ...
    'FinDiffType','forward', ...
    'TolFun',1e-6);

vec_I         = ones(n_total,1);

while vError > 1e-4 && iter <= maxIter
    tic
    parfor ii = 1:length(vec_K)
        
        ind = 1;
        
        handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
        
        x0 = [vec_K_H_prime(ii,1);
            vec_K_R_prime(ii,1);
            vec_K_S_prime(ii,1);
            vec_R_D_prime(ii,1);
            vec_R_B_prime(ii,1);
            0;
            0;
            vec_Q(ii,1)];

        [x_prime_new,~,fval] = fsolve(handle,x0,options);
                
        if x_prime_new(1) < 0 && x_prime_new(2) >= 0
            ind = 2;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,~,fval] = fsolve(handle,x0,options);
                        
            if x_prime_new(2) < 0
                ind = 4;
            
                handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
                states(ii,:),Z_grid',Z_prob_ext(ii,:),...
                funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
                N_S_prime_old(ii,:),ind);

                [x_prime_new,~,fval] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) >= 0 && x_prime_new(2) < 0
            ind = 3;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,~,fval] = fsolve(handle,x0,options);
                                    
            if x_prime_new(2) < 0
                ind = 4;
            
                handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
                states(ii,:),Z_grid',Z_prob_ext(ii,:),...
                funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
                N_S_prime_old(ii,:),ind);

                [x_prime_new,~,fval] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) < 0 && x_prime_new(2) < 0
            ind = 4;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),Z_grid',Z_prob_ext(ii,:),...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,~,fval] = fsolve(handle,x0,options);
        end

        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_K_S_prime(ii,1) = x_prime_new(3);
        vec_R_D_prime(ii,1) = x_prime_new(4);
        vec_R_B_prime(ii,1) = x_prime_new(5);
        lambda_H(ii,1) = x_prime_new(6);
        lambda_R(ii,1) = x_prime_new(7);
        vec_Q(ii,1) = x_prime_new(8);
        
        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
            + vec_K_S_prime(ii,1);
        vec_I(ii,1) = vec_K_prime(ii,1) - (1-p.delta)*vec_K(ii,1);

        [N_H_prime_new(ii,:),N_R_prime_new(ii,:),N_S_prime_new(ii,:),...
         vec_D_prime(ii,:), vec_B_prime(ii,:)] = HH_RB_SB_compute_N_prime...
         (p, x_prime_new, states(ii,:), Z_grid', Z_prob_ext(ii,:), funs, ...
         N_H_prime_old(ii,:),N_R_prime_old(ii,:), N_S_prime_old(ii,:));

    end
    toc
    
    % -- balance sheet constraint of retail bank: --  
    vec_C_H = vec_N_H - vec_Q.*vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_Q.*vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
    vec_C_S = vec_N_S - vec_Q.*vec_K_S_prime + vec_B_prime; % ////// 

    V_N_H = vec_C_H .^(-p.sigma);
    V_N_R = vec_C_R .^(-p.sigma_R);
    V_N_S = vec_C_S .^(-p.sigma_S);  % /////
%     V_N_R = 1 - p.iota*vec_C_R.*(vec_C_R < 0);
%     V_N_S = 1 - p.iota*vec_C_S.*(vec_C_S < 0);  % /////
    V_Q =  (p.theta*(vec_I./vec_K - p.delta).*vec_I./vec_K - ...
        p.theta/2*(vec_I./vec_K - p.delta).^2);
    
    vError = norm([V_N_H; V_N_R; V_N_S] - [V_N_H_old; V_N_R_old; V_N_S_old],'inf');
    fprintf('Iteration %d: Value Function Error %6.6f \n \n',iter,vError) 

    weight = 0.5;
    N_H_prime_old = weight*N_H_prime_new + (1-weight)*N_H_prime_old;
    N_R_prime_old = weight*N_R_prime_new + (1-weight)*N_R_prime_old;
    N_S_prime_old = weight*N_S_prime_new + (1-weight)*N_S_prime_old;
    
    weight = 1;
    vec_Q = weight*vec_Q + (1-weight)*vec_Q_old;
    V_N_H = weight*V_N_H + (1-weight)*V_N_H_old;
    V_N_R = weight*V_N_R + (1-weight)*V_N_R_old;
    V_N_S = weight*V_N_S + (1-weight)*V_N_S_old;
 
    funs = struct;
    funs.v_N_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_N_H, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.v_N_R_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_N_R, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.v_N_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid,  K_grid}, ...
        reshape(V_N_S, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.vec_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////
    funs.v_Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid}, ...
        reshape(V_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');  % /////

    V_N_H_old = V_N_H;
    V_N_R_old = V_N_R;
    V_N_S_old = V_N_S;
    vec_Q_old = vec_Q;
    V_Q_old = V_Q;
    
    iter = iter + 1;
end

save(strcat('Result_NoRun_',date))

% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBurnin = 1000;
nSimulations = nBurnin + 250;  % number of simulated periods 

% -- Simulate a sequence of Z: choose between (1) & (2) --

%- (1) random sequence of Z -
% seq_Z_transpose = simulateMarkovChain(1,Z_grid,Z_prob,nSimulations);
% seq_Z = seq_Z_transpose';

% - (2) impulse response function -
seq_Z = Z_grid(3,1)*ones(1,nSimulations);
seq_Z(1,nBurnin + 30) = Z_grid(1,1);  % productivity shock in the 30th period

% -- Interpolate policy function -- ////
funs.K_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_H_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.K_R_int = griddedInterpolant({N_H_grid, N_R_grid ,N_S_grid, K_grid},...
    reshape(vec_K_R_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.K_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_S_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_D_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.R_D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_D_prime, n_N_H, n_N_R , n_N_S, n_K),'linear');
funs.R_B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs.Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');

seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state //////
seq_K   = ones(1, nSimulations)*K_SS;    % state

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice %////
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice %////
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price  %////
seq_R_K_prime = ones(1, nSimulations);  % price
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);  % price
seq_Q_old = seq_Q;

qError = 10;
 
while qError > 1e-6;
    for iii = 1: nSimulations-1;
        seq_K_H_prime(1, iii) = funs.K_H_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));
        seq_K_R_prime(1, iii) = funs.K_R_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));
        seq_K_S_prime(1, iii) = funs.K_S_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));              
        seq_R_D_prime(1, iii) = funs.R_D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));
        seq_R_B_prime(1, iii) = funs.R_B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));

        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  
        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);
        seq_Q(1,iii)    = funs.Q_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                            seq_N_S(1, iii),seq_K(1, iii));

        seq_D_prime(1, iii)   = funs.D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));

        seq_B_prime(1, iii)   = funs.B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
end

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime;  % /////
seq_Y   = seq_Z .* seq_K.^p.alpha;


% redefine sequence for plot

seq_K_h   = seq_K(1,nBurnin + 1:end-1);
seq_N_H_h = seq_N_H(1,nBurnin + 1:end-1);
seq_N_R_h = seq_N_R(1,nBurnin + 1:end-1);
seq_N_S_h = seq_N_S(1,nBurnin + 1:end-1);

seq_K_H_prime_h = seq_K_H_prime(1,nBurnin + 1:end-1);
seq_K_R_prime_h = seq_K_R_prime(1,nBurnin + 1:end-1);
seq_K_S_prime_h = seq_K_S_prime(1,nBurnin + 1:end-1);

seq_D_prime_h   = seq_D_prime(1,nBurnin + 1:end-1);
seq_B_prime_h   = seq_B_prime(1,nBurnin + 1:end-1);

seq_R_D_prime_h = seq_R_D_prime(1,nBurnin + 1:end-1);
seq_R_B_prime_h = seq_R_B_prime(1,nBurnin + 1:end-1);
seq_R_K_prime_h = seq_R_K_prime(1,nBurnin + 1:end-1);

seq_C_H_h = seq_C_H(1,nBurnin + 1:end-1);
seq_C_R_h = seq_C_R(1,nBurnin + 1:end-1);
seq_C_S_h = seq_C_S(1,nBurnin + 1:end-1);
seq_Y_h   = seq_Y(1,nBurnin + 1:end-1);






%
%% -- Simulation plot --

figure

subplot(4,4,1);
plot (seq_K_H_prime_l);
hold on
plot (seq_K_H_prime_h, 'm--');
xlabel('time');
ylabel('K^H_{prime}');
title('Simulated path: K^H_{prime}');
% 
% subplot(4,4,2);
% plot (seq_K_R_prime);
% xlabel('time');
% ylabel('K^R_{prime}');
% title('Simulated path: K^R_{prime}');
% 
% subplot(4,4,3);
% plot (seq_K_S_prime);
% xlabel('time');
% ylabel('K^S_{prime}');
% title('Simulated path: K^S_{prime}');
% 
% subplot(4,4,4);
% plot (seq_K);
% hold on
% plot (min(K_grid)*ones(1,nSimulations- nBurnin), 'r');
% plot (max(K_grid)*ones(1,nSimulations- nBurnin), 'r');
% hold off
% xlabel('time');
% ylabel('K');
% title('Simulated path: K');
% 
% 
% subplot(4,4,5);
% plot (seq_D_prime);
% xlabel('time');
% ylabel('D_{prime}');
% title('Simulated path: D_{prime}');
% 
% 
% subplot(4,4,6);
% plot (seq_B_prime);
% xlabel('time');
% ylabel('B_{prime}');
% title('Simulated path: B_{prime}');
% 
% subplot(4,4,7);
% plot (seq_N_H);
% hold on
% plot (min(N_H_grid)*ones(1,nSimulations- nBurnin), 'r');
% plot (max(N_H_grid)*ones(1,nSimulations- nBurnin), 'r');
% hold off
% xlabel('time');
% ylabel('N_H');
% title('Simulated path: N^H');
% 
% subplot(4,4,8);
% plot (seq_N_R);
% hold on
% plot (min(N_R_grid)*ones(1,nSimulations- nBurnin), 'r');
% plot (max(N_R_grid)*ones(1,nSimulations- nBurnin), 'r');
% hold off
% xlabel('time');
% ylabel('N_R');
% title('Simulated path: N^R');
% 
% subplot(4,4,9);
% plot (seq_N_S);
% hold on
% plot (min(N_S_grid)*ones(1,nSimulations- nBurnin), 'r');
% plot (max(N_S_grid)*ones(1,nSimulations- nBurnin), 'r');
% hold off
% xlabel('time');
% ylabel('N^S');
% title('Simulated path: N^S');
% 
% 
% subplot(4,4,10);
% plot (seq_R_D_prime);
% xlabel('time');
% ylabel('R^D_{prime}');
% title('Simulated path: R^D_{prime}');
% 
% 
% subplot(4,4,11);
% plot (seq_R_B_prime);
% xlabel('time');
% ylabel('R^B_{prime}');
% title('Simulated path: R^B_{prime}');
% 
% 
% subplot(4,4,12);
% plot (seq_R_K_prime);
% xlabel('time');
% ylabel('R^K_{prime}');
% title('Simulated path: R^K_{prime}');
% 
% subplot(4,4,13);
% plot (seq_C_H);
% xlabel('time');
% ylabel('C^H');
% title('Simulated path: C^H');
% 
% subplot(4,4,14);
% plot (seq_C_R);
% xlabel('time');
% ylabel('C^R');
% title('Simulated path: C^R');
% 
% subplot(4,4,15);
% plot (seq_C_S);
% xlabel('time');
% ylabel('C^S');
% title('Simulated path: C^S');
% 
% subplot(4,4,16);
% plot (seq_Y);
% xlabel('time');
% ylabel('Y');
% title('Simulated path: Y');