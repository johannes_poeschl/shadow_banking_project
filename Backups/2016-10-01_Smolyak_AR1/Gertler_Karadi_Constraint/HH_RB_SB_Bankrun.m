clear

p.gamma_R = 0.08;
    
load(strcat('Result_NoRun_',date,'_Gamma_',num2str(p.gamma_R),'.mat'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unit_min = -ones(1,4);
unit_max = ones(1,4);
orig_min = [N_H_l N_R_l K_l Z_l];
orig_max = [N_H_u N_R_u K_u Z_u];

% 1 Smolyak approximation level
approx_level = [2 2 2 2];
max_approx_level = max(approx_level);
nDims = 4;

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states,1);
vec_N_H = states(:,1);
vec_N_R = states(:,2);
vec_K = states(:,3);
vec_Z = states(:,4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess for the run solution (we use the no run solution)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% first, compute the smolyak polynomials on the no run grid
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);
    
Run_Grid_Eval = [vec_N_H vec_N_R p.exoInc*ones(n_total,1) vec_K vec_Z];
Run_Grid_Eval = Rescale(Run_Grid_Eval,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
Run_Poly_Eval = Smolyak_Polynomial(Run_Grid_Eval,funs.nDims,funs.max_approx_level,funs.elem);

alfa = No_Run_Poly\pols.K_H_prime;
K_H_prime_run = Run_Poly_Eval*alfa;
    
alfa = No_Run_Poly\pols.K_R_prime;
K_R_prime_run = Run_Poly_Eval*alfa;
    
alfa = No_Run_Poly\pols.R_D_prime;
R_D_prime_run = Run_Poly_Eval*alfa;
    
alfa = No_Run_Poly\pols.D_prime;
D_prime_run = Run_Poly_Eval*alfa;

V_N_H_run       = Run_Poly_Eval*funs.v_N_H_int;
V_N_R_run       = Run_Poly_Eval*funs.v_N_R_int;
Q_run           = Run_Poly_Eval*funs.vec_Q_int;
V_Q_run         = Run_Poly_Eval*funs.v_Q_int;
Pi_Q_run        = Run_Poly_Eval*funs.Pi_Q_prime_int;

V_N_H_old       = V_N_H_run;
V_N_R_old       = V_N_R_run;
Q_old           = Q_run;
V_Q_old         = V_Q_run;
Pi_Q_old        = Pi_Q_run;

funs_run = struct;
funs_run.v_N_H_int = base_poly\V_N_H_run;  % /////
funs_run.v_N_R_int = base_poly\V_N_R_run;  % /////
funs_run.vec_Q_int = base_poly\Q_run;  % /////
funs_run.v_Q_int = base_poly\V_Q_run;  % /////
funs_run.Pi_Q_prime_int = base_poly\Pi_Q_run;

% help
funs_run.orig_min = orig_min;
funs_run.orig_max = orig_max;
funs_run.unit_min = unit_min;
funs_run.unit_max = unit_max;
funs_run.approx_level = approx_level;
funs_run.max_approx_level = max_approx_level;
funs_run.elem = elem_aniso;
funs_run.nDims = nDims;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_H_prime_new_no_run = ones(n_total,n_Z);
N_R_prime_new_no_run = ones(n_total,n_Z);
N_H_prime_new_run = ones(n_total,n_Z);
N_R_prime_new_run = ones(n_total,n_Z);

N_H_prime_old_no_run = ones(n_total,n_Z);
N_R_prime_old_no_run = ones(n_total,n_Z);
N_H_prime_old_run = ones(n_total,n_Z);
N_R_prime_old_run = ones(n_total,n_Z);

vec_K_prime = ones(n_total,1);
vec_K_H_prime = K_H_prime_run;
vec_K_R_prime = K_R_prime_run;
vec_D_prime = D_prime_run;
vec_R_D_prime = R_D_prime_run;  
lambda_H = zeros(n_total,1);
vec_Q = Q_run(:,1);
vec_I = ones(n_total,1);

options = optimset('Display','off', ...
    'FinDiffType','forward', ...
    'TolFun',1e-6);

vError = 10;
nError = 10;
iter = 1;
maxIter = 250;
    
while (vError > 1e-4 || nError > 1e-3) && iter <= maxIter;
tic2 = tic;
    for ii = 1:n_total
        
        logZ_prime = p.rho*states(ii,4) + p.sigma_Z*eps_Z;

        ind = 1;

        handle = @(x_prime) HH_RB_SB_compute_residual_run(p,x_prime, ...
            states(ii,:),logZ_prime',Z_prob',...
            funs, funs_run, ...
            N_H_prime_old_no_run(ii,:),N_R_prime_old_no_run(ii,:), ...
            N_H_prime_old_run(ii,:),N_R_prime_old_run(ii,:),ind);

        x0 = [vec_K_H_prime(ii,1);
            max(vec_K_R_prime(ii,1),0);
            vec_R_D_prime(ii,1);
            0;
            vec_Q(ii,1)];

        [x_prime_new,fval] = fsolve(handle,x0,options);

        if x_prime_new(1) < 0
            ind = 2;

            handle = @(x_prime) HH_RB_SB_compute_residual_run(p,x_prime, ...
                states(ii,:),logZ_prime',Z_prob',...
                funs, funs_run, ...
                N_H_prime_old_no_run(ii,:),N_R_prime_old_no_run(ii,:), ...
                N_H_prime_old_run(ii,:),N_R_prime_old_run(ii,:),ind);

            [x_prime_new,fval] = fsolve(handle,x0,options);
        end

        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_R_D_prime(ii,1) = x_prime_new(3);
        lambda_H(ii,1) = x_prime_new(4);
        vec_Q(ii,1) = x_prime_new(5);

        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1);
        vec_I(ii,1) = vec_K_prime(ii,1) - (1-p.delta)*vec_K(ii,1);

        [N_H_prime_new_no_run(ii,:),N_R_prime_new_no_run(ii,:), ...
         N_H_prime_new_run(ii,:),N_R_prime_new_run(ii,:), ...
            vec_D_prime(ii,:)] = HH_RB_SB_compute_N_prime_run(p,x_prime_new, ...
            states(ii,:),logZ_prime',Z_prob',...
            funs, funs_run, ...
            N_H_prime_old_no_run(ii,:),N_R_prime_old_no_run(ii,:), ...
            N_H_prime_old_run(ii,:),N_R_prime_old_run(ii,:));
    end
    toc(tic2)

    vec_C_H = vec_N_H - vec_Q.*vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_Q.*vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime;

    V_N_H_run = vec_C_H .^(-p.sigma);
    V_N_R_run = vec_C_R .^(-p.sigma_R);
    V_Q_run =  (p.theta*(vec_I./vec_K - p.delta).*vec_I./vec_K - ...
        p.theta/2*(vec_I./vec_K - p.delta).^2);
    Pi_Q_run = (vec_Q-1).*vec_I - p.theta/2*(vec_I./vec_K - p.delta).^2.*vec_K;
    
    vError = norm([V_N_H_run; V_N_R_run] - [V_N_H_old; V_N_R_old],'inf');
    
    nError = norm([N_H_prime_new_no_run; N_R_prime_new_no_run; ...
        N_H_prime_new_run; N_R_prime_new_run] - ...
        [N_H_prime_old_no_run; N_R_prime_old_no_run; ...
        N_H_prime_old_run; N_R_prime_old_run],'inf');
    
    fprintf('Iteration %d: Value Function Error %6.6f \n',iter,vError)
    fprintf('Iteration %d: net Worth Error %6.6f \n \n',iter,nError)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Updating Step
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    V_N_H_old       = V_N_H_run;
    V_N_R_old       = V_N_R_run;
    Q_old           = Q_run;
    V_Q_old         = V_Q_run;

    weight = 0.1;
    N_H_prime_old_no_run = weight*N_H_prime_new_no_run + (1-weight)*N_H_prime_old_no_run;
    N_R_prime_old_no_run = weight*N_R_prime_new_no_run + (1-weight)*N_R_prime_old_no_run;
    N_H_prime_old_run = weight*N_H_prime_new_run + (1-weight)*N_H_prime_old_run;
    N_R_prime_old_run = weight*N_R_prime_new_run + (1-weight)*N_R_prime_old_run;
    
    funs_run.v_N_H_int = base_poly\V_N_H_run;  % /////
    funs_run.v_N_R_int = base_poly\V_N_R_run;  % /////
    funs_run.vec_Q_int = base_poly\Q_run;  % /////
    funs_run.v_Q_int = base_poly\V_Q_run;  % /////
    funs_run.Pi_Q_prime_int = base_poly\Pi_Q_run;

    iter = iter + 1;
end

pols_run = struct;
pols_run.K_H_prime = vec_K_H_prime;
pols_run.K_R_prime = vec_K_R_prime;
pols_run.R_D_prime = vec_R_D_prime;
pols_run.D_prime = vec_D_prime;

vals_run = struct;
vals_run.V_N_H       = V_N_H_run;
vals_run.V_N_R       = V_N_R_run;
vals_run.Q           = Q_run;
vals_run.V_Q         = V_Q_run;
vals_run.Pi_Q_run    = Pi_Q_run;

save(strcat('Result_Run_',date,'_Gamma_',num2str(p.gamma_R),'.mat'))
