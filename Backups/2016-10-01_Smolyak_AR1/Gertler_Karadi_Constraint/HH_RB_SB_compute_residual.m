function euler_residual = HH_RB_SB_compute_residual(p, x0, states, logZ_prime, ...
    Z_prob, funs, N_H_prime_old,N_R_prime_old, N_S_prime_old,ind)

N_H = states(1,1);
N_R = states(1,2);
N_S = states(1,3);  % /////
K   = states(1,4);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);  % /////
R_D_prime = x0(4);
R_B_prime = x0(5);  % /////
lambda_H = x0(6);
lambda_R = x0(7);
Q = x0(8);

K_prime = (K_H_prime + K_R_prime + K_S_prime);  % /////
I = K_prime - (1-p.delta)*K; 

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));

% construct the polynomial at the evaluation point
eval_point = [N_H_prime_old' N_R_prime_old' N_S_prime_old' K_prime' logZ_prime'];
eval_point_rescaled =  Rescale(eval_point,funs.unit_min,funs.unit_max,...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
Q_prime = (eval_poly*funs.vec_Q_int)';
Pi_Q_prime = (eval_poly*funs.Pi_Q_prime_int)';

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
W_prime = (1-p.alpha)*exp(logZ_prime).*K_prime.^p.alpha;

% balance sheet constraint of shadow bank:
min_R_K_prime = min(R_K_prime);
B_prime = (1-p.phi)*min_R_K_prime./R_B_prime.*K_S_prime;  % /////

% balance sheet constraint of retail bank:
% D_prime = ...
%     max((1 - p.gamma_R*p.omega_K)*Q*K_R_prime + (1-p.gamma_R*p.omega_B)*B_prime, ...
%     (1 - p.gamma_R*p.omega_K)*Q*K_R_prime);
D_prime = ...
    (1 - p.gamma_R*p.omega_K)*Q*K_R_prime + (1-p.gamma_R*p.omega_B)*B_prime;
% N'
N_H_prime = R_K_prime*K_H_prime + R_D_prime*D_prime + W_prime + Pi_Q_prime;
N_R_prime = R_K_prime*K_R_prime + R_B_prime*B_prime - R_D_prime*D_prime + p.exoInc;  % /////
N_S_prime = R_K_prime*K_S_prime - R_B_prime*B_prime + p.exoInc;  % /////

% consumptions
C_H = N_H - Q * K_H_prime - p.alpha_H / 2 * K_H_prime^2 - D_prime;
C_R = N_R - Q * K_R_prime - p.alpha_R / 2 * K_R_prime^2 - B_prime + D_prime;  % /////
C_S = N_S - Q * K_S_prime + B_prime;  % /////

% construct the polynomial at the evaluation point
eval_point = [N_H_prime' N_R_prime' N_S_prime' K_prime' logZ_prime'];
eval_point_rescaled =  Rescale(eval_point,funs.unit_min,funs.unit_max, ...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
v_N_H_1 = (eval_poly*funs.v_N_H_int)';
v_N_R_1 = (eval_poly*funs.v_N_R_int)';
v_N_S_1 = (eval_poly*funs.v_N_S_int)';
V_Q_1 = (eval_poly*funs.v_Q_int)';

EV_K_H = sum(Z_prob.*v_N_H_1.*R_K_prime,2);
EV_D   = sum(Z_prob.*v_N_H_1*R_D_prime,2);
EV_K_R = sum(Z_prob.*v_N_R_1.*(R_K_prime - (1-p.gamma_R*p.omega_K)*Q*R_D_prime),2);
EV_B   = sum(Z_prob.*v_N_R_1.*(R_B_prime - (1-p.gamma_R*p.omega_B)*R_D_prime),2);
EV_K_S = sum(Z_prob.*v_N_S_1.*(R_K_prime - min_R_K_prime*(1-p.phi)),2);
EV_Q   = sum(Z_prob.*V_Q_1,2);

euler_residual = zeros(8,1);
euler_residual(1) = C_H^(-p.sigma)*(Q + p.alpha_H*K_H_prime) - p.beta_H * EV_K_H - lambda_H;
euler_residual(2) = C_H^(-p.sigma) - p.beta_H * EV_D;
euler_residual(3) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_K*Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R - lambda_R;
euler_residual(4) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_B)-p.beta_R * EV_B;
euler_residual(5) = C_S^(-p.sigma_S)*(Q - (1-p.phi)*min_R_K_prime/R_B_prime) - p.beta_S * EV_K_S;

if ind == 1
    euler_residual(6) = lambda_H;
    euler_residual(7) = lambda_R;
elseif ind == 2
    euler_residual(6) = K_H_prime;
    euler_residual(7) = lambda_R;
elseif ind == 3
    euler_residual(6) = lambda_H;
    euler_residual(7) = K_R_prime;
else
    euler_residual(6) = K_H_prime;
    euler_residual(7) = K_R_prime;
end

euler_residual(8) = (Q - 1 - p.theta*(I/K - p.delta) + p.beta_H*EV_Q);
