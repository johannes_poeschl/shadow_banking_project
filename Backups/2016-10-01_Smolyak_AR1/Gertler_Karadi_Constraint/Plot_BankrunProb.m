%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all

addpath('./Smolyak_Anisotropic_JMMV_2014')

nBurnin = 100; %%%%%
nSimulations = nBurnin + 500;  % number of simulated periods 

gammaVal = {'0.05','0.08','0.12','0.15','0.2'};   %%%%%
gammaLevel = [0.05, 0.08, 0.12, 0.15, 0.2];  %%%%%

nu = 0.5; %%%%% probability that bankrun happens in case bankrun condition is met, i.e. coordination failure

nAverage = 100; %%%%% number of simulations for each capital requirement level

Prob_bankrun = zeros(size(gammaVal));  %%%%% initial bankrun probability for each capital requirement level

tic

for kk = 1:size(gammaVal,2)  %%%%% for each gammaLevel
for tt = 1:nAverage  %%%%% for each simulation 
    
Sum_prob_bankrun = 0;  %%%%% 

load(strcat('Result_NoRun_26-Aug-2016_Gamma_',gammaVal{kk},'.mat'));
load(strcat('Result_Run_26-Aug-2016_Gamma_',gammaVal{kk},'.mat'));

% -- Simulate a sequence of Z: choose between (1) & (2) --

%- (1) random sequence of Z -
seq_Z_transpose = simulateMarkovChain(Z_grid(5),Z_grid,Z_prob,nSimulations);
seq_Z = seq_Z_transpose';

% - (2) impulse response function -
% seq_Z = Z_grid(5,1)*ones(1,nSimulations);
% seq_Z(1,nBurnin + 30) = Z_grid(1,1);  % productivity shock in the 30th period

% -- Interpolate policy function -- ////
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

funs.K_H_int = No_Run_Poly\pols.K_H_prime;
funs.K_R_int = No_Run_Poly\pols.K_R_prime;
funs.K_S_int = No_Run_Poly\pols.K_S_prime;
funs.D_int = No_Run_Poly\pols.D_prime;
funs.B_int = No_Run_Poly\pols.B_prime;
funs.R_D_int = No_Run_Poly\pols.R_D_prime;
funs.R_B_int =  No_Run_Poly\pols.R_B_prime;

Run_Grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
Run_Poly = Smolyak_Polynomial(Run_Grid,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

funs_run.K_H_int = Run_Poly\pols_run.K_H_prime;
funs_run.K_R_int = Run_Poly\pols_run.K_R_prime;
funs_run.D_int = Run_Poly\pols_run.D_prime;
funs_run.R_D_int = Run_Poly\pols_run.R_D_prime;

% states, no run
seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state 
seq_K   = ones(1, nSimulations)*K_SS;    % state

% states, run
seq_N_H_star = ones(1, nSimulations)*N_H_SS; 
seq_N_R_star = ones(1, nSimulations)*N_R_SS;
seq_N_S_star = p.exoInc*ones(1, nSimulations);

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice 
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price 
seq_R_K_prime = ones(1, nSimulations);  % price
seq_R_K_prime_star = ones(1, nSimulations);
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);        % price, no run
seq_Q_old = seq_Q;                      % price, no run, guess
seq_Q_star = ones(1, nSimulations);     % price, run
seq_Q_star_old = ones(1, nSimulations); % price, run, guess

seq_run = zeros(1, nSimulations);            %%%%% whether bankrun condition is met in period iii
seq_run_happened = zeros(1, nSimulations);   %%%%% whether the run indeed happened in period iii
seq_run_persist = zeros(1, nSimulations);    %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(1, nSimulations);  %%%%% whether starts in period iii

qError = 10;

seq_random = normrnd(0,1,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 

while qError > 1e-6;
    
    for iii = 1: nSimulations-1
                      
            if iii > 1
            seq_run(1,iii) = (1-p.phi)*seq_R_K_prime_star(1,iii-1)*seq_K_S_prime(1,iii-1) < ...
                seq_R_B_prime(1,iii-1)*seq_B_prime(1,iii-1);
            end
            
        if (seq_run(1,iii) == 1  && seq_random(iii) < norminv(nu)) || seq_run_persist(1, iii) == 1; %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
            
            seq_run_happened(1,iii) = 1;  %%%%% bankrun happens
            
            if seq_run_persist(1, iii) ~= 1
                seq_run_triggered(1,iii) = 1;
            end
       
            seq_N_H(1, iii) = seq_N_H_star(1,iii);
            seq_N_R(1 ,iii) = seq_N_R_star(1,iii);
            seq_N_S(1, iii) = seq_N_S_star(1,iii);

            eval_point = [seq_N_H(1,iii) seq_N_R(1,iii) seq_K(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
            
            seq_K_H_prime(1, iii) = eval_poly*funs_run.K_H_int;
            seq_K_R_prime(1, iii) = eval_poly*funs_run.K_R_int;
            seq_K_S_prime(1, iii) = 0;              
            seq_R_D_prime(1, iii) = eval_poly*funs_run.R_D_int;
            seq_R_B_prime(1, iii) = 0;          
            seq_D_prime(1, iii)   = eval_poly*funs_run.D_int;          
            seq_B_prime(1, iii)   = 0;
            seq_Q(1, iii)    = eval_poly*funs_run.vec_Q_int;
            seq_Q_star(1, iii) = seq_Q(1,iii);
            
            seq_run_persist(1, iii+1) = seq_random(iii) < norminv(7/8); %%%%% whether bankrun persists to the next period
                          
        else
            
            eval_point = [seq_N_H(1,iii) seq_N_R(1,iii) seq_N_S(1, iii) seq_K(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);           
            
            seq_K_H_prime(1, iii) = eval_poly*funs.K_H_int;
            seq_K_R_prime(1, iii) = eval_poly*funs.K_R_int;
            seq_K_S_prime(1, iii) = eval_poly*funs.K_S_int;              
            seq_R_D_prime(1, iii) = eval_poly*funs.R_D_int;
            seq_R_B_prime(1, iii) = eval_poly*funs.R_B_int;          
            seq_D_prime(1, iii)   = eval_poly*funs.D_int;          
            seq_B_prime(1, iii)   = eval_poly*funs.B_int;
            seq_Q(1,iii)          = eval_poly*funs.vec_Q_int;
            
            eval_point = [seq_N_H_star(1,iii) seq_N_R_star(1,iii) seq_K(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
            
            seq_Q_star(1,iii) = eval_poly*funs_run.vec_Q_int;
        end
        
        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  

        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);  

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);
                      
        seq_R_K_prime_star(1,iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
        
        seq_N_H_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_H_prime(1, iii) + ...
                 seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
             
        seq_N_R_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_R_prime(1, iii) + ...
                         (1-p.phi)*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii)...
                          - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                          + p.exoInc;
                      
%         seq_N_S_star(1, iii+1) = p.exoInc + p.phi*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii);            
        seq_N_S_star(1, iii+1) = p.exoInc;   
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);

end

Sum_prob_bankrun = mean(seq_run_triggered(nBurnin+1:end)) + Sum_prob_bankrun; %%%%%

end

% seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
% seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
% seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime;  % /////
% seq_Y   = seq_Z .* seq_K.^p.alpha;
% 
% leverage_R = seq_D_prime./(seq_K_R_prime.*seq_Q + seq_B_prime);
% leverage_S = seq_B_prime./(seq_K_S_prime.*seq_Q);
% grossLeverageBoth = (seq_B_prime + seq_D_prime)./((seq_K_R_prime + seq_K_S_prime).*seq_Q + seq_B_prime);
% 
% seq_K_stored(:,:,kk)   = seq_K(1,nBurnin + 1:end-1);
% seq_N_H_stored(:,:,kk) = seq_N_H(1,nBurnin + 1:end-1);
% seq_N_R_stored(:,:,kk) = seq_N_R(1,nBurnin + 1:end-1);
% seq_N_S_stored(:,:,kk) = seq_N_S(1,nBurnin + 1:end-1);
% 
% seq_K_H_prime_stored(:,:,kk) = seq_K_H_prime(1,nBurnin + 1:end-1);
% seq_K_R_prime_stored(:,:,kk) = seq_K_R_prime(1,nBurnin + 1:end-1);
% seq_K_S_prime_stored(:,:,kk) = seq_K_S_prime(1,nBurnin + 1:end-1);
% 
% seq_D_prime_stored(:,:,kk)   = seq_D_prime(1,nBurnin + 1:end-1);
% seq_B_prime_stored(:,:,kk)   = seq_B_prime(1,nBurnin + 1:end-1);
% 
% seq_R_D_prime_stored(:,:,kk) = seq_R_D_prime(1,nBurnin + 1:end-1);
% seq_R_B_prime_stored(:,:,kk) = seq_R_B_prime(1,nBurnin + 1:end-1);
% seq_R_K_prime_stored(:,:,kk) = seq_R_K_prime(1,nBurnin + 1:end-1);
% 
% seq_C_H_stored(:,:,kk) = seq_C_H(1,nBurnin + 1:end-1);
% seq_C_R_stored(:,:,kk) = seq_C_R(1,nBurnin + 1:end-1);
% seq_C_S_stored(:,:,kk) = seq_C_S(1,nBurnin + 1:end-1);
% seq_Y_stored(:,:,kk)   = seq_Y(1,nBurnin + 1:end-1);
% seq_Q_stored(:,:,kk)   = seq_Q(nBurnin+1:end-1);
% seq_Q_star_stored(:,:,kk) = seq_Q_star(nBurnin+1:end-1);
% 
% leverage_R_stored(:,:,kk) = leverage_R(nBurnin+1:end-1);
% leverage_S_stored(:,:,kk) = leverage_S(nBurnin+1:end-1);
% grossLeverageBoth_stored(:,:,kk) = grossLeverageBoth(nBurnin+1:end-1);
Prob_bankrun(kk) = Sum_prob_bankrun/nAverage;
end

elapsedTime = toc

figure
plot(gammaLevel, Prob_bankrun);
xlabel('Capital requirement');
ylabel('Probability of bankrun per quarter');
title('Probability of Bankrun as a Function of Capital Requirement','LineWidth',4);
grid on
saveas(gcf,strcat('./Figures/Bankrun_Prob.fig'))

%%
% -- Simulation plots --

% time = -29:69;
% 
% vals = zeros(4,size(time,2),2,5);
% names = cell(4,1,5);
% 
%     for kk = 1:2
%         vals(:,:,kk,1) = [seq_K_H_prime_stored(:,:,kk); seq_K_R_prime_stored(:,:,kk); seq_K_S_prime_stored(:,:,kk); seq_K_stored(:,:,kk)];
%         names(:,:,1) = {'K^H','K^R','K^S','K'}';
% 
%         vals(:,:,kk,2) = [seq_N_H_stored(:,:,kk); seq_N_R_stored(:,:,kk); seq_N_S_stored(:,:,kk); seq_N_H_stored(:,:,kk) + seq_N_R_stored(:,:,kk) + seq_N_S_stored(:,:,kk)];
%         names(:,:,2) = {'N^H','N^R','N^S','N'}';
% 
%         vals(:,:,kk,3) = [seq_C_H_stored(:,:,kk); seq_C_R_stored(:,:,kk); seq_C_S_stored(:,:,kk); seq_C_H_stored(:,:,kk) + seq_C_R_stored(:,:,kk) + seq_C_S_stored(:,:,kk)];
%         names(:,:,3) = {'C^H','C^R','C^S','C'}';
% 
%         vals(:,:,kk,4) = [seq_R_D_prime_stored(:,:,kk); seq_R_B_prime_stored(:,:,kk); seq_Q_stored(:,:,kk); seq_Q_star_stored(:,:,kk)];
%         names(:,:,4) = {'R^D','R^B','Q','Q*'}';
%         
%         vals(:,:,kk,5) = [seq_D_prime_stored(:,:,kk); seq_B_prime_stored(:,:,kk); seq_Y_stored(:,:,kk); seq_R_K_prime_stored(:,:,kk)];
%         names(:,:,5) = {'D','B','Y','R_K'}';
%     end
% 
% 
% for jj = 1:5
%     figure
%     for ii = 1:size(vals,1)
%         subplot(2,2,ii);
%         plot (time,100*(vals(ii,:,1,jj) - vals(ii,1,1,jj))./(0.01 + vals(ii,1,1,jj)),'b','LineWidth',2);
%         grid on
%         hold on
%         plot (time,100*(vals(ii,:,2,jj) - vals(ii,1,2,jj))./(0.01 + vals(ii,1,2,jj)),'r:','LineWidth',2);
%         xlabel('Time (Quarters)');
%         ylabel('% Dev from SS');
%         title(names{ii,1,jj});
%         legend('No Bank Run','Bank Run')
%     end
%     saveas(gcf,strcat('./Figures/IRF_BankRun_',num2str(jj),'.fig'))
% end
