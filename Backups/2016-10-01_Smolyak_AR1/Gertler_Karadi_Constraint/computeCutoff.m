load(strcat('Result_Run_',date,'_Gamma_0.08.mat'))

funs_run = funs;

load(strcat('Result_Run_',date,'_Gamma_0.08.mat'))

z_Hat_prime = ones(n_total,1)*Z_grid(3,1);
q_Hat_prime = ones(n_total,1);

tic
for ii = 1:n_total
    
    pols = [vec_K_H_prime(ii,1) vec_K_R_prime(ii,1) vec_K_S_prime(ii,1) ...
        vec_R_D_prime(ii,1) vec_R_B_prime(ii,1) vec_D_prime(ii,1) vec_B_prime(ii,1)];
    
    handle = @(x0) findZhat(x0,pols,p,funs_run);
    x0 = [z_Hat_prime(ii,1) q_Hat_prime(ii,1)];
    
    x_out = fsolve(handle,x0,options);
    
    z_Hat_prime(ii,1) = x_out(1);
    
end
toc

prob_bank_run_next_period = normcdf(log(z_Hat_prime),0,p.sigma_Z);