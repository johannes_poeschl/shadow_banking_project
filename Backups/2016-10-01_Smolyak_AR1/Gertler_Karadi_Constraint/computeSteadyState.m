function resid_SS = computeSteadyState(vars,p)

K_H_SS = vars(1);
K_R_SS = vars(2);
K_S_SS = vars(3);

K_SS = K_H_SS + K_R_SS + K_S_SS;

% -- Calculate SS -- //////
R_D_SS = 1/p.beta_H;
R_B_SS = p.gamma_R * p.omega_B / p.beta_R + R_D_SS * (1-p.gamma_R * p.omega_B);
R_K_SS = p.alpha*K_SS^(p.alpha-1) + 1 - p.delta;

dB_S_SS_dK_S_SS =  1/(1-R_B_SS)*(exp(p.phi*K_S_SS*(1-p.beta_S))*p.phi*(1-p.beta_S) + 1 - R_K_SS);

resid_SS(1) = K_H_SS - 1/ p.alpha_H * (p.beta_H * R_K_SS - 1);
resid_SS(2) = K_R_SS - 1/ p.alpha_R * (p.beta_R *(R_K_SS-R_D_SS*(1-p.gamma_R*p.omega_K))-p.gamma_R*p.omega_K);
resid_SS(3) = - 1 + dB_S_SS_dK_S_SS + p.beta_S*(R_K_SS - R_B_SS*dB_S_SS_dK_S_SS);