function resid = findZhat(x0, pols, p, funs)
K_H_prime = pols(1);
K_R_prime = pols(2);
K_S_prime = pols(3);
R_D_prime = pols(4);
R_B_prime = pols(5);
D_prime = pols(6);
B_prime = pols(7);

Z_hat_prime = x0(1);
Q_star_prime = x0(2);

K_prime = K_H_prime + K_R_prime + K_S_prime;
r_K_prime = p.alpha*Z_hat_prime*K_prime^(p.alpha-1);
W_H_prime = (1-p.alpha)*Z_hat_prime*K_prime^p.alpha;
R_K_star_prime = r_K_prime + (1-p.delta)*Q_star_prime;
N_H_star_prime = R_K_star_prime * K_H_prime + R_D_prime * D_prime + W_H_prime;
N_R_star_prime = R_K_star_prime*K_S_prime + R_K_star_prime * K_R_prime - R_D_prime * D_prime + p.exoInc;

resid = zeros(2,1);
 
resid(1) = R_K_star_prime*K_S_prime - R_B_prime*B_prime;
resid(2) = Q_star_prime - funs.vec_Q_int(N_H_star_prime, N_R_star_prime, K_prime);
