%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear

nBurnin = 1000;
nSimulations = nBurnin + 100;  % number of simulated periods 

gammaVal = {'0.08','0.08'};
% -- Simulate a sequence of Z: choose between (1) & (2) --

for kk = 1:2
load(strcat('Result_NoRun_23-Jul-2016_Gamma_',gammaVal{kk},'.mat'));

%- (1) random sequence of Z -
% seq_Z_transpose = simulateMarkovChain(1,Z_grid,Z_prob,nSimulations);
% seq_Z = seq_Z_transpose';

% - (2) impulse response function -
seq_Z = Z_grid(3,1)*ones(1,nSimulations);
seq_Z(1,nBurnin + 30) = Z_grid(1,1);  % productivity shock in the 30th period

% -- Interpolate policy function -- ////
funs_NoRun = struct;
funs_NoRun.K_H_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_H_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.K_R_int = griddedInterpolant({N_H_grid, N_R_grid ,N_S_grid, K_grid},...
    reshape(vec_K_R_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.K_S_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_K_S_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_D_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.R_D_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_D_prime, n_N_H, n_N_R , n_N_S, n_K),'linear');
funs_NoRun.R_B_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_R_B_prime, n_N_H, n_N_R, n_N_S, n_K),'linear');
funs_NoRun.Q_int = griddedInterpolant({N_H_grid, N_R_grid, N_S_grid, K_grid},...
    reshape(vec_Q, n_N_H, n_N_R, n_N_S, n_K),'linear');

load(strcat('Result_Run_23-Jul-2016_Gamma_',gammaVal{kk},'.mat'));
funs_Run = struct;
funs_Run.K_H_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(K_H_prime_run(:,1), n_N_H, n_N_R, n_K),'linear');
funs_Run.K_R_int = griddedInterpolant({N_H_grid, N_R_grid , K_grid},...
    reshape(K_R_prime_run(:,1), n_N_H, n_N_R, n_K),'linear');
funs_Run.D_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(D_prime_run(:,1), n_N_H, n_N_R, n_K),'linear');
funs_Run.R_D_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(R_D_prime_run(:,1), n_N_H, n_N_R , n_K),'linear');
funs_Run.Q_int = griddedInterpolant({N_H_grid, N_R_grid, K_grid},...
    reshape(Q_run(:,1), n_N_H, n_N_R, n_K),'linear');

% states, no run
seq_N_H = ones(1, nSimulations)*N_H_SS;  % state
seq_N_R = ones(1, nSimulations)*N_R_SS;  % state
seq_N_S = ones(1, nSimulations)*N_S_SS;  % state //////
seq_K   = ones(1, nSimulations)*K_SS;    % state

% states, run
seq_N_H_star = ones(1, nSimulations)*N_H_SS; 
seq_N_R_star = ones(1, nSimulations)*N_R_SS;
seq_N_S_star = p.exoInc*ones(1, nSimulations);

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice %////
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice %////
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price  %////
seq_R_K_prime = ones(1, nSimulations);  % price
seq_R_K_prime_star = ones(1, nSimulations);
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);  % price, no run
seq_Q_old = seq_Q; % price, no run, guess
seq_Q_star = ones(1, nSimulations); % price, run
seq_Q_star_old = ones(1, nSimulations); % price, run, guess

seq_run = zeros(1, nSimulations);

qError = 10;
 
while qError > 1e-6;
    for iii = 1: nSimulations-1
        seq_run(1,iii) = (1-p.phi)*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii) < ...
                     seq_R_B_prime(1, iii)*seq_B_prime(1, iii);
        
        seq_K_H_prime(1, iii) = funs_NoRun.K_H_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));
        seq_K_R_prime(1, iii) = funs_NoRun.K_R_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));
        seq_K_S_prime(1, iii) = funs_NoRun.K_S_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));              
        seq_R_D_prime(1, iii) = funs_NoRun.R_D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));
        seq_R_B_prime(1, iii) = funs_NoRun.R_B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii),seq_K(1, iii));

        seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  
        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);
        seq_Q(1,iii)    = funs_NoRun.Q_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                            seq_N_S(1, iii),seq_K(1, iii));
                        
        seq_Q_star(1,iii) = funs_Run.Q_int(seq_N_H_star(1,iii),seq_N_R_star(1,iii), ...
            seq_K(1,iii));

        seq_D_prime(1, iii)   = funs_NoRun.D_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));

        seq_B_prime(1, iii)   = funs_NoRun.B_int(seq_N_H(1, iii), seq_N_R(1, iii),...
                          seq_N_S(1, iii), seq_K(1, iii));

        seq_R_K_prime(1, iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);
                      
        seq_R_K_prime_star(1,iii) = p.alpha * seq_Z(1, iii + 1) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * seq_Z(1, iii + 1) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
                        
        seq_N_H_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_H_prime(1, iii) + ...
                 seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;
        seq_N_S_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii) -...
                     seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                    + p.exoInc;
              
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);
end

display(100*mean(seq_run(nBurnin+1:end)))

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime;  % /////
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime;  % /////
seq_Y   = seq_Z .* seq_K.^p.alpha;

leverage_R = seq_D_prime./(seq_K_R_prime.*seq_Q + seq_B_prime);
leverage_S = seq_B_prime./(seq_K_S_prime.*seq_Q);
grossLeverageBoth = (seq_B_prime + seq_D_prime)./((seq_K_R_prime + seq_K_S_prime).*seq_Q + seq_B_prime);

seq_K_stored(:,:,kk)   = seq_K(1,nBurnin + 1:end-1);
seq_N_H_stored(:,:,kk) = seq_N_H(1,nBurnin + 1:end-1);
seq_N_R_stored(:,:,kk) = seq_N_R(1,nBurnin + 1:end-1);
seq_N_S_stored(:,:,kk) = seq_N_S(1,nBurnin + 1:end-1);

seq_K_H_prime_stored(:,:,kk) = seq_K_H_prime(1,nBurnin + 1:end-1);
seq_K_R_prime_stored(:,:,kk) = seq_K_R_prime(1,nBurnin + 1:end-1);
seq_K_S_prime_stored(:,:,kk) = seq_K_S_prime(1,nBurnin + 1:end-1);

seq_D_prime_stored(:,:,kk)   = seq_D_prime(1,nBurnin + 1:end-1);
seq_B_prime_stored(:,:,kk)   = seq_B_prime(1,nBurnin + 1:end-1);

seq_R_D_prime_stored(:,:,kk) = seq_R_D_prime(1,nBurnin + 1:end-1);
seq_R_B_prime_stored(:,:,kk) = seq_R_B_prime(1,nBurnin + 1:end-1);
seq_R_K_prime_stored(:,:,kk) = seq_R_K_prime(1,nBurnin + 1:end-1);

seq_C_H_stored(:,:,kk) = seq_C_H(1,nBurnin + 1:end-1);
seq_C_R_stored(:,:,kk) = seq_C_R(1,nBurnin + 1:end-1);
seq_C_S_stored(:,:,kk) = seq_C_S(1,nBurnin + 1:end-1);
seq_Y_stored(:,:,kk)   = seq_Y(1,nBurnin + 1:end-1);
seq_Q_stored(:,:,kk)   = seq_Q(nBurnin+1:end-1);
seq_Q_star_stored(:,:,kk) = seq_Q_star(nBurnin+1:end-1);

leverage_R_stored(:,:,kk) = leverage_R(nBurnin+1:end-1);
leverage_S_stored(:,:,kk) = leverage_S(nBurnin+1:end-1);
grossLeverageBoth_stored(:,:,kk) = grossLeverageBoth(nBurnin+1:end-1);
end
%%
% -- Simulation plots --

time = -29:69;

vals = zeros(4,size(time,2),2,5);
names = cell(4,1,5);

for jj = 1:4
    for kk = 1:2
        vals(:,:,kk,1) = [seq_K_H_prime_stored(:,:,kk); seq_K_R_prime_stored(:,:,kk); seq_K_S_prime_stored(:,:,kk); seq_K_stored(:,:,kk)];
        names(:,:,1) = {'K^H','K^R','K^S','K'}';

        vals(:,:,kk,2) = [seq_N_H_stored(:,:,kk); seq_N_R_stored(:,:,kk); seq_N_S_stored(:,:,kk); seq_N_H_stored(:,:,kk) + seq_N_R_stored(:,:,kk) + seq_N_S_stored(:,:,kk)];
        names(:,:,2) = {'N^H','N^R','N^S','N'}';

        vals(:,:,kk,3) = [seq_C_H_stored(:,:,kk); seq_C_R_stored(:,:,kk); seq_C_S_stored(:,:,kk); seq_C_H_stored(:,:,kk) + seq_C_R_stored(:,:,kk) + seq_C_S_stored(:,:,kk)];
        names(:,:,3) = {'C^H','C^R','C^S','C'}';

        vals(:,:,kk,4) = [seq_R_D_prime_stored(:,:,kk); seq_R_B_prime_stored(:,:,kk); seq_Q_stored(:,:,kk); seq_Q_star_stored(:,:,kk)];
        names(:,:,4) = {'R^D','R^B','Q','Q*'}';
        
        vals(:,:,kk,5) = [seq_D_prime_stored(:,:,kk); seq_B_prime_stored(:,:,kk); seq_Y_stored(:,:,kk); seq_R_K_prime_stored(:,:,kk)];
        names(:,:,5) = {'D','B','Y','R_K'}';
    end
end

for jj = 1:5
    figure
    for ii = 1:size(vals,1)
        subplot(2,2,ii);
        plot (time,100*(vals(ii,:,1,jj) - vals(ii,1,1,jj))./vals(ii,1,1,jj),'LineWidth',2);
        grid on
        hold on
        plot (time,100*(vals(ii,:,2,jj) - vals(ii,1,2,jj))./vals(ii,1,2,jj),'LineWidth',2);
        xlabel('Time (Quarters)');
        ylabel('% Dev from SS');
        title(names{ii,1,jj});
        legend('Low CR (0.08)','High CR (0.2)')
    end
    
        saveas(gcf,strcat('./Figures/IRF_NoRun_',num2str(jj),'.fig'))
end
