clear
clc

addpath('./Smolyak_Anisotropic_JMMV_2014')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

useInitialGuess = 1;
usePreviousSolution = 1;

par_name = 'omegaB';

if strcmp(par_name,'omegaB') == 1
    parSpace = 0.5:0.1:2;
elseif strcmp(par_name,'gammaR') == 1
    parSpace = 0.02:0.02:0.24;
end
%     gammaVals = 0.08;

p = struct;

% - Parameters calibrated to non-SS - 
p.rho = 0.8; 
p.sigma_Z = 0.01;  
if useInitialGuess == 1
    p.theta = 0.25; %0.15; %0.25;  % capital adjustment cost %need to keep it small for solutions, 
                % model cannot solve for solutions if big
else
    p.theta = 0;
end

% - parameters calibrated to SS -
p.alpha = 0.36;
p.beta_H = 0.9921;
p.beta_R = 0.9763;
p.beta_S  = 0.9369;
p.delta = 0.025;
p.sigma = 1;
p.alpha_H = 7.8721e-4;  %GKP   % Household managerial cost
p.alpha_R = 4.5356e-4;  %GKP  % Retail banks managerial cost
p.sigma_R = 1;
p.sigma_S = 1;
p.iota = 0;
p.pi = 7/8;
% p.phi = 0.3040;  % 0.15; % 0.3040;   % level of financial friction (moral hazard) in S-B
p.phi = 0.1051;

% - Policy parameters - 
%p.gamma_R = 0.08;  %GAMMA_R
p.omega_K = 0.5;  % risk weights 
 

% - Uncalibrated - 
p.exoInc = 2.5; %0.15; % either add this exogenous income or make banks linear in net worth.

for tt = 1:length(parSpace)
    
if strcmp(par_name,'omegaB') == 1
    p.omega_B = parSpace(tt);  % risk weights;
else
    p.omega_B = 1;  % risk weights
end
    
if strcmp(par_name,'gammaR') == 1
    p.gamma_R = parSpace(tt);
else
    p.gamma_R = 0.08;
end 
    
    
% -- Calculate SS -- //////
R_D_SS = 1/p.beta_H;
R_B_SS = p.gamma_R * p.omega_B / p.beta_R + R_D_SS * (1-p.gamma_R * p.omega_B);
R_K_SS = 1 / (p.beta_S * p.phi + 1/R_B_SS*(1 - p.phi));
r_K_SS = R_K_SS - 1 + p.delta;
K_H_SS = 1/ p.alpha_H * (p.beta_H * R_K_SS - 1);
K_R_SS = 1/ p.alpha_R * (p.beta_R *(R_K_SS-R_D_SS*(1-p.gamma_R*p.omega_K))-p.gamma_R*p.omega_K);
K_SS = (r_K_SS/p.alpha)^(1/(p.alpha-1));
K_S_SS = K_SS - K_H_SS - K_R_SS;
B_SS = (1-p.phi) * R_K_SS * K_S_SS / R_B_SS;
D_SS = K_R_SS + B_SS - p.gamma_R * (p.omega_K*K_R_SS + p.omega_B*B_SS);
W_SS = (1-p.alpha)*K_SS^p.alpha;
N_H_SS = R_D_SS*D_SS + R_K_SS * K_H_SS + W_SS;
N_R_SS = R_K_SS*K_R_SS + R_B_SS*B_SS - R_D_SS*D_SS + p.exoInc;
N_S_SS = R_K_SS*K_S_SS - R_B_SS*B_SS + p.exoInc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_Z = 10;

scale1 = 0.25;
scale2 = 0.25;
scale3 = 0.25;

K_l = (1-scale3) * K_SS;
K_u = (1+scale3) * K_SS;

N_H_l = (1-scale1)*N_H_SS;
N_H_u = (1+scale2)*N_H_SS;

N_R_l = (1-scale1)*N_R_SS;
N_R_u = (1+scale1)*N_R_SS;

N_S_l = (1-scale1)*N_S_SS;
N_S_u = (1+scale1)*N_S_SS;  % /////

uncondStd = sqrt(p.sigma_Z^2/(1-p.rho^2));
Z_l = -3*uncondStd;
Z_u = 3*uncondStd;

unit_min = -ones(1,5);
unit_max = ones(1,5);
orig_min = [N_H_l N_R_l N_S_l K_l Z_l];
orig_max = [N_H_u N_R_u N_S_u K_u Z_u];

% 1 Smolyak approximation level
approx_level = [2 2 2 2 2];
max_approx_level = max(approx_level);
nDims = 5;

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states,1);
vec_N_H = states(:,1);
vec_N_R = states(:,2);
vec_N_S = states(:,3);
vec_K = states(:,4);
vec_logZ = states(:,5);

[logZ_grid, Z_prob_Tauchen] = tauchen(n_Z, 0, p.rho, p.sigma_Z, 3);
% Z_prob_ext = kron(Z_prob,ones(n_total,1));
% Z_grid = exp(logZ_grid);

[eps_Z_Gauss,Z_prob_Gauss1] = qnwnorm(n_Z,0,1);

Z_prob_Gauss = repmat(Z_prob_Gauss1',n_Z,1);
logZ_new_Tauchen = repmat(logZ_grid',n_Z,1);
logZ_new_Gauss = p.rho*repmat(logZ_grid,1,n_Z) + p.sigma_Z*repmat(eps_Z_Gauss',n_Z,1);

E_Z_prime_Tauchen = sum(Z_prob_Tauchen.*logZ_new_Tauchen,2);
E_Z_prime_Gauss = sum(Z_prob_Gauss.*logZ_new_Gauss,2);

[eps_Z,Z_prob] = qnwnorm(n_Z,0,1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if useInitialGuess == 0
    vec_K_H_prime = zeros(n_total,1);
    vec_K_R_prime = zeros(n_total,1);
    vec_K_S_prime = zeros(n_total,1); 
    vec_D_prime = zeros(n_total,1);
    vec_B_prime = zeros(n_total,1); 


    vec_Q = ones(n_total,1);
    Pi_Q_prime = zeros(n_total,1);
    
    vec_C_H = vec_N_H - vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
    vec_C_S = vec_N_S - vec_K_S_prime + vec_B_prime; 

    V_N_H = vec_C_H .^(-p.sigma);
    V_N_R = vec_C_R .^(-p.sigma_R);
    V_N_S = vec_C_S .^(-p.sigma_S);  
    V_Q = zeros(n_total,1);
    
else
    load('initialGuess')
    
    V_N_H = vals.V_N_H;
    V_N_R = vals.V_N_R;
    V_N_S = vals.V_N_S; 
    
    vec_Q = vals.vec_Q;
    V_Q = vals.V_Q;
    Pi_Q_prime = vals.Pi_Q_prime;
end
    
    
V_N_H_old = V_N_H;
V_N_R_old = V_N_R;
V_N_S_old = V_N_S; 
V_Q_old = V_Q;
vec_Q_old = vec_Q;
Pi_Q_prime_Old = Pi_Q_prime;

funs = struct;

% interpolated values
funs.v_N_H_int = base_poly\V_N_H; 
funs.v_N_R_int = base_poly\V_N_R; 
funs.v_N_S_int = base_poly\V_N_S;  
funs.vec_Q_int = base_poly\vec_Q;  
funs.v_Q_int = base_poly\V_Q;  
funs.Pi_Q_prime_int = base_poly\Pi_Q_prime;

% help
funs.orig_min = orig_min;
funs.orig_max = orig_max;
funs.unit_min = unit_min;
funs.unit_max = unit_max;
funs.approx_level = approx_level;
funs.max_approx_level = max_approx_level;
funs.elem = elem_aniso;
funs.nDims = nDims;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_H_prime_new = repmat(vec_N_H, 1,n_Z + 1);
N_R_prime_new = repmat(vec_N_R, 1,n_Z + 1);
N_S_prime_new = repmat(vec_N_S, 1,n_Z + 1); 

N_H_prime_old = N_H_prime_new;
N_R_prime_old = N_R_prime_new;
N_S_prime_old = N_S_prime_new; 

vError = 10;
nError = 10;
iter = 1;
maxIter =  250;
initialIter = 1;

vec_K_prime = K_SS*ones(n_total,1);
if useInitialGuess == 0
    vec_K_H_prime = ones(n_total,1);
    vec_K_R_prime = ones(n_total,1);
    vec_K_S_prime = ones(n_total,1); 
    vec_R_D_prime = ones(n_total,1);  
    vec_R_B_prime = ones(n_total,1); 
    lambda_H = zeros(n_total,1);
    lambda_R = zeros(n_total,1);
else
    vec_K_H_prime = pols.K_H_prime;
    vec_K_R_prime = pols.K_R_prime;
    vec_K_S_prime = pols.K_S_prime; 
    vec_R_D_prime = pols.R_D_prime;  
    vec_R_B_prime = pols.R_B_prime; 
    lambda_H = zeros(n_total,1);
    lambda_R = zeros(n_total,1);
end

options = optimset('Display','off','MaxIter',1e3,'MaxFunEvals',1e3);

vec_I   = ones(n_total,1);

worst_Z = p.rho*min(vec_logZ) + p.sigma_Z*min(eps_Z);

while (vError > 1e-4 || nError > 1e-2) && iter <= maxIter
    tic

    parfor ii = 1:n_total    
        ind = 1;
        
%         display(ii)

        logZ_prime = [p.rho*states(ii,5) + p.sigma_Z*eps_Z; worst_Z];
        Z_prob1 = [Z_prob; 0];
        
        handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);

        x0 = [vec_K_H_prime(ii,1);
            vec_K_R_prime(ii,1);
            vec_K_S_prime(ii,1);
            vec_R_D_prime(ii,1);
            vec_R_B_prime(ii,1);
            0;
            0;
            vec_Q(ii,1)];
        
        [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
        if x_prime_new(1) < 0 && x_prime_new(2) >= 0
            ind = 2;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            if x_prime_new(2) < 0
                ind = 4;
            
                handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
                states(ii,:),logZ_prime',Z_prob1',...
                funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
                N_S_prime_old(ii,:),ind);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) >= 0 && x_prime_new(2) < 0
            ind = 3;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            if x_prime_new(2) < 0
                ind = 4;
            
                handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
                states(ii,:),logZ_prime',Z_prob1',...
                funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
                N_S_prime_old(ii,:),ind);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) < 0 && x_prime_new(2) < 0
            ind = 4;
            
            handle = @(x_prime) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:),ind);
            
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
        end
        
        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_K_S_prime(ii,1) = x_prime_new(3);
        vec_R_D_prime(ii,1) = x_prime_new(4);
        vec_R_B_prime(ii,1) = x_prime_new(5);
        lambda_H(ii,1) = x_prime_new(6);
        lambda_R(ii,1) = x_prime_new(7);
        vec_Q(ii,1) = x_prime_new(8);
        
        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
            + vec_K_S_prime(ii,1);
        vec_I(ii,1) = vec_K_prime(ii,1) - (1-p.delta)*vec_K(ii,1);

        [N_H_prime_new(ii,:),N_R_prime_new(ii,:),N_S_prime_new(ii,:),...
         vec_D_prime(ii,:), vec_B_prime(ii,:)] = HH_RB_SB_compute_N_prime...
         (p, x_prime_new, states(ii,:), logZ_prime',Z_prob1', funs, ...
         N_H_prime_old(ii,:),N_R_prime_old(ii,:), N_S_prime_old(ii,:));
     
        
      %---------------------------------------------------------------------------
        [EV_D(ii,1), EV_B(ii,1)] = compute_EV(p,x_prime_new, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:));  
      %---------------------------------------------------------------------------

      
    end
    toc
    
%     close(h)
    
    % -- balance sheet constraint of retail bank: --  
    vec_C_H = vec_N_H - vec_Q.*vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_Q.*vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
    vec_C_S = vec_N_S - vec_Q.*vec_K_S_prime + vec_B_prime; % ////// 

    V_N_H = vec_C_H .^(-p.sigma);
    V_N_R = vec_C_R .^(-p.sigma_R);
    V_N_S = vec_C_S .^(-p.sigma_S);  % /////

    V_Q =  (p.theta*(vec_I./vec_K - p.delta).*vec_I./vec_K - ...
        p.theta/2*(vec_I./vec_K - p.delta).^2);
    
    Pi_Q_prime = (vec_Q-1).*vec_I - p.theta/2*(vec_I./vec_K - p.delta).^2.*vec_K;
    
    vError = norm([V_N_H; V_N_R; V_N_S] - [V_N_H_old; V_N_R_old; V_N_S_old],'inf');
    nError = norm([N_H_prime_new; N_R_prime_new; N_S_prime_new] - ...
        [N_H_prime_old; N_R_prime_old; N_S_prime_old],'inf');
    fprintf('Iteration %d: Value Function Error %6.6f \n',iter,vError)
    fprintf('Iteration %d: net Worth Error %6.6f \n \n',iter,nError)
    if iter == 1
        weight = 1;
    else
        weight = 0.1;
    end
    N_H_prime_old = weight*N_H_prime_new + (1-weight)*N_H_prime_old;
    N_R_prime_old = weight*N_R_prime_new + (1-weight)*N_R_prime_old;
    N_S_prime_old = weight*N_S_prime_new + (1-weight)*N_S_prime_old;  
    
    weight = 1;
    vec_Q = weight*vec_Q + (1-weight)*vec_Q_old;
    V_Q = weight*V_Q + (1-weight)*V_Q_old;
    Pi_Q_prime = weight*Pi_Q_prime + (1-weight)*Pi_Q_prime_Old;
    
    V_N_H = weight*V_N_H + (1-weight)*V_N_H_old;
    V_N_R = weight*V_N_R + (1-weight)*V_N_R_old;
    V_N_S = weight*V_N_S + (1-weight)*V_N_S_old;
 
    % interpolated values
    funs.v_N_H_int = base_poly\V_N_H; 
    funs.v_N_R_int = base_poly\V_N_R; 
    funs.v_N_S_int = base_poly\V_N_S; 
    funs.vec_Q_int = base_poly\vec_Q; 
    funs.v_Q_int = base_poly\V_Q;  
    funs.Pi_Q_prime_int = base_poly\Pi_Q_prime;

    V_N_H_old = V_N_H;
    V_N_R_old = V_N_R;
    V_N_S_old = V_N_S;
    vec_Q_old = vec_Q;
    V_Q_old = V_Q;
    Pi_Q_prime_Old = Pi_Q_prime;
    iter = iter + 1;
end

%%
% % check how the functions look like
% nPlot = 15;
% n_h_grid = linspace(N_H_l,N_H_u,nPlot);
% n_r_grid = linspace(N_R_l,N_R_u,nPlot);
% n_s_grid = linspace(N_S_l,N_S_u,nPlot);
% K_grid = linspace(K_l,K_u,nPlot);
% Z_grid = linspace(Z_l,Z_u,5);
% 
% [n_h_nd,n_r_nd,n_s_nd,k_nd,z_nd] = ndgrid(n_h_grid,n_r_grid,n_s_grid,K_grid,Z_grid);
% 
% % states_plot = [vec(n_h_nd) vec(n_r_nd) N_S_SS*ones(nPlot^2,1) K_SS*ones(nPlot^2,1)];
% states_plot = [vec(n_h_nd) vec(n_r_nd) vec(n_s_nd) vec(k_nd) vec(z_nd)];
% states_plot_resc = Rescale(states_plot,unit_min,unit_max,orig_min,orig_max);
% states_plot_poly = Smolyak_Polynomial(states_plot_resc,nDims,max_approx_level,elem_aniso);
% 
% V_N_H_plot = states_plot_poly*funs.v_N_H_int;
% V_N_H_plot1= reshape(V_N_H_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% V_N_R_plot = states_plot_poly*funs.v_N_R_int;
% V_N_R_plot1= reshape(V_N_R_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% V_N_S_plot = states_plot_poly*funs.v_N_S_int;
% V_N_S_plot1= reshape(V_N_S_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% Q_plot = states_plot_poly*funs.vec_Q_int;
% Q_plot1= reshape(Q_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% V_Q_plot = states_plot_poly*funs.v_Q_int;
% V_Q_plot1= reshape(Q_plot,nPlot,nPlot,nPlot,nPlot,5);

% hf1 = figure;
% subplot(2,2,1)
% surf(V_N_H_plot1(:,:,5,1,3));
% title('V_N_H')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(2,2,2)
% surf(V_N_R_plot1(:,:,10,10,3));
% title('V_N_R')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(2,2,3)
% surf(V_N_S_plot1(:,:,10,10,3));
% title('V_N_S')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(2,2,4)
% surf(Q_plot1(:,:,10,1,3));
% title('Q')
% xlabel('N_R')
% ylabel('N_H')
% 
% hf2 = figure;
% subplot(2,2,1)
% surf(squeeze(V_N_H_plot1(10,10,:,:,3)));
% title('V_N_H')
% xlabel('K')
% ylabel('N_S')
% 
% subplot(2,2,2)
% surf(squeeze(V_N_R_plot1(10,10,:,:,3)));
% title('V_N_R')
% xlabel('K')
% ylabel('N_S')
% 
% subplot(2,2,3)
% surf(squeeze(V_N_S_plot1(10,10,:,:,3)));
% title('V_N_S')
% xlabel('K')
% ylabel('N_S')
% 
% subplot(2,2,4)
% surf(squeeze(Q_plot1(1,1,:,:,3)));
% title('Q')
% xlabel('K')
% ylabel('N_S')
% 
% figure
% subplot(1,2,1)
% surf(squeeze(V_Q_plot1(:,:,10,10,3)));
% title('V_Q')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(1,2,2)
% surf(squeeze(V_Q_plot1(10,10,:,:,3)));
% title('V_Q')
% xlabel('K')
% ylabel('N_S')
% 
% close all

%%
pols = struct;
pols.K_H_prime = vec_K_H_prime;
pols.K_R_prime = vec_K_R_prime;
pols.K_S_prime = vec_K_S_prime;
pols.R_D_prime = vec_R_D_prime;
pols.R_B_prime = vec_R_B_prime;
pols.D_prime = vec_D_prime;
pols.B_prime = vec_B_prime;
pols.Q  = vec_Q;
pols.N_H_prime = N_H_prime_new;
pols.N_R_prime = N_R_prime_new;
pols.N_S_prime = N_S_prime_new;

vals = struct;
vals.V_N_H = V_N_H;
vals.V_N_R = V_N_R;
vals.V_N_S = V_N_S;
vals.vec_Q = vec_Q;
vals.V_Q = V_Q;
vals.Pi_Q_prime = Pi_Q_prime;

if useInitialGuess == 0 || usePreviousSolution == 1
    save('initialGuess','vals','pols')
end

%% Check if borrowing constraints are indeed binding

FOC_D_R_prime = vec_C_R.^(-p.sigma) - p.beta_R.*EV_D;
FOC_B_S_prime = vec_C_S.^(-p.sigma) - p.beta_S.*EV_B;

figure
plot(FOC_D_R_prime);
hold on 
plot(FOC_B_S_prime);
hold off
legend('FOC(D^R'')', 'FOC(B^S'')')
% 
% close all
% fprintf('Borrowing constraints are binding if FOC_D_R_prime and FOC_B_S_prime are positive \n')

save(strcat('Result_NoRun_',date,'_',par_name,'_',num2str(parSpace(tt)),'.mat'))
end