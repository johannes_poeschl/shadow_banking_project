%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
close all

addpath('./Smolyak_Anisotropic_JMMV_2014')

% nu = 0.025; %%%%% probability that bankrun happens in case bankrun condition is met, i.e. coordination failure
nu = 0.025;

tic
nBurnin = 200; 
nSimulations = nBurnin + 800;  % number of simulated periods 
nPanels = 100; %%%%% number of simulations for each capital requirement level

par_name = 'gamma';

if strcmp(par_name,'omegaB') == 1
    parSpace = 0.5:0.1:2;
    parNameAlt = '\omega^B';
elseif strcmp(par_name,'gamma') == 1
    parNameAlt = '\Gamma_R';
    parSpace = 0.02:0.02:0.2;
end

dateVal = '14-Oct-2016'; %date; %%%%%

bankRunProb = zeros(nPanels,length(parSpace));
cons_eq_H = zeros(nPanels,length(parSpace));
cons_eq_R = zeros(nPanels,length(parSpace));
cons_eq_S = zeros(nPanels,length(parSpace));

mean_C_H = zeros(1,length(parSpace));
mean_C_R = zeros(1,length(parSpace));
mean_C_S = zeros(1,length(parSpace));

sd_C_H = zeros(1,length(parSpace));
sd_C_R = zeros(1,length(parSpace));
sd_C_S = zeros(1,length(parSpace));

mean_Q = zeros(1,length(parSpace));
mean_Q_star = zeros(1,length(parSpace));
sd_Q = zeros(1,length(parSpace));

load(strcat('Result_NoRun_',dateVal,'_',par_name,'_',num2str(parSpace(1)),'.mat'))

%--------------------------------------------------
seq_logZ = zeros(nPanels,nSimulations);
n_Z = 25;
[Z_grid,Z_prob] = tauchen(n_Z,0,p.rho,p.sigma_Z,4);
%--------------------------------------------------

for jj = 1:nPanels  %%%%% for each simulation 
    % --  random sequence of Z --
    seq_logZ_transpose = simulateMarkovChain(Z_grid(13),Z_grid,Z_prob,nSimulations);
    seq_logZ(jj,:) = seq_logZ_transpose';
end

seq_random1 = normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
seq_random2 = normrnd(0,1,nPanels,nSimulations); 

for kk = 1:length(parSpace) %%%%% for each capital requirement level

load(strcat('Result_NoRun_',dateVal,'_',par_name,'_',num2str(parSpace(kk)),'.mat'))
load(strcat('Result_Run_',dateVal,'_',par_name,'_',num2str(parSpace(kk)),'.mat'))

% -- Interpolate policy function -- 

% - no run case -
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

funs.K_H_int = No_Run_Poly\pols.K_H_prime;
funs.K_R_int = No_Run_Poly\pols.K_R_prime;
funs.K_S_int = No_Run_Poly\pols.K_S_prime;
funs.D_int   = No_Run_Poly\pols.D_prime;
funs.B_int   = No_Run_Poly\pols.B_prime;
funs.R_D_int = No_Run_Poly\pols.R_D_prime;
funs.R_B_int = No_Run_Poly\pols.R_B_prime;

% - run case -

Run_Grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
Run_Poly = Smolyak_Polynomial(Run_Grid,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

funs_run.K_H_int = Run_Poly\pols_run.K_H_prime;
funs_run.K_R_int = Run_Poly\pols_run.K_R_prime;
funs_run.D_int   = Run_Poly\pols_run.D_prime;
funs_run.R_D_int = Run_Poly\pols_run.R_D_prime;

% - states, no run -
seq_N_H = ones(nPanels, nSimulations)*N_H_SS;  % state
seq_N_R = ones(nPanels, nSimulations)*N_R_SS;  % state
seq_N_S = ones(nPanels, nSimulations)*N_S_SS;  % state 
seq_K   = ones(nPanels, nSimulations)*K_SS;    % state

% - states, run -
seq_N_H_star = ones(nPanels, nSimulations)*N_H_SS;    % state
seq_N_R_star = ones(nPanels, nSimulations)*N_R_SS;    % state
seq_N_S_star = ones(nPanels, nSimulations)*p.exoInc;  % state

seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice 
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_I         = ones(nPanels, nSimulations);  % choice

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_old      = ones(nPanels, nSimulations);  % price, no run, guess
seq_Q_star     = ones(nPanels, nSimulations);  % price, run
seq_Q_star_old = ones(nPanels, nSimulations);  % price, run, guess

seq_R_K_prime_star = ones(nPanels, nSimulations);

seq_run           = zeros(nPanels, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_happened  = zeros(nPanels, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(nPanels, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(nPanels, nSimulations);   %%%%% whether a run starts in period iii
seq_recovery = zeros(nPanels, nSimulations);

qError = 10;
while qError > 1e-6;
    
    for iii = 1: nSimulations-1
                      
        if iii > 1
            seq_run(:,iii) = (1-p.phi)*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1) - ...
                seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1) < 0;
            
            seq_recovery(:,iii) = (1-p.phi)*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1)./(seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1));
            seq_recovery(seq_B_prime(:,iii-1) == 0,iii) = 1;
        end

        A = seq_run(:,iii).*(seq_random1(:,iii) < norminv(nu));  % bankrun condition is met and coordination fails
        B = seq_run_persist(:, iii) == 1; % bankrun continues

        seq_run_happened(:,iii) =  1 - (1-A).*(1-B); %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
        seq_run_triggered(:,iii) = A.*(1-B);
        seq_run_persist(:, iii+1) = seq_run_happened(:,iii).*(seq_random2(:,iii) < norminv(7/8)); %%%%% whether bankrun persists to the next period

        % bank run stuff
        seq_N_H(:, iii) = seq_N_H_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_H(:,iii).*(1-seq_run_happened(:,iii));
        seq_N_R(: ,iii) = seq_N_R_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_R(:,iii).*(1-seq_run_happened(:,iii));
        seq_N_S(:, iii) = seq_N_S_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_S(:,iii).*(1-seq_run_happened(:,iii));

        eval_point = [seq_N_H(:,iii) seq_N_R(:,iii) seq_K(:,iii) seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

        seq_K_H_prime_run = eval_poly*funs_run.K_H_int;
        seq_K_R_prime_run = eval_poly*funs_run.K_R_int;
        seq_K_S_prime_run = 0;              
        seq_R_D_prime_run = eval_poly*funs_run.R_D_int;
        seq_R_B_prime_run = R_B_SS;          
        seq_D_prime_run   = eval_poly*funs_run.D_int;          
        seq_B_prime_run   = 0;
        seq_Q_run         = eval_poly*funs_run.vec_Q_int;

        % no bank run stuff
        eval_point = [seq_N_H(:,iii) seq_N_R(:,iii) seq_N_S(:, iii) seq_K(:,iii)  seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);           

        seq_K_H_prime_no_run = eval_poly*funs.K_H_int;
        seq_K_R_prime_no_run = eval_poly*funs.K_R_int;
        seq_K_S_prime_no_run = eval_poly*funs.K_S_int;              
        seq_R_D_prime_no_run = eval_poly*funs.R_D_int;
        seq_R_B_prime_no_run = eval_poly*funs.R_B_int;          
        seq_D_prime_no_run   = eval_poly*funs.D_int;          
        seq_B_prime_no_run   = eval_poly*funs.B_int;
        seq_Q_no_run         = eval_poly*funs.vec_Q_int;

        seq_K_H_prime(:, iii) = seq_K_H_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_H_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_R_prime(:, iii) = seq_K_R_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_R_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_S_prime(:, iii) = seq_K_S_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_S_prime_no_run.*(1-seq_run_happened(:,iii));              
        seq_R_D_prime(:, iii) = seq_R_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_D_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_R_B_prime(:, iii) = seq_R_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_B_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_D_prime(:, iii)   = seq_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_D_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_B_prime(:, iii)   = seq_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_B_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_Q(:, iii)         = seq_Q_run.*seq_run_happened(:,iii) + ...
            seq_Q_no_run.*(1-seq_run_happened(:,iii));

        eval_point = [seq_N_H_star(:,iii) seq_N_R_star(:,iii) seq_K(:,iii)  seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

        seq_Q_star(:,iii) = eval_poly*funs_run.vec_Q_int;
        
        seq_K(:, iii+1) = seq_K_H_prime(:, iii) + seq_K_R_prime(:, iii) + seq_K_S_prime(:, iii);  

        seq_I(:,iii)    = seq_K(:,iii+1) - (1-p.delta)*seq_K(:,iii);  

        seq_R_K_prime(:, iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(:, iii + 1);
                      
        seq_R_K_prime_star(:,iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(:, iii + 1);

        seq_W_prime(:, iii)  = (1-p.alpha) * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^p.alpha;

        seq_N_H(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_H_prime(:, iii) + ...
                             seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
                         
        seq_N_R(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_R_prime(:, iii) + ...
                  seq_R_B_prime(:,iii).*seq_B_prime(:,iii) ...
                  - seq_R_D_prime(:, iii).*seq_D_prime(:, iii) ...
                  + p.exoInc;     
              
        seq_N_S(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_S_prime(:, iii) -...
                             seq_R_B_prime(:, iii).*seq_B_prime(:, iii) ...
                            + p.exoInc;
        
        seq_N_H_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_H_prime(:, iii) + ...
                 seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
             
        seq_N_R_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_R_prime(:, iii) + ...
                         (1-p.phi)*seq_R_K_prime_star(:, iii).*seq_K_S_prime(:, iii)...
                          - seq_R_D_prime(:, iii).*seq_D_prime(:, iii) ...
                          + p.exoInc;
                      
        %seq_N_S_star(1, iii+1) = p.exoInc + p.phi*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii);            
        seq_N_S_star(:, iii+1) = p.exoInc*ones(nPanels,1);   
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);

end

seq_C_H = max(1e-9,seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime); %%%%%%%%%%%%%
seq_C_R = max(1e-9,seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime);  %%%%%%%%%%%%%
seq_C_S = max(1e-9,seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime); %%%%%%%%%%%%%


util_H = log(seq_C_H(:,nBurnin+1:end));
util_R = log(seq_C_R(:,nBurnin+1:end));
util_S = log(seq_C_S(:,nBurnin+1:end));

tt = repmat(0:1:nSimulations-nBurnin-1,nPanels,1);

seq_V_H = sum(p.beta_H.^tt.*util_H,2);
seq_V_R = sum(p.beta_R.^tt.*util_R,2);
seq_V_S = sum(p.beta_S.^tt.*util_S,2);

cons_eq_H(:,kk) = exp((1-p.beta_H)*seq_V_H);
cons_eq_R(:,kk) = exp((1-p.beta_R)*seq_V_R);
cons_eq_S(:,kk) = exp((1-p.beta_S)*seq_V_S);

mean_C_H(1,kk) = mean(mean(seq_C_H(:,nBurnin+1:end),2));
mean_C_R(1,kk) = mean(mean(seq_C_R(:,nBurnin+1:end),2));
mean_C_S(1,kk) = mean(mean(seq_C_S(:,nBurnin+1:end),2));

sd_C_H(1,kk) = mean(std(seq_C_H(:,nBurnin+1:end),[],2));
sd_C_R(1,kk) = mean(std(seq_C_R(:,nBurnin+1:end),[],2));
sd_C_S(1,kk) = mean(std(seq_C_S(:,nBurnin+1:end),[],2));

mean_Q(1,kk) = mean(mean(seq_Q_old(:,nBurnin+1:end),2));
mean_Q_star(1,kk) =  mean(mean(seq_Q_star(:,nBurnin+1:end),2));
sd_Q(1,kk) = mean(std(seq_Q_old(:,nBurnin+1:end),[],2));

bankRunProb(:,kk) = mean(seq_run_triggered(:,nBurnin+1:end),2);

end

cons_eqv_H = mean(cons_eq_H,1);
cons_eqv_R = mean(cons_eq_R,1);
cons_eqv_S = mean(cons_eq_S,1);

save(strcat('Result_Simulation_',par_name,'_',date,'.mat'))

%%

mu_R = linspace(0,1,500);
mu_S = mu_R;

nMu = length(mu_R);

[nd_mu_R,nd_mu_S] = ndgrid(mu_R,mu_S);

nd_mu_H = 1 - nd_mu_R - nd_mu_S;

nd_mu_H(nd_mu_H < 0) = 0;

vec_mu = [vec(nd_mu_H) vec(nd_mu_R) vec(nd_mu_S)];

max_req = zeros(nMu^2,1);
max_val = zeros(nMu^2,1);

for ii = 1:nMu^2
    [max_val(ii),max_pos] = max(vec_mu(ii,1)*cons_eqv_H + ...
        vec_mu(ii,2)*cons_eqv_R + vec_mu(ii,3)*cons_eqv_S);
    
    max_req(ii) = parSpace(max_pos);
end

nd_req = reshape(max_req,nMu,nMu);
nd_req(nd_mu_H == 0) = nan;

nd_val = reshape(max_val,nMu,nMu);
nd_val(nd_mu_H == 0) = nan;

figure
subplot(1,2,1)
contourf(mu_S,mu_R,nd_req)
xlabel('mu_S')
ylabel('mu_R')
title(strcat('Optimal',{' '},parNameAlt))

subplot(1,2,2)
contourf(mu_S,mu_R,nd_val)
xlabel('mu_S')
ylabel('mu_R')
title('Pareto Frontier')

saveas(gcf,strcat('./Figures/Pareto_Frontier_',par_name,'_',dateVal,'.fig'))

% save(strcat('WelfarePlots_',date,'.mat'))
%save(strcat('Result_Run_',date,'_Gamma_',num2str(p.gamma_R),'.mat'))


%% Plot
% elapsedTime = toc

fontOptions = {'FontSize',15};

figure
plot(parSpace,100*mean(bankRunProb,1),'LineWidth',2);
xlabel(parNameAlt);
ylabel('Probability of bankrun per quarter (%)');
title('Probability of Bankrun as a Function of Capital Requirement');
xlim([min(parSpace) max(parSpace)])
grid on
saveas(gcf,strcat('./Figures/Bankrun_Prob_',par_name,'_',dateVal,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [29.7/2 21/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Bankrun_Prob_',par_name);
print(figureName,'-dpdf')
print(figureName,'-dpng')
print(figureName,'-deps')
%%
yVals = [cons_eqv_H; cons_eqv_R; cons_eqv_S];

names = {'Cons Eq HH','Cons Eq R Banks','Cons Eq S Banks'};

figure
for ii = 1:size(yVals,1)
    subplot(1,3,ii)
    plot(parSpace,yVals(ii,:),'LineWidth',2)
    grid on
    title(names{ii},fontOptions{:})
    xlabel(parNameAlt,fontOptions{:})
    ylabel('Perm Cons Equiv',fontOptions{:})
end
saveas(gcf,strcat('./Figures/Welfare_',par_name,'_',date,'.fig'))


%%
fontOptions = {'FontSize',12};
names = {'Welfare of HH','Welfare of R Banks','Welfare of S Banks'};
figure
for ii = 1:size(yVals,1)
    subplot(2,2,ii)
    plot(parSpace,yVals(ii,:),'LineWidth',2)
    grid on
    title(names{ii},fontOptions{:})
    xlabel(parNameAlt,fontOptions{:})
    ylabel('Perm Cons Equiv',fontOptions{:})
    xlim([min(parSpace) max(parSpace)])
end
subplot(2,2,4)
contourf(mu_S,mu_R,nd_req)
xlabel('mu_S')
ylabel('mu_R')
title(strcat('Optimal',{' '},parNameAlt),fontOptions{:})


% saveas(gcf,strcat('./Figures/Welfare_',par_name,'_',date,'.fig'))