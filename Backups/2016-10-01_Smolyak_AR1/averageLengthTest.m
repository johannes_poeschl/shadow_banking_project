nIter = 100;
rateVal = zeros(nIter,1);
rateVal(1) = 1;
piVal = 7/8;

for s = 2:100
    
    rateVal(s) = rateVal(s-1) + piVal^(s-1)*s;
    
end

rateVal = (1-piVal)*rateVal;