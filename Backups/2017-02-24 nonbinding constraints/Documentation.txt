Solution and Simulation files for the paper 

"TBA"

by Johannes Poeschl and Xue Zhang

In this file, I document all changes that I made to the code after 03.01.2017.

The version from 03.01.2017 is for all purposes considered the first running version of the code.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Changes

03.01.2017

- Added a "backup" folder. All future backups should be stored in this folder.
- Added a "results" folder. All future results should be stored in this folder.
- Replaced the save and load destinations into the "results"-folder