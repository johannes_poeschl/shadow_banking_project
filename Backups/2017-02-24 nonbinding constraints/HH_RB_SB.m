% This file computes the no bank run solution.
%
% Dependencies: This code uses the toolbox provided by Judd, Maliar, Maliar
% & Valero (2014). This code also uses the CompEcon Toolbox by Miranda &
% Fackler. In addition, this code depends on the following functions:
%
%               - HH_RB_SB_compute_residual
%               - HH_RB_SB_compute_N_prime
%
% Output:       - initialGuess.mat
%               - Result_NoRun.mat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
clc

addpath('./Smolyak_Anisotropic_JMMV_2014')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
useInitialGuess = 1;
usePreviousSolution = 1;

parName = 'gammaR';
if strcmp(parName,'omegaB') == 1
    parSpace = 1;
elseif strcmp(parName,'gammaR') == 1
%     parSpace = 0.08;
    parSpace = 0.02:0.02:0.2;
end

p = struct;

% - Parameters calibrated to non-SS - 
p.rho = 0.8; 
p.sigma_Z = 0.01;  
if useInitialGuess == 1
    p.theta = 0.25; %0.15; %0.25;  % capital adjustment cost %need to keep it small for solutions, 
               % model cannot solve for solutions if big
else
    p.theta = 0;
end

% - parameters calibrated to SS -
p.alpha = 0.36;
p.beta_H = 0.9921;
p.beta_R = 0.9763;
p.beta_S  = 0.9369;
p.delta = 0.025;
p.sigma = 1;
p.alpha_H = 7.8721e-4;  %GKP   % Household managerial cost
p.alpha_R = 4.5356e-4;  %GKP  % Retail banks managerial cost
p.sigma_R = 1;
p.sigma_S = 1;
p.iota = 0;
p.pi = 7/8;
p.phi = 0.1051;

% - Policy parameters - 
%p.gamma_R = 0.08;  %GAMMA_R
p.omega_K = 0.5;  % risk weights 

% - Uncalibrated - 
p.exoInc = 2.5; %0.15; % either add this exogenous income or make banks linear in net worth.

for tt1 = 1:length(parSpace)

if strcmp(parName,'omegaB') == 1
    p.omega_B = parSpace(tt1);  % risk weights;
else
    p.omega_B = 1;  % risk weights
end
    
if strcmp(parName,'gammaR') == 1
    p.gamma_R = parSpace(tt1);
else
    p.gamma_R = 0.08;
end

p.nu = 0.025;
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

steady = computeSteadyState(p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

scale = 0.25;

K_l = (1-scale) * steady.K;
K_u = (1+scale) * steady.K;

N_H_l = (1-scale)*steady.N_H;
N_H_u = (1+scale)*steady.N_H;

N_R_l = (1-scale)*steady.N_R;
N_R_u = (1+scale)*steady.N_R;

N_S_l = (1-scale)*steady.N_S;
N_S_u = (1+scale)*steady.N_S;  % /////

p.uncondStd = sqrt(p.sigma_Z^2/(1-p.rho^2));
Z_l = -3*p.uncondStd;
Z_u = 3*p.uncondStd;

unit_min = -ones(1,5);
unit_max = ones(1,5);
orig_min = [N_H_l N_R_l N_S_l K_l Z_l];
orig_max = [N_H_u N_R_u N_S_u K_u Z_u];

clear K_l K_u N_H_l N_H_u N_R_l N_R_u N_S_l N_S_u Z_l Z_u

% 1 Smolyak approximation level
approx_level = [2 2 2 2 2];
max_approx_level = max(approx_level);
nDims = 5;

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states,1);
vec_N_H = states(:,1);
vec_N_R = states(:,2);
vec_N_S = states(:,3);
vec_K = states(:,4);
vec_logZ = states(:,5);

n_Z = 9;
[eps_Z,Z_prob] = qnwnorm(n_Z,0,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if useInitialGuess == 0
    vec_K_H_prime = zeros(n_total,1);
    vec_K_R_prime = zeros(n_total,1);
    vec_K_S_prime = zeros(n_total,1); 
    vec_D_prime = zeros(n_total,1);
    vec_B_prime = zeros(n_total,1); 


    vec_Q = ones(n_total,1);
    Pi_Q_prime = zeros(n_total,1);
    
    vec_C_H = vec_N_H - vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
    vec_C_S = vec_N_S - vec_K_S_prime + vec_B_prime; 

    V_N_H = vec_C_H .^(-p.sigma);
    V_N_R = vec_C_R .^(-p.sigma_R);
    V_N_S = vec_C_S .^(-p.sigma_S);  
    V_Q = zeros(n_total,1);
    
else
    load('initialGuess')
    
    V_N_H = vals.V_N_H;
    V_N_R = vals.V_N_R;
    V_N_S = vals.V_N_S; 
    
    vec_Q = vals.vec_Q;
    V_Q = vals.V_Q;
    Pi_Q_prime = vals.Pi_Q_prime;
end    
    
V_N_H_old = V_N_H;
V_N_R_old = V_N_R;
V_N_S_old = V_N_S; 
V_Q_old = V_Q;
vec_Q_old = vec_Q;
Pi_Q_prime_Old = Pi_Q_prime;

funs = struct;

% interpolated values
funs.v_N_H_int = base_poly\V_N_H; 
funs.v_N_R_int = base_poly\V_N_R; 
funs.v_N_S_int = base_poly\V_N_S;  
funs.vec_Q_int = base_poly\vec_Q;  
funs.v_Q_int = base_poly\V_Q;  
funs.Pi_Q_prime_int = base_poly\Pi_Q_prime;

% help
funs.orig_min = orig_min;
funs.orig_max = orig_max;
funs.unit_min = unit_min;
funs.unit_max = unit_max;
funs.approx_level = approx_level;
funs.max_approx_level = max_approx_level;
funs.elem = elem_aniso;
funs.nDims = nDims;
funs.base_poly = base_poly;

clear orig_min orig_max unit_min unit_max approx_level max_approx_level ...
    elem nDims base_poly elem_iso elem_aniso unit_grid

N_H_prime_new = repmat(vec_N_H, 1,n_Z + 1);
N_R_prime_new = repmat(vec_N_R, 1,n_Z + 1);
N_S_prime_new = repmat(vec_N_S, 1,n_Z + 1); 

N_H_prime_old = N_H_prime_new;
N_R_prime_old = N_R_prime_new;
N_S_prime_old = N_S_prime_new;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vError = 10;
nError = 10;
iter = 1;
maxIter =  250;
initialIter = 1;

vec_K_prime = steady.K*ones(n_total,1);
if useInitialGuess == 0
    vec_K_H_prime = ones(n_total,1);
    vec_K_R_prime = ones(n_total,1);
    vec_K_S_prime = ones(n_total,1); 
    vec_R_D_prime = ones(n_total,1);  
    vec_R_B_prime = ones(n_total,1);
    vec_D_prime   = ones(n_total,1);
    vec_B_prime   = ones(n_total,1);
    lambda_H = zeros(n_total,1);
    lambda_R = zeros(n_total,1);
else
    vec_K_H_prime = pols.K_H_prime;
    vec_K_R_prime = pols.K_R_prime;
    vec_K_S_prime = pols.K_S_prime; 
    vec_R_D_prime = pols.R_D_prime;  
    vec_R_B_prime = pols.R_B_prime;
    vec_D_prime   = pols.D_prime;
    vec_B_prime   = pols.B_prime;
    lambda_H = zeros(n_total,1);
    lambda_R = zeros(n_total,1);
end

options = optimset('Display','off','MaxIter',1e3,'MaxFunEvals',1e3);

vec_I   = ones(n_total,1);
EV_D    = zeros(n_total,1);
EV_B    = zeros(n_total,1);

worst_Z = p.rho*min(vec_logZ) + p.sigma_Z*min(eps_Z);

while (vError > 1e-4 || nError > 1e-2) && iter <= maxIter
    tic

    parfor ii = 1:n_total
        
        indK = 1;
        indD = 0;
        indB = 0;
        
%         display(ii)

        logZ_prime = [p.rho*states(ii,5) + p.sigma_Z*eps_Z; worst_Z];
        Z_prob1 = [Z_prob; 0];
        
        K_prime = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) + vec_K_S_prime(ii,1);
        
        % construct the polynomial at the evaluation point      
        outerHandle = @(x_prime,indK,indD,indB) HH_RB_SB_compute_residual(p,x_prime, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),N_S_prime_old(ii,:),indK,indD,indB);
        
        handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

        x0 = [vec_K_H_prime(ii,1);
            vec_K_R_prime(ii,1);
            vec_K_S_prime(ii,1);
            vec_R_D_prime(ii,1);
            vec_R_B_prime(ii,1);
            vec_Q(ii,1)
            vec_D_prime(ii,1)
            vec_B_prime(ii,1)];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the FOC with borrowing constraint
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
        if x_prime_new(1) < 0 && x_prime_new(2) >= 0
            indK = 2;
            
            handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);
            
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            if x_prime_new(2) < 0
                indK = 4;
            
                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) >= 0 && x_prime_new(2) < 0
            indK = 3;
            
            handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);
            
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            if x_prime_new(2) < 0
                indK = 4;
            
                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            end
        elseif x_prime_new(1) < 0 && x_prime_new(2) < 0
            indK = 4;
            
            handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);
            
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
        end
        
        % check if the foc for retail bank forrowing is positive at the
        % constraint. If not, solve the model without the borrowing
        % constraint
        
        foc = outerHandle(x_prime_new,indK,1,1);
        
%         display('With Binding Constraint')
%         display(x_prime_new(9))
%         display(foc(9))
        
        if foc(7) < 0
            % use the previous solution as initial guess 
            x0 = x_prime_new;
            
            indD = 1;
            
            handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Solve the FOC without borrowing constraint
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            
            if x_prime_new(1) < 0 && x_prime_new(2) >= 0
                indK = 2;

                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                if x_prime_new(2) < 0
                    indK = 4;

                    handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                    [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                end
            elseif x_prime_new(1) >= 0 && x_prime_new(2) < 0
                indK = 3;

                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                if x_prime_new(2) < 0
                    indK = 4;

                    handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                    [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                end
            elseif x_prime_new(1) < 0 && x_prime_new(2) < 0
                indK = 4;

                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            end
        end
        
%         foc = outerHandle(x_prime_new,indK,1);
        
%         display('Without Binding Constraint')
%         display(x_prime_new(9))
%         display(foc(9))
        
        % check if the foc for shadow bank forrowing is positive at the
        % constraint. If not, solve the model without the borrowing
        % constraint
        
        foc = outerHandle(x_prime_new,indK,1,1);

        if foc(8) < 0
            indB = 1;
            
            handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Solve the FOC without borrowing constraint
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
            [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            
            if x_prime_new(1) < 0 && x_prime_new(2) >= 0
                indK = 2;

                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                if x_prime_new(2) < 0
                    indK = 4;

                    handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                    [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                end
            elseif x_prime_new(1) >= 0 && x_prime_new(2) < 0
                indK = 3;

                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                if x_prime_new(2) < 0
                    indK = 4;

                    handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                    [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
                end
            elseif x_prime_new(1) < 0 && x_prime_new(2) < 0
                indK = 4;

                handle = @(x_prime) outerHandle(x_prime,indK,indD,indB);

                [x_prime_new,fval,exitflag] = fsolve(handle,x0,options);
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Update the solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_K_S_prime(ii,1) = x_prime_new(3);
        vec_R_D_prime(ii,1) = x_prime_new(4);
        vec_R_B_prime(ii,1) = x_prime_new(5);
        vec_Q(ii,1)         = x_prime_new(6);
        vec_D_prime(ii,1)   = x_prime_new(7);
        vec_B_prime(ii,1)   = x_prime_new(8);
        
        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
            + vec_K_S_prime(ii,1);
        vec_I(ii,1) = vec_K_prime(ii,1) - (1-p.delta)*vec_K(ii,1);

        [N_H_prime_new(ii,:),N_R_prime_new(ii,:),N_S_prime_new(ii,:),...
         ~, ~] = HH_RB_SB_compute_N_prime...
         (p, x_prime_new, states(ii,:), logZ_prime',Z_prob1', funs, ...
         N_H_prime_old(ii,:),N_R_prime_old(ii,:), N_S_prime_old(ii,:));     
        
      %---------------------------------------------------------------------------
        [EV_D(ii,1), EV_B(ii,1)] = compute_EV(p,x_prime_new, ...
            states(ii,:),logZ_prime',Z_prob1',...
            funs, N_H_prime_old(ii,:),N_R_prime_old(ii,:),...
            N_S_prime_old(ii,:));  
      %---------------------------------------------------------------------------
      
    end
    toc
    
%     close(h)
    
    % -- balance sheet constraint of retail bank: --  
    vec_C_H = vec_N_H - vec_Q.*vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_Q.*vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime - vec_B_prime;
    vec_C_S = vec_N_S - vec_Q.*vec_K_S_prime + vec_B_prime; % ////// 

    V_N_H = vec_C_H .^(-p.sigma);
    V_N_R = vec_C_R .^(-p.sigma_R);
    V_N_S = vec_C_S .^(-p.sigma_S);  % /////

    V_Q =  (p.theta*(vec_I./vec_K - p.delta).*vec_I./vec_K - ...
        p.theta/2*(vec_I./vec_K - p.delta).^2);
    
    Pi_Q_prime = (vec_Q-1).*vec_I - p.theta/2*(vec_I./vec_K - p.delta).^2.*vec_K;
    
    vError = norm([V_N_H; V_N_R; V_N_S] - [V_N_H_old; V_N_R_old; V_N_S_old],'inf');
    nError = norm([N_H_prime_new; N_R_prime_new; N_S_prime_new] - ...
        [N_H_prime_old; N_R_prime_old; N_S_prime_old],'inf');
    fprintf('Iteration %d: Value Function Error %6.6f \n',iter,vError)
    fprintf('Iteration %d: net Worth Error %6.6f \n \n',iter,nError)
    if iter == 1
        weight = 1;
    else
        weight = 0.2;
    end
    N_H_prime_old = weight*N_H_prime_new + (1-weight)*N_H_prime_old;
    N_R_prime_old = weight*N_R_prime_new + (1-weight)*N_R_prime_old;
    N_S_prime_old = weight*N_S_prime_new + (1-weight)*N_S_prime_old;  
    
    vec_Q = weight*vec_Q + (1-weight)*vec_Q_old;
    V_Q = weight*V_Q + (1-weight)*V_Q_old;
    Pi_Q_prime = weight*Pi_Q_prime + (1-weight)*Pi_Q_prime_Old;
    
    V_N_H = weight*V_N_H + (1-weight)*V_N_H_old;
    V_N_R = weight*V_N_R + (1-weight)*V_N_R_old;
    V_N_S = weight*V_N_S + (1-weight)*V_N_S_old;
 
    % interpolated values
    funs.v_N_H_int = funs.base_poly\V_N_H; 
    funs.v_N_R_int = funs.base_poly\V_N_R; 
    funs.v_N_S_int = funs.base_poly\V_N_S; 
    funs.vec_Q_int = funs.base_poly\vec_Q; 
    funs.v_Q_int = funs.base_poly\V_Q;  
    funs.Pi_Q_prime_int = funs.base_poly\Pi_Q_prime;

    V_N_H_old = V_N_H;
    V_N_R_old = V_N_R;
    V_N_S_old = V_N_S;
    vec_Q_old = vec_Q;
    V_Q_old = V_Q;
    Pi_Q_prime_Old = Pi_Q_prime;
    iter = iter + 1;
end

%%
% check how the functions look like
% nPlot = 10;
% n_h_grid = linspace(min(vec_N_H),max(vec_N_H),nPlot);
% n_r_grid = linspace(min(vec_N_R),max(vec_N_R),nPlot);
% n_s_grid = linspace(min(vec_N_S),max(vec_N_S),nPlot);
% K_grid = linspace(min(vec_K),max(vec_K),nPlot);
% Z_grid = linspace(min(vec_Z),max(vec_Z),5);
% 
% [n_h_nd,n_r_nd,n_s_nd,k_nd,z_nd] = ndgrid(n_h_grid,n_r_grid,n_s_grid,K_grid,Z_grid);
% 
% states_plot = [vec(n_h_nd) vec(n_r_nd) vec(n_s_nd) vec(k_nd) vec(z_nd)];
% states_plot_resc = Rescale(states_plot,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
% states_plot_poly = Smolyak_Polynomial(states_plot_resc,funs.nDims,funs.max_approx_level,funs.elem);
% 
% V_N_H_plot = states_plot_poly*funs.v_N_H_int;
% V_N_H_plot1= reshape(V_N_H_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% V_N_R_plot = states_plot_poly*funs.v_N_R_int;
% V_N_R_plot1= reshape(V_N_R_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% V_N_S_plot = states_plot_poly*funs.v_N_S_int;
% V_N_S_plot1= reshape(V_N_S_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% Q_plot = states_plot_poly*funs.vec_Q_int;
% Q_plot1= reshape(Q_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% V_Q_plot = states_plot_poly*funs.v_Q_int;
% V_Q_plot1= reshape(Q_plot,nPlot,nPlot,nPlot,nPlot,5);
% 
% hf1 = figure;
% subplot(2,2,1)
% surf(V_N_H_plot1(:,:,5,1,3));
% title('V_N_H')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(2,2,2)
% surf(V_N_R_plot1(:,:,10,10,3));
% title('V_N_R')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(2,2,3)
% surf(V_N_S_plot1(:,:,10,10,3));
% title('V_N_S')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(2,2,4)
% surf(Q_plot1(:,:,10,1,3));
% title('Q')
% xlabel('N_R')
% ylabel('N_H')
% 
% hf2 = figure;
% subplot(2,2,1)
% surf(squeeze(V_N_H_plot1(10,10,:,:,3)));
% title('V_N_H')
% xlabel('K')
% ylabel('N_S')
% 
% subplot(2,2,2)
% surf(squeeze(V_N_R_plot1(10,10,:,:,3)));
% title('V_N_R')
% xlabel('K')
% ylabel('N_S')
% 
% subplot(2,2,3)
% surf(squeeze(V_N_S_plot1(10,10,:,:,3)));
% title('V_N_S')
% xlabel('K')
% ylabel('N_S')
% 
% subplot(2,2,4)
% surf(squeeze(Q_plot1(1,1,:,:,3)));
% title('Q')
% xlabel('K')
% ylabel('N_S')
% 
% figure
% subplot(1,2,1)
% surf(squeeze(V_Q_plot1(:,:,10,10,3)));
% title('V_Q')
% xlabel('N_R')
% ylabel('N_H')
% 
% subplot(1,2,2)
% surf(squeeze(V_Q_plot1(10,10,:,:,3)));
% title('V_Q')
% xlabel('K')
% ylabel('N_S')

%%
pols = struct;
pols.K_H_prime = vec_K_H_prime;
pols.K_R_prime = vec_K_R_prime;
pols.K_S_prime = vec_K_S_prime;
pols.R_D_prime = vec_R_D_prime;
pols.R_B_prime = vec_R_B_prime;
pols.D_prime = vec_D_prime;
pols.B_prime = vec_B_prime;
pols.N_H_prime = N_H_prime_new;
pols.N_R_prime = N_R_prime_new;
pols.N_S_prime = N_S_prime_new;
pols.C_H = vec_C_H;
pols.C_R = vec_C_R;
pols.C_S = vec_C_S;
pols.I = vec_I;
pols.K_prime = vec_K_prime;

vals = struct;
vals.V_N_H = V_N_H;
vals.V_N_R = V_N_R;
vals.V_N_S = V_N_S;
vals.vec_Q = vec_Q;
vals.V_Q = V_Q;
vals.Pi_Q_prime = Pi_Q_prime;

if useInitialGuess == 0 || usePreviousSolution == 1
    save('initialGuess','vals','pols')
end

clear vec_K_H_prime vec_K_R_prime vec_K_S_prime vec_R_D_prime vec_R_B_prime ...
    vec_D_prime vec_B_prime vec_Q N_H_prime_new N_H_prime_old N_R_prime_new ...
    N_R_prime_old N_S_prime_new N_S_prime_old V_N_H V_N_H_old V_N_R V_N_R_old ...
    V_N_S V_N_S_old vec_Q_old V_Q V_Q_old Pi_Q_prime Pi_Q_prime_old ...
    vec_C_H vec_C_R vec_C_S vec_I vec_K_prime

%% Check if borrowing constraints are indeed binding

FOC_D_R_prime = pols.C_R.^(-p.sigma) - p.beta_R.*EV_D;
FOC_B_S_prime = pols.C_S.^(-p.sigma) - p.beta_S.*EV_B;

figure
plot(FOC_D_R_prime);
hold on 
plot(FOC_B_S_prime);
hold off
legend('FOC(D^R'')', 'FOC(B^S'')')
% 
% close all
% fprintf('Borrowing constraints are binding if FOC_D_R_prime and FOC_B_S_prime are positive \n')

save(strcat('./Results/Result_NoRun_',date,'_',parName,'_',num2str(parSpace(tt1)),'.mat'))
end