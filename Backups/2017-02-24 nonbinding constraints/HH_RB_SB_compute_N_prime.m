function [N_H_prime,N_R_prime,N_S_prime,D_prime,B_prime] = HH_RB_SB_compute_N_prime(p, x0, states, ...
    logZ_prime, Z_prob, funs, N_H_prime_old,N_R_prime_old, N_S_prime_old)
% HH_RB_SB_compute_N_prime computes the next period net worth

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);  % /////
R_D_prime = x0(4);
R_B_prime = x0(5);  % /////
D_prime = x0(7);
B_prime = x0(8);

K_prime = (K_H_prime + K_R_prime + K_S_prime);  % /////

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));

% construct the polynomial at the evaluation point
eval_point = [N_H_prime_old' N_R_prime_old' N_S_prime_old' K_prime' logZ_prime'];
eval_point_rescaled =  Rescale(eval_point,funs.unit_min,funs.unit_max,...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
Q_prime = (eval_poly*funs.vec_Q_int)';
Pi_Q_prime = (eval_poly*funs.Pi_Q_prime_int)';

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
W_prime = (1-p.alpha)*exp(logZ_prime).*K_prime.^p.alpha;

% N'
N_H_prime = R_K_prime*K_H_prime + R_D_prime*D_prime + W_prime + Pi_Q_prime;
N_R_prime = R_K_prime*K_R_prime + R_B_prime*B_prime - R_D_prime*D_prime + p.exoInc;  % /////
N_S_prime = R_K_prime*K_S_prime - R_B_prime*B_prime + p.exoInc;  % /////