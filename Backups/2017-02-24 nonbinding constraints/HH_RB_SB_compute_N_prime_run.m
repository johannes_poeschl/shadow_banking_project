function [N_H_prime_no_run,N_R_prime_no_run, ...
    N_H_prime_run,N_R_prime_run, ...
    D_prime] = HH_RB_SB_compute_N_prime_run(p, x0, states, logZ_prime, ...
    Z_prob, funs, funs_run, N_H_prime_old_no_run, N_R_prime_old_no_run, ...
    N_H_prime_old_run, N_R_prime_old_run)
% HH_RB_SB_compute_N_prime_run computes the next period net worth in a run

nZ = length(logZ_prime);

K_H_prime = x0(1);
K_R_prime = x0(2);
R_D_prime = x0(3);
D_prime = x0(5);

K_prime = K_H_prime + K_R_prime;  % /////

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));
W_prime = (1-p.alpha)*exp(logZ_prime).*K_prime.^p.alpha;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Capital Return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% No Run Capital Return
% no run evaluation points and poly
eval_point = [N_H_prime_old_no_run' N_R_prime_old_no_run' ...
    p.exoInc*ones(nZ,1) K_prime' logZ_prime'];
eval_point_rescaled = Rescale(eval_point,funs.unit_min, ...
    funs.unit_max,funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

Q_prime_no_run = (eval_poly*funs.vec_Q_int)'; % /////
Pi_Q_prime_no_run= (eval_poly*funs.Pi_Q_prime_int)';
R_K_prime_no_run = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + ...
    (1 - p.delta)*Q_prime_no_run;

% Run Capital Return
% run evaluation grid and poly
eval_point = [N_H_prime_old_run' N_R_prime_old_run' K_prime' logZ_prime'];
eval_point_rescaled = Rescale(eval_point,funs_run.unit_min, ...
    funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims, ...
    funs_run.max_approx_level,funs_run.elem);

Q_prime_run = (eval_poly*funs_run.vec_Q_int)'; % /////
Pi_Q_prime_run= (eval_poly*funs_run.Pi_Q_prime_int)';
R_K_prime_run = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + ...
    (1 - p.delta)*Q_prime_run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Updated Net Worth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N', No Run
N_H_prime_no_run = R_K_prime_no_run*K_H_prime + R_D_prime*D_prime +  ...
    W_prime + Pi_Q_prime_no_run;
N_R_prime_no_run = R_K_prime_no_run*K_R_prime - R_D_prime*D_prime + p.exoInc;  % /////

% N', Run
N_H_prime_run = R_K_prime_run*K_H_prime + R_D_prime*D_prime +  ...
    W_prime + Pi_Q_prime_run;
N_R_prime_run = R_K_prime_run*K_R_prime - R_D_prime*D_prime + p.exoInc;  % /////