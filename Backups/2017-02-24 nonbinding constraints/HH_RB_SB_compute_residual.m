function foc_residual = HH_RB_SB_compute_residual(p, x0, states, logZ_prime, ...
    Z_prob, funs, N_H_prime_old,N_R_prime_old,N_S_prime_old,indK,indD,indB)
% HH_RB_SB_compute_residual computes the FOC residuals
%
% Inputs:
%   - p             struct array containing parameters
%   - x0            8x1 column vector of unknown choice variables
%   - states        1x4 row vector of state variables
%   - logZ_prime    1xZ row vector of future log productivity realizations
%   - Z_prob        1xZ row vector of probabilities of future productivity
%                   realizations
%   - funs          struct array that contains interpolation coefficients
%                   and other information required for interpolation
%   - N_H_prime_old 1xZ vector of future household net worth realizations
%   - N_R_prime_old 1xZ vector of future retail bank net worth realizations
%   - N_S_prime_old 1xZ vector of future shadow bank net worth realizations
%   - ind           1x1 scalar case indicator
%
% Outputs:
%   - foc_residual  8x1 column vector of FOC residuals

N_H = states(1,1);
N_R = states(1,2);
N_S = states(1,3);  % /////
K   = states(1,4);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);  % /////
R_D_prime = x0(4);
R_B_prime = x0(5);  % /////
Q = x0(6);
D_prime = x0(7);
B_prime = x0(8);

K_prime = (K_H_prime + K_R_prime + K_S_prime);  % /////
I = K_prime - (1-p.delta)*K; 

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));

% construct the polynomial at the evaluation point
eval_point = [N_H_prime_old' N_R_prime_old' N_S_prime_old' K_prime' logZ_prime'];
eval_point_rescaled =  Rescale(eval_point,funs.unit_min,funs.unit_max,...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
Q_prime = (eval_poly*funs.vec_Q_int)';
Pi_Q_prime = (eval_poly*funs.Pi_Q_prime_int)';

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
W_prime = (1-p.alpha)*exp(logZ_prime).*K_prime.^p.alpha;

% balance sheet constraint of shadow bank:
min_R_K_prime = min(R_K_prime);
B_prime_max = (1-p.phi)*min_R_K_prime./R_B_prime.*K_S_prime;  % /////

% balance sheet constraint of retail bank:
D_prime_max = (1 - p.gamma_R*p.omega_K)*Q*K_R_prime + (1-p.gamma_R*p.omega_B)*B_prime;

% N'
N_H_prime = R_K_prime*K_H_prime + R_D_prime*D_prime + W_prime + Pi_Q_prime;
N_R_prime = R_K_prime*K_R_prime + R_B_prime*B_prime - R_D_prime*D_prime + p.exoInc;  % /////
N_S_prime = R_K_prime*K_S_prime - R_B_prime*B_prime + p.exoInc;  % /////

% consumptions
C_H = N_H - Q * K_H_prime - p.alpha_H / 2 * K_H_prime^2 - D_prime;
C_R = N_R - Q * K_R_prime - p.alpha_R / 2 * K_R_prime^2 - B_prime + D_prime;  % /////
C_S = N_S - Q * K_S_prime + B_prime;  % /////

% construct the polynomial at the evaluation point
eval_point = [N_H_prime' N_R_prime' N_S_prime' K_prime' logZ_prime'];
eval_point_rescaled =  Rescale(eval_point,funs.unit_min,funs.unit_max, ...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
v_N_H_1 = (eval_poly*funs.v_N_H_int)';
v_N_R_1 = (eval_poly*funs.v_N_R_int)';
v_N_S_1 = (eval_poly*funs.v_N_S_int)';
V_Q_1 = (eval_poly*funs.v_Q_int)';

EV_K_H = sum(Z_prob.*v_N_H_1.*R_K_prime,2);
EV_D_H = sum(Z_prob.*v_N_H_1*R_D_prime,2);

EV_Q   = sum(Z_prob.*V_Q_1,2);

foc_residual = zeros(8,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if indK == 1 || indK == 3
    foc_residual(1) = C_H^(-p.sigma)*(Q + p.alpha_H*K_H_prime) - ...
        p.beta_H * EV_K_H;
elseif indK == 2 || indK == 4
    foc_residual(1) = K_H_prime;
end

foc_residual(2) = C_H^(-p.sigma) - p.beta_H * EV_D_H;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if indD == 0 && (indK == 1 || indK == 2) % binding constraint
    EV_K_R = sum(Z_prob.*v_N_R_1.*(R_K_prime - (1-p.gamma_R*p.omega_K)*Q*R_D_prime),2);
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = (R_B_prime - (1-p.gamma_R*p.omega_B)*R_D_prime)*EV_N_R;
    
    foc_residual(3) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_K*Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R;
    foc_residual(4) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_B)-p.beta_R * EV_B_R;
    
    foc_residual(7) = D_prime - D_prime_max;
elseif indD == 1 && (indK == 1 || indK == 2) % non-binding constraint
    EV_K_R = sum(Z_prob.*v_N_R_1.*R_K_prime,2);
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = R_B_prime*EV_N_R;
    EV_D_R = R_D_prime*EV_N_R;
    
    foc_residual(3) = C_R^(-p.sigma_R)*(Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R;
    foc_residual(4) = C_R^(-p.sigma_R)*1 - p.beta_R * EV_B_R; 
    
    foc_residual(7) = C_R.^(-p.sigma) - p.beta_R.*EV_D_R;
elseif indD == 0 && (indK == 3 || indK == 4) % binding constraint, no capital holdings
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = (R_B_prime - (1-p.gamma_R*p.omega_B)*R_D_prime)*EV_N_R;
    
    foc_residual(3) = K_R_prime;
    foc_residual(4) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_B)-p.beta_R * EV_B_R;
    
    foc_residual(7) = D_prime - D_prime_max;
elseif indD == 1 && (indK == 3 || indK == 4) % non-binding constraint
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = R_B_prime*EV_N_R;
    EV_D_R = R_D_prime*EV_N_R;
    
    foc_residual(3) = K_R_prime;
    foc_residual(4) = C_R^(-p.sigma_R)*1 - p.beta_R * EV_B_R; 
    
    foc_residual(7) = C_R.^(-p.sigma) - p.beta_R.*EV_D_R;
else
    error('indD must be 0 or 1')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if indB == 0 % binding constraint
    EV_K_S = sum(Z_prob.*v_N_S_1.*(R_K_prime - min_R_K_prime*(1-p.phi)),2);
    
    foc_residual(5) = C_S^(-p.sigma_S)*(Q - (1-p.phi)*min_R_K_prime/R_B_prime) - p.beta_S * EV_K_S;

    foc_residual(8) = B_prime - B_prime_max;
elseif indB == 1 % non-binding constraint
    EV_K_S = sum(Z_prob.*v_N_S_1.*R_K_prime,2);
    EV_B_S = R_B_prime*sum(Z_prob.*v_N_S_1,2);
    
    foc_residual(5) = C_S^(-p.sigma_S)*Q - p.beta_S * EV_K_S;
    
    foc_residual(8) = C_S.^(-p.sigma_S) - p.beta_S.*EV_B_S;
else
    error('indB must be 0 or 1')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Capital Producer FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(6) = (Q - 1 - p.theta*(I/K - p.delta) + p.beta_H*EV_Q);
