% This file computes impulse response functions

%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all

addpath('./Smolyak_Anisotropic_JMMV_2014')

bankRun = 1;

nBurnin = 1000;
nSimulations = nBurnin + 100;  % number of simulated periods 

parName = 'gammaR';
parVals = [0.16 0.16];

dateVal = '19-Dec-2016';
dateVal2 = '19-Dec-2016';
% -- Simulate a sequence of Z: choose between (1) & (2) --

if exist(strcat('Result_NoRun_',dateVal,'_',parName,'_',num2str(parVals(1)),'.mat'),'file') == 2
    load(strcat('Result_NoRun_',dateVal,'_',parName,'_',num2str(parVals(1)),'.mat'));
    load(strcat('Result_Run_',dateVal,'_',parName,'_',num2str(parVals(1)),'.mat'));
else
    load(strcat('Result_NoRun_',dateVal2,'_',parName,'_',num2str(parVals(1)),'.mat'));
    load(strcat('Result_Run_',dateVal2,'_',parName,'_',num2str(parVals(1)),'.mat'));
end

epsZ = zeros(1,nSimulations);
epsZ(1,nBurnin + 30) = -3*p.sigma_Z/sqrt((1-p.rho^2));
seq_Z = zeros(1,nSimulations);

for kk = 1:2
    if exist(strcat('Result_NoRun_',dateVal,'_',parName,'_',num2str(parVals(kk)),'.mat'),'file') == 2
        load(strcat('Result_NoRun_',dateVal,'_',parName,'_',num2str(parVals(kk)),'.mat'));
        load(strcat('Result_Run_',dateVal,'_',parName,'_',num2str(parVals(kk)),'.mat'));
    else
        load(strcat('Result_NoRun_',dateVal2,'_',parName,'_',num2str(parVals(kk)),'.mat'));
        load(strcat('Result_Run_',dateVal2,'_',parName,'_',num2str(parVals(kk)),'.mat'));
    end


%- (1) random sequence of Z -
% seq_Z_transpose = simulateMarkovChain(1,Z_grid,Z_prob,nSimulations);
% seq_Z = seq_Z_transpose';

% - (2) impulse response function -

% -- Interpolate policy function -- 
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

funs.K_H_int = No_Run_Poly\pols.K_H_prime;
funs.K_R_int = No_Run_Poly\pols.K_R_prime;
funs.K_S_int = No_Run_Poly\pols.K_S_prime;
funs.D_int = No_Run_Poly\pols.D_prime;
funs.B_int = No_Run_Poly\pols.B_prime;
funs.R_D_int = No_Run_Poly\pols.R_D_prime;
funs.R_B_int =  No_Run_Poly\pols.R_B_prime;

Run_Grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
Run_Poly = Smolyak_Polynomial(Run_Grid,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

funs_run.K_H_int = Run_Poly\pols_run.K_H_prime;
funs_run.K_R_int = Run_Poly\pols_run.K_R_prime;
funs_run.D_int = Run_Poly\pols_run.D_prime;
funs_run.R_D_int = Run_Poly\pols_run.R_D_prime;

% states, no run
seq_N_H = ones(1, nSimulations)*steady.N_H;  % state
seq_N_R = ones(1, nSimulations)*steady.N_R;  % state
seq_N_S = ones(1, nSimulations)*steady.N_S;  % state 
seq_K   = ones(1, nSimulations)*steady.K;    % state

% states, run
seq_N_H_star = ones(1, nSimulations)*steady.N_H; 
seq_N_R_star = ones(1, nSimulations)*steady.N_R;
seq_N_S_star = p.exoInc*ones(1, nSimulations);

seq_K_H_prime = ones(1, nSimulations);  % choice
seq_K_R_prime = ones(1, nSimulations);  % choice
seq_K_S_prime = ones(1, nSimulations);  % choice 
seq_D_prime   = ones(1, nSimulations);  % choice
seq_B_prime   = ones(1, nSimulations);  % choice 
seq_I         = ones(1, nSimulations);  % choice

seq_R_D_prime = ones(1, nSimulations);  % price 
seq_R_B_prime = ones(1, nSimulations);  % price 
seq_R_K_prime = ones(1, nSimulations);  % price
seq_R_K_prime_star = ones(1, nSimulations);
seq_W_prime   = ones(1, nSimulations);  % price
seq_Q   = ones(1, nSimulations);  % price, no run
seq_Q_old = seq_Q; % price, no run, guess
seq_Q_star = ones(1, nSimulations); % price, run
seq_Q_star_old = ones(1, nSimulations); % price, run, guess

seq_run = zeros(1, nSimulations);

qError = 10;

while qError > 1e-6;
    tt= 1;
    for iii = 1: nSimulations-1
        seq_Z(1,iii+1) = p.rho*seq_Z(1,iii) + epsZ(1,iii+1);
        
        if iii > 1
            seq_run(1,iii) = (1-p.phi)*seq_R_K_prime_star(1,iii-1)*seq_K_S_prime(1,iii-1) < ...
                seq_R_B_prime(1,iii-1)*seq_B_prime(1,iii-1);
%             seq_run(1,iii) = seq_R_K_prime_star(1,iii-1)*seq_K_S_prime(1,iii-1) < ...
%                  seq_R_B_prime(1,iii-1)*seq_B_prime(1,iii-1);
        end
        
        if iii == nBurnin + 30 && bankRun == 1  && kk == 2               
            seq_N_H(1, iii) = seq_N_H_star(1,iii);
            seq_N_R(1 ,iii) = seq_N_R_star(1,iii);
            seq_N_S(1, iii) = seq_N_S_star(1,iii);

            eval_point = [seq_N_H(1,iii) seq_N_R(1,iii) seq_K(1,iii) seq_Z(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
            
            seq_K_H_prime(1, iii) = eval_poly*funs_run.K_H_int;
            seq_K_R_prime(1, iii) = eval_poly*funs_run.K_R_int;
            seq_K_S_prime(1, iii) = 0;              
            seq_R_D_prime(1, iii) = eval_poly*funs_run.R_D_int;
            seq_R_B_prime(1, iii) = steady.R_D;          
            seq_D_prime(1, iii)   = eval_poly*funs_run.D_int;          
            seq_B_prime(1, iii)   = 0;
            seq_Q(1,iii)    = eval_poly*funs_run.vec_Q_int;
            seq_Q_star(1,iii) = seq_Q_star(1,iii);
        else
            eval_point = [seq_N_H(1,iii) seq_N_R(1,iii) seq_N_S(1, iii) seq_K(1,iii) seq_Z(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);           
            
            seq_K_H_prime(1, iii) = eval_poly*funs.K_H_int;
            seq_K_R_prime(1, iii) = eval_poly*funs.K_R_int;
            seq_K_S_prime(1, iii) = eval_poly*funs.K_S_int;              
            seq_R_D_prime(1, iii) = eval_poly*funs.R_D_int;
            seq_R_B_prime(1, iii) = eval_poly*funs.R_B_int;          
            seq_D_prime(1, iii)   = eval_poly*funs.D_int;          
            seq_B_prime(1, iii)   = eval_poly*funs.B_int;
            seq_Q(1,iii)    = eval_poly*funs.vec_Q_int;
            
            eval_point = [seq_N_H_star(1,iii) seq_N_R_star(1,iii) seq_K(1,iii) seq_Z(1,iii)];
            eval_point_rescaled = Rescale(eval_point, ...
                funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
            eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
            
            seq_Q_star(1,iii) = eval_poly*funs_run.vec_Q_int;
        end
        
         seq_K(1, iii+1) = seq_K_H_prime(1, iii) + seq_K_R_prime(1, iii) + seq_K_S_prime(1, iii);  

        seq_I(1,iii)    = seq_K(1,iii+1) - (1-p.delta)*seq_K(1,iii);  

        seq_R_K_prime(1, iii) = p.alpha * exp(seq_Z(1, iii + 1)) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(1, iii + 1);
                      
        seq_R_K_prime_star(1,iii) = p.alpha * exp(seq_Z(1, iii + 1)) * seq_K(1, iii+1)^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(1, iii + 1);

        seq_W_prime(1, iii)  = (1-p.alpha) * exp(seq_Z(1, iii + 1)) * seq_K(1, iii+1)^p.alpha;

        seq_N_H(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_H_prime(1, iii) + ...
                             seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
        seq_N_R(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_R_prime(1, iii) + ...
                  seq_R_B_prime(1,iii)*seq_B_prime(1,iii) ...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;     
        seq_N_S(1, iii+1) =  seq_R_K_prime(1, iii)*seq_K_S_prime(1, iii) -...
                             seq_R_B_prime(1, iii)*seq_B_prime(1, iii) ...
                            + p.exoInc;
        
        seq_N_H_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_H_prime(1, iii) + ...
                 seq_R_D_prime(1, iii)*seq_D_prime(1, iii) + seq_W_prime(1, iii);
             
%         seq_N_R_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_R_prime(1, iii) + ...
%                          seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii)...
%                           - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
%                           + p.exoInc;                 
                      
        seq_N_R_star(1, iii+1) =  seq_R_K_prime_star(1, iii)*seq_K_R_prime(1, iii) + ...
                 (1-p.phi)*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii)...
                  - seq_R_D_prime(1, iii)*seq_D_prime(1, iii) ...
                  + p.exoInc;
              
        seq_N_S_star(1, iii+1) = p.exoInc + p.phi*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii);            
%         seq_N_S_star(1, iii+1) = p.exoInc;   
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);
end

display(100*mean(seq_run(nBurnin+1:end)))

seq_C_H = seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime;
seq_C_R = seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime; 
seq_C_S = seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime; 
seq_Y   = exp(seq_Z) .* seq_K.^p.alpha;

leverage_R = seq_D_prime./(seq_K_R_prime.*seq_Q + seq_B_prime);
leverage_S = seq_B_prime./(seq_K_S_prime.*seq_Q);
grossLeverageBoth = (seq_B_prime + seq_D_prime)./((seq_K_R_prime + seq_K_S_prime).*seq_Q + seq_B_prime);

seq_K_stored(:,:,kk)   = seq_K(1,nBurnin + 1:end-1);
seq_N_H_stored(:,:,kk) = seq_N_H(1,nBurnin + 1:end-1);
seq_N_R_stored(:,:,kk) = seq_N_R(1,nBurnin + 1:end-1);
seq_N_S_stored(:,:,kk) = seq_N_S(1,nBurnin + 1:end-1);

seq_K_H_prime_stored(:,:,kk) = seq_K_H_prime(1,nBurnin + 1:end-1);
seq_K_R_prime_stored(:,:,kk) = seq_K_R_prime(1,nBurnin + 1:end-1);
seq_K_S_prime_stored(:,:,kk) = seq_K_S_prime(1,nBurnin + 1:end-1);

seq_D_prime_stored(:,:,kk)   = seq_D_prime(1,nBurnin + 1:end-1);
seq_B_prime_stored(:,:,kk)   = seq_B_prime(1,nBurnin + 1:end-1);

seq_R_D_prime_stored(:,:,kk) = seq_R_D_prime(1,nBurnin + 1:end-1);
seq_R_B_prime_stored(:,:,kk) = seq_R_B_prime(1,nBurnin + 1:end-1);
seq_R_K_prime_stored(:,:,kk) = seq_R_K_prime(1,nBurnin + 1:end-1);

seq_C_H_stored(:,:,kk) = seq_C_H(1,nBurnin + 1:end-1);
seq_C_R_stored(:,:,kk) = seq_C_R(1,nBurnin + 1:end-1);
seq_C_S_stored(:,:,kk) = seq_C_S(1,nBurnin + 1:end-1);
seq_Y_stored(:,:,kk)   = seq_Y(1,nBurnin + 1:end-1);
seq_Q_stored(:,:,kk)   = seq_Q(nBurnin+1:end-1);
seq_Q_star_stored(:,:,kk) = seq_Q_star(nBurnin+1:end-1);

leverage_R_stored(:,:,kk) = leverage_R(nBurnin+1:end-1);
leverage_S_stored(:,:,kk) = leverage_S(nBurnin+1:end-1);
grossLeverageBoth_stored(:,:,kk) = grossLeverageBoth(nBurnin+1:end-1);

end

%%
% -- Simulation plots --

time = -29:69;

vals = zeros(4,size(time,2),2,5);
names = cell(4,1,5);

    for kk = 1:2
        vals(:,:,kk,1) = [seq_K_H_prime_stored(:,:,kk); seq_K_R_prime_stored(:,:,kk); seq_K_S_prime_stored(:,:,kk); seq_K_stored(:,:,kk)];
        names(:,:,1) = {'K^H','K^R','K^S','K'}';

        vals(:,:,kk,2) = [seq_N_H_stored(:,:,kk); seq_N_R_stored(:,:,kk); seq_N_S_stored(:,:,kk); seq_N_H_stored(:,:,kk) + seq_N_R_stored(:,:,kk) + seq_N_S_stored(:,:,kk)];
        names(:,:,2) = {'N^H','N^R','N^S','N'}';

        vals(:,:,kk,3) = [seq_C_H_stored(:,:,kk); seq_C_R_stored(:,:,kk); seq_C_S_stored(:,:,kk); seq_C_H_stored(:,:,kk) + seq_C_R_stored(:,:,kk) + seq_C_S_stored(:,:,kk)];
        names(:,:,3) = {'C^H','C^R','C^S','C'}';

        vals(:,:,kk,4) = [seq_R_D_prime_stored(:,:,kk); seq_R_B_prime_stored(:,:,kk); seq_Q_stored(:,:,kk); seq_Q_star_stored(:,:,kk)];
        names(:,:,4) = {'R^D','R^B','Q','Q*'}';
        
        vals(:,:,kk,5) = [seq_D_prime_stored(:,:,kk); seq_B_prime_stored(:,:,kk); seq_Y_stored(:,:,kk); seq_R_K_prime_stored(:,:,kk)];
        names(:,:,5) = {'D','B','Y','R_K'}';
    end


for jj = 1:5
    figure
    for ii = 1:size(vals,1)
        subplot(2,2,ii);
        plot (time,100*(vals(ii,:,1,jj) - vals(ii,1,1,jj))./(0.01 + vals(ii,1,1,jj)),'b','LineWidth',2);
        grid on
        hold on
        plot (time,100*(vals(ii,:,2,jj) - vals(ii,1,2,jj))./(0.01 + vals(ii,1,2,jj)),'r:','LineWidth',2);
        xlabel('Time (Quarters)');
        ylabel('% Dev from SS');
        title(names{ii,1,jj});
        legend('No Bank Run','Bank Run')
    end
    saveas(gcf,strcat('./Figures/IRF_BankRun_',num2str(jj),'b.fig'))
end


%% figure

time = -29:69;

vals = zeros(12,size(time,2),2);
names = cell(12,1);

for kk = 1:2
    vals(:,:,kk) = [exp(seq_Z(nBurnin+1:end-1)); seq_K_stored(:,:,kk); seq_Y_stored(:,:,kk); seq_Q_stored(:,:,kk); ...
          seq_K_H_prime_stored(:,:,kk); seq_K_R_prime_stored(:,:,kk); seq_K_S_prime_stored(:,:,kk); seq_D_prime_stored(:,:,kk); ...
          seq_B_prime_stored(:,:,kk); seq_C_H_stored(:,:,kk); seq_C_R_stored(:,:,kk); seq_C_S_stored(:,:,kk); ...
        ];

    names(:,:) = {'log(Z)','K','Y','Q','K^H','K^R','K^S','D','B','C^H','C^R','C^S',}';
end

jj = 1;

figure
for ii = 1:size(vals,1)
    subplot(4,4,ii);
    line1 = plot (time,100*(vals(ii,:,1,jj) - vals(ii,1,1,jj))./(0.01 + vals(ii,1,1,jj)),'b','LineWidth',2);    hold on
    if ii > 1
        line2 = plot (time,100*(vals(ii,:,2,jj) - vals(ii,1,2,jj))./(0.01 + vals(ii,1,2,jj)),'r:','LineWidth',2);
    end
    xlabel('Time (Quarters)');
    ylabel('% Dev from SS');
    title(names{ii,1,jj});
    xlim([time(1) time(end)])
    if ii == 4
        
    end
end
newUnits = 'normalized';
newPosition = [0.4 0.1 0.2 0.2];
hl = legend([line1,line2],{'No Bank Run','Bank Run'});
set(hl,'Position',newPosition,'FontSize',15,'Orientation','horizontal')

figureName = './Figures/IRF_BankRun_slides';
saveas(gcf,strcat(figureName,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [29.7 21];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')