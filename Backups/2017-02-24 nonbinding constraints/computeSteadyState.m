function steady = computeSteadyState(p)

R_D_SS = 1/p.beta_H;
R_B_SS = p.gamma_R * p.omega_B / p.beta_R + R_D_SS * (1-p.gamma_R * p.omega_B);
R_K_SS = 1 / (p.beta_S * p.phi + 1/R_B_SS*(1 - p.phi));
r_K_SS = R_K_SS - 1 + p.delta;
K_H_SS = 1/ p.alpha_H * (p.beta_H * R_K_SS - 1);
K_R_SS = 1/ p.alpha_R * (p.beta_R *(R_K_SS-R_D_SS*(1-p.gamma_R*p.omega_K))-p.gamma_R*p.omega_K);
K_SS = (r_K_SS/p.alpha)^(1/(p.alpha-1));
K_S_SS = K_SS - K_H_SS - K_R_SS;
B_SS = (1-p.phi) * R_K_SS * K_S_SS / R_B_SS;
D_SS = K_R_SS + B_SS - p.gamma_R * (p.omega_K*K_R_SS + p.omega_B*B_SS);
W_SS = (1-p.alpha)*K_SS^p.alpha;
N_H_SS = R_D_SS*D_SS + R_K_SS * K_H_SS + W_SS;
N_R_SS = R_K_SS*K_R_SS + R_B_SS*B_SS - R_D_SS*D_SS + p.exoInc;
N_S_SS = R_K_SS*K_S_SS - R_B_SS*B_SS + p.exoInc;

% collect all steady state values in a struct array
steady      = struct;
steady.R_D  = R_D_SS;
steady.R_B  = R_B_SS;
steady.R_K  = R_K_SS;
steady.r_K  = r_K_SS;
steady.K_H  = K_H_SS;
steady.K_R  = K_R_SS;
steady.K    = K_SS;
steady.K_S  = K_S_SS;
steady.B    = B_SS;
steady.D    = D_SS;
steady.W    = W_SS;
steady.N_H  = N_H_SS;
steady.N_R  = N_R_SS;
steady.N_S  = N_S_SS;