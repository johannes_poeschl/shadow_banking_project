function TS = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady)
% simulateModel simulates model time series
%
% Inputs:
%   - p:        struct array containing the parameters
%   - shocks:   struct array containing the shocks
%   - funs:     struct array of interpolation accessories
%   - pols:     struct array for the policy functions
%   - steady:   struct array containing the steady state
%
% Outputs:
%   - TS:       time series structure of the endogenous states

[nPanels,nSimulations] = size(shocks.logZ);

seq_logZ = shocks.logZ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - no run case -
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

funs.K_H_int = No_Run_Poly\pols.K_H_prime;
funs.K_R_int = No_Run_Poly\pols.K_R_prime;
funs.K_S_int = No_Run_Poly\pols.K_S_prime;
funs.D_int   = No_Run_Poly\pols.D_prime;
funs.B_int   = No_Run_Poly\pols.B_prime;
funs.R_D_int = No_Run_Poly\pols.R_D_prime;
funs.R_B_int = No_Run_Poly\pols.R_B_prime;

% - run case -

Run_Grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
Run_Poly = Smolyak_Polynomial(Run_Grid,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

funs_run.K_H_int = Run_Poly\pols_run.K_H_prime;
funs_run.K_R_int = Run_Poly\pols_run.K_R_prime;
funs_run.D_int   = Run_Poly\pols_run.D_prime;
funs_run.R_D_int = Run_Poly\pols_run.R_D_prime;

% - states, no run -
seq_N_H = ones(nPanels, nSimulations)*steady.N_H;  % state
seq_N_R = ones(nPanels, nSimulations)*steady.N_R;  % state
seq_N_S = ones(nPanels, nSimulations)*steady.N_S;  % state 
seq_K   = ones(nPanels, nSimulations)*steady.K;    % state

% - states, run -
seq_N_H_star = ones(nPanels, nSimulations)*steady.N_H;    % state
seq_N_R_star = ones(nPanels, nSimulations)*steady.N_R;    % state
seq_N_S_star = ones(nPanels, nSimulations)*p.exoInc;  % state

seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice 
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_I         = ones(nPanels, nSimulations);  % choice

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_old      = ones(nPanels, nSimulations);  % price, no run, guess
seq_Q_star     = ones(nPanels, nSimulations);  % price, run
seq_Q_star_old = ones(nPanels, nSimulations);  % price, run, guess

seq_R_K_prime_star = ones(nPanels, nSimulations);

seq_run           = zeros(nPanels, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_happened  = zeros(nPanels, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(nPanels, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(nPanels, nSimulations);   %%%%% whether a run starts in period iii
seq_recovery = zeros(nPanels, nSimulations);

qError = 10;

while qError > 1e-6;
    
    for iii = 1: nSimulations-1
                      
        if iii > 1
            seq_run(:,iii) = (1-p.phi)*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1) - ...
                seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1) < 0;
            
            seq_recovery(:,iii) = (1-p.phi)*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1)./(seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1));
            seq_recovery(seq_B_prime(:,iii-1) == 0,iii) = 1;
        end

        A = seq_run(:,iii).*(shocks.nu(:,iii) < norminv(p.nu));  % bankrun condition is met and coordination fails
        B = seq_run_persist(:, iii) == 1; % bankrun continues

        seq_run_happened(:,iii) =  1 - (1-A).*(1-B); %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
        seq_run_triggered(:,iii) = A.*(1-B);
        seq_run_persist(:, iii+1) = seq_run_happened(:,iii).*(shocks.pi(:,iii) < norminv(p.pi)); %%%%% whether bankrun persists to the next period

        % bank run stuff
        seq_N_H(:, iii) = seq_N_H_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_H(:,iii).*(1-seq_run_happened(:,iii));
        seq_N_R(: ,iii) = seq_N_R_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_R(:,iii).*(1-seq_run_happened(:,iii));
        seq_N_S(:, iii) = seq_N_S_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_S(:,iii).*(1-seq_run_happened(:,iii));

        eval_point = [seq_N_H(:,iii) seq_N_R(:,iii) seq_K(:,iii) seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

        seq_K_H_prime_run = eval_poly*funs_run.K_H_int;
        seq_K_R_prime_run = eval_poly*funs_run.K_R_int;
        seq_K_S_prime_run = 0;              
        seq_R_D_prime_run = eval_poly*funs_run.R_D_int;
        seq_R_B_prime_run = steady.R_B;          
        seq_D_prime_run   = eval_poly*funs_run.D_int;          
        seq_B_prime_run   = 0;
        seq_Q_run         = eval_poly*funs_run.vec_Q_int;

        % no bank run stuff
        eval_point = [seq_N_H(:,iii) seq_N_R(:,iii) seq_N_S(:, iii) seq_K(:,iii)  seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);           

        seq_K_H_prime_no_run = eval_poly*funs.K_H_int;
        seq_K_R_prime_no_run = eval_poly*funs.K_R_int;
        seq_K_S_prime_no_run = eval_poly*funs.K_S_int;              
        seq_R_D_prime_no_run = eval_poly*funs.R_D_int;
        seq_R_B_prime_no_run = eval_poly*funs.R_B_int;          
        seq_D_prime_no_run   = eval_poly*funs.D_int;          
        seq_B_prime_no_run   = eval_poly*funs.B_int;
        seq_Q_no_run         = eval_poly*funs.vec_Q_int;

        seq_K_H_prime(:, iii) = seq_K_H_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_H_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_R_prime(:, iii) = seq_K_R_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_R_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_S_prime(:, iii) = seq_K_S_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_S_prime_no_run.*(1-seq_run_happened(:,iii));              
        seq_R_D_prime(:, iii) = seq_R_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_D_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_R_B_prime(:, iii) = seq_R_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_B_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_D_prime(:, iii)   = seq_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_D_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_B_prime(:, iii)   = seq_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_B_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_Q(:, iii)         = seq_Q_run.*seq_run_happened(:,iii) + ...
            seq_Q_no_run.*(1-seq_run_happened(:,iii));

        eval_point = [seq_N_H_star(:,iii) seq_N_R_star(:,iii) seq_K(:,iii)  seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

        seq_Q_star(:,iii) = eval_poly*funs_run.vec_Q_int;
        
        seq_K(:, iii+1) = seq_K_H_prime(:, iii) + seq_K_R_prime(:, iii) + seq_K_S_prime(:, iii);  

        seq_I(:,iii)    = seq_K(:,iii+1) - (1-p.delta)*seq_K(:,iii);  

        seq_R_K_prime(:, iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(:, iii + 1);
                      
        seq_R_K_prime_star(:,iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(:, iii + 1);

        seq_W_prime(:, iii)  = (1-p.alpha) * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^p.alpha;

        seq_N_H(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_H_prime(:, iii) + ...
                             seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
                         
        seq_N_R(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_R_prime(:, iii) + ...
                  seq_R_B_prime(:,iii).*seq_B_prime(:,iii) ...
                  - seq_R_D_prime(:, iii).*seq_D_prime(:, iii) ...
                  + p.exoInc;     
              
        seq_N_S(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_S_prime(:, iii) -...
                             seq_R_B_prime(:, iii).*seq_B_prime(:, iii) ...
                            + p.exoInc;
        
        seq_N_H_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_H_prime(:, iii) + ...
                 seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
             
        seq_N_R_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_R_prime(:, iii) + ...
                         (1-p.phi)*seq_R_K_prime_star(:, iii).*seq_K_S_prime(:, iii)...
                          - seq_R_D_prime(:, iii).*seq_D_prime(:, iii) ...
                          + p.exoInc;
                      
        %seq_N_S_star(1, iii+1) = p.exoInc + p.phi*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii);            
        seq_N_S_star(:, iii+1) = p.exoInc*ones(nPanels,1);   
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TS = struct;
TS.N_H = seq_N_H;
TS.N_R = seq_N_R;
TS.N_S = seq_N_S;
TS.K = seq_K;
TS.run = seq_run_happened;