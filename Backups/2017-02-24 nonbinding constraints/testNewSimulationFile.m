% set simulation parameters
nBurnin = 200; 
nSimulations = nBurnin + 800;  % number of simulated periods 
nPanels = 100; %%%%% number of simulations for each capital requirement level

% draw shocks
seq_logZ = zeros(nPanels,nSimulations);
n_Z = 25;
[Z_grid,Z_prob] = tauchen(n_Z,0,p.rho,p.sigma_Z,4);

for jj = 1:nPanels  %%%%% for each simulation 
    % --  random sequence of Z --
    seq_logZ_transpose = simulateMarkovChain(Z_grid(13),Z_grid,Z_prob,nSimulations);
    seq_logZ(jj,:) = seq_logZ_transpose';
end

shocks.logZ = seq_logZ;
shocks.nu = normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
shocks.pi = normrnd(0,1,nPanels,nSimulations);

TS = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

