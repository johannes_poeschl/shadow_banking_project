% This file computes the bank run solution in a loop.
clear

usePreviousSolution_Run = 1;
dateVal = '28-Feb-2017';

par_name = 'gammaR';

if strcmp(par_name,'omegaB') == 1
    parSpace_run = 1;
elseif strcmp(par_name,'gammaR') == 1
    parSpace_run = 0.08:0.02:0.2;
elseif strcmp(par_name,'baseline') == 1
    parSpace_run = 1;
end

for tt2 = 1:length(parSpace_run);
    
if strcmp(par_name,'omegaB') == 1
    p.omega_B = parSpace_run(tt2);  % risk weights;
end
    
if strcmp(par_name,'gammaR') == 1
    p.gamma_R = parSpace_run(tt2);
end 

load(strcat('./Results/Result_NoRun_',dateVal,'_',par_name,'_',num2str(parSpace_run(tt2)),'.mat'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unit_min = funs.unit_min([1 2 4 5]);
unit_max = funs.unit_max([1 2 4 5]);
orig_min = funs.orig_min([1 2 4 5]);
orig_max = funs.orig_max([1 2 4 5]);

% 1 Smolyak approximation level
approx_level = [2 2 2 2];
max_approx_level = max(approx_level);
nDims = 4;

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states,1);
vec_N_H = states(:,1);
vec_N_R = states(:,2);
vec_K = states(:,3);
vec_Z = states(:,4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess for the run solution (we use the no run solution)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% first, compute the smolyak polynomials on the no run grid
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);
    
Run_Grid_Eval = [vec_N_H vec_N_R p.exoInc*ones(n_total,1) vec_K vec_Z];
Run_Grid_Eval = Rescale(Run_Grid_Eval,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
Run_Poly_Eval = Smolyak_Polynomial(Run_Grid_Eval,funs.nDims,funs.max_approx_level,funs.elem);

alfa = No_Run_Poly\pols.K_H_prime;
K_H_prime_run = Run_Poly_Eval*alfa;
    
alfa = No_Run_Poly\pols.K_R_prime;
K_R_prime_run = Run_Poly_Eval*alfa;
    
alfa = No_Run_Poly\pols.R_D_prime;
R_D_prime_run = Run_Poly_Eval*alfa;
    
alfa = No_Run_Poly\pols.D_prime;
D_prime_run = Run_Poly_Eval*alfa;

if usePreviousSolution_Run == 0 || tt2 == 1
    V_N_H_run       = Run_Poly_Eval*funs.v_N_H_int;
    V_N_R_run       = Run_Poly_Eval*funs.v_N_R_int;
    Q_run           = Run_Poly_Eval*funs.vec_Q_int;
    Pi_Q_run        = Run_Poly_Eval*funs.Pi_Q_prime_int;
    
    vec_K_H_prime   = ones(n_total,1);
    vec_K_R_prime   = ones(n_total,1);
    vec_D_prime     = ones(n_total,1);
    vec_R_D_prime   = ones(n_total,1); 
    
    N_H_prime_new_no_run = repmat(vec_N_H,1,grids.n_Z);
    N_R_prime_new_no_run = repmat(vec_N_R,1,grids.n_Z);
    N_H_prime_new_run    = repmat(vec_N_H,1,grids.n_Z);
    N_R_prime_new_run    = repmat(vec_N_R,1,grids.n_Z);
else
    load('initialGuess_Run.mat')
    V_N_H_run       = vals_run.V_N_H;
    V_N_R_run       = vals_run.V_N_R;
    Q_run           = vals_run.Q;
    Pi_Q_run        = vals_run.Pi_Q_run;
    
    vec_K_H_prime   = pols_run.K_H_prime;
    vec_K_R_prime   = pols_run.K_R_prime;
    vec_D_prime     = pols_run.D_prime;
    vec_R_D_prime   = pols_run.R_D_prime; 
    
    N_H_prime_new_no_run = pols_run.N_H_prime;
    N_R_prime_new_no_run = pols_run.N_R_prime;
    N_H_prime_new_run    = pols_run.N_H_prime_star;
    N_R_prime_new_run    = pols_run.N_R_prime_star;
end

V_N_H_old       = V_N_H_run;
V_N_R_old       = V_N_R_run;
Q_old           = Q_run;
Pi_Q_old        = Pi_Q_run;

funs_run = struct;
funs_run.v_N_H_int = base_poly\V_N_H_run; 
funs_run.v_N_R_int = base_poly\V_N_R_run;
funs_run.vec_Q_int = base_poly\Q_run;  
funs_run.Pi_Q_prime_int = base_poly\Pi_Q_run;

% help
funs_run.orig_min = orig_min;
funs_run.orig_max = orig_max;
funs_run.unit_min = unit_min;
funs_run.unit_max = unit_max;
funs_run.approx_level = approx_level;
funs_run.max_approx_level = max_approx_level;
funs_run.elem = elem_aniso;
funs_run.nDims = nDims;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_H_prime_old_no_run = N_H_prime_new_no_run;
N_R_prime_old_no_run = N_R_prime_new_no_run;
N_H_prime_old_run = N_H_prime_new_run;
N_R_prime_old_run = N_R_prime_new_run;

vec_Q = Q_run;
vec_I = ones(n_total,1);
vec_K_prime = ones(n_total,1);

options = optimset('Display','off', ...
    'FinDiffType','forward', ...
    'TolFun',1e-6);

vError = 10;
nError = 10;
iter = 1;
maxIter = 250;
    
while (vError > 1e-4 || nError > 1e-2) && iter <= maxIter;
tic2 = tic;
    for ii = 1:n_total
        
        logZ_prime = p.rho*states(ii,4) + p.sigma_Z*grids.eps_Z; % no adjustment necessary, since no shadow banks

        indK = 1;
        indD = 0;

        outerHandle = @(x_prime,indK,indD) HH_RB_SB_compute_residual_run(p,x_prime, ...
            states(ii,:),logZ_prime',grids.Z_prob',...
            funs, funs_run, ...
            N_H_prime_old_no_run(ii,:),N_R_prime_old_no_run(ii,:), ...
            N_H_prime_old_run(ii,:),N_R_prime_old_run(ii,:),indK,indD);
        
        handle = @(x_prime) outerHandle(x_prime,indK,indD);

        x0 = [vec_K_H_prime(ii,1);
            vec_K_R_prime(ii,1);
            vec_R_D_prime(ii,1);
            vec_Q(ii,1);
            vec_D_prime(ii,1)];

            [x_prime_new,fval,exitFlag1] = fsolve(handle,x0,options);
        
            if exitFlag1 == 0  || x_prime_new(2) < 0
                x0 = [steady.K_H steady.K_R 1 1 steady.D]';
                [x_prime_new,fval,exitFlag1] = fsolve(handle,x0,options);
            end

            if exitFlag1 == 0  || x_prime_new(2) < 0
                x0 = [1 1 1 1 1]';
                [x_prime_new,fval,exitFlag1] = fsolve(handle,x0,options);
            end
%             display(states(ii,:))
%             display(ii)
%             display(exitFlag1)
%             display(x_prime_new)

            if x_prime_new(1) < 0
                indK = 2;

                handle = @(x_prime) outerHandle(x_prime,indK,indD);

                [x_prime_new,fval,exitFlag1a] = fsolve(handle,x0,options);
            end
            
            x_prime_new = max(x_prime_new,0);

        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_R_D_prime(ii,1) = x_prime_new(3);
        vec_Q(ii,1) = x_prime_new(4);
        vec_D_prime(ii,1) = x_prime_new(5);

        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1);
        vec_I(ii,1) = vec_K_prime(ii,1) - (1-p.delta)*vec_K(ii,1);

        [N_H_prime_new_no_run(ii,:),N_R_prime_new_no_run(ii,:), ...
         N_H_prime_new_run(ii,:),N_R_prime_new_run(ii,:)] = ...
         HH_RB_SB_compute_N_prime_run(p,x_prime_new, ...
            logZ_prime',...
            funs, funs_run, ...
            N_H_prime_old_no_run(ii,:),N_R_prime_old_no_run(ii,:), ...
            N_H_prime_old_run(ii,:),N_R_prime_old_run(ii,:));
    end
    toc(tic2)

    vec_C_H = vec_N_H - vec_Q.*vec_K_H_prime - p.alpha_H/2 * vec_K_H_prime.^2 - vec_D_prime;
    vec_C_R = vec_N_R - vec_Q.*vec_K_R_prime - p.alpha_R/2 * vec_K_R_prime.^2 + vec_D_prime;

    V_N_H_run = vec_C_H .^(-p.sigma);
    V_N_R_run = vec_C_R .^(-p.sigma_R);
    Pi_Q_run = (vec_Q-1).*vec_I - p.theta/2*(vec_I./vec_K - p.delta).^2.*vec_K;
    
    vError = norm([V_N_H_run; V_N_R_run] - [V_N_H_old; V_N_R_old],'inf');
    
    nError = norm([N_H_prime_new_no_run; N_R_prime_new_no_run; ...
        N_H_prime_new_run; N_R_prime_new_run] - ...
        [N_H_prime_old_no_run; N_R_prime_old_no_run; ...
        N_H_prime_old_run; N_R_prime_old_run],'inf');
    
    fprintf('Iteration %d: Value Function Error %6.6f \n',iter,vError)
    fprintf('Iteration %d: net Worth Error %6.6f \n \n',iter,nError)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Updating Step
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    weight = 1;

    V_N_H_old       = weight*V_N_H_run + (1-weight)*V_N_H_old;
    V_N_R_old       = weight*V_N_R_run + (1-weight)*V_N_R_old;

    weight = 0.25;
    
    Q_old           = weight*Q_run + (1-weight)*Q_old;
    N_H_prime_old_no_run = weight*N_H_prime_new_no_run + (1-weight)*N_H_prime_old_no_run;
    N_R_prime_old_no_run = weight*N_R_prime_new_no_run + (1-weight)*N_R_prime_old_no_run;
    N_H_prime_old_run = weight*N_H_prime_new_run + (1-weight)*N_H_prime_old_run;
    N_R_prime_old_run = weight*N_R_prime_new_run + (1-weight)*N_R_prime_old_run;
    
    funs_run.v_N_H_int = base_poly\V_N_H_run;  
    funs_run.v_N_R_int = base_poly\V_N_R_run; 
    funs_run.vec_Q_int = base_poly\Q_run;  
    funs_run.Pi_Q_prime_int = base_poly\Pi_Q_run;

    iter = iter + 1;
end

pols_run = struct;
pols_run.K_H_prime = vec_K_H_prime;
pols_run.K_R_prime = vec_K_R_prime;
pols_run.R_D_prime = vec_R_D_prime;
pols_run.D_prime = vec_D_prime;
pols_run.N_R_prime = N_R_prime_new_no_run;
pols_run.N_R_prime_star  = N_R_prime_new_run;
pols_run.N_H_prime = N_H_prime_new_no_run;
pols_run.N_H_prime_star  = N_H_prime_new_run;

vals_run = struct;
vals_run.V_N_H       = V_N_H_run;
vals_run.V_N_R       = V_N_R_run;
vals_run.Q           = Q_run;
vals_run.Pi_Q_run    = Pi_Q_run;

save(strcat('./Results/Result_Run_',date,'_',par_name,'_',num2str(parSpace_run(tt2)),'.mat'), ...
    'p','vals_run','pols_run','funs_run','grids','steady')
save('initialGuess_Run','vals_run','pols_run')
end
