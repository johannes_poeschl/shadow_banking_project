function [N_H_prime,N_R_prime,N_S_prime,R_K_prime,D_prime_max,B_prime_max] = ...
    HH_RB_SB_compute_N_prime(p, x0, ...
    logZ_prime, funs, N_H_prime_old,N_R_prime_old, N_S_prime_old)
% HH_RB_SB_compute_N_prime computes the next period net worth

n_Z = length(logZ_prime);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);  % /////
R_D_prime = x0(4);
R_B_prime = x0(5);  % /////
Q = x0(6);
D_prime = x0(7);
B_prime = x0(8);

K_prime = (K_H_prime + K_R_prime + K_S_prime);  % /////

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));

% construct the polynomial at the evaluation point
eval_point = [N_H_prime_old' N_R_prime_old' N_S_prime_old' K_prime' logZ_prime'];

% adjust the evaluation points to lie within the grid
eval_point_adj = [bound(N_H_prime_old',funs.orig_min(1),funs.orig_max(1)) ...
    bound(N_R_prime_old',funs.orig_min(2),funs.orig_max(2)) ...
    bound(N_S_prime_old',funs.orig_min(3),funs.orig_max(3)) ...
    bound(K_prime',funs.orig_min(4),funs.orig_max(4)) ...
    logZ_prime'];

eval_point_rescaled =  Rescale(eval_point_adj,funs.unit_min,funs.unit_max,...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
Q_prime = (eval_poly*funs.vec_Q_int);
Pi_Q_prime = (eval_poly*funs.Pi_Q_prime_int);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(eval_point - eval_point_adj))) > 0
    df = 1e-2;
    
    aux = eye(4);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(eval_point_adj,4,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_minus =  Rescale(eval_point_plus,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_minus,funs.nDims, ...
        funs.max_approx_level,funs.elem);

    Q_prime_plus = (eval_poly_plus*funs.vec_Q_int);
    Pi_Q_prime_plus = (eval_poly_plus*funs.Pi_Q_prime_int);

    Q_prime_plus = reshape(Q_prime_plus,n_Z,4);
    Pi_Q_prime_plus = reshape(Pi_Q_prime_plus,n_Z,4);

    Q_prime_diff = [(Q_prime_plus - repmat(Q_prime,1,4))./df zeros(n_Z,1)];
    Pi_Q_prime_diff = [(Pi_Q_prime_plus - repmat(Pi_Q_prime,1,4))./df zeros(n_Z,1)];

    Q_prime = Q_prime + sum(Q_prime_diff.*(eval_point - eval_point_adj),2);
    Pi_Q_prime = Pi_Q_prime + sum(Pi_Q_prime_diff.*(eval_point - eval_point_adj),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Q_prime = Q_prime';
Pi_Q_prime = Pi_Q_prime';

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
W_prime = (1-p.alpha)*exp(logZ_prime).*K_prime.^p.alpha;

% N'
N_H_prime = R_K_prime*K_H_prime + R_D_prime*D_prime + W_prime + Pi_Q_prime;
N_R_prime = R_K_prime*K_R_prime + R_B_prime*B_prime - R_D_prime*D_prime + p.exoInc;  % /////
N_S_prime = R_K_prime*K_S_prime - R_B_prime*B_prime + p.exoInc;  % /////

% balance sheet constraint of shadow bank:
min_R_K_prime = min(R_K_prime);
B_prime_max = (1-p.phi)*min_R_K_prime./R_B_prime.*K_S_prime;  % /////

% balance sheet constraint of retail bank:
D_prime_max = (1 - p.gamma_R*p.omega_K)*Q*K_R_prime + (1-p.gamma_R*p.omega_B)*B_prime;
