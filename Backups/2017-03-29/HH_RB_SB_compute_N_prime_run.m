function [N_H_prime_no_run,N_R_prime_no_run, ...
    N_H_prime_run,N_R_prime_run, ...
    R_K_prime_no_run,R_K_prime_run, ...
    D_prime_max] = HH_RB_SB_compute_N_prime_run(p, x0, logZ_prime, ...
    funs, funs_run, ...
    N_H_prime_old_no_run, N_R_prime_old_no_run, ...
    N_H_prime_old_run, N_R_prime_old_run)
% HH_RB_SB_compute_N_prime_run computes the next period net worth in a run

n_Z = length(logZ_prime);

K_H_prime = x0(1);
K_R_prime = x0(2);
R_D_prime = x0(3);
Q = x0(4);
D_prime = x0(5);

K_prime = K_H_prime + K_R_prime;  % /////

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));
W_prime = (1-p.alpha)*exp(logZ_prime).*K_prime.^p.alpha;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Capital Return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% No Run Capital Return
% no run evaluation points and poly
eval_point = [N_H_prime_old_no_run' N_R_prime_old_no_run' ...
    p.exoInc*ones(n_Z,1) K_prime' logZ_prime'];

eval_point_adj = [bound(N_H_prime_old_no_run',funs.orig_min(1),funs.orig_max(1)) ...
    bound(N_R_prime_old_no_run',funs.orig_min(2),funs.orig_max(2)) ...
    bound(p.exoInc*ones(n_Z,1),funs.orig_min(3),funs.orig_max(3)) ...
    bound(K_prime',funs.orig_min(4),funs.orig_max(4)) ...
    logZ_prime'];

eval_point_rescaled = Rescale(eval_point_adj,funs.unit_min, ...
    funs.unit_max,funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

Q_prime_no_run = (eval_poly*funs.vec_Q_int); % /////
Pi_Q_prime_no_run= (eval_poly*funs.Pi_Q_prime_int);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(eval_point - eval_point_adj))) > 0
    df = 1e-2;
    
    aux = eye(4);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(eval_point_adj,4,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_plus =  Rescale(eval_point_plus,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_plus,funs.nDims, ...
        funs.max_approx_level,funs.elem);

    Q_prime_no_run_plus = (eval_poly_plus*funs.vec_Q_int);
    Pi_Q_prime_no_run_plus = (eval_poly_plus*funs.Pi_Q_prime_int);

    Q_prime_no_run_plus = reshape(Q_prime_no_run_plus,n_Z,4);
    Pi_Q_prime_no_run_plus = reshape(Pi_Q_prime_no_run_plus,n_Z,4);

    Q_prime_no_run_diff = [(Q_prime_no_run_plus - repmat(Q_prime_no_run,1,4))./df zeros(n_Z,1)];
    Pi_Q_prime_no_run_diff = [(Pi_Q_prime_no_run_plus - repmat(Pi_Q_prime_no_run,1,4))./df zeros(n_Z,1)];

    Q_prime_no_run = Q_prime_no_run + sum(Q_prime_no_run_diff.*(eval_point - eval_point_adj),2);
    Pi_Q_prime_no_run = Pi_Q_prime_no_run + sum(Pi_Q_prime_no_run_diff.*(eval_point - eval_point_adj),2);
end

Q_prime_no_run = Q_prime_no_run';
Pi_Q_prime_no_run = Pi_Q_prime_no_run';
R_K_prime_no_run = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + ...
    (1 - p.delta)*Q_prime_no_run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run Capital Return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% run evaluation grid and poly
eval_point = [N_H_prime_old_run' N_R_prime_old_run' K_prime' logZ_prime'];

eval_point_adj = [bound(N_H_prime_old_run',funs_run.orig_min(1),funs_run.orig_max(1)) ...
    bound(N_R_prime_old_run',funs_run.orig_min(2),funs_run.orig_max(2)) ...
    bound(K_prime',funs_run.orig_min(3),funs_run.orig_max(3)) ...
    logZ_prime'];

eval_point_rescaled = Rescale(eval_point_adj,funs_run.unit_min, ...
    funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims, ...
    funs_run.max_approx_level,funs_run.elem);

Q_prime_run = (eval_poly*funs_run.vec_Q_int); % /////
Pi_Q_prime_run= (eval_poly*funs_run.Pi_Q_prime_int);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(eval_point - eval_point_adj))) > 0
    df = 1e-2;
    nVars = 3;
    
    aux = eye(nVars);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(eval_point_adj,nVars,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_plus =  Rescale(eval_point_plus,funs_run.unit_min,funs_run.unit_max, ...
        funs_run.orig_min,funs_run.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_plus,funs_run.nDims, ...
        funs_run.max_approx_level,funs_run.elem);

    Q_prime_run_plus = (eval_poly_plus*funs_run.vec_Q_int);
    Pi_Q_prime_run_plus = (eval_poly_plus*funs_run.Pi_Q_prime_int);

    Q_prime_run_plus = reshape(Q_prime_run_plus,n_Z,nVars);
    Pi_Q_prime_run_plus = reshape(Pi_Q_prime_run_plus,n_Z,nVars);

    Q_prime_run_diff = [(Q_prime_run_plus - repmat(Q_prime_run,1,nVars))./df zeros(n_Z,1)];
    Pi_Q_prime_run_diff = [(Pi_Q_prime_run_plus - repmat(Pi_Q_prime_run,1,nVars))./df zeros(n_Z,1)];

    Q_prime_run = Q_prime_run + sum(Q_prime_run_diff.*(eval_point - eval_point_adj),2);
    Pi_Q_prime_run = Pi_Q_prime_run + sum(Pi_Q_prime_run_diff.*(eval_point - eval_point_adj),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Q_prime_run = Q_prime_run'; % /////
Pi_Q_prime_run= Pi_Q_prime_run';
R_K_prime_run = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + ...
    (1 - p.delta)*Q_prime_run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% balance sheet constraint of retail bank:
% min_R_K_prime = min([R_K_prime_no_run R_K_prime_run]);
D_prime_max =  ...
    (1 - p.gamma_R*p.omega_K)*Q*K_R_prime;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Updated Net Worth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N', No Run
N_H_prime_no_run = R_K_prime_no_run*K_H_prime + R_D_prime*D_prime +  ...
    W_prime + Pi_Q_prime_no_run;
N_R_prime_no_run = R_K_prime_no_run*K_R_prime - R_D_prime*D_prime + p.exoInc;  % /////

% N', Run
N_H_prime_run = R_K_prime_run*K_H_prime + R_D_prime*D_prime +  ...
    W_prime + Pi_Q_prime_run;
N_R_prime_run = R_K_prime_run*K_R_prime - R_D_prime*D_prime + p.exoInc;  % /////