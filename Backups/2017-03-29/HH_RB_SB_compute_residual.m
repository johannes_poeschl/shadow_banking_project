function foc_residual = HH_RB_SB_compute_residual(p, x0, states, logZ_prime, ...
    Z_prob, funs, N_H_prime_old,N_R_prime_old,N_S_prime_old,indK,indD,indB)
% HH_RB_SB_compute_residual computes the FOC residuals
%
% Inputs:
%   - p             struct array containing parameters
%   - x0            8x1 column vector of unknown choice variables
%   - states        1x4 row vector of state variables
%   - logZ_prime    1xZ row vector of future log productivity realizations
%   - Z_prob        1xZ row vector of probabilities of future productivity
%                   realizations
%   - funs          struct array that contains interpolation coefficients
%                   and other information required for interpolation
%   - N_H_prime_old 1xZ vector of future household net worth realizations
%   - N_R_prime_old 1xZ vector of future retail bank net worth realizations
%   - N_S_prime_old 1xZ vector of future shadow bank net worth realizations
%   - ind           1x1 scalar case indicator
%
% Outputs:
%   - foc_residual  8x1 column vector of FOC residuals

n_Z = length(logZ_prime);

N_H = states(1,1);
N_R = states(1,2);
N_S = states(1,3);  % /////
K   = states(1,4);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);  % /////
R_D_prime = x0(4);
R_B_prime = x0(5);  % /////
Q = x0(6);
D_prime = x0(7);
B_prime = x0(8);

K_prime = (K_H_prime + K_R_prime + K_S_prime);  % /////
I = K_prime - (1-p.delta)*K; 

% market clearing conditions:
K_prime = K_prime * ones(1,n_Z);

[N_H_prime,N_R_prime,N_S_prime,R_K_prime,D_prime_max,B_prime_max] = ...
    HH_RB_SB_compute_N_prime(p, ...
    x0, logZ_prime, funs, ...
    N_H_prime_old, N_R_prime_old, N_S_prime_old);

% consumptions
C_H = N_H - Q * K_H_prime - p.alpha_H / 2 * K_H_prime^2 - D_prime;
C_R = N_R - Q * K_R_prime - p.alpha_R / 2 * K_R_prime^2 - B_prime + D_prime;  % /////
C_S = N_S - Q * K_S_prime + B_prime;  % /////

% construct the polynomial at the evaluation point
eval_point = [N_H_prime' N_R_prime' N_S_prime' K_prime' logZ_prime'];

% adjust the evaluation points to lie within the grid
eval_point_adj = [bound(N_H_prime',funs.orig_min(1),funs.orig_max(1)) ...
    bound(N_R_prime',funs.orig_min(2),funs.orig_max(2)) ...
    bound(N_S_prime',funs.orig_min(3),funs.orig_max(3)) ...
    bound(K_prime',funs.orig_min(4),funs.orig_max(4)) ...
    logZ_prime'];

eval_point_rescaled =  Rescale(eval_point_adj,funs.unit_min,funs.unit_max, ...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
v_N_H_1 = (eval_poly*funs.v_N_H_int);
v_N_R_1 = (eval_poly*funs.v_N_R_int);
v_N_S_1 = (eval_poly*funs.v_N_S_int);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(eval_point - eval_point_adj))) > 0
    df = 1e-2;
    
    aux = eye(4);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(eval_point_adj,4,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_minus =  Rescale(eval_point_plus,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_minus,funs.nDims, ...
        funs.max_approx_level,funs.elem);

    v_N_H_1_plus = (eval_poly_plus*funs.v_N_H_int);
    v_N_R_1_plus = (eval_poly_plus*funs.v_N_R_int);
    v_N_S_1_plus = (eval_poly_plus*funs.v_N_S_int);

    v_N_H_1_plus = reshape(v_N_H_1_plus,n_Z,4);
    v_N_R_1_plus = reshape(v_N_R_1_plus,n_Z,4);
    v_N_S_1_plus = reshape(v_N_S_1_plus,n_Z,4);

    v_N_H_1_diff = [(v_N_H_1_plus - repmat(v_N_H_1,1,4))./df zeros(n_Z,1)];
    v_N_R_1_diff = [(v_N_R_1_plus - repmat(v_N_R_1,1,4))./df zeros(n_Z,1)];
    v_N_S_1_diff = [(v_N_S_1_plus - repmat(v_N_S_1,1,4))./df zeros(n_Z,1)];

    v_N_H_1 = v_N_H_1 + sum(v_N_H_1_diff.*(eval_point - eval_point_adj),2);
    v_N_R_1 = v_N_R_1 + sum(v_N_R_1_diff.*(eval_point - eval_point_adj),2);
    v_N_S_1 = v_N_S_1 + sum(v_N_S_1_diff.*(eval_point - eval_point_adj),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

v_N_H_1 = v_N_H_1';
v_N_R_1 = v_N_R_1';
v_N_S_1 = v_N_S_1';

EV_K_H = sum(Z_prob.*v_N_H_1.*R_K_prime,2);
EV_D_H = sum(Z_prob.*v_N_H_1*R_D_prime,2);

foc_residual = zeros(8,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if indK == 1 || indK == 3
    foc_residual(1) = C_H^(-p.sigma)*(Q + p.alpha_H*K_H_prime) - ...
        p.beta_H * EV_K_H;
elseif indK == 2 || indK == 4
    foc_residual(1) = K_H_prime;
end

foc_residual(2) = C_H^(-p.sigma) - p.beta_H * EV_D_H;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if indD == 0 && (indK == 1 || indK == 2)
    % binding borrowing constraint, nonbinding capital constraints
    EV_K_R = sum(Z_prob.*v_N_R_1.*(R_K_prime - (1-p.gamma_R*p.omega_K)*Q*R_D_prime),2);
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = (R_B_prime - (1-p.gamma_R*p.omega_B)*R_D_prime)*EV_N_R;
    
    foc_residual(3) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_K*Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R;
    foc_residual(4) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_B)-p.beta_R * EV_B_R;    
    foc_residual(7) = D_prime - D_prime_max;
elseif indD == 1 && (indK == 1 || indK == 2) 
    % non-binding borrowing constraint, nonbinding capital constraints
    EV_K_R = sum(Z_prob.*v_N_R_1.*R_K_prime,2);
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = R_B_prime*EV_N_R;
    EV_D_R = R_D_prime*EV_N_R;
    
    foc_residual(3) = C_R^(-p.sigma_R)*(Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R;
    foc_residual(4) = C_R^(-p.sigma_R)*1 - p.beta_R * EV_B_R; 
    
    foc_residual(7) = C_R.^(-p.sigma) - p.beta_R.*EV_D_R;
elseif indD == 0 && (indK == 3 || indK == 4) 
    % binding borrowing constraint, no capital holdings
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = (R_B_prime - (1-p.gamma_R*p.omega_B)*R_D_prime)*EV_N_R;
    
    foc_residual(3) = K_R_prime;
    foc_residual(4) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_B)-p.beta_R * EV_B_R;
    
    foc_residual(7) = D_prime - D_prime_max;
elseif indD == 1 && (indK == 3 || indK == 4) 
    % non-binding borrowing constraint, no capital holdings
    EV_N_R = sum(Z_prob.*v_N_R_1,2);
    EV_B_R = R_B_prime*EV_N_R;
    EV_D_R = R_D_prime*EV_N_R;
    
    foc_residual(3) = K_R_prime;
    foc_residual(4) = C_R^(-p.sigma_R)*1 - p.beta_R * EV_B_R; 
    
    foc_residual(7) = C_R.^(-p.sigma) - p.beta_R.*EV_D_R;
else
    error('indD must be 0 or 1')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if indB == 0 
    % binding borrowing constraint
    EV_K_S = sum(Z_prob.*v_N_S_1.*(R_K_prime - min(R_K_prime)*(1-p.phi)),2);
    
    foc_residual(5) = C_S^(-p.sigma_S)*(Q - (1-p.phi)*min(R_K_prime)/R_B_prime)...
        - p.beta_S * EV_K_S;
    foc_residual(8) = B_prime - B_prime_max;
elseif indB == 1 
    % non-binding borrowing constraint
    EV_K_S = sum(Z_prob.*v_N_S_1.*R_K_prime,2);
    EV_B_S = R_B_prime*sum(Z_prob.*v_N_S_1,2);
    
    foc_residual(5) = C_S^(-p.sigma_S)*Q - p.beta_S * EV_K_S;    
    foc_residual(8) = C_S.^(-p.sigma_S) - p.beta_S.*EV_B_S;
else
    error('indB must be 0 or 1')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Capital Producer FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(6) = Q - 1 - p.theta*(I/K - p.delta);
