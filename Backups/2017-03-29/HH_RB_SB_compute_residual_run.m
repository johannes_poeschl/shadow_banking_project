function euler_residual = HH_RB_SB_compute_residual_run(p, x0, states, logZ_prime, ...
    Z_prob, funs, funs_run, N_H_prime_old_no_run, N_R_prime_old_no_run, ...
    N_H_prime_old_run, N_R_prime_old_run,indK,indD)
% HH_RB_SB_compute_residual_run computes the F.O.C. residuals in a run.

n_Z = length(logZ_prime);

N_H = states(1,1);
N_R = states(1,2);
K   = states(1,3);

K_H_prime = x0(1);
K_R_prime = x0(2);
R_D_prime = x0(3);
Q = x0(4);
D_prime = x0(5);

K_prime = K_H_prime + K_R_prime;  % /////
I = K_prime - (1-p.delta)*K; 

% market clearing conditions:
K_prime = K_prime * ones(size(logZ_prime));

[N_H_prime_no_run,N_R_prime_no_run, ...
    N_H_prime_run,N_R_prime_run, ...
    R_K_prime_no_run,R_K_prime_run, ...
    D_prime_max] = HH_RB_SB_compute_N_prime_run(p, x0, logZ_prime, ...
    funs, funs_run, ...
    N_H_prime_old_no_run, N_R_prime_old_no_run, ...
    N_H_prime_old_run, N_R_prime_old_run);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Expectations, No Run
eval_point = [N_H_prime_no_run' N_R_prime_no_run' p.exoInc*ones(n_Z,1) K_prime' logZ_prime'];

eval_point_adj = [bound(N_H_prime_no_run',funs.orig_min(1),funs.orig_max(1)) ...
    bound(N_R_prime_no_run',funs.orig_min(2),funs.orig_max(2)) ...
    bound(p.exoInc*ones(n_Z,1),funs.orig_min(3),funs.orig_max(3)) ...
    bound(K_prime',funs.orig_min(4),funs.orig_max(4)) ...
    logZ_prime'];

eval_point_rescaled = Rescale(eval_point_adj,funs.unit_min,funs.unit_max, ...
    funs.orig_min,funs.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims, ...
    funs.max_approx_level,funs.elem);

v_N_H_1 = (eval_poly*funs.v_N_H_int);  % /////
v_N_R_1_noRun = (eval_poly*funs.v_N_R_int);  % /////

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(eval_point - eval_point_adj))) > 0
    df = 1e-2;
    
    aux = eye(4);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(eval_point_adj,4,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_plus =  Rescale(eval_point_plus,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_plus,funs.nDims, ...
        funs.max_approx_level,funs.elem);

    v_N_H_1_plus = (eval_poly_plus*funs.v_N_H_int);
    v_N_R_1_plus = (eval_poly_plus*funs.v_N_R_int);

    v_N_H_1_plus = reshape(v_N_H_1_plus,n_Z,4);
    v_N_R_1_plus = reshape(v_N_R_1_plus,n_Z,4);

    v_N_H_1_diff = [(v_N_H_1_plus - repmat(v_N_H_1,1,4))./df zeros(n_Z,1)];
    v_N_R_1_diff = [(v_N_R_1_plus - repmat(v_N_R_1_noRun,1,4))./df zeros(n_Z,1)];

    v_N_H_1 = v_N_H_1 + sum(v_N_H_1_diff.*(eval_point - eval_point_adj),2);
    v_N_R_1_noRun = v_N_R_1_noRun + sum(v_N_R_1_diff.*(eval_point - eval_point_adj),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

v_N_H_1 = v_N_H_1';
v_N_R_1_noRun = v_N_R_1_noRun';

EV_K_H_no_run = sum(Z_prob.*v_N_H_1.*R_K_prime_no_run,2);
EV_D_H_no_run   = sum(Z_prob.*v_N_H_1*R_D_prime,2);

% Expectations, Run
eval_point = [N_H_prime_run' N_R_prime_run' K_prime' logZ_prime'];

eval_point_adj = [bound(N_H_prime_run',funs_run.orig_min(1),funs_run.orig_max(1)) ...
    bound(N_R_prime_run',funs_run.orig_min(2),funs_run.orig_max(2)) ...
    bound(K_prime',funs_run.orig_min(3),funs_run.orig_max(3)) ...
    logZ_prime'];

eval_point_rescaled = Rescale(eval_point_adj,funs_run.unit_min, ...
    funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims, ...
    funs_run.max_approx_level,funs_run.elem);

v_N_H_1 = (eval_poly*funs_run.v_N_H_int);  % /////
v_N_R_1_Run = (eval_poly*funs_run.v_N_R_int);  % /////

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(eval_point - eval_point_adj))) > 0
    df = 1e-2;
    nVars = 3;
    
    aux = eye(nVars);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(eval_point_adj,nVars,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_plus =  Rescale(eval_point_plus,funs_run.unit_min,funs_run.unit_max, ...
        funs_run.orig_min,funs_run.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_plus,funs_run.nDims, ...
        funs_run.max_approx_level,funs_run.elem);

    v_N_H_1_plus = (eval_poly_plus*funs_run.v_N_H_int);
    v_N_R_1_plus = (eval_poly_plus*funs_run.v_N_R_int);

    v_N_H_1_plus = reshape(v_N_H_1_plus,n_Z,nVars);
    v_N_R_1_plus = reshape(v_N_R_1_plus,n_Z,nVars);

    v_N_H_1_diff = [(v_N_H_1_plus - repmat(v_N_H_1,1,nVars))./df zeros(n_Z,1)];
    v_N_R_1_diff = [(v_N_R_1_plus - repmat(v_N_R_1_Run,1,nVars))./df zeros(n_Z,1)];

    v_N_H_1 = v_N_H_1 + sum(v_N_H_1_diff.*(eval_point - eval_point_adj),2);
    v_N_R_1_Run = v_N_R_1_Run + sum(v_N_R_1_diff.*(eval_point - eval_point_adj),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

v_N_H_1 = v_N_H_1';
v_N_R_1_Run = v_N_R_1_Run';

EV_K_H_run = sum(Z_prob.*v_N_H_1.*R_K_prime_run,2);
EV_D_H_run   = sum(Z_prob.*v_N_H_1*R_D_prime,2);

% Expectations, combined
EV_K_H  = p.pi*EV_K_H_run + (1-p.pi)*EV_K_H_no_run;
EV_D_H    = p.pi*EV_D_H_run + (1-p.pi)*EV_D_H_no_run;

% consumptions
C_H = N_H - Q * K_H_prime - p.alpha_H / 2 * K_H_prime^2 - D_prime;
C_R = N_R - Q * K_R_prime - p.alpha_R / 2 * K_R_prime^2 + D_prime;  % /////

euler_residual = zeros(5,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if indK == 1
    euler_residual(1) = C_H^(-p.sigma)*(Q + p.alpha_H*K_H_prime) - p.beta_H * EV_K_H;
elseif indK == 2
    euler_residual(1) = K_H_prime;
end

euler_residual(2) = C_H^(-p.sigma) - p.beta_H * EV_D_H;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if indD == 0 % binding constraint
    EV_K_R_no_run = sum(Z_prob.*v_N_R_1_noRun.*(R_K_prime_no_run - ...
        (1-p.gamma_R*p.omega_K)*Q*R_D_prime),2);
    
    EV_K_R_run = sum(Z_prob.*v_N_R_1_Run.*(R_K_prime_run - ...
        (1-p.gamma_R*p.omega_K)*Q*R_D_prime),2);
    
    EV_K_R  = p.pi*EV_K_R_run + (1-p.pi)*EV_K_R_no_run;
   
    euler_residual(3) = C_R^(-p.sigma_R)*(p.gamma_R*p.omega_K*Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R;
    euler_residual(5) = D_prime - D_prime_max; 
elseif indD == 1 % non-binding constraint
    EV_K_R_no_run = sum(Z_prob.*v_N_R_1_noRun.*R_K_prime_no_run,2);
    EV_D_R_no_run = R_D_prime*sum(Z_prob.*v_N_R_1_noRun,2);
    
    EV_K_R_run = sum(Z_prob.*v_N_R_1_Run.*R_K_prime_run,2);
    EV_D_R_run = R_D_prime*sum(Z_prob.*v_N_R_1_Run,2);
    
    EV_K_R  = p.pi*EV_K_R_run + (1-p.pi)*EV_K_R_no_run;
    EV_D_R  = p.pi*EV_D_R_run + (1-p.pi)*EV_D_R_no_run;
   
    euler_residual(3) = C_R^(-p.sigma_R)*(Q + p.alpha_R*K_R_prime)- p.beta_R * EV_K_R;
    
    euler_residual(5) = C_R^(-p.sigma_R) - p.beta_R * EV_D_R;
else
    error('indD must be 0 or 1')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Capital Producer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

euler_residual(4) = Q - 1 - p.theta*(I/K - p.delta);
