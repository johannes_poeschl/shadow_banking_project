% Computes the welfare effects of capital requirements and the optimal CR

%% Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
close all

addpath('./Smolyak_Anisotropic_JMMV_2014')

% nu = 0.025; %%%%% probability that bankrun happens in case bankrun condition is met, i.e. coordination failure

tic
nBurnin = 250; 
nSimulations = nBurnin + 1000;  % number of simulated periods 
nPanels = 250; %%%%% number of simulations for each capital requirement level

par_name = 'gammaR';

if strcmp(par_name,'omegaB') == 1
    parSpace_sim = 0.5:0.1:2;
    parNameAlt = '\omega^B';
elseif strcmp(par_name,'gammaR') == 1
    parNameAlt = '\Gamma_R';
    parSpace_sim = 0.08:0.02:0.2;
end

dateVal_sim = '28-Feb-2017'; %date; %%%%%
dateVal_sim_alt = '28-Feb-2017';

load(strcat('./Results/Result_NoRun_',dateVal_sim_alt,'_',par_name,'_',num2str(parSpace_sim(1)),'.mat'))

% Draw shocks
seq_logZ = zeros(nPanels,nSimulations);
n_Z = 25;
[Z_grid,Z_prob] = tauchen(n_Z,0,p.rho,p.sigma_Z,4);

for jj = 1:nPanels  %%%%% for each simulation 
    % --  random sequence of Z --
    seq_logZ_transpose = simulateMarkovChain(Z_grid(13),Z_grid,Z_prob,nSimulations);
    seq_logZ(jj,:) = seq_logZ_transpose';
end

seq_random1 = normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
seq_random2 = normrnd(0,1,nPanels,nSimulations);

% Preallocation
bankRunProb = zeros(nPanels,length(parSpace_sim));
cons_eq_H = zeros(nPanels,length(parSpace_sim));
cons_eq_R = zeros(nPanels,length(parSpace_sim));
cons_eq_S = zeros(nPanels,length(parSpace_sim));

mean_C_H = zeros(1,length(parSpace_sim));
mean_C_R = zeros(1,length(parSpace_sim));
mean_C_S = zeros(1,length(parSpace_sim));

sd_C_H = zeros(1,length(parSpace_sim));
sd_C_R = zeros(1,length(parSpace_sim));
sd_C_S = zeros(1,length(parSpace_sim));

mean_Q      = zeros(1,length(parSpace_sim));
sd_Q        = zeros(1,length(parSpace_sim));
mean_Q_diff = zeros(1,length(parSpace_sim));
sd_Q_diff   = zeros(1,length(parSpace_sim));
mean_Q_star = zeros(1,length(parSpace_sim));
sd_Q_star   = zeros(1,length(parSpace_sim));

mean_lev_R  = zeros(1,length(parSpace_sim));
sd_lev_R    = zeros(1,length(parSpace_sim));
mean_lev_S  = zeros(1,length(parSpace_sim));
sd_lev_S    = zeros(1,length(parSpace_sim));

mean_recov  = zeros(1,length(parSpace_sim));

bindingConstr_R = zeros(1,length(parSpace_sim));

for kk = 1:length(parSpace_sim) %%%%% for each capital requirement level

    if parSpace_sim(kk) > 0.08
        load(strcat('./Results/Result_NoRun_',dateVal_sim,'_',par_name,'_',num2str(parSpace_sim(kk)),'.mat'))
        load(strcat('./Results/Result_Run_',dateVal_sim,'_',par_name,'_',num2str(parSpace_sim(kk)),'.mat'))
    else
        load(strcat('./Results/Result_NoRun_',dateVal_sim_alt,'_',par_name,'_',num2str(parSpace_sim(kk)),'.mat'))
        load(strcat('./Results/Result_Run_',dateVal_sim_alt,'_',par_name,'_',num2str(parSpace_sim(kk)),'.mat'))
    end

% -- Interpolate policy function -- 

% - no run case -
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

funs.K_H_int = No_Run_Poly\pols.K_H_prime;
funs.K_R_int = No_Run_Poly\pols.K_R_prime;
funs.K_S_int = No_Run_Poly\pols.K_S_prime;
funs.D_int   = No_Run_Poly\pols.D_prime;
funs.B_int   = No_Run_Poly\pols.B_prime;
funs.R_D_int = No_Run_Poly\pols.R_D_prime;
funs.R_B_int = No_Run_Poly\pols.R_B_prime;

% - run case -
Run_Grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
Run_Poly = Smolyak_Polynomial(Run_Grid,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

funs_run.K_H_int = Run_Poly\pols_run.K_H_prime;
funs_run.K_R_int = Run_Poly\pols_run.K_R_prime;
funs_run.D_int   = Run_Poly\pols_run.D_prime;
funs_run.R_D_int = Run_Poly\pols_run.R_D_prime;

% - states, no run -
seq_N_H = ones(nPanels, nSimulations)*steady.N_H;  % state
seq_N_R = ones(nPanels, nSimulations)*steady.N_R;  % state
seq_N_S = ones(nPanels, nSimulations)*steady.N_S;  % state 
seq_K   = ones(nPanels, nSimulations)*steady.K;    % state

% - states, run -
seq_N_H_star = ones(nPanels, nSimulations)*steady.N_H;    % state
seq_N_R_star = ones(nPanels, nSimulations)*steady.N_R;    % state
seq_N_S_star = ones(nPanels, nSimulations)*p.exoInc;  % state

seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice 
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_I         = ones(nPanels, nSimulations);  % choice

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_old      = ones(nPanels, nSimulations);  % price, no run, guess
seq_Q_star     = ones(nPanels, nSimulations);  % price, run
seq_Q_star_old = ones(nPanels, nSimulations);  % price, run, guess

seq_R_K_prime_star = ones(nPanels, nSimulations);

seq_run           = zeros(nPanels, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_happened  = zeros(nPanels, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(nPanels, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(nPanels, nSimulations);   %%%%% whether a run starts in period iii
seq_recovery = zeros(nPanels, nSimulations);

qError = 10;
while qError > 1e-6;
    
    for iii = 1: nSimulations-1
                      
        if iii > 1
            seq_run(:,iii) = (1-p.phi)*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1) - ...
                seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1) < 0;
            
            seq_recovery(:,iii) = (1-p.phi)*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1)./(seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1));
            seq_recovery(seq_B_prime(:,iii-1) == 0,iii) = 0;
        end

        A = seq_run(:,iii).*(seq_random1(:,iii) < norminv(p.nu));  % bankrun condition is met and coordination fails
        B = seq_run_persist(:, iii) == 1; % bankrun continues

        seq_run_happened(:,iii) =  1 - (1-A).*(1-B); %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
        seq_run_triggered(:,iii) = A.*(1-B);
        seq_run_persist(:, iii+1) = seq_run_happened(:,iii).*(seq_random2(:,iii) < norminv(7/8)); %%%%% whether bankrun persists to the next period

        % bank run stuff
        seq_N_H(:, iii) = max(seq_N_H_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_H(:,iii).*(1-seq_run_happened(:,iii)),funs.orig_min(1));
        seq_N_R(: ,iii) = max(seq_N_R_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_R(:,iii).*(1-seq_run_happened(:,iii)),funs.orig_min(2));
        seq_N_S(:, iii) = max(seq_N_S_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_S(:,iii).*(1-seq_run_happened(:,iii)),funs.orig_min(3));

        eval_point = [seq_N_H(:,iii) seq_N_R(:,iii) seq_K(:,iii) seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

        seq_K_H_prime_run = eval_poly*funs_run.K_H_int;
        seq_K_R_prime_run = eval_poly*funs_run.K_R_int;
        seq_K_S_prime_run = 0;              
        seq_R_D_prime_run = eval_poly*funs_run.R_D_int;
        seq_R_B_prime_run = steady.R_B;          
        seq_D_prime_run   = eval_poly*funs_run.D_int;          
        seq_B_prime_run   = 0;
        seq_Q_run         = eval_poly*funs_run.vec_Q_int;

        % no bank run stuff
        eval_point = [seq_N_H(:,iii) seq_N_R(:,iii) seq_N_S(:, iii) seq_K(:,iii)  seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs.nDims,funs.max_approx_level,funs.elem);           

        seq_K_H_prime_no_run = eval_poly*funs.K_H_int;
        seq_K_R_prime_no_run = eval_poly*funs.K_R_int;
        seq_K_S_prime_no_run = eval_poly*funs.K_S_int;              
        seq_R_D_prime_no_run = eval_poly*funs.R_D_int;
        seq_R_B_prime_no_run = eval_poly*funs.R_B_int;          
        seq_D_prime_no_run   = eval_poly*funs.D_int;          
        seq_B_prime_no_run   = eval_poly*funs.B_int;
        seq_Q_no_run         = eval_poly*funs.vec_Q_int;
        
        % actual policy functions
        seq_K_H_prime(:, iii) = seq_K_H_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_H_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_R_prime(:, iii) = seq_K_R_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_R_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_S_prime(:, iii) = seq_K_S_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_S_prime_no_run.*(1-seq_run_happened(:,iii));              
        seq_R_D_prime(:, iii) = seq_R_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_D_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_R_B_prime(:, iii) = seq_R_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_B_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_D_prime(:, iii)   = seq_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_D_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_B_prime(:, iii)   = seq_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_B_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_Q(:, iii)         = seq_Q_run.*seq_run_happened(:,iii) + ...
            seq_Q_no_run.*(1-seq_run_happened(:,iii));
        
        % maximum borrowing level for shadow banks
        eval_point = [seq_N_H_star(:,iii) seq_N_R_star(:,iii) seq_K(:,iii)  seq_logZ(:,iii)];
        eval_point_rescaled = Rescale(eval_point, ...
            funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
        eval_poly = Smolyak_Polynomial(eval_point_rescaled,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

        seq_Q_star(:,iii) = eval_poly*funs_run.vec_Q_int;
        
        seq_K(:, iii+1) = seq_K_H_prime(:, iii) + seq_K_R_prime(:, iii) + seq_K_S_prime(:, iii);  

        seq_I(:,iii)    = seq_K(:,iii+1) - (1-p.delta)*seq_K(:,iii);  

        seq_R_K_prime(:, iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(:, iii + 1);
                      
        seq_R_K_prime_star(:,iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(:, iii + 1);

        seq_W_prime(:, iii)  = (1-p.alpha) * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^p.alpha;

        seq_N_H(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_H_prime(:, iii) + ...
                             seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
                         
        seq_N_R(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_R_prime(:, iii) + ...
                  seq_R_B_prime(:,iii).*seq_B_prime(:,iii) ...
                  - seq_R_D_prime(:, iii).*seq_D_prime(:, iii) ...
                  + p.exoInc;     
              
        seq_N_S(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_S_prime(:, iii) -...
                             seq_R_B_prime(:, iii).*seq_B_prime(:, iii) ...
                            + p.exoInc;
        
        seq_N_H_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_H_prime(:, iii) + ...
                 seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
             
        seq_N_R_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_R_prime(:, iii) + ...
                         (1-p.phi)*seq_R_K_prime_star(:, iii).*seq_K_S_prime(:, iii)...
                          - seq_R_D_prime(:, iii).*seq_D_prime(:, iii) ...
                          + p.exoInc;
                      
        seq_N_S_star(1, iii+1) = p.exoInc + p.phi*seq_R_K_prime_star(1, iii)*seq_K_S_prime(1, iii);            
    end
    
    qError = norm(vec(seq_Q_old - seq_Q),'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);

end

% additional statistics
seq_D_prime_max = (1-p.gamma_R*p.omega_B)*seq_B_prime + ...
    (1-p.gamma_R*p.omega_K)*seq_Q.*seq_K_R_prime;

seq_bindingConstr_R = seq_D_prime == seq_D_prime_max;

seq_leverage_R = seq_D_prime./(seq_B_prime + seq_Q.*seq_K_R_prime);
seq_leverage_S = seq_B_prime./(seq_Q.*seq_K_S_prime);

seq_C_H = max(1e-9,seq_N_H - seq_Q .* seq_K_H_prime - p.alpha_H/2 * seq_K_H_prime.^2 - seq_D_prime); %%%%%%%%%%%%%
seq_C_R = max(1e-9,seq_N_R - seq_Q .* seq_K_R_prime - p.alpha_R/2 * seq_K_R_prime.^2 - seq_B_prime + seq_D_prime);  %%%%%%%%%%%%%
seq_C_S = max(1e-9,seq_N_S - seq_Q .* seq_K_S_prime + seq_B_prime);

% remove the burnin period
seq_C_H_1 = seq_C_H(:,nBurnin+1:end);
seq_C_R_1 = seq_C_R(:,nBurnin+1:end);
seq_C_S_1 = seq_C_S(:,nBurnin+1:end);
seq_Q_old_1 = seq_Q_old(:,nBurnin+1:end);
seq_Q_star_1 = seq_Q_star_old(:,nBurnin+1:end);
seq_leverage_R_1 = seq_leverage_R(:,nBurnin+1:end);
seq_leverage_S_1 = seq_leverage_S(:,nBurnin+1:end);
seq_run_happened_1 = seq_run_happened(:,nBurnin+1:end);
seq_run_triggered_1 = seq_run_triggered(:,nBurnin+1:end);
seq_recovery_1 = seq_recovery(:,nBurnin+1:end);
seq_bindingConstr_R_1 = 0 + seq_bindingConstr_R(:,nBurnin+1:end);

seq_leverage_R_1(seq_run_happened_1 == 1) = nan;
seq_leverage_S_1(seq_run_happened_1 == 1) = nan;
seq_recovery_1(seq_run_happened_1 == 1) = nan;
seq_Q_star_1(seq_run_happened_1 == 1) = nan;

seq_Q_diff = (seq_Q_star_1 - seq_Q_old_1)./seq_Q_old_1;

%% Statistics
util_H = log(seq_C_H_1);
util_R = log(seq_C_R_1);
util_S = log(seq_C_S_1);

tt = repmat(0:1:nSimulations-nBurnin-1,nPanels,1);

seq_V_H = sum(p.beta_H.^tt.*util_H,2);
seq_V_R = sum(p.beta_R.^tt.*util_R,2);
seq_V_S = sum(p.beta_S.^tt.*util_S,2);

cons_eq_H(:,kk) = exp((1-p.beta_H)*seq_V_H);
cons_eq_R(:,kk) = exp((1-p.beta_R)*seq_V_R);
cons_eq_S(:,kk) = exp((1-p.beta_S)*seq_V_S);

mean_C_H(1,kk) = mean(mean(seq_C_H_1,2));
mean_C_R(1,kk) = mean(mean(seq_C_R_1,2));
mean_C_S(1,kk) = mean(mean(seq_C_S_1,2));

sd_C_H(1,kk) = mean(std(seq_C_H_1,[],2));
sd_C_R(1,kk) = mean(std(seq_C_R_1,[],2));
sd_C_S(1,kk) = mean(std(seq_C_S_1,[],2));

mean_Q(1,kk)        = mean(mean(seq_Q_old_1,2));
sd_Q(1,kk)          = mean(std(seq_Q_old_1,[],2));

mean_Q_diff(1,kk)   = mean(mean(seq_Q_diff,2,'omitnan'));
sd_Q_diff(1,kk)     = mean(std(seq_Q_diff,[],2,'omitnan'));

mean_Q_star(1,kk)   = mean(mean(seq_Q_star,2,'omitnan'));
sd_Q_star(1,kk)     = mean(std(seq_Q_star,[],2,'omitnan'));

mean_lev_R(1,kk) = mean(mean(seq_leverage_R_1,2,'omitnan'));
sd_lev_R(1,kk)   = mean(std(seq_leverage_R_1,[],2,'omitnan'));

mean_lev_S(1,kk) = mean(mean(seq_leverage_S_1,2,'omitnan'));
sd_lev_S(1,kk)   = mean(std(seq_leverage_S_1,[],2,'omitnan'));

mean_recov(1,kk) = mean(mean(seq_recovery_1,2,'omitnan'));

bankRunProb(1,kk) = mean(mean(seq_run_triggered_1,2));

bindingConstr_R(1,kk) = mean(mean(seq_bindingConstr_R_1,2));

end

cons_eqv_H = mean(cons_eq_H,1);
cons_eqv_R = mean(cons_eq_R,1);
cons_eqv_S = mean(cons_eq_S,1);

save(strcat('Result_Simulation_',par_name,'_',date,'.mat'))

%%

mu_R = linspace(0,1,500);
mu_S = mu_R;

nMu = length(mu_R);

[nd_mu_R,nd_mu_S] = ndgrid(mu_R,mu_S);

nd_mu_H = 1 - nd_mu_R - nd_mu_S;

nd_mu_H(nd_mu_H < 0) = 0;

vec_mu = [vec(nd_mu_H) vec(nd_mu_R) vec(nd_mu_S)];

max_req = zeros(nMu^2,1);
max_val = zeros(nMu^2,1);

for ii = 1:nMu^2
    [max_val(ii),max_pos] = max(vec_mu(ii,1)*cons_eqv_H + ...
        vec_mu(ii,2)*cons_eqv_R + vec_mu(ii,3)*cons_eqv_S);
    
    max_req(ii) = parSpace_sim(max_pos);
end

nd_req = reshape(max_req,nMu,nMu);
nd_req(nd_mu_H == 0) = nan;

nd_val = reshape(max_val,nMu,nMu);
nd_val(nd_mu_H == 0) = nan;

figure
subplot(1,2,1)
contourf(mu_S,mu_R,nd_req)
xlabel('mu_S')
ylabel('mu_R')
title(strcat('Optimal',{' '},parNameAlt))

subplot(1,2,2)
contourf(mu_S,mu_R,nd_val)
xlabel('mu_S')
ylabel('mu_R')
title('Pareto Frontier')

saveas(gcf,strcat('./Figures/Pareto_Frontier_',par_name,'_',dateVal_sim,'.fig'))

% save(strcat('WelfarePlots_',date,'.mat'))
%save(strcat('Result_Run_',date,'_Gamma_',num2str(p.gamma_R),'.mat'))


%% Plots
% elapsedTime = toc

% Bank Run Probability
fontOptions = {'FontSize',15};

figure
plot(parSpace_sim,100*bankRunProb(1,:),'LineWidth',2);
xlabel(parNameAlt);
ylabel('Probability of bankrun per quarter (%)');
title('Probability of Bankrun as a Function of Capital Requirement');
xlim([min(parSpace_sim) max(parSpace_sim)])
grid on
saveas(gcf,strcat('./Figures/Bankrun_Prob_',par_name,'_',dateVal_sim,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [29.7/2 21/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Bankrun_Prob_',par_name);
print(figureName,'-dpdf')
print(figureName,'-dpng')
print(figureName,'-deps')

%%
% Drop in Liquidation Prices and Recovery Values

figure
subplot(2,2,1)
plot(parSpace_sim,100*mean_Q_diff(1,:),'LineWidth',2);
xlabel(parNameAlt);
ylabel('(Q^* - Q)/Q (%)');
title('Difference in Capital Prices');
xlim([min(parSpace_sim) max(parSpace_sim)])
grid on

subplot(2,2,2)
plot(parSpace_sim,100*mean_recov(1,:),'LineWidth',2);
xlabel(parNameAlt);
ylabel('Recovery per Unit of Debt (%)');
title('Recovery of Retail Banks');
xlim([min(parSpace_sim) max(parSpace_sim)])
grid on

subplot(2,2,3)
plot(parSpace_sim,100*mean_lev_R(1,:),'LineWidth',2);
xlabel(parNameAlt);
ylabel('Retail Bank Leverage (%)');
title('Retail Bank Leverage');
xlim([min(parSpace_sim) max(parSpace_sim)])
grid on

subplot(2,2,4)
plot(parSpace_sim,100*mean_lev_S(1,:),'LineWidth',2);
xlabel(parNameAlt);
ylabel('Shadow Bank Leverage (%)');
title('Shadow Bank Leverage (%)');
xlim([min(parSpace_sim) max(parSpace_sim)])
grid on

saveas(gcf,strcat('./Figures/Cap_Price_and_Recov_',par_name,'_',dateVal_sim,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Cap_Price_and_Recov_',par_name);
print(figureName,'-dpdf')
print(figureName,'-dpng')
print(figureName,'-deps')

%%
yVals = [cons_eqv_H; cons_eqv_R; cons_eqv_S];

names = {'Cons Eq HH','Cons Eq R Banks','Cons Eq S Banks'};

figure
for ii = 1:size(yVals,1)
    subplot(1,3,ii)
    plot(parSpace_sim,yVals(ii,:),'LineWidth',2)
    grid on
    title(names{ii},fontOptions{:})
    xlabel(parNameAlt,fontOptions{:})
    ylabel('Perm Cons Equiv',fontOptions{:})
end
saveas(gcf,strcat('./Figures/Welfare_',par_name,'_',date,'.fig'))


%% Welfare and Optimal Capital Requirement
fontOptions = {'FontSize',12};
names = {'Welfare of HH','Welfare of R Banks','Welfare of S Banks'};
figure
for ii = 1:size(yVals,1)
    subplot(2,2,ii)
    plot(parSpace_sim,100*(yVals(ii,:)/yVals(ii,parSpace_sim == 0.08)-1),'LineWidth',2)
    grid on
    title(names{ii},fontOptions{:})
    xlabel(parNameAlt,fontOptions{:})
    ylabel('\Delta Perm Cons Equiv (%)',fontOptions{:})
    xlim([min(parSpace_sim) max(parSpace_sim)])
end
subplot(2,2,4)
contourf(mu_S,mu_R,nd_req)
xlabel('\mu_S')
ylabel('\mu_R')
title(strcat('Optimal',{' '},parNameAlt),fontOptions{:})


% saveas(gcf,strcat('./Figures/Welfare_',par_name,'_',date,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Welfare_',par_name);
print(figureName,'-dpdf')
print(figureName,'-dpng')
print(figureName,'-deps')