function boundedVal = bound(origVal,minVal,maxVal)
%bound bounds a scalar or vector between minVal and maxVal

boundedVal1 = max(origVal,minVal);
boundedVal = min(boundedVal1,maxVal);