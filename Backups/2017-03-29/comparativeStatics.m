% This file computes the steady state of the new model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('./Smolyak_Anisotropic_JMMV_2014')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parSpace = 0.02:0.01:0.5;
nPars = length(parSpace);

vec_D       = zeros(nPars,1);
vec_B       = zeros(nPars,1);
vec_K_H     = zeros(nPars,1);
vec_K_R     = zeros(nPars,1);
vec_K_S     = zeros(nPars,1);
vec_K       = zeros(nPars,1);
vec_phi_R   = zeros(nPars,1);
vec_phi_S   = zeros(nPars,1);
vec_C_H     = zeros(nPars,1);
vec_C_R     = zeros(nPars,1);
vec_C_S     = zeros(nPars,1);
vec_Y       = zeros(nPars,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;
p.rho_Z     = 0.8;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.25;                 % Capital Adjustment Cost
p.alpha     = 0.36;                 % Production Function Curvature
p.beta      = 0.99;                 % Household Discount Factor
p.sigma_R   = 1 - 0.925/p.beta;     % Retail Bank Exit Probability
p.sigma_S   = 1 - 0.775/p.beta;     % Shadow Bank Exit Probability
p.delta     = 0.025;                % Depreciation Rate
p.eta_H     = 0.003;                % Household Capital Holding Cost
p.eta_R     = 0.001;                % Retail Bank Capital Holding Cost
p.gamma     = 0.5;                  % Relative Diversion Share, Interbank Loans
p.omega     = 0.5;                  % Divertable Asset Multiplier, Shadow Banks
p.psi       = 0.1;                  % Divertable Asset Share, Retail Banks
p.exoInc    = 0.005;                % Exogenous Income of Bankers
p.nu        = 0.025;                % Probability of Bank Run Starting
p.pi        = 7/8;                  % Probability of Bank Run Continuing
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ii = 1:length(parSpace)

p.phi_bar   = 1/parSpace(ii);               % Capital Requirement

handle = @(x) computeSteadyState(x,p);

x0 = [1 10 10]';
x1 = fsolve(handle,x0);

[~,steady] = handle(x1);

vec_D(ii)   = steady.D;
vec_B(ii)   = steady.B;
vec_K_H(ii) = steady.K_H;
vec_K_R(ii) = steady.K_R;
vec_K_S(ii) = steady.K_S;
vec_K(ii)   = steady.K;
vec_phi_R(ii) = steady.phi_R;
vec_phi_S(ii) = steady.phi_S;
vec_C_H(ii) = steady.C_H;
vec_C_R(ii) = p.sigma_R*steady.N_R;
vec_C_S(ii) = p.sigma_S*steady.N_S;
vec_Y(ii)   = steady.Y;
end

%% Plots

plotVals = [vec_D vec_B 1./vec_phi_R 1./vec_phi_S vec_K_H vec_K_R vec_K_S vec_K ...
    vec_C_H vec_C_R vec_C_S vec_Y];
plotNames = {'D','B','\phi_R','\phi_S','K_H','K_R','K_S','K','C_H','C_R','C_S','Y'};

figure
for ii = 1:size(plotVals,2)
   subplot(3,4,ii)
   plot(parSpace,plotVals(:,ii),'LineWidth',2)
   title(plotNames{ii})
   xlabel('\Gamma_R')
   xlim([parSpace(1) parSpace(end)])
end

figureName = './Figures/Comparative_Statics';
saveas(gcf,strcat(figureName,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [29.7 21];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')