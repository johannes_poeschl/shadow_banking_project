function [resid,steady] = computeSteadyState(x,p)

K_H = x(1);
K_R = x(2);
K_S = x(3);

% Market Clearing
K = K_H + K_R + K_S;
exoInc = p.exoInc*K;

% Final Goods Producer
r_K = p.alpha*K^(p.alpha-1);
W = (1-p.alpha)*K^p.alpha;

% Returns
R_D     = 1/p.beta;
R_K     = r_K + 1 - p.delta;
R_K_H   = R_K/(1 + p.eta_H*K_H);
R_K_R   = R_K/(1 + p.eta_R*K_R);
R_B     = p.gamma*R_K_R + (1-p.gamma)*R_D;

% Shadow Bank
phi_S   = p.beta*p.sigma_S/(p.psi*p.omega*(1-p.beta*(1-p.sigma_S))) - ...
            (1-p.omega)/p.omega;
N_S     = exoInc/(1 - ((R_K - R_B)*phi_S + R_B)*(1-p.sigma_S));
B       = (phi_S - 1)*N_S;

% Retail Bank
f_R     = p.eta_R*K_R;
phi_R   = min(p.beta*p.sigma_R/(p.psi*(1-p.beta*(1-p.sigma_R))),p.phi_bar);
aux1    = (R_B - R_D - p.gamma*(R_K_R - R_D))*B*(1-p.sigma_R) + exoInc;
aux2    = 1 - (1-p.sigma_R)*((R_K_R - R_D)*phi_R + R_D);
N_R     = aux1/aux2;
D       = (phi_R - 1)*N_R + (1 - p.gamma)*B;

% Household
N_H = (r_K + 1 - p.delta)*K_H + R_D*D + W;
C_H = N_H + (f_R - p.eta_R/2*K_R)*K_R - (1 + p.eta_H/2*K_H)*K_H - D;

resid    = zeros(3,1);
resid(1) = p.beta*R_K_H - 1;
resid(2) = phi_R*N_R - (1 + f_R)*K_R - p.gamma*B;
resid(3) = phi_S*N_S - K_S;

% collect all steady state values in a struct array
steady      = struct;
steady.R_D  = R_D;
steady.R_B  = R_B;
steady.R_K  = R_K;
steady.K_H  = K_H;
steady.K_R  = K_R;
steady.K_S  = K_S;
steady.K    = K;
steady.B    = B;
steady.D    = D;
steady.W    = W;
steady.N_H  = N_H;
steady.N_R  = N_R;
steady.N_S  = N_S;
steady.C_H  = C_H;
steady.phi_R = phi_R;
steady.phi_S = phi_S;
steady.Y    = K^p.alpha - p.eta_H/2*K_H^2 - p.eta_R/2*K_R^2;