% comparative statics with respect to theta and kappa
clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space for Theta and Kappa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

thetaVals = [1 2 4];
kappaVals = 250;

[thetaSpace,kappaSpace] = ndgrid(thetaVals,kappaVals);

vec_thetaSpace = vec(thetaSpace);
vec_kappaSpace = vec(kappaSpace);

nVals = length(vec_thetaSpace);

clear thetaSpace kappaSpace

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Other Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;
p.rho_Z     = 0.8;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.5;                  % Initial Capital Adjustment Cost
p.kappa     = 0;                   % Initial Asymmetry, Capital Adjustment Cost
p.alpha     = 0.95;                 % Production Function Curvature
p.beta      = 0.9902;               % Household Discount Factor
p.sigma_R   = 0.1109;               % Retail Bank Exit Probability
p.sigma_S   = 0.3110;               % Shadow Bank Exit Probability
p.delta     = 0.025;                % Depreciation Rate
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.phi_bar   = 1/0.08;               % Capital Requirement
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.psi       = 0.1336;               % Divertable Asset Share, Retail Banks
p.omega     = 0.4605;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.005;                % Exogenous Income of Bankers
p.nu        = 0.025;                % Probability of Bank Run Starting
p.pi        = 7/8;                  % Probability of Bank Run Continuing
p.sigma     = 1;                    % HH risk aversion
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling

%% Solve for the equilibrium

for kk = 1:nVals
    p.theta_max = vec_thetaSpace(kk);                  % Capital Adjustment Cost
    p.kappa_max = vec_kappaSpace(kk);                  % Asymmetry, Capital Adjustment Cost

    [pols,funs,grd,steady,states] = function_HH_RB_SB(p);

    [pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p);
end

%% Simulate

stored_Tables = cell(nVals,3);

for kk = 1:nVals
    p.theta_max = vec_thetaSpace(kk);                  % Capital Adjustment Cost
    p.kappa_max = vec_kappaSpace(kk);
    
    fileName = strcat('./Results/',date,'_No_Run_Solution_','theta_',num2str(p.theta_max),'_kappa_',num2str(p.kappa_max),'.mat');
    load(fileName)
    
    fileName = strcat('./Results/',date,'_Run_Solution_','theta_',num2str(p.theta_max),'_kappa_',num2str(p.kappa_max),'.mat');
    load(fileName)
    
    [SS_Bounds,Bank_Run_Statistics,Business_Cycle_Statistics] = ...
        function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady);

    stored_Tables(kk,1) = {Business_Cycle_Statistics};
    stored_Tables(kk,2) = {Bank_Run_Statistics};
    stored_Tables(kk,3) = {SS_Bounds};
end

save(strcat('./Results/',date,'Comparative_Statics_Theta_Kappa.mat'),'stored_Tables')

%%

if length(thetaVals) > 1 && length(kappaVals) == 1
    xVals = thetaVals;
    xNames = {'\theta'};
elseif length(thetaVals) == 1 && length(kappaVals) > 1
    xVals = kappaVals;
    xNames = {'\kappa'};
end

varSelect = [1 2 3 4 5 6];
iter = 0;

yVals = cell(length(varSelect),4);
yVals(:) = {ones(nVals,1)};

yNames = {'Q','Q*','x','\phi_R','\phi_S','I'};

for jj = varSelect
    iter = iter + 1;
    for kk = 1:nVals
    yVals{iter,1}(kk) = stored_Tables{kk, 2}.Mean(jj);
    yVals{iter,2}(kk) = stored_Tables{kk, 2}.Median(jj);
    yVals{iter,3}(kk) = stored_Tables{kk, 2}.p05(jj);
    yVals{iter,4}(kk) = stored_Tables{kk, 2}.p95(jj);
    end
end

% nor = @(x) (x/x(1));
nor = @(x) x;

figure
for jj = 1:length(varSelect)
    subplot(2,3,jj)
    plot(xVals,nor(yVals{jj,1}),'b','LineWidth',2)
    hold
    plot(xVals,nor(yVals{jj,2}),'r','LineWidth',2)
    plot(xVals,nor(yVals{jj,3}),'g','LineWidth',2)
    plot(xVals,nor(yVals{jj,4}),'k','LineWidth',2)
    legend('Mean','p50','p05','p95')
    title(yNames{jj})
    xlabel(xNames)
end

    