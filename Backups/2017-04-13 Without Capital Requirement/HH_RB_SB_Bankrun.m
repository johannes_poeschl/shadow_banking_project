% This file computes the bank run solution in a loop.
clear

usePreviousSolution_Run = 1;
dateVal = '12-Apr-2017';
par_name = 'gammaR';

load(strcat('./Results/','No_Run_Solution.mat'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reset Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p.theta     = 0.5;                    % Initial Capital Adjustment Cost
p.kappa     = 0;                  % Asymmetry, Capital Adjustment Cost

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solve Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
smol_vals = [pols_run.K_prime pols_run.I pols_run.Q pols_run.C_H];
plotNames = {'K''','I','Q','C_H'};

% smol_vals = [pols_run.phi_R pols_run.K_R_prime pols_run.V_R pols_run.Q];
% plotNames = {'\phi_R','K_R_''','V_R','I'};

% smol_vals = [pols_run.R_D_prime pols_run.R_B_prime pols_run.D_prime zeros(size(pols_run.D_prime))];
% plotNames = {'R_D''','R_B''','D''','B'''};

plot_vals = cell(4,1);
for ii = 1:4
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs_run,[20,1,1])};
end

hf1 = figure;
for ii = 1:length(plot_vals)
    subplot(2,2,ii)
    plot(plot_vals{ii});
    title(plotNames{ii})
    xlabel('N_R')
end

% Value Functions as a function of K and Z
plot_vals = cell(4,1);
for ii = 1:4
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs_run,[1,20,15])};
end

hf2 = figure;
for ii = 1:length(plot_vals)
    subplot(2,2,ii)
    surfc(plot_vals{ii});
    title(plotNames{ii})
    ylabel('K')
    xlabel('logZ')
end