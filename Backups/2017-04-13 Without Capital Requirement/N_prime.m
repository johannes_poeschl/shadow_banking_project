function [N_R_prime,N_S_prime] = N_prime(p, states, x0, additionalVars, ...
    nextStates, nextVals)
% HH_RB_SB_compute_N_prime computes the next period net worth

N_R = states(1,1);
N_S = states(1,2);
K   = states(1,3);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);

phi_R   = additionalVars(1);
phi_S   = additionalVars(2);
B_prime = additionalVars(4);
Q       = additionalVars(5);
R_D_prime = additionalVars(6);
R_B_prime = additionalVars(7);

K_prime = K_H_prime + K_R_prime + K_S_prime;
logZ_prime = nextStates(:,4)';

Q_prime = nextVals{4};

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
R_K_R_prime = R_K_prime/(Q + p.eta_R*K_R_prime/K);

% N'
N_R_prime = (((R_K_R_prime - R_D_prime)*phi_R + R_D_prime)*N_R + ...
    ((R_B_prime - R_D_prime) - p.gamma*(R_K_R_prime - R_D_prime))*B_prime)*(1-p.sigma_R) + ...
    p.exoInc*K;  % /////
N_S_prime = ((R_K_prime - R_B_prime)*phi_S + R_B_prime)*N_S*(1-p.sigma_S) + p.exoInc*K;  % /////
