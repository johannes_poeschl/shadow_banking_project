% Steady State calibration of the model

clear
clc

% Data targets
% R_D = 1.008;
% R_B = 1.0093;
% R_K = 1.0151;
% R_K_R =

R_D = 1.04^0.25;
R_B = 1.048^0.25;
R_K = 1.064^0.25;
R_K_R = 1.052^0.25;

K_H_K = 0.2; 
K_R_K = 0.4;
K_S_K = 1 - K_R_K - K_H_K;
phi_S = 20;
phi_R = 10;

p = struct;

% Parameter group 1
p.alpha = 0.95;
p.delta = 0.025;
p.gamma_R = 0.08;
p.exoInc  = 0.005;
p.mu_Z  = log((R_K - 1 + p.delta)/p.alpha);
% p.mu_Z  = 0;

% auxiliary variables
K = ((R_K - (1 - p.delta))/p.alpha/exp(p.mu_Z))^(1/(p.alpha-1));

% Parameter group 2 from Steady state conditions
p.beta      = 1/R_D;
p.eta_H     = (p.beta*R_K - 1)/K_H_K;
p.eta_R     = (R_K/R_K_R - 1)/K_R_K;
p.gamma     = (R_B - R_D)/(R_K_R - R_D);
p.sigma_S   = 1 - (1 - phi_S*p.exoInc/K_S_K)/((R_K - R_B)*phi_S + R_B);

N_S = K_S_K*K/phi_S;
n_S = (N_S - p.exoInc*K)/(1-p.sigma_S);
B = (phi_S - 1)*N_S;
N_R = 1/phi_R*((1 + p.eta_R*K_R_K)*K_R_K*K + p.gamma*B);

p.sigma_R   = 1 - (1 - ((R_B - R_D - p.gamma*(R_K_R - R_D))*B + p.exoInc*K)/N_R)/ ...
    ((R_K_R - R_D)*phi_R + R_D);

n_R = (N_R - p.exoInc*K)/(1-p.sigma_R);

p.psi       = 1/phi_R*p.beta*p.sigma_R*n_R/(N_R-p.beta*(1-p.sigma_R)*n_R);
p.omega     = 1/(phi_S - 1)*(p.beta*p.sigma_S*n_S/(p.psi*(N_S-p.beta*(1-p.sigma_S)*n_S)) - 1);

display(p)

