% This file computes the steady state of the new model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('./Smolyak_Anisotropic_JMMV_2014')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parName = 'eta_R';
parName_TeX = strcat('\',parName);

xLow = 0.15;
xHigh  = 0.5;

parSpace = linspace(1-xLow,1+xHigh,30);
nPars = length(parSpace);

vec_D       = zeros(nPars,1);
vec_B       = zeros(nPars,1);
vec_K_H     = zeros(nPars,1);
vec_K_R     = zeros(nPars,1);
vec_K_S     = zeros(nPars,1);
vec_K       = zeros(nPars,1);
vec_phi_R   = zeros(nPars,1);
vec_phi_S   = zeros(nPars,1);
vec_C_H     = zeros(nPars,1);
vec_C_R     = zeros(nPars,1);
vec_C_S     = zeros(nPars,1);
vec_Y       = zeros(nPars,1);
vec_R_K_spread = zeros(nPars,1);
vec_R_B_spread = zeros(nPars,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;
p.rho_Z     = 0.8;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.5;                  % Initial Capital Adjustment Cost
p.theta_max = 5;                    % Capital Adjustment Cost
p.kappa     = 0.5;                  % Asymmetry, Capital Adjustment Cost
p.kappa_max = 5;
p.alpha     = 0.36;                 % Production Function Curvature
p.beta      = 0.99;                 % Household Discount Factor
p.sigma_R   = 1 - 0.925/p.beta;     % Retail Bank Exit Probability
p.sigma_S   = 1 - 0.85/p.beta;      % Shadow Bank Exit Probability
p.delta     = 0.025;                % Depreciation Rate
p.eta_H     = 0.0279;                % Household Capital Holding Cost
p.eta_R     = 0.0140;               % Retail Bank Capital Holding Cost
p.phi_bar   = 1/0.08;               % Capital Requirement
p.gamma     = 0.19;                 % Relative Divertable Share of Interbank Loans
p.psi       = 0.17;                 % Divertable Asset Share, Retail Banks
p.omega     = 0.6;                 % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.0037;                  % Exogenous Income of Bankers
p.nu        = 0.025;                % Probability of Bank Run Starting
p.pi        = 7/8;                  % Probability of Bank Run Continuing
p.sigma     = 1;                    % Probability of Bank Run Continuing

p_baseline = p;

for ii = 1:length(parSpace)
    eval(strcat('p.',parName,'=',num2str(parSpace(ii)),'*','p_baseline.',parName,';'))

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Steady State
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    handle = @(x) computeSteadyState(x,p);    
    
    if ii > 1
        x0 = x1;
    else
        x0 = [1 10 10]';
    end
    
    [x1,~,exitFlag] = fsolve(handle,x0);
    
    if exitFlag ~= 1
        error('Could Not Find a Steady State')
    end

    [~,steady] = handle(x1);

    vec_D(ii)   = steady.D;
    vec_B(ii)   = steady.B;
    vec_K_H(ii) = steady.K_H/steady.K;
    vec_K_R(ii) = steady.K_R/steady.K;
    vec_K_S(ii) = steady.K_S/steady.K;
    vec_K(ii)   = steady.K;
    vec_phi_R(ii) = steady.phi_R;
    vec_phi_S(ii) = steady.phi_S;
    vec_C_H(ii) = steady.C_H;
    vec_C_R(ii) = p.sigma_R*steady.N_R;
    vec_C_S(ii) = p.sigma_S*steady.N_S;
    vec_Y(ii)   = steady.Y;
    vec_R_K_spread(ii) = steady.R_K^4 - steady.R_B^4;
    vec_R_B_spread(ii) = steady.R_B^4 - steady.R_D^4;
end

%% Plots

plotVals = [vec_phi_R vec_phi_S vec_R_K_spread vec_R_B_spread vec_K_H vec_K_R vec_K_S vec_K ...
    vec_C_H vec_C_R vec_C_S vec_Y];
plotNames = {'\phi_R','\phi_S','R_K - R_B','R_B - R_D','K_H','K_R','K_S','K','C_H','C_R','C_S','Y'};
eval(strcat('xVals = p_baseline.',parName,'*parSpace;'))

figure
for ii = 1:size(plotVals,2)
   subplot(3,4,ii)
   plot(xVals,plotVals(:,ii),'LineWidth',2)
   title(plotNames{ii})
   xlabel(parName_TeX)
   xlim([xVals(1) xVals(end)])
   yLim = ylim;
   ylim([yLim(1)-1e-4 yLim(2)+1e-4])
end

figureName = strcat('./Figures/Comparative_Statics_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [29.7 21];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')