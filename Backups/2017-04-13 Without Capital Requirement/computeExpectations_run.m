function [ nextVals_run ] = computeExpectations_run( nextStates_run,funs_run )

n_Z = size(nextStates_run,1);

% adjust the evaluation point to lie within the grid
nextStates_int = nextStates_run;
for ii = 1:size(nextStates_run,2)
    nextStates_int(:,ii) = bound(nextStates_run(:,ii),funs_run.orig_min(ii),funs_run.orig_max(ii));
end

nextStates_unit =  Rescale(nextStates_int,funs_run.unit_min,funs_run.unit_max, ...
    funs_run.orig_min,funs_run.orig_max);
nextStates_poly = Smolyak_Polynomial(nextStates_unit,funs_run.nDims, ...
    funs_run.max_approx_level,funs_run.elem);

% evaluate the interpolated functions
C_H_1 = (nextStates_poly*funs_run.C_H);
V_R_1 = (nextStates_poly*funs_run.V_R);
Q_1 = (nextStates_poly*funs_run.Q);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(nextStates_run - nextStates_int))) > 0
    df = 1e-2;
    
    nDims = 2;
    
    aux = eye(nDims);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(nextStates_int,nDims,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_minus =  Rescale(eval_point_plus,funs_run.unit_min,funs_run.unit_max, ...
        funs_run.orig_min,funs_run.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_minus,funs_run.nDims, ...
        funs_run.max_approx_level,funs_run.elem);

    C_H_1_plus = (eval_poly_plus*funs_run.C_H);
    V_R_1_plus = (eval_poly_plus*funs_run.V_R);
    Q_1_plus   = (eval_poly_plus*funs_run.Q);
    
    C_H_1_plus = reshape(C_H_1_plus,n_Z,nDims);
    V_R_1_plus = reshape(V_R_1_plus,n_Z,nDims);
    Q_1_plus = reshape(Q_1_plus,n_Z,nDims);
    
    C_H_1_diff = [(C_H_1_plus - repmat(C_H_1,1,nDims))./df zeros(n_Z,1)];
    V_R_1_diff = [(V_R_1_plus - repmat(V_R_1,1,nDims))./df zeros(n_Z,1)];
    Q_1_diff = [(Q_1_plus - repmat(Q_1,1,nDims))./df zeros(n_Z,1)];
    
    C_H_1 = C_H_1 + sum(C_H_1_diff.*(nextStates_run - nextStates_int),2);
    V_R_1 = V_R_1 + sum(V_R_1_diff.*(nextStates_run - nextStates_int),2);
    Q_1 = Q_1 + sum(Q_1_diff.*(nextStates_run - nextStates_int),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C_H_prime = max(C_H_1',1e-2);
V_R_prime = V_R_1';
Q_prime = max(Q_1',0);

nextVals_run = cell(3,1);
nextVals_run(1) = {C_H_prime};
nextVals_run(2) = {V_R_prime};
nextVals_run(3) = {Q_prime};

