function [pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p)

kappa_step = (p.kappa_max - p.kappa)/(25-1);
theta_step = (p.theta_max - p.theta)/(25-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Steady State (Alt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K = ((1.064^0.25 - (1 - p.delta))/p.alpha/exp(p.mu_Z))^(1/(p.alpha-1));

handle = @(x) computeSteadyState_noShadowBanks(x,p);

x0 = [0.2 0.4 0.0655]'*K;
x1 = fsolve(handle,x0);

[~,steady_no_shadow] = handle(x1);

% display(steady_no_shadow)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reset Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p.theta     = 0.5;                    % Initial Capital Adjustment Cost
p.kappa     = 10;                  % Asymmetry, Capital Adjustment Cost

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unit_min = funs.unit_min([1 3 4]);
unit_max = funs.unit_max([1 3 4]);
orig_min = funs.orig_min([1 3 4]);
orig_max = funs.orig_max([1 3 4]);

% 1 Smolyak approximation level
approx_level = [4 4 3];
max_approx_level = max(approx_level);
nDims = length(approx_level);

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states,1);
vec_N_R = states(:,1);
vec_K = states(:,2);
vec_Z = states(:,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess for the run solution (we use the no run solution)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% first, compute the smolyak polynomials on the no run grid
No_Run_Grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
No_Run_Poly = Smolyak_Polynomial(No_Run_Grid,funs.nDims,funs.max_approx_level,funs.elem);

coeffs_K_H = No_Run_Poly\pols.K_H_prime;
coeffs_K_R = No_Run_Poly\pols.K_R_prime;

coeffs_N_R = zeros(length(coeffs_K_H),grd.n_Z);

for jj = 1:grd.n_Z
    coeffs_N_R(:,jj) = No_Run_Poly\pols.N_R_prime(:,jj);
end

Run_Grid_Eval = [vec_N_R p.exoInc*vec_K vec_K vec_Z];
Run_Grid_Eval = Rescale(Run_Grid_Eval,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
Run_Poly_Eval = Smolyak_Polynomial(Run_Grid_Eval,funs.nDims,funs.max_approx_level,funs.elem);

vec_C_H_star       = Run_Poly_Eval*funs.C_H;
vec_V_R_star       = Run_Poly_Eval*funs.V_R;
% vec_Q_star         = Run_Poly_Eval*funs.Q;
vec_Q_star         = ones(size(vec_V_R_star));

vec_C_H_star_old       = vec_C_H_star;
vec_V_R_star_old       = vec_V_R_star;
vec_Q_star_old         = vec_Q_star;

vec_K_H_prime       = ones(n_total,1)*steady_no_shadow.K_H;
vec_K_R_prime       = ones(n_total,1)*steady_no_shadow.K_R;
vec_D_prime         = ones(n_total,1);
vec_R_D_prime_star  = ones(n_total,1);
vec_R_B_prime_star  = ones(n_total,1);
vec_phi_R_star      = ones(n_total,1); 

% LoMs
N_R_prime_new       = Run_Poly_Eval*coeffs_N_R;
N_R_prime_new_run   = Run_Poly_Eval*coeffs_N_R;
N_S_prime           = p.exoInc*repmat(vec_K,1,grd.n_Z);
vec_K_prime_new     = vec_K_H_prime + vec_K_R_prime;

N_R_prime_old = N_R_prime_new;
N_R_prime_old_run = N_R_prime_new_run;
vec_K_prime_old = vec_K_prime_new;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funs_run = struct;
funs_run.C_H = base_poly\vec_C_H_star; 
funs_run.V_R = base_poly\vec_V_R_star;
funs_run.Q   = base_poly\vec_Q_star;  

% help
funs_run.orig_min = orig_min;
funs_run.orig_max = orig_max;
funs_run.unit_min = unit_min;
funs_run.unit_max = unit_max;
funs_run.approx_level = approx_level;
funs_run.max_approx_level = max_approx_level;
funs_run.elem = elem_aniso;
funs_run.nDims = nDims;
funs_run.base_poly = base_poly;

clear orig_min orig_max unit_min unit_max approx_level max_approx_level ...
    elem nDims base_poly elem_iso elem_aniso unit_grid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

options = optimset('Display','off', ...
    'FinDiffType','forward', ...
    'TolFun',1e-6);

vError = 10;
nError = 10;
iter = 1;
maxIter = 250;
    
while (vError > 1e-4 || nError > 1e-3) && iter <= maxIter
tic2 = tic;
    for ii = 1:n_total
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Aggregate LoM for Z'
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states(ii,3) + p.sigma_Z*grd.eps_Z; % no adjustment necessary, since no shadow banks
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Next Period Value Functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        nextStates = [N_R_prime_old(ii,:)' N_S_prime(ii,:)' ...
            vec_K_prime_old(ii,1)*ones(size(logZ_prime)) logZ_prime];
        
        nextStates_run    = [N_R_prime_old_run(ii,:)' ...
            vec_K_prime_old(ii,1)*ones(size(logZ_prime)) logZ_prime];
        
        nextVals = computeExpectations(nextStates,funs);
        
        nextVals_run = computeExpectations_run(nextStates_run,funs_run);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the System of FOC
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x_prime) FOC_run(p,x_prime, ...
            states(ii,:),nextVals,nextVals_run,nextStates,grd.Z_prob');        

        x0 = [vec_K_H_prime(ii,1);
            vec_K_R_prime(ii,1)
            vec_C_H_star(ii,1)];

        [x_prime_new,~,~] = fsolve(handle,x0,options);      
            
        [~,additionalVars] = handle(x_prime_new);
%         x_prime_new = max(x_prime_new,0);
%         additionalVars = max(additionalVars,0);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Store Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        
        vec_phi_R_star(ii,1)        = additionalVars(1);
        vec_C_H_star(ii,1)          = additionalVars(2);
        vec_Q_star(ii,1)            = additionalVars(3);
        vec_R_D_prime_star(ii,1)    = additionalVars(4);
        vec_R_B_prime_star(ii,1)    = additionalVars(5)	;

        vec_K_prime_new(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Update the Net Worth
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [N_R_prime_new(ii,:),N_R_prime_new_run(ii,:)] = ...
             N_prime_run(p,states,x_prime_new,additionalVars,nextStates, ...
             nextVals,nextVals_run);
    end
    elapsedTime = toc(tic2);
    
    N_R_prime_new = bound(N_R_prime_new,0,funs_run.orig_max(1));
    N_R_prime_new_run = bound(N_R_prime_new_run,0,funs_run.orig_max(1));
%     vec_K_prime_new = bound(vec_K_prime_new,funs_run.orig_min(2),funs_run.orig_max(2));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Other Variables not needed in the Solution Algorithm
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_I = vec_K_prime_new - (1-p.delta)*vec_K;
    vec_D_prime = (vec_phi_R_star-1).*vec_N_R;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_V_R_star = (p.sigma_R + (1-p.sigma_R)*p.psi*vec_phi_R_star).*(vec_N_R - p.exoInc*vec_K)/(1-p.sigma_R);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check Convergence Criteria
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vError = norm([vec_C_H_star; vec_V_R_star] - [vec_C_H_star_old; vec_V_R_star_old],'inf');
    
    nError = norm([N_R_prime_new; N_R_prime_new_run] - ...
        [N_R_prime_old; N_R_prime_old_run],'inf');
    
    kError = norm(vec_K_prime_new - vec_K_prime_old,'inf');
    
%     if mod(iter,10) == 0  || iter == 1
        fprintf('Iteration %d: \n',iter);
        fprintf('Elapsed Time: %6.2f Seconds \n',elapsedTime);
        fprintf('Value Function Error: %6.6f \n',vError)
        fprintf('Net Worth Error %6.6f \n \n',max(kError,nError))
%     end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Updating Step
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    weight_V = 1;
    weight_N = 0.5;
    
    vec_C_H_star_old       = weight_V*vec_C_H_star + (1-weight_V)*vec_C_H_star_old;
    vec_V_R_star_old       = weight_V*vec_V_R_star + (1-weight_V)*vec_V_R_star_old;
        
    vec_K_prime_old         = weight_N*vec_K_prime_new + (1-weight_N)*vec_K_prime_old;
    vec_Q_star_old          = weight_N*vec_Q_star + (1-weight_N)*vec_Q_star_old;
    N_R_prime_old           = weight_N*N_R_prime_new + (1-weight_N)*N_R_prime_old;
    N_R_prime_old_run       = weight_N*N_R_prime_new_run + (1-weight_N)*N_R_prime_old_run;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Interpolate Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    funs_run.C_H = funs_run.base_poly\vec_C_H_star_old;  
    funs_run.V_R = funs_run.base_poly\vec_V_R_star_old; 
    funs_run.Q   = funs_run.base_poly\vec_Q_star_old;  
    
    if iter >= 25
        p.theta = min(p.theta + theta_step,p.theta_max);
        p.kappa = min(p.kappa + kappa_step,p.kappa_max);
    end
    iter = iter + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check the State Space Bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
minVals = [min(min(vec(N_R_prime_new)),min(vec(N_R_prime_new_run))) ...
    min(vec_K_prime_new)];
maxVals = [max(max(vec(N_R_prime_new)),max(vec(N_R_prime_new_run))) ...
    max(vec_K_prime_new)];

fprintf('Lower Bounds (<1 means violation): \n')
display(minVals./funs_run.orig_min(1:2))

fprintf('Upper Bounds (<1 means violation): \n')
display(funs_run.orig_max(1:2)./maxVals)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Store Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pols_run = struct;
pols_run.K_H_prime  = vec_K_H_prime;
pols_run.K_R_prime  = vec_K_R_prime;
pols_run.K_prime    = vec_K_prime_new;
pols_run.I          = vec_I;
pols_run.phi_R      = vec_phi_R_star;
pols_run.R_D_prime  = vec_R_D_prime_star;
pols_run.R_B_prime  = vec_R_B_prime_star;
pols_run.D_prime    = vec_D_prime;
pols_run.N_R_prime  = N_R_prime_new;
pols_run.C_H        = vec_C_H_star;
pols_run.V_R        = vec_V_R_star;
pols_run.Q          = vec_Q_star;
pols_run.N_R_prime_star = N_R_prime_new_run;

fileName = strcat('./Results/',date,'_Run_Solution_','theta_',num2str(p.theta),'_kappa_',num2str(p.kappa),'.mat');

save(fileName,'p','pols_run','funs_run')
save('initialGuess_Run','pols_run')
