% set simulation parameters
nBurnin = 500; 
nSimulations = nBurnin + 100;  % number of simulated periods 
nPanels = 2; %%%%% number of simulations for each capital requirement level

% draw shocks
seq_epsZ = zeros(nPanels,nSimulations);
seq_epsZ(1,nBurnin+25) = -2*p.sigma_Z;
seq_epsZ(2,nBurnin+25) = 2*p.sigma_Z;
seq_logZ = zeros(nPanels,nSimulations);
n_Z = 25;
[Z_grid,Z_prob] = tauchen(n_Z,0,p.rho_Z,p.sigma_Z,3);

for tt = 2:nSimulations  %%%%% for each simulation 
    seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + seq_epsZ(:,tt);
end

shocks.logZ = seq_logZ;
shocks.nu = normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
shocks.pi = normrnd(0,1,nPanels,nSimulations);
shocks.burnin = nBurnin;

% [TS,TS_add] = simulateModel_noRuns(p,shocks,funs,pols,steady);
[TS,TS_add] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

%% Plots

plotVals = cell(12,1);
plotVals(1) = {TS.logZ};
plotVals(2) = {TS_add.Y};
plotVals(3) = {TS_add.K_S_prime};
plotVals(4) = {TS_add.K_R_prime};
plotVals(5) = {TS_add.K_H_prime};
plotVals(6) = {TS_add.C_H};
plotVals(7) = {TS.Q};
plotVals(8) = {TS_add.R_B_prime - TS_add.R_D_prime};
plotVals(9) = {TS_add.phi_S};
plotVals(10) = {TS_add.phi_R};
plotVals(11) = {TS.N_S};
plotVals(12) = {TS.N_R};

plotNames = {'log(Z)','Y','K_S''','K_R''','K_H''','C_H','Q','R_B - R_D', ...
    '\phi_S','\phi_R','N_S','N_R'};

figure
for ii = 1:length(plotVals)
    subplot(3,4,ii)
    yVal = plotVals{ii};
    if ii ~= 1 && ii ~= 8
        plot(yVal(1,:)./yVal(1,1) - 1,':','LineWidth',2)
        hold
        plot(1- yVal(2,:)./yVal(2,1),'r','LineWidth',2)
    else
        plot(yVal(1,:) - yVal(1,1),':','LineWidth',2)
        hold
        plot(yVal(2,1) - yVal(2,:),'r','LineWidth',2)
    end
    title(plotNames{ii})
end
legend('Negative Shock','Positive Shock')


