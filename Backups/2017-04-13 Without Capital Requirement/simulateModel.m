function [TS,TS_add] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady)
% simulateModel simulates model time series
%
% Inputs:
%   - p:        struct array containing the parameters
%   - shocks:   struct array containing the shocks
%   - funs:     struct array of interpolation accessories
%   - pols:     struct array for the policy functions
%   - steady:   struct array containing the steady state
%
% Outputs:
%   - TS:       time series structure of the endogenous states

[nPanels,nSimulations] = size(shocks.logZ);

seq_logZ = shocks.logZ;

p.nu = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - no run case -
sz_array = [20 20 20 20];
nd_K_H_prime = smolyak_plot(pols.K_H_prime,funs,sz_array);
nd_K_R_prime = smolyak_plot(pols.K_R_prime,funs,sz_array);
nd_K_S_prime = smolyak_plot(pols.K_S_prime,funs,sz_array);
nd_R_D_prime = smolyak_plot(pols.R_D_prime,funs,sz_array);
nd_R_B_prime = smolyak_plot(pols.R_B_prime,funs,sz_array);
[nd_Q,cellVals] = smolyak_plot(pols.Q,funs,sz_array);

cell_grid = {cellVals{1},cellVals{2},cellVals{3},cellVals{4}};

int = struct;
int.K_H_prime = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime = griddedInterpolant(cell_grid,nd_K_R_prime);
int.K_S_prime = griddedInterpolant(cell_grid,nd_K_S_prime);
int.R_D_prime = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q       = griddedInterpolant(cell_grid,nd_Q);

% - run case -
sz_array = [20 20 10];
nd_K_H_prime = smolyak_plot(pols_run.K_H_prime,funs_run,sz_array);
nd_K_R_prime = smolyak_plot(pols_run.K_R_prime,funs_run,sz_array);
nd_R_D_prime = smolyak_plot(pols_run.R_D_prime,funs_run,sz_array);
nd_R_B_prime = smolyak_plot(pols_run.R_B_prime,funs_run,sz_array);
[nd_Q,cellVals] = smolyak_plot(pols_run.Q,funs_run,sz_array);

cell_grid = {cellVals{1},cellVals{2},cellVals{3}};

int.K_H_prime_star = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime_star = griddedInterpolant(cell_grid,nd_K_R_prime);
int.R_D_prime_star = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime_star = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q_star         = griddedInterpolant(cell_grid,nd_Q);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial Conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - states, no run -
seq_N_H = ones(nPanels, nSimulations)*steady.N_H;  % state
seq_N_R = ones(nPanels, nSimulations)*steady.N_R;  % state
seq_N_S = ones(nPanels, nSimulations)*steady.N_S;  % state 
seq_K   = ones(nPanels, nSimulations)*steady.K;    % state

% - states, run -
seq_N_H_star = ones(nPanels, nSimulations)*steady.N_H;    % state
seq_N_R_star = ones(nPanels, nSimulations)*steady.N_R;    % state
seq_N_S_star = seq_K*p.exoInc;  % state

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preallocation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice
seq_K_prime   = ones(nPanels, nSimulations);
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_I         = ones(nPanels, nSimulations);  % choice
seq_phi_R     = ones(nPanels, nSimulations);  % choice
seq_phi_S     = ones(nPanels, nSimulations);  % choice

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_old      = ones(nPanels, nSimulations);  % price, no run, guess
seq_Q_star     = ones(nPanels, nSimulations);  % price, run
seq_Q_star_old = ones(nPanels, nSimulations);  % price, run, guess

seq_R_K_prime_star = ones(nPanels, nSimulations);

seq_run           = zeros(nPanels, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_happened  = zeros(nPanels, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(nPanels, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(nPanels, nSimulations);   %%%%% whether a run starts in period iii
seq_recovery      = ones(nPanels, nSimulations);

qError = 10;

while qError > 1e-6
    
    for iii = 1: nSimulations
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % determine whether the run condition is met
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if iii > 1        
            seq_recovery(:,iii) = 0.95*seq_R_K_prime_star(:,iii-1).*seq_K_S_prime(:,iii-1)./ ...
                (seq_R_B_prime(:,iii-1).*seq_B_prime(:,iii-1));
            
            seq_run(:,iii) = seq_recovery(:,iii) < 1;
            
            seq_recovery(seq_B_prime(:,iii-1) == 0,iii) = 1;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % determine whether the economy is in a bank run
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        A = seq_run(:,iii).*(shocks.nu(:,iii) < norminv(p.nu));  % bankrun condition is met and coordination fails
        B = seq_run_persist(:, iii) == 1; % bankrun continues

        seq_run_happened(:,iii) =  1 - (1-A).*(1-B); %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
        seq_run_triggered(:,iii) = A.*(1-B);
        seq_run_persist(:, iii+1) = seq_run_happened(:,iii).*(shocks.pi(:,iii) < norminv(p.pi)); %%%%% whether bankrun persists to the next period
        
        seq_N_H(:, iii) = seq_N_H_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_H(:,iii).*(1-seq_run_happened(:,iii));
        seq_N_R(: ,iii) = bound(seq_N_R_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_R(:,iii).*(1-seq_run_happened(:,iii)),p.exoInc*seq_K(:,iii),2*funs.orig_max(1));
        seq_N_S(:, iii) = bound(seq_N_S_star(:,iii).*seq_run_happened(:,iii) + ...
                        seq_N_S(:,iii).*(1-seq_run_happened(:,iii)),p.exoInc*seq_K(:,iii),funs.orig_max(2));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % bank run policy functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
        eval_point = [seq_N_R(:,iii) seq_K(:,iii) seq_logZ(:,iii)];

        seq_K_H_prime_run = int.K_H_prime_star(eval_point);
        seq_K_R_prime_run = int.K_R_prime_star(eval_point);
        seq_R_D_prime_run = int.R_D_prime_star(eval_point);
        seq_R_B_prime_run = int.R_B_prime_star(eval_point);                         
        seq_Q_run         = int.Q_star(eval_point);

        seq_B_prime_run   = 0;
        seq_K_S_prime_run = 0;
        
        seq_phi_R_run = (seq_Q_run + ...
            p.eta_R*seq_K_R_prime_run./seq_K(:,iii)).*seq_K_R_prime_run./seq_N_R(:,iii); 
        seq_D_prime_run   = (seq_phi_R_run-1).*seq_N_R(:,iii); 

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % no bank run policy functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        eval_point = [seq_N_R(:,iii) seq_N_S(:, iii) seq_K(:,iii)  seq_logZ(:,iii)];

        seq_K_H_prime_no_run = int.K_H_prime(eval_point);
        seq_K_R_prime_no_run = int.K_R_prime(eval_point);
        seq_K_S_prime_no_run = int.K_S_prime(eval_point);           
        seq_R_D_prime_no_run = int.R_D_prime(eval_point);
        seq_R_B_prime_no_run = int.R_B_prime(eval_point);          
        seq_Q_no_run         = int.Q(eval_point);  
        
        seq_phi_S_no_run = seq_K_S_prime_no_run.*seq_Q_no_run./seq_N_S(:,iii);
        seq_B_prime_no_run   = (seq_phi_S_no_run - 1).*seq_N_S(:,iii);
        
        seq_phi_R_no_run = ((seq_Q_no_run + ...
            p.eta_R*seq_K_R_prime_no_run./seq_K(:,iii)).*seq_K_R_prime_no_run ...
            + p.gamma*seq_B_prime_no_run)./seq_N_R(:,iii);        
        seq_D_prime_no_run   = (seq_phi_R_no_run - 1).*seq_N_R(:,iii) + ...
            (1-p.gamma)*seq_B_prime_no_run;       
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % determine whether the run or no run policies are realized
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        seq_K_H_prime(:, iii) = seq_K_H_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_H_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_R_prime(:, iii) = seq_K_R_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_R_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_K_S_prime(:, iii) = seq_K_S_prime_run.*seq_run_happened(:,iii) + ...
            seq_K_S_prime_no_run.*(1-seq_run_happened(:,iii));              
        seq_R_D_prime(:, iii) = seq_R_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_D_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_R_B_prime(:, iii) = seq_R_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_R_B_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_D_prime(:, iii)   = seq_D_prime_run.*seq_run_happened(:,iii) + ...
            seq_D_prime_no_run.*(1-seq_run_happened(:,iii));          
        seq_B_prime(:, iii)   = seq_B_prime_run.*seq_run_happened(:,iii) + ...
            seq_B_prime_no_run.*(1-seq_run_happened(:,iii));
        seq_phi_R(:,iii) = seq_phi_R_run.*seq_run_happened(:,iii) + ...
            seq_phi_R_no_run.*(1-seq_run_happened(:,iii));
        seq_phi_S(:,iii) = ...
            seq_phi_S_no_run.*(1-seq_run_happened(:,iii));
        seq_Q(:, iii)         = seq_Q_run.*seq_run_happened(:,iii) + ...
            seq_Q_no_run.*(1-seq_run_happened(:,iii));
        
        seq_K_prime(:,iii) = seq_K_H_prime(:, iii) + seq_K_R_prime(:, iii) + seq_K_S_prime(:, iii);
        seq_I(:,iii)    =  seq_K_prime(:,iii) - (1-p.delta)*seq_K(:,iii);  
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % determine the counterfactual liquidation price of capital 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        eval_point = [seq_N_R_star(:,iii) seq_K(:,iii) seq_logZ(:,iii)];
        
        seq_Q_star(:,iii) = int.Q_star(eval_point);
        
        if iii == nSimulations
            break
        end
 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % update the state variables
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        seq_K(:, iii+1) = seq_K_prime(:,iii);  

        seq_R_K_prime(:, iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_old(:, iii + 1);
                      
        seq_R_K_prime_star(:,iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                          + (1-p.delta)*seq_Q_star_old(:, iii + 1);

        seq_W_prime(:, iii)  = (1-p.alpha) * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^p.alpha;

        seq_N_H(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_H_prime(:, iii) + ...
                             seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
                         
        seq_N_R(:, iii+1) =  (seq_R_K_prime(:, iii).*seq_K_R_prime(:, iii) ...
                  + seq_R_B_prime(:,iii).*seq_B_prime(:,iii) ...
                  - seq_R_D_prime(:, iii).*seq_D_prime(:, iii))*(1-p.sigma_R) ...
                  + p.exoInc*seq_K(:,iii+1);
              
        seq_N_S(:, iii+1) =  (seq_R_K_prime(:, iii).*seq_K_S_prime(:, iii) ...
                            - seq_R_B_prime(:, iii).*seq_B_prime(:, iii))*(1-p.sigma_S) ...
                            + p.exoInc*seq_K(:,iii+1);
      
        seq_N_H_star(:, iii+1) =  seq_R_K_prime_star(:, iii).*seq_K_H_prime(:, iii) + ...
                 seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);
             
        seq_N_R_star(:, iii+1) =  (seq_R_K_prime_star(:, iii).*seq_K_R_prime(:, iii) ...
                          + seq_recovery(:, iii).*seq_R_B_prime(:,iii).*seq_B_prime(:,iii) ...
                          - seq_R_D_prime(:, iii).*seq_D_prime(:, iii))*(1-p.sigma_R) ...
                          + p.exoInc*seq_K(:,iii);
                      
        seq_N_S_star(:, iii+1) = funs.orig_min(2)*ones(nPanels,1);   
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);
    seq_Q_star_old = seq_Q_star*weight + seq_Q_star_old*(1-weight);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq_Y = exp(seq_logZ).*seq_K.^(p.alpha) ...
    + 2*p.exoInc*seq_K ...
    - p.eta_H/2*seq_K_H_prime.^2./seq_K_prime ...
    - p.eta_R/2*seq_K_R_prime.^2./seq_K_prime;

seq_C_H = seq_Y - seq_Q.*seq_I ...
    - p.sigma_R*(seq_N_R - p.exoInc*seq_K)/(1 - p.sigma_R) ...
    - p.sigma_S*(seq_N_S - p.exoInc*seq_K)/(1 - p.sigma_S);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TS = struct;
TS.logZ     = seq_logZ(:,shocks.burnin+1:end);
TS.N_H      = seq_N_H(:,shocks.burnin+1:end);
TS.N_R      = seq_N_R(:,shocks.burnin+1:end);
TS.N_S      = seq_N_S(:,shocks.burnin+1:end);
TS.K        = seq_K(:,shocks.burnin+1:end);
TS.Q        = seq_Q_old(:,shocks.burnin+1:end);
TS.Q_star   = seq_Q_star_old(:,shocks.burnin+1:end);

TS_add = struct;
TS_add.Y            =  seq_Y(:,shocks.burnin+1:end);
TS_add.I            =  seq_I(:,shocks.burnin+1:end);
TS_add.C_H          =  seq_C_H(:,shocks.burnin+1:end);
TS_add.D_prime      =  seq_D_prime(:,shocks.burnin+1:end);
TS_add.B_prime      =  seq_B_prime(:,shocks.burnin+1:end);
TS_add.R_D_prime    =  seq_R_D_prime(:,shocks.burnin+1:end);
TS_add.R_B_prime    =  seq_R_B_prime(:,shocks.burnin+1:end);
TS_add.phi_R        =  seq_phi_R(:,shocks.burnin+1:end);
TS_add.phi_S        =  seq_phi_S(:,shocks.burnin+1:end);
TS_add.K_H_prime    =  seq_K_H_prime(:,shocks.burnin+1:end);
TS_add.K_R_prime    =  seq_K_R_prime(:,shocks.burnin+1:end);
TS_add.K_S_prime    =  seq_K_S_prime(:,shocks.burnin+1:end);
TS_add.recov        =  seq_recovery(:,shocks.burnin+1:end);