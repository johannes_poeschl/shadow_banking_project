function [TS,TS_add] = simulateModel_noRuns(p,shocks,funs,pols,steady)
% simulateModel simulates model time series
%
% Inputs:
%   - p:        struct array containing the parameters
%   - shocks:   struct array containing the shocks
%   - funs:     struct array of interpolation accessories
%   - pols:     struct array for the policy functions
%   - steady:   struct array containing the steady state
%
% Outputs:
%   - TS:       time series structure of the endogenous states

[nPanels,nSimulations] = size(shocks.logZ);

seq_logZ = shocks.logZ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% interpolate the policy functions on a rectangular grid
sz_array = [20 20 20 10];

tic
nd_K_H_prime = smolyak_plot(pols.K_H_prime,funs,sz_array);
nd_K_R_prime = smolyak_plot(pols.K_R_prime,funs,sz_array);
nd_K_S_prime = smolyak_plot(pols.K_S_prime,funs,sz_array);
nd_R_D_prime = smolyak_plot(pols.R_D_prime,funs,sz_array);
nd_R_B_prime = smolyak_plot(pols.R_B_prime,funs,sz_array);
[nd_Q,cellVals] = smolyak_plot(pols.Q,funs,sz_array);
elapsedTime = toc;

fprintf('Interpolation took %6.2f seconds \n',elapsedTime)

cell_grid = {cellVals{1},cellVals{2},cellVals{3},cellVals{4}};

int = struct;
int.K_H_prime = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime = griddedInterpolant(cell_grid,nd_K_R_prime);
int.K_S_prime = griddedInterpolant(cell_grid,nd_K_S_prime);
int.R_D_prime = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q       = griddedInterpolant(cell_grid,nd_Q);

% - states, no run -
seq_N_H = ones(nPanels, nSimulations)*steady.N_H;  % state
seq_N_R = ones(nPanels, nSimulations)*steady.N_R;  % state
seq_N_S = ones(nPanels, nSimulations)*steady.N_S;  % state 
seq_K   = ones(nPanels, nSimulations)*steady.K;    % state

seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice 
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_phi_S     = ones(nPanels, nSimulations);
seq_phi_R     = ones(nPanels, nSimulations);

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_old      = ones(nPanels, nSimulations);  % price, no run, guess

qError = 10;

while qError > 1e-6
    
    for iii = 1: nSimulations
        % no bank run stuff
        eval_point = [seq_N_R(:,iii) seq_N_S(:, iii) seq_K(:,iii)  seq_logZ(:,iii)];

        seq_K_H_prime(:, iii) = int.K_H_prime(eval_point);
        seq_K_R_prime(:, iii) = int.K_R_prime(eval_point);
        seq_K_S_prime(:, iii) = int.K_S_prime(eval_point);              
        seq_R_D_prime(:, iii) = int.R_D_prime(eval_point);
        seq_R_B_prime(:, iii) = int.R_B_prime(eval_point);          
        seq_Q(:, iii)         = int.Q(eval_point);
        
        seq_phi_S(:,iii) = seq_K_S_prime(:,iii).*seq_Q(:,iii)./seq_N_S(:,iii);
        seq_B_prime(:, iii)   = (seq_phi_S(:,iii) - 1).*seq_N_S(:,iii);
        
        seq_phi_R(:,iii) = ((seq_Q(:,iii) + p.eta_R*seq_K_R_prime(:,iii)./seq_K(:,iii)).*seq_K_R_prime(:,iii) ...
            + p.gamma*seq_B_prime(:,iii))./seq_N_R(:,iii);        
        seq_D_prime(:, iii)   = (seq_phi_R(:,iii) - 1).*seq_N_R(:,iii) + ...
            (1-p.gamma)*seq_B_prime(:,iii);                
        
        if iii < nSimulations
            seq_K(:, iii+1) = seq_K_H_prime(:, iii) + seq_K_R_prime(:, iii) + seq_K_S_prime(:, iii);  

            seq_R_K_prime(:, iii) = p.alpha * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^(p.alpha-1)...
                              + (1-p.delta)*seq_Q_old(:, iii + 1);

            seq_W_prime(:, iii)  = (1-p.alpha) * exp(seq_logZ(:, iii + 1)) .* seq_K(:, iii+1).^p.alpha;

            seq_N_H(:, iii+1) =  seq_R_K_prime(:, iii).*seq_K_H_prime(:, iii) + ...
                                 seq_R_D_prime(:, iii).*seq_D_prime(:, iii) + seq_W_prime(:, iii);

            seq_N_R(:, iii+1) =  (seq_R_K_prime(:, iii).*seq_K_R_prime(:, iii) + ...
                              seq_R_B_prime(:,iii).*seq_B_prime(:,iii) ...
                              - seq_R_D_prime(:, iii).*seq_D_prime(:, iii))*(1-p.sigma_R) ...
                              + p.exoInc*seq_K(:,iii);     

            seq_N_S(:, iii+1) =  (seq_R_K_prime(:, iii).*seq_K_S_prime(:, iii) -...
                                 seq_R_B_prime(:, iii).*seq_B_prime(:, iii))*(1-p.sigma_S) ...
                                + p.exoInc*seq_K(:,iii);
        end
    end
    
    qError = norm(seq_Q_old - seq_Q,'inf');
    fprintf('Simulation Error: %6.6f \n',qError);

    weight = 0.5;
    seq_Q_old = seq_Q*weight + seq_Q_old*(1-weight);     
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq_I = seq_K(:,2:end) - (1-p.delta)*seq_K(:,1:end-1);

seq_Y = exp(seq_logZ(:,2:end)).*seq_K(:,2:end).^(p.alpha) ...
    + 2*p.exoInc*seq_K(:,2:end) ...
    - p.eta_H/2*seq_K_H_prime(:,2:end).^2./seq_K(:,2:end) ...
    - p.eta_R/2*seq_K_R_prime(:,2:end).^2./seq_K(:,2:end);

seq_C_H = seq_Y - seq_Q(:,2:end).*seq_I;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TS = struct;
TS.logZ   = seq_logZ(:,shocks.burnin+1:end);
TS.N_H = seq_N_H(:,shocks.burnin+1:end);
TS.N_R = seq_N_R(:,shocks.burnin+1:end);
TS.N_S = seq_N_S(:,shocks.burnin+1:end);
TS.K = seq_K(:,shocks.burnin+1:end);
TS.Q = seq_Q_old(:,shocks.burnin+1:end);

TS_add = struct;
TS_add.Y    =  seq_Y(:,shocks.burnin:end);
TS_add.I    =  seq_I(:,shocks.burnin:end);
TS_add.C_H  =  seq_C_H(:,shocks.burnin:end);
TS_add.D_prime    =  seq_D_prime(:,shocks.burnin+1:end);
TS_add.B_prime    =  seq_B_prime(:,shocks.burnin+1:end);
TS_add.R_D_prime  =  seq_R_D_prime(:,shocks.burnin+1:end);
TS_add.R_B_prime  =  seq_R_B_prime(:,shocks.burnin+1:end);
TS_add.phi_R      =  seq_phi_R(:,shocks.burnin+1:end);
TS_add.phi_S      =  seq_phi_S(:,shocks.burnin+1:end);
TS_add.K_H_prime  =  seq_K_H_prime(:,shocks.burnin+1:end);
TS_add.K_R_prime  =  seq_K_R_prime(:,shocks.burnin+1:end);
TS_add.K_S_prime  =  seq_K_S_prime(:,shocks.burnin+1:end);