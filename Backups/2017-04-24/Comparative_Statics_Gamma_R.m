% comparative statics with respect to theta and kappa
clear
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space for Theta and Kappa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Gamma_R_Vals = 0.05:0.01:0.2;

nVals = length(Gamma_R_Vals);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Other Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;
p.rho_Z     = 0.8;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.beta      = 0.9902;               % Household Discount Factor
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.delta     = 0.025;                % Depreciation Rate
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.gamma_bar = p.gamma;
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers
p.nu        = 0.025;                % Probability of Bank Run Starting
p.pi        = 7/8;                  % Probability of Bank Run Continuing
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.95;                 % Production Function Curvature
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling
p.theta_max = 20;                    % Capital Adjustment Cost
p.kappa_max = 0;                  % Asymmetry, Capital Adjustment Cost

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for kk = 1:nVals
    
    p.phi_bar   = 1/Gamma_R_Vals(kk);               % Capital Requirement

    [pols,funs,grd,steady,states] = function_HH_RB_SB(p);

    [pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p);
    
    save(strcat('./Results/Results_GammaR_',num2str(Gamma_R_Vals(kk)),'.mat'), ...
        'pols','funs','pols_run','funs_run','p','steady')

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stored_Tables = cell(nVals,3);

for kk = 1:nVals   
    load(strcat('./Results/Results_GammaR_',num2str(Gamma_R_Vals(kk)),'.mat'))
    
    p.gamma_bar = p.gamma;
    
    [SS_Bounds,Bank_Run_Statistics,Business_Cycle_Statistics] = ...
    function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady);

    stored_Tables(kk,1) = {Business_Cycle_Statistics};
    stored_Tables(kk,2) = {Bank_Run_Statistics};
    stored_Tables(kk,3) = {SS_Bounds};
    
    save(strcat('./Results/Moments_GammaR_',num2str(Gamma_R_Vals(kk)),'.mat'), ...
        'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
xNames = {'\alpha'};

varSelect = [1 2 3 4 5 6];
iter = 0;

yVals = cell(length(varSelect),4);
yVals(:) = {ones(nVals,1)};

yNames = {'Q','Q*','x','\phi_R','\phi_S','I'};

for jj = varSelect
    iter = iter + 1;
    for kk = 1:nVals
    yVals{iter,1}(kk) = stored_Tables{kk, 2}.Mean(jj);
    yVals{iter,2}(kk) = stored_Tables{kk, 2}.Median(jj);
    yVals{iter,3}(kk) = stored_Tables{kk, 2}.p05(jj);
    yVals{iter,4}(kk) = stored_Tables{kk, 2}.p95(jj);
    end
end

% nor = @(x) (x/x(1));
nor = @(x) x;

figure
for jj = 1:length(varSelect)
    subplot(2,3,jj)
    plot(xVals,nor(yVals{jj,1}),'b','LineWidth',2)
    hold
    plot(xVals,nor(yVals{jj,2}),'r','LineWidth',2)
    plot(xVals,nor(yVals{jj,3}),'g','LineWidth',2)
    plot(xVals,nor(yVals{jj,4}),'k','LineWidth',2)
    legend('Mean','p50','p05','p95')
    title(yNames{jj})
    xlabel(xNames)
end