function [resid,steady] = computeSteadyState_noShadowBanks(x,p)

K_H = x(1);
K_R = x(2);
N_R = x(3);

% Market Clearing
K = K_H + K_R;
exoInc = p.exoInc*K;

% Final Goods Producer
r_K = p.alpha*exp(p.mu_Z)*K^(p.alpha-1);
W = (1-p.alpha)*exp(p.mu_Z)*K^p.alpha;

% Returns
R_D     = 1/p.beta;
R_K     = r_K + 1 - p.delta;
R_K_H   = R_K/(1 + p.eta_H*K_H/K);
R_K_R   = R_K/(1 + p.eta_R*K_R/K);

% Retail Bank
f_R     = p.eta_R*K_R/K;
n_R     = (N_R - exoInc)/(1-p.sigma_R);
phi_R   = p.beta*p.sigma_R*n_R/(p.psi*(N_R-p.beta*(1-p.sigma_R)*n_R));
D       = (phi_R - 1)*N_R;

% Household
N_H = (r_K + 1 - p.delta)*K_H + R_D*D + W;
Y = exp(p.mu_Z)*K^p.alpha + exoInc - p.eta_H/2*K_H^2/K - p.eta_R/2*K_R^2/K;
C_H = Y - p.delta*K - p.sigma_R*n_R;

resid    = zeros(3,1);
resid(1) = p.beta*R_K_H - 1;
resid(2) = phi_R*N_R - (1 + f_R)*K_R;
resid(3) = N_R - (((R_K_R - R_D)*phi_R + R_D)*N_R)*(1-p.sigma_R) - exoInc;

% collect all steady state values in a struct array
steady      = struct;
steady.R_D  = R_D;
steady.R_K  = R_K;
steady.K_H  = K_H;
steady.K_R  = K_R;
steady.K    = K;
steady.D    = D;
steady.W    = W;
steady.N_H  = N_H;
steady.N_R  = N_R;
steady.C_H  = C_H;
steady.phi_R = phi_R;
steady.Y    = Y;