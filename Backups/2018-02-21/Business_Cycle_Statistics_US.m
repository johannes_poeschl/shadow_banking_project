clear

load('Business_Cycle_Data_US')

%define a start date and stop date
startDate = datenum('1957-01-01');
stopDate  = datenum('2007-12-31');
dateRange = datenum(Date);
isInSample = dateRange >= startDate & dateRange <= stopDate;

% collect data in an array
yy = [log(GDP(isInSample)) log(Consumption(isInSample)) log(Investment(isInSample))  ...
    log(BankLending(isInSample)) cell2mat(BAA_Spread(isInSample))/100];

% detrend
yy_trend = hpfilter2(yy,1600);
yy_cycle = yy - yy_trend;

% statistics of interest
stDevs = std(yy_cycle)';
stDevs(2:end-1) = stDevs(2:end-1)/stDevs(1);
corrMat = corr(yy_cycle);
corrWithGDP = corrMat(:,1);
autoCorrs = zeros(size(yy,2),1);
for ii = 1:size(yy,2)
    autoCorrs(ii) = corr(yy_cycle(2:end,ii),yy_cycle(1:end-1,ii));
end

% Output in a table
rowNames = {'Y','C','I','K_R','R_K - R_D'};
varNames = {'StDevs','CorrWithGDP','AutoCorr'};
DataStatistics = table(stDevs,corrWithGDP,autoCorrs,'RowNames',rowNames,'VariableNames',varNames);

save('DataStatistics','DataStatistics')

input = struct;
input.data = table2array(DataStatistics);
input.tableColLabels = varNames;
input.tableRowLabels = rowNames;
input.booktabs = 1;
lTable = latexTable(input);