% comparative statics with respect to theta and kappa
clear
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
solveModel = 1;
simulModel = 1;
anticipated_runs = 1;

reg_order = {'static','static','run_exempt'};
run_order = {'no_runs','with_runs','with_runs'};
Gamma_R_Vals = [1e-4 0.01:0.01:0.2];

% reg_order = {'static'};
% run_order = {'no_runs'};
% Gamma_R_Vals = [1e-4 0.01:0.01:0.19];

% reg_order = {'run_exempt'};
% run_order = {'with_runs'};
% Gamma_R_Vals = [1e-4 0.01:0.01:0.2];

% reg_order = {'static'};
% run_order = {'with_runs'};
% Gamma_R_Vals = 1e-4;

for xx = 1:length(reg_order)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space for phi_R_bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% types of capital requirements: static, run_exempt, dynamic
is_reg = reg_order{xx};

% types of runs: no_runs, with_runs
is_run = run_order{xx};

nVals = length(Gamma_R_Vals);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Other Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = struct;

% Parameters chosen to match steady state properties of the model
p.beta      = 0.9902;               % Household Discount Factor
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers

% Parameters chosen to match dynamics
p.rho_Z     = 0.9;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.theta_max = 10;                    % Capital Adjustment Cost
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.kappa_max = 0;                    % Asymmetry, Capital Adjustment Cost

% Standard Parameters
p.delta     = 0.025;                % Depreciation Rate
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.36;                 % Production Function Curvature

% Bank Run Parameters
if strcmp(is_run,'with_runs') == 1
    p.sigma_xi  = 0.01875;
%     p.sigma_xi  = 0.01825;
%     p.sigma_xi  = 0.0125;
elseif strcmp(is_run,'no_runs') == 1
    p.sigma_xi  = 0;
end
p.pi        = 12/13;                % Probability of Bank Run Continuing
p.tau       = 0;

% regulation parameters
p.gamma_bar = p.gamma;
if strcmp(is_reg,'static') == 1 || strcmp(is_reg,'run_exempt') == 1
    p.phi_Q = 0;
elseif strcmp(is_reg,'countercyclical') == 1
    p.phi_Q = -1;
elseif strcmp(is_reg,'procyclical') == 1
    p.phi_Q = 0.5;
end

p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling

if solveModel == 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    delete('initialGuess.mat','initialGuess_Run.mat')

    if exist(strcat('./Results/',is_reg,'_',is_run),'dir') == 0
        mkdir(strcat('./Results/',is_reg,'_',is_run))
    end

    for kVal = 1:nVals
        p.phi_bar   = 1/Gamma_R_Vals(kVal);               % Capital Requirement
        
        if anticipated_runs == 0 || (anticipated_runs == 1 && kVal == 1)
            [pols,funs,grd,steady,states] = function_HH_RB_SB(p,is_reg,is_run);
        
            [pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p,is_reg,is_run);
        end
        
        if anticipated_runs == 1 && strcmp(is_run,'with_runs') == 1
            [pols,pols_run,funs,funs_run,grd,steady,states_no_run,states_run] = function_HH_RB_SB_anti(p,is_reg,is_run);
        end
        
        save(strcat('./Results/',is_reg,'_',is_run,'/Results_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
            'pols','funs','pols_run','funs_run','p','steady')

    end
end

if simulModel == 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    delete('shocks.mat')    

    for kVal = 1:nVals     
        load(strcat('./Results/',is_reg,'_',is_run,'/Results_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'))
        
        [SS_Bounds,Bank_Run_Statistics,Business_Cycle_Statistics,Welfare_Table] = ...
        function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,is_run);

        save(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
            'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stored_Tables = cell(nVals,4);
for kVal = 1:nVals
    load(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
        'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')
    
    stored_Tables(kVal,1) = {Business_Cycle_Statistics};
    stored_Tables(kVal,2) = {Bank_Run_Statistics};
    stored_Tables(kVal,3) = {SS_Bounds};
    stored_Tables(kVal,4) = {Welfare_Table};
end  

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'\Gamma^R'};

varSelect = [1 2 3 4 5 6];
iter = 0;

yVals = cell(length(varSelect),4);
yVals(:) = {ones(nVals,1)};

yNames = {'Q','Q*','x','\phi_R','\phi_S','I'};

for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
    yVals{iter,1}(kVal) = stored_Tables{kVal, 2}.Mean(jRow);
    yVals{iter,2}(kVal) = stored_Tables{kVal, 2}.Median(jRow);
    yVals{iter,3}(kVal) = stored_Tables{kVal, 2}.p05(jRow);
    yVals{iter,4}(kVal) = stored_Tables{kVal, 2}.p95(jRow);
    end
end

% nor = @(x) (x/x(1));
nor = @(x) x;

figure
for jRow = 1:length(varSelect)
    subplot(2,3,jRow)
    plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
    hold
    plot(xVals,nor(yVals{jRow,2}),'r','LineWidth',2)
    plot(xVals,nor(yVals{jRow,3}),'g','LineWidth',2)
    plot(xVals,nor(yVals{jRow,4}),'k','LineWidth',2)
    legend('Mean','p50','p05','p95')
    title(yNames{jRow})
    xlabel(xNames)
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

if exist(strcat('./Figures/',is_reg,'_',is_run),'dir') == 0
    mkdir(strcat('./Figures/',is_reg,'_',is_run))
end

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Comparative_Statics');
print(figureName,'-dpdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'$1/\bar{\phi}$'};

varSelect = [1 2 3];
iter = 0;

yVals = cell(length(varSelect),1);
yVals(:) = {ones(nVals,1)};

yNames = {'Welfare, HH','Welfare, RB','Welfare, SB','Bank Run Prob'};

for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
        yVals{iter,1}(kVal) = stored_Tables{kVal, 4}.Welfare(jRow);
    end
end

for kVal = 1:nVals
    yVals{4,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
end

nor = @(x) 100*(x/x(1) - 1);
% nor = @(x) x;

for jRow = 1:4
    subplot(2,2,jRow)
    if jRow < 4
        plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
        ylabel('% \Delta vs Baseline')
    else
        plot(xVals,100*yVals{jRow,1},'b','LineWidth',2)
        ylabel('%')
    end
    title(yNames{jRow})
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,'Interpreter','latex')  
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Welfare');
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bank Run Prob Only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'$1/\bar{\phi}$'};

yVals = cell(length(varSelect),1);
yVals(:) = {ones(nVals,1)};

yNames = {'Bank Run Probability E[p_t]'};

for kVal = 1:nVals
    yVals{1,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
end

figure
for jRow = 1:1
    plot(xVals,100*yVals{jRow,1},'b','LineWidth',2)
    ylabel('%')
    title(yNames{jRow})
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,'Interpreter','latex')  
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Bank_Run_Prob_',is_reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% "Optimal" Capital Requirements
close

welf = struct;
welf.HH = yVals{1};
welf.RB = yVals{2};
welf.SB = yVals{3};

% if regulator only cares about households
[val_HH_1,pos_HH_1] = max(welf.HH);

welf.opt_CR_1 = Gamma_R_Vals(pos_HH_1);
welf.change_HH_1 = 100*(val_HH_1/welf.HH(1) - 1);
welf.change_RB_1 = 100*(welf.RB(pos_HH_1)/welf.RB(1) - 1);
welf.change_SB_1 = 100*(welf.SB(pos_HH_1)/welf.SB(1) - 1);

% if regulator cares about all agents equally
[val_HH_2,pos_HH_2] = max(welf.HH + welf.RB + welf.SB);

welf.opt_CR_2 = Gamma_R_Vals(pos_HH_2);
welf.change_HH_2 = 100*(val_HH_2/welf.HH(1) - 1);
welf.change_RB_2 = 100*(welf.RB(pos_HH_2)/welf.RB(1) - 1);
welf.change_SB_2 = 100*(welf.SB(pos_HH_2)/welf.SB(1) - 1);
end