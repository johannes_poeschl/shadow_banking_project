% comparative statics with respect to theta and kappa
clear
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space for Theta and Kappa
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Gamma_R_Vals = 0.01:0.01:0.15;
phi_R_Vals = 1./Gamma_R_Vals;

nVals = length(Gamma_R_Vals);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Other Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;

% Parameters chosen to match steady state properties of the model
p.beta      = 0.9902;               % Household Discount Factor
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers

% Parameters chosen to match dynamics
p.rho_Z     = 0.8;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.theta_max = 15;                   % Capital Adjustment Cost
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.kappa_max = 0;                    % Asymmetry, Capital Adjustment Cost

% Standard Parameters
p.delta     = 0.025;                % Depreciation Rate
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.36;                 % Production Function Curvature

% Bank Run Parameters
p.nu        = 0.025;                % Probability of Bank Run Starting
p.pi        = 0;                  % Probability of Bank Run Continuing

p.gamma_bar = p.gamma;
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for kVal = 1:nVals
    
    p.phi_bar   = phi_R_Vals(kVal);               % Capital Requirement

    [pols,funs,grd,steady,states] = function_HH_RB_SB(p);

    [pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p);
    
    save(strcat('./Results/Results_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
        'pols','funs','pols_run','funs_run','p','steady')

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stored_Tables = cell(nVals,4);

for kVal = 1:nVals   
    load(strcat('./Results/Results_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'))
    
    p.gamma_bar = p.gamma;
    
    [SS_Bounds,Bank_Run_Statistics,Business_Cycle_Statistics,Welfare_Table] = ...
    function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady);

    stored_Tables(kVal,1) = {Business_Cycle_Statistics};
    stored_Tables(kVal,2) = {Bank_Run_Statistics};
    stored_Tables(kVal,3) = {SS_Bounds};
    stored_Tables(kVal,4) = {Welfare_Table};
    
    save(strcat('./Results/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
        'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'\Gamma^R'};

varSelect = [1 2 3 4 5 6];
iter = 0;

yVals = cell(length(varSelect),4);
yVals(:) = {ones(nVals,1)};

yNames = {'Q','Q*','x','\phi_R','\phi_S','I'};

for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
    yVals{iter,1}(kVal) = stored_Tables{kVal, 2}.Mean(jRow);
    yVals{iter,2}(kVal) = stored_Tables{kVal, 2}.Median(jRow);
    yVals{iter,3}(kVal) = stored_Tables{kVal, 2}.p05(jRow);
    yVals{iter,4}(kVal) = stored_Tables{kVal, 2}.p95(jRow);
    end
end

% nor = @(x) (x/x(1));
nor = @(x) x;

figure
for jRow = 1:length(varSelect)
    subplot(2,3,jRow)
    plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
    hold
    plot(xVals,nor(yVals{jRow,2}),'r','LineWidth',2)
    plot(xVals,nor(yVals{jRow,3}),'g','LineWidth',2)
    plot(xVals,nor(yVals{jRow,4}),'k','LineWidth',2)
    legend('Mean','p50','p05','p95')
    title(yNames{jRow})
    xlabel(xNames)
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Comparative_Statics');
print(figureName,'-dpdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'\Gamma^R'};

varSelect = [1 2 3];
iter = 0;

yVals = cell(length(varSelect),1);
yVals(:) = {ones(nVals,1)};

yNames = {'Welfare, HH','Welfare, RB','Welfare, SB','Bank Run Prob'};

for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
        yVals{iter,1}(kVal) = stored_Tables{kVal, 4}.Welfare(jRow);
    end
end

for kVal = 1:nVals
    yVals{4,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
end

nor = @(x) 100*(x/x(1) - 1);
% nor = @(x) x;

figure
for jRow = 1:4
    subplot(2,2,jRow)
    plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
    title(yNames{jRow})
    xlim([min(xVals) max(xVals)])
    xlabel(xNames)
    ylabel('% \Delta vs Baseline')
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Welfare');
print(figureName,'-dpdf')