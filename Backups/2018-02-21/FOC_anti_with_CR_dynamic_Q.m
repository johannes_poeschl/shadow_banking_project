function [foc_residual,additionalVars] = FOC_anti_with_CR_dynamic_Q(p, x0, states, ...
    nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob,x_prime,p_prime,Q)
% HH_RB_SB_compute_residual computes the FOC residuals
%
% Inputs:
%   - p             struct array containing parameters
%   - x0            8x1 column vector of unknown choice variables
%   - states        1x4 row vector of state variables
%   - logZ_prime    1xZ row vector of future log productivity realizations
%   - Z_prob        1xZ row vector of probabilities of future productivity
%                   realizations
%   - funs          struct array that contains interpolation coefficients
%                   and other information required for interpolation
%   - N_H_prime_old 1xZ vector of future household net worth realizations
%   - N_R_prime_old 1xZ vector of future retail bank net worth realizations
%   - N_S_prime_old 1xZ vector of future shadow bank net worth realizations
%   - ind           1x1 scalar case indicator
%
% Outputs:
%   - foc_residual  8x1 column vector of FOC residuals

p.gamma = p.gamma_bar;

N_R = states(1,1);
N_S = states(1,2);
K   = states(1,3);
logZ = states(1,4);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);
C_H       = x0(4);

N_R_prime = nextStates(:,1)';
N_S_prime = nextStates(:,2)';
K_prime = K_H_prime + K_R_prime + K_S_prime;
logZ_prime = nextStates(:,4)';

N_R_prime_star = nextStates_Run(:,1)';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I           = K_prime - (1-p.delta)*K;

adj_cost       	= p.theta/2*(I/K - p.delta)^2*K;

phi_S       = Q*K_S_prime/N_S;
B_prime     = (phi_S - 1)*N_S;
phi_R       = ((Q + p.eta_R*K_R_prime/K)*K_R_prime + p.gamma*B_prime)/N_R;
Y           = exp(logZ)*K^p.alpha + 2*p.exoInc*K ...
    - p.eta_H/2*(K_H_prime/K)^2*K ...
    - p.eta_R/2*(K_R_prime/K)^2*K;
r_K_prime   = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C_H_prime = nextVals{1};
V_R_prime = nextVals{2};
V_S_prime = nextVals{3};
Q_prime   = nextVals{4};

C_H_prime_star  = nextVals_Run{1};
V_R_prime_star  = nextVals_Run{2};
Q_prime_star    = nextVals_Run{3};

R_K_prime = r_K_prime + (1-p.delta)*Q_prime;
R_K_prime_star = r_K_prime + (1-p.delta)*Q_prime_star;

EV_K_H = sum(Z_prob.*((1-p_prime).*C_H_prime.^(-p.sigma).*R_K_prime + ...
    p_prime.*C_H_prime_star.^(-p.sigma).*R_K_prime_star));
EC_H = sum(Z_prob.*((1-p_prime).*C_H_prime.^(-p.sigma) + ...
    p_prime.*C_H_prime_star.^(-p.sigma)));
EV_R = sum(Z_prob.*(p_prime.*V_R_prime_star + (1-p_prime).*V_R_prime));

n_R_prime = (N_R_prime - p.exoInc*K_prime)/(1-p.sigma_R);
n_S_prime = (N_S_prime - p.exoInc*K_prime)/(1-p.sigma_S);
n_R_prime_star = (N_R_prime_star - p.exoInc*K_prime)/(1-p.sigma_R);

Omega_R = V_R_prime./n_R_prime;
Omega_R(n_R_prime <= 0) = 1;
Omega_R_star = V_R_prime_star./n_R_prime_star;
Omega_R_star(n_R_prime_star <= 0) = 1;

Omega_S = V_S_prime./n_S_prime;
Omega_S(n_S_prime <= 0) = 1;
Omega_S_star = 1;

E_Omega = sum(Z_prob.*((1-p_prime).*Omega_R + p_prime.*Omega_R_star));
E_x_prime = sum(Z_prob.*(p_prime.*Omega_R_star.*x_prime + (1-p_prime).*Omega_R));
E_R_K_prime_star = sum(Z_prob.*R_K_prime_star);

E_R_K_R_prime = sum(Z_prob.* ...
    ((1-p_prime).*Omega_R.*R_K_prime + p_prime.*Omega_R_star.*R_K_prime_star)) ...
    /(Q + p.eta_R*K_R_prime/K);

E_R_K_S_prime = sum(Z_prob.* ...
    ((1-p_prime).*Omega_S.*R_K_prime + p_prime.*Omega_S_star.*R_K_prime_star)) ...
    /Q;

EV_S = sum(Z_prob.*(1-p_prime).*V_S_prime);

R_D_prime = C_H^(-p.sigma)/(p.beta * EC_H);
R_B_prime = (E_R_K_R_prime*p.gamma + R_D_prime.*E_Omega*(1-p.gamma))./E_x_prime;

foc_residual = zeros(4,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(1) = C_H^(-p.sigma)*(Q + p.eta_H*K_H_prime/K) - ...
    p.beta * EV_K_H;

foc_residual(2) = C_H - (Y - I - adj_cost ...
    - p.sigma_R*(N_R - p.exoInc*K)/(1-p.sigma_R) ...
    - p.sigma_S*(N_S - p.exoInc*K)/(1-p.sigma_S));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(3) = phi_R - p.phi_bar*Q^p.phi_Q;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(4) = p.psi*(phi_S*p.omega + (1-p.omega))*N_S - p.beta*EV_S;

additionalVars = zeros(11,1);
additionalVars(1) = phi_R;
additionalVars(2) = phi_S;
additionalVars(3) = C_H;
additionalVars(4) = B_prime;
additionalVars(5) = Q;
additionalVars(6) = R_D_prime;
additionalVars(7) = R_B_prime;
additionalVars(8) = E_R_K_R_prime;
additionalVars(9) = E_R_K_S_prime;
additionalVars(10) = E_R_K_prime_star;
additionalVars(11) = p.beta*EV_R;
additionalVars(12) = p.beta*EV_S;