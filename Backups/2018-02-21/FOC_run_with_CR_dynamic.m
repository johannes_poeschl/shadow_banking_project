function [foc_residual,additionalVars] = FOC_run_with_CR_dynamic(p, x0, states, ...
    nextVals, nextVals_run, nextStates, nextStates_run, Z_prob)
% HH_RB_SB_compute_residual_run computes the F.O.C. residuals in a run.

p.gamma = p.gamma_bar;

N_R = states(1,1);
K   = states(1,2);
logZ = states(1,3);

K_H_prime = x0(1);
K_R_prime = x0(2);
C_H_star  = x0(3);

N_R_prime = nextStates(:,1)';
N_R_prime_star = nextStates_run(:,1)';
K_prime = K_H_prime + K_R_prime;
logZ_prime = nextStates(:,4)';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I           = K_prime - (1-p.delta)*K;

if p.kappa == 0
    Q_star          = 1 + p.theta*(I/K - p.delta);
    adj_cost       	= p.theta/2*(I/K - p.delta)^2*K;
else
    Q_star          = 1 + p.theta/p.kappa*(1 - exp(-p.kappa*(I/K - p.delta)));
    adj_cost        = p.theta/p.kappa^2*(exp(-p.kappa*(I/K - p.delta)) + ...
        p.kappa*(I/K - p.delta) - 1)*K;
end

phi_R_star       = ((Q_star + p.eta_R*K_R_prime/K)*K_R_prime)/N_R;
Y           = exp(logZ)*K^p.alpha;

r_K_prime   = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha-1);
W           = (1-p.alpha)*exp(logZ)*K^p.alpha;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C_H_prime = nextVals{1};
V_R_prime = nextVals{2};
Q_prime   = nextVals{4};

C_H_prime_star = nextVals_run{1};
V_R_prime_star = nextVals_run{2};
Q_prime_star   = nextVals_run{3};

R_K_prime       = r_K_prime + (1-p.delta)*Q_prime;
R_K_prime_star  = r_K_prime + (1-p.delta)*Q_prime_star;

EV_K_H = Z_prob*( ...
    (1-p.pi)*C_H_prime.^(-p.sigma).*R_K_prime + ...
    p.pi*C_H_prime_star.^(-p.sigma).*R_K_prime_star)';

EC_H = Z_prob*( ...
    (1-p.pi)*C_H_prime.^(-p.sigma) + ...
    p.pi*C_H_prime_star.^(-p.sigma))';

EV_R = Z_prob*((1-p.pi)*V_R_prime + p.pi*V_R_prime_star)';

n_R_prime = (N_R_prime - p.exoInc*K_prime)/(1-p.sigma_R);
n_R_prime_star = (N_R_prime_star - p.exoInc*K_prime)/(1-p.sigma_R);

Omega_R = V_R_prime./n_R_prime;
Omega_R_star = V_R_prime_star./n_R_prime_star;

Omega_R(n_R_prime <= 0) = 1;
Omega_R_star(n_R_prime_star <= 0) = 1;

E_Omega = Z_prob*((1-p.pi)*Omega_R + p.pi*Omega_R_star)';

E_R_K_prime = Z_prob*( ...
    (1-p.pi)*Omega_R.*R_K_prime + p.pi*Omega_R_star.*R_K_prime_star)' ...
    /(Q_star + p.eta_R*K_R_prime/K);

R_D_prime_star = C_H_star^(-p.sigma)/(p.beta * EC_H);
R_B_prime_star = E_R_K_prime./E_Omega*p.gamma + R_D_prime_star*(1-p.gamma);

foc_residual = zeros(3,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(1) = C_H_star^(-p.sigma)*(Q_star + p.eta_H*K_H_prime/K) - ...
    p.beta * EV_K_H;

foc_residual(2) = C_H_star - (Y + p.exoInc*K - p.tau*W - I - adj_cost ...
    - p.eta_H/2*K_H_prime^2/K - p.eta_R/2*K_R_prime^2/K ...
    - p.sigma_R*(N_R - p.exoInc*K)/(1-p.sigma_R));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(3) = phi_R_star - p.phi_bar*Q_star^p.phi_Q;

additionalVars = zeros(5,1);
additionalVars(1) = phi_R_star;
additionalVars(2) = C_H_star;
additionalVars(3) = Q_star;
additionalVars(4) = R_D_prime_star;
additionalVars(5) = R_B_prime_star;
