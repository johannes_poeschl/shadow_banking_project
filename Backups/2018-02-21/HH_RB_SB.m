% This file computes the no bank run solution.
%
% Dependencies: This code uses the toolbox provided by Judd, Maliar, Maliar
% & Valero (2014). This code also uses the CompEcon Toolbox by Miranda &
% Fackler. In addition, this code depends on the following functions:
%
%               - HH_RB_SB_compute_residual
%               - HH_RB_SB_compute_N_prime
%
% Output:       - initialGuess.mat
%               - Result_NoRun.mat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('./Smolyak_Anisotropic_JMMV_2014')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = struct;

% Parameters chosen to match steady state properties of the model
p.beta      = 0.9902;               % Household Discount Factor
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers

% Parameters chosen to match dynamics
p.rho_Z     = 0.9;                  % Persistence, TFP
p.sigma_Z   = 0.01;                % Volatility, TFP
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.theta_max = 10;                   % Capital Adjustment Cost
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.kappa_max = 0;                    % Asymmetry, Capital Adjustment Cost

% Standard Parameters
p.delta     = 0.025;                % Depreciation Rate
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.36;                 % Production Function Curvature

% Bank Run Parameters
p.sigma_xi  = 0.0125;                % Probability of Bank Run Starting
p.pi        = 12/13;                  % Probability of Bank Run Continuing
p.tau       = 0;

% Technical Stuff
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling

% Regulation Parameters
p.phi_bar   = 1/0.0001;
p.phi_Q     = 0;
p.gamma_bar = p.gamma;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solve the model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[pols,funs,grd,steady,states] = function_HH_RB_SB(p,'static','with_runs');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

smol_vals = [pols.K_prime pols.I pols.Q pols.C_H];
plotNames = {'K''','I','Q','C_H'};

% smol_vals = [pols.phi_R pols.phi_S pols.K_R_prime pols.K_S_prime];
% plotNames = {'\phi_R','\phi_S','K_R_''','K_S_'''};

% smol_vals = [pols.R_D_prime pols.R_B_prime pols.D_prime pols.B_prime];
% plotNames = {'R_D''','R_B''','D''','B'''};

plot_vals = cell(4,1);
for ii = 1:4
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs,[20,15,1,1])};
end

[~,cellGrid] = smolyak_plot(smol_vals(:,end),funs,[20,15,1,1]);

hf1 = figure;
for ii = 1:length(plot_vals)
    subplot(2,2,ii)
    surfc(cellGrid{2}/steady.N_S,cellGrid{1}/steady.N_R,plot_vals{ii});
    title(plotNames{ii})
    xlabel('N_S')
    ylabel('N_R')
end

% Value Functions as a function of K and Z
plot_vals = cell(4,1);
for ii = 1:4
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs,[1,1,20,15])};
end

[~,cellGrid] = smolyak_plot(smol_vals(:,end),funs,[1,1,20,15]);

hf2 = figure;
for ii = 1:length(plot_vals)
    subplot(2,2,ii)
    surfc(cellGrid{4}/p.mu_Z,cellGrid{3}/steady.K,plot_vals{ii});
    title(plotNames{ii})
    ylabel('K')
    xlabel('logZ')
end
