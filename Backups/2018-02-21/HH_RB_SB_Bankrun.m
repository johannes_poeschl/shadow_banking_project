% This file computes the bank run solution in a loop.
clear

usePreviousSolution_Run = 1;

load(strcat('./Results/','No_Run_Solution.mat'))

reg = 'run_exempt';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reset Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p.theta     = 0.25;                    % Initial Capital Adjustment Cost
p.kappa     = 0;                  % Asymmetry, Capital Adjustment Cost

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solve Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[pols_run,funs_run] = function_HH_RB_SB_Bankrun(pols,funs,grd,p,reg,'with_runs');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
smol_vals = [pols_run.K_prime pols_run.I pols_run.Q pols_run.C_H];
plotNames = {'K''','I','Q','C_H'};
% 
% smol_vals = [pols_run.phi_R pols_run.K_R_prime pols_run.K_H_prime pols_run.Q];
% plotNames = {'\phi_R','K_R''','K_H''','I'};

% smol_vals = [pols_run.R_D_prime pols_run.R_B_prime pols_run.D_prime zeros(size(pols_run.D_prime))];
% plotNames = {'R_D''','R_B''','D''','B'''};

plot_vals = cell(4,1);
for ii = 1:4
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs_run,[20,1,1])};
end

[~,cell_grid] = smolyak_plot(smol_vals(:,ii),funs_run,[20,1,1]);

hf1 = figure;
for ii = 1:length(plot_vals)
    subplot(2,2,ii)
    plot(cell_grid{1},plot_vals{ii});
    title(plotNames{ii})
    xlabel('N_R')
end

% Value Functions as a function of K and Z
plot_vals = cell(4,1);
for ii = 1:4
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs_run,[1,20,15])};
end

hf2 = figure;
for ii = 1:length(plot_vals)
    subplot(2,2,ii)
    surfc(plot_vals{ii});
    title(plotNames{ii})
    ylabel('K')
    xlabel('logZ')
end

%% Plot of the bank run condition (1)

% bank run probability as a function of n_r and n_s, holding k and z
% constant

smol_vals = [pols.phi_S pols.R_B_prime pols.K_prime];
plotNames = {'K''','I','Q'};

nPlots = size(smol_vals,2);

plot_vals = cell(nPlots,1);
for ii = 1:nPlots
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs,[20,15,1,1])};
end

plot_vals(4) = {smolyak_plot(pols_run.N_R_prime(:,5),funs_run,[20,1,1])};
plot_vals(5) = {smolyak_plot(pols_run.K_prime,funs_run,[20,1,1])};

future_states = [plot_vals{4} plot_vals{5} p.mu_Z*ones(size(plot_vals{5}))];
future_states_resc = Rescale(future_states,funs_run.unit_min,funs_run.unit_max, ...
    funs_run.orig_min,funs_run.orig_max);
future_poly   = Smolyak_Polynomial(future_states_resc,funs_run.nDims, ...
    funs_run.max_approx_level,funs_run.elem);

plot_Q_star_prime = future_poly*funs_run.Q;
    
[~,cellGrid] = smolyak_plot(smol_vals(:,end),funs,[20,15,1,1]);

% grid over z(t+1)
plot_phi_S = plot_vals{1};
plot_R_B_prime = plot_vals{2};
plot_r_K_prime = p.alpha*exp(p.mu_Z)*plot_vals{3}.^(p.alpha-1);
plot_bank_run = (plot_r_K_prime + (1-p.delta))./plot_R_B_prime.*plot_phi_S./(plot_phi_S-1);
% plot_bank_run_prob = normcdf(1./plot_bank_run,1,0.015);

fontOptions = {'FontSize',20};

figure
contourf(cellGrid{2},cellGrid{1},plot_bank_run)
set(gca,fontOptions{:})
xlabel('N^S',fontOptions{:})
ylabel('N^R',fontOptions{:})
title('Bank Run Condition',fontOptions{:})

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/BankRun_Condition_NR_NS');
print(figureName,'-dpdf')
%% Plot of the bank run condition (2)

% bank run probability as a function of k and z, holding n_r and n_s as
% well as z' and xi constant

smol_vals = [pols.phi_S pols.R_B_prime pols.K_prime];
plotNames = {'K''','I','Q'};

nPlots = size(smol_vals,2);

plot_vals = cell(nPlots,1);
for ii = 1:nPlots
    plot_vals(ii) = {smolyak_plot(smol_vals(:,ii),funs,[1,1,15,20])};
end

plot_vals(4) = {smolyak_plot(pols_run.N_R_prime(:,5),funs_run,[1,15,20])};
plot_vals(5) = {smolyak_plot(pols_run.K_prime,funs_run,[1,15,20])};

future_states = [vec(plot_vals{4}) vec(plot_vals{5}) vec(p.mu_Z*ones(size(plot_vals{5})))];
future_states_resc = Rescale(future_states,funs_run.unit_min,funs_run.unit_max, ...
    funs_run.orig_min,funs_run.orig_max);
future_poly   = Smolyak_Polynomial(future_states_resc,funs_run.nDims, ...
    funs_run.max_approx_level,funs_run.elem);

plot_Q_star_prime = reshape(future_poly*funs_run.Q,15,20);
    
[~,cellGrid] = smolyak_plot(smol_vals(:,end),funs,[1,1,15,20]);

% grid over z(t+1)
plot_phi_S = plot_vals{1};
plot_R_B_prime = plot_vals{2};
plot_r_K_prime = p.alpha*exp(p.mu_Z)*plot_vals{3}.^(p.alpha-1);
plot_bank_run = (plot_r_K_prime + (1-p.delta))./plot_R_B_prime.*plot_phi_S./(plot_phi_S-1);
plot_bank_run_prob = normcdf(1./plot_bank_run,1,0.015);
% plot_Q_star_prime

figure
contourf(cellGrid{4},cellGrid{3},plot_bank_run)
set(gca,fontOptions{:})
xlabel('log(Z)',fontOptions{:})
ylabel('K',fontOptions{:})
title('Bank Run Condition',fontOptions{:})

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/BankRun_Condition_K_Z');
print(figureName,'-dpdf')