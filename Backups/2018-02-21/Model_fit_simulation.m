% set simulation parameters
nBurnin = 200; 
nSimulations = nBurnin + 800;  % number of simulated periods 
nPanels = 1000; %%%%% number of simulations for each capital requirement level
bankRun = 1;

% p.pi = 0;



% draw shocks
seq_logZ = ones(nPanels,nSimulations)*p.mu_Z;
seq_epsZ = randn(nPanels,nSimulations);

for tt = 2:nSimulations  %%%%% for each simulation    
    seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + p.sigma_Z*seq_epsZ(:,tt);
end

shocks.logZ = seq_logZ;
if bankRun == 0
    shocks.nu = 1 + 0*normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
else
    shocks.nu = 1 + .015*normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
end
shocks.pi = normrnd(0,1,nPanels,nSimulations);
shocks.burnin = nBurnin;

% [TS,TS_add] = simulateModel_noRuns(p,shocks,funs,pols,steady);
[TS,TS_add] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

%% Simulation Boundaries
% check the simulation boundaries

RowNames = {'N_R','N_S','K'};

Min_Sim = [min(vec(TS.N_R));  min(vec(TS.N_S)); min(vec(TS.K))];
Lower_Bound_SS = funs.orig_min(1:3)';

Max_Sim = [max(vec(TS.N_R));  max(vec(TS.N_S)); max(vec(TS.K))];
Upper_Bound_SS = funs.orig_max(1:3)';

SS_Bounds  = table(Min_Sim,Lower_Bound_SS,Max_Sim,Upper_Bound_SS,'RowNames',RowNames);
display(SS_Bounds)

%% Welfare Statistics

tt = 1:(nSimulations - nBurnin);

disc_H = p.beta.^tt;
util_H = sum(repmat(disc_H,nPanels,1).*TS_add.C_H,2);
ex_ante_util_H = mean(exp((1-p.beta)*util_H));

disc_R = (p.beta*(1-p.sigma_R)).^tt;
util_R = sum(repmat(disc_R,nPanels,1).*TS.N_R,2);
ex_ante_util_R = mean(util_R);

disc_S = (p.beta*(1-p.sigma_S)).^tt;
util_S = sum(repmat(disc_S,nPanels,1).*TS.N_S,2);
ex_ante_util_S = mean(util_S);

RowNames = {'HH','RB','SB'};

if bankRun == 1
    welfare = [ex_ante_util_H; ex_ante_util_R; ex_ante_util_S];
else
    load('Welfare_Table_with_Runs')
    util_without_Runs = [ex_ante_util_H; ex_ante_util_R; ex_ante_util_S];
    util_with_Runs = Welfare_Table_with_Runs.Welfare;
    welfare = 100*(util_without_Runs./util_with_Runs - 1);
end

Welfare_Table = table(welfare, ...
    'RowNames',RowNames,'VariableNames',{'Welfare'});

if bankRun == 1
    Welfare_Table_with_Runs = Welfare_Table;
    save('Welfare_Table_with_Runs','Welfare_Table_with_Runs')
end

display(Welfare_Table)

input = struct;
input.data = table2array(Welfare_Table);
input.tableRowLabels = RowNames;
input.booktabs = 1;
lTable = latexTable(input);

%% Bank Run Statistics

RowNames = {'Q','Q*','x','phi_R','phi_S','p(x < 1)','p(I < 0)'};

mean_Q  = mean(vec(TS.Q));
median_Q = quantile(vec(TS.Q),0.5);
p05_Q = quantile(vec(TS.Q),0.05);
p95_Q = quantile(vec(TS.Q),0.95);
std_Q = std(vec(TS.Q));

mean_Q_star = mean(vec(TS.Q_star));
median_Q_star = quantile(vec(TS.Q_star),0.5);
p05_Q_star = quantile(vec(TS.Q_star),0.05);
p95_Q_star = quantile(vec(TS.Q_star),0.95);
std_Q_star = std(vec(TS.Q_star));

mean_recov = mean(vec(TS_add.recov));
median_recov = quantile(vec(TS_add.recov),0.5);
p05_recov = quantile(vec(TS_add.recov),0.05);
p95_recov = quantile(vec(TS_add.recov),0.95);
std_recov = std(vec(TS_add.recov));

mean_phi_R = mean(vec(TS_add.phi_R));
median_phi_R = quantile(vec(TS_add.phi_R),0.5);
p05_phi_R = quantile(vec(TS_add.phi_R),0.05);
p95_phi_R = quantile(vec(TS_add.phi_R),0.95);
std_phi_R = std(vec(TS_add.phi_R));

mean_phi_S = mean(vec(TS_add.phi_S));
median_phi_S = quantile(vec(TS_add.phi_S),0.5);
p05_phi_S = quantile(vec(TS_add.phi_S),0.05);
p95_phi_S = quantile(vec(TS_add.phi_S),0.95);
std_phi_S = std(vec(TS_add.phi_S));

prob_run = mean(vec(TS_add.recov < 1));
prob_neg_inv = mean(vec(TS_add.I < p.delta*TS.K));

mean_Vals = [mean_Q; mean_Q_star; mean_recov; mean_phi_R; mean_phi_S; prob_run; prob_neg_inv];
median_Vals = [median_Q; median_Q_star; median_recov; median_phi_R; median_phi_S; 0; 0];
p05_Vals = [p05_Q; p05_Q_star; p05_recov; p05_phi_R; p05_phi_S; 0; 0];
p95_Vals = [p95_Q; p95_Q_star; p95_recov; p95_phi_R; p95_phi_S; 0; 0];
std_Vals = [std_Q; std_Q_star; std_recov; std_phi_R; std_phi_S; 0; 0];

Bank_Run_Statistics = table(mean_Vals,median_Vals,p05_Vals,p95_Vals,std_Vals, ...
    'RowNames',RowNames, ...
    'VariableNames',{'Mean','Median','p05','p95','StDev'});

display(Bank_Run_Statistics)

%% Business Cycle Statistics

load('DataStatistics')
data_StDev = DataStatistics.StDevs;
data_CorrWithGDP = DataStatistics.CorrWithGDP;
data_AutoCorr = DataStatistics.AutoCorr;

pct_dev_Y = TS_add.Y/steady.Y - 1;
pct_dev_C_H = TS_add.C_H/steady.C_H - 1;
pct_dev_I = TS_add.I/(steady.K*p.delta) - 1;
pct_dev_K_R_prime = TS_add.K_R_prime/steady.K_R - 1;
pct_dev_K_S_prime = TS_add.K_S_prime/steady.K_S - 1;
RK_RD_prime = TS_add.R_K_prime.^4 - TS_add.R_D_prime.^4;

% standard deviations
sd_Y = std(vec(pct_dev_Y));
sd_I = std(vec(pct_dev_I));
sd_C_H = std(vec(pct_dev_C_H));
sd_K_R_prime = std(vec(pct_dev_K_R_prime));
sd_K_S_prime = std(vec(pct_dev_K_S_prime));
sd_RK_RD_prime = std(vec(RK_RD_prime));

corr_C_H = corr(vec(pct_dev_Y),vec(pct_dev_C_H));
corr_I = corr(vec(pct_dev_Y),vec(pct_dev_I));
corr_K_R_prime = corr(vec(pct_dev_Y),vec(pct_dev_K_R_prime));
corr_K_S_prime = corr(vec(pct_dev_Y),vec(pct_dev_K_S_prime));
corr_RK_RD_prime = corr(vec(pct_dev_Y),vec(RK_RD_prime));

autoCorr_Y = corr(vec(pct_dev_Y(:,2:end)),vec(pct_dev_Y(:,1:end-1)));
autoCorr_C_H = corr(vec(pct_dev_C_H(:,2:end)),vec(pct_dev_C_H(:,1:end-1)));
autoCorr_I = corr(vec(pct_dev_I(:,2:end)),vec(pct_dev_I(:,1:end-1)));
autoCorr_K_R_prime = corr(vec(pct_dev_K_R_prime(:,2:end)),vec(pct_dev_K_R_prime(:,1:end-1)));
autoCorr_K_S_prime = corr(vec(pct_dev_K_S_prime(:,2:end)),vec(pct_dev_K_S_prime(:,1:end-1)));
autoCorr_RK_RD_prime = corr(vec(RK_RD_prime(:,2:end)),vec(RK_RD_prime(:,1:end-1)));

model_StDev = [sd_Y; sd_C_H/sd_Y; sd_I/sd_Y; sd_K_R_prime/sd_Y; sd_RK_RD_prime];
model_CorrWithGDP = [1; corr_C_H; corr_I; corr_K_R_prime; corr_RK_RD_prime];
model_AutoCorr = [autoCorr_Y; autoCorr_C_H; autoCorr_I; autoCorr_K_R_prime; autoCorr_RK_RD_prime];

RowNames = {'Output (Y)','Consumption (C_H)','Investment (I)','R-Bank Lending (K^R'')','Loan Spread (R^K - R^D)'};

Business_Cycle_Statistics  = table(model_StDev,data_StDev, ...
    model_CorrWithGDP,data_CorrWithGDP, ...
    model_AutoCorr,data_AutoCorr, ...
    'RowNames',RowNames);
display(Business_Cycle_Statistics)

save(strcat('./Results/Moments_','theta_',num2str(p.theta),'_kappa_',num2str(p.kappa),'.mat'),'SS_Bounds','Business_Cycle_Statistics','Bank_Run_Statistics')

input = struct;
input.data = table2array(Business_Cycle_Statistics);
input.tableRowLabels = RowNames;
input.booktabs = 1;
lTable = latexTable(input);