% comparative statics with respect to theta and kappa
clear
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
solveModel = 0;
simulModel = 1;

addpath('.\Smolyak_Anisotropic_JMMV_2014')  

reg_order = {'static'};
run_order = {'with_runs'};
Gamma_R_Vals = 0.0001;

for xx = 1:length(reg_order)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space for phi_R_bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% types of capital requirements: static, run_exempt, dynamic
is_reg = reg_order{xx};

% types of runs: no_runs, with_runs
is_run = run_order{xx};

nVals = length(Gamma_R_Vals);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Other Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = struct;

% Parameters chosen to match steady state properties of the model
p.beta      = 0.9902;               % Household Discount Factor
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers

% Parameters chosen to match dynamics
p.rho_Z     = 0.9;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.theta_max = 10;                    % Capital Adjustment Cost
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.kappa_max = 0;                    % Asymmetry, Capital Adjustment Cost

% Standard Parameters
p.delta     = 0.025;                % Depreciation Rate
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.36;                 % Production Function Curvature

% Bank Run Parameters
if strcmp(is_run,'with_runs') == 1
    p.sigma_xi  = 0.0125;                % Probability of Bank Run Starting
elseif strcmp(is_run,'no_runs') == 1
    p.sigma_xi  = 0;
end
p.pi        = 12/13;                  % Probability of Bank Run Continuing

p.gamma_bar = p.gamma;
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling

if simulModel == 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for kVal = 1:nVals   
       %load(strcat('./Results/',is_reg,'_',is_run,'/Results_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'))
        load('Results_GammaR_0.0001.mat')
        
        [nPanels,nSimulations,nBurnin,TS,TS_add,SS_Bounds,Bank_Run_Statistics,Business_Cycle_Statistics,Welfare_Table] = ...
        function_simpleSimulation_withTS(pols,funs,pols_run,funs_run,p,steady,is_run);

%         save(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
%             'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')
    end
end

end

%%
d.lrgdp_mat = diff(log(TS_add.Y'));
d.linvr_mat = diff(log(TS_add.I'));
d.lcred_mat = diff(log(TS_add.K_S_prime'+TS_add.K_R_prime'));
d.run_mat = TS.run_starts(:,2:end)';
d.country_mat = repmat([1:nPanels]',[1,(nSimulations-nBurnin-1)])';

d.lrgdp_vec = reshape(d.lrgdp_mat,[nPanels*(nSimulations-nBurnin-1),1]);
d.linvr_vec = reshape(d.linvr_mat,[nPanels*(nSimulations-nBurnin-1),1]);
d.lcred_vec = reshape(d.lcred_mat,[nPanels*(nSimulations-nBurnin-1),1]);
%d.run_vec = reshape(d.run_mat,[nPanels*(nSimulations-nBurnin-1),1]);
d.country_vec = reshape(d.country_mat,[nPanels*(nSimulations-nBurnin-1),1]);

d.run_dummies = zeros([size(d.run_mat) 12]);
d.run_dummies(:,:,1) = d.run_mat;

for jj = 2:size(d.run_dummies,3)
    d.run_dummies(:,:,jj) = [zeros(1,nPanels); d.run_dummies(1:end-1,:,jj-1)];
end

d.run_dummies_sorted = zeros(numel(d.run_mat),12);
for jj = 1:size(d.run_dummies,3)
    d.run_dummies_sorted(:,jj) = vec(d.run_dummies(:,:,jj));
end

d.X = [ones(numel(d.run_mat),1) d.run_dummies_sorted];

d.coefficients_gdp = (d.X' * d.X)\(d.X' * d.lrgdp_vec);
d.beta_gdp = sum(d.coefficients_gdp);

d.coefficients_inv = (d.X' * d.X)\(d.X' * d.linvr_vec);
d.beta_inv = sum(d.coefficients_inv);

d.coefficients_cred = (d.X' * d.X)\(d.X' * d.lcred_vec);
d.beta_cred = sum(d.coefficients_cred);