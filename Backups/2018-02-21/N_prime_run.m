function [N_R_prime_no_run,N_R_prime_run] = N_prime_run(p, states, x0, additionalVars, ...
    nextStates, nextVals, nextVals_run)
% HH_RB_SB_compute_N_prime_run computes the next period net worth in a run

N_R = states(1,1);
K   = states(1,2);

K_H_prime = x0(1);
K_R_prime = x0(2);

phi_R_star       = additionalVars(1);
Q_star           = additionalVars(3);
R_D_prime_star   = additionalVars(4);
D_prime = (Q_star + p.eta_R*K_R_prime./K).*K_R_prime - N_R;

K_prime = K_H_prime + K_R_prime;
logZ_prime = nextStates(:,4)';

Q_prime      = nextVals{4};
Q_prime_star = nextVals_run{3};

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
R_K_R_prime = R_K_prime/(Q_star + p.eta_R*K_R_prime/K);

R_K_prime_star = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime_star;
R_K_R_prime_star = R_K_prime_star/(Q_star + p.eta_R*K_R_prime/K);

% N'
% N_R_prime_no_run = (((R_K_R_prime - R_D_prime_star)*phi_R_star + R_D_prime_star)*N_R)*(1-p.sigma_R) + ...
%     p.exoInc*K;  % /////
% N_R_prime_run = (((R_K_R_prime_star - R_D_prime_star)*phi_R_star + R_D_prime_star)*N_R)*(1-p.sigma_R) + ...
%     p.exoInc*K;  % /////
N_R_prime_no_run = (R_K_prime*K_R_prime - R_D_prime_star*D_prime)*(1-p.sigma_R) + ...
    p.exoInc*K_prime;  % /////
N_R_prime_run = (R_K_prime_star*K_R_prime - R_D_prime_star*D_prime)*(1-p.sigma_R) + ...
    p.exoInc*K_prime;  % /////