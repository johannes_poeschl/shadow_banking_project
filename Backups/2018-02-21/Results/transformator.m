gVals = 0.02:0.01:0.2;
nVals = length(gVals);

for ii = 1:nVals
    load(strcat('Moments_GammaR_',num2str(gVals(ii)),'_run_exempt.mat'))
    save(strcat('Moments_GammaR_',num2str(gVals(ii)),'_static.mat'), ...
        'Bank_Run_Statistics','Business_Cycle_Statistics','SS_Bounds', ...
        'Welfare_Table')
    
    load(strcat('Results_GammaR_',num2str(gVals(ii)),'_static.mat'))
    save(strcat('Results_GammaR_',num2str(gVals(ii)),'.mat'),'p', ...
        'funs','funs_run','pols','pols_run','steady')
end