% function [vals,vals_run] = computeValueFunctions(p,pols,pols_run,funs,funs_run)

% state space, no run
unit_grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,funs.elem);
states = Rescale(unit_grid,funs.orig_min,funs.orig_max,funs.unit_min,funs.unit_max);
n_no_run = length(states);

% state space, run
unit_grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,funs_run.elem);
states_run = Rescale(unit_grid,funs_run.orig_min,funs_run.orig_max,funs_run.unit_min,funs_run.unit_max);
n_run = length(states_run);

% initial guess for the value function
V_H_no_run = zeros(n_no_run,1);
V_H_run = zeros(n_run,1);

% interpolate
coeffs_V_H_no_run = funs.base_poly\V_H_no_run;
coeffs_V_H_run = funs_run.base_poly\V_H_run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% No Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 1:n_no_run
% compute future states
% no run to no run
next_states

% no run to run

% compute future value functions
nextVals

% compute current value function
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%