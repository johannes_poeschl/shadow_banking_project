function [pols,funs,grd,steady,states] = function_HH_RB_SB(p,reg,run)

kappa_step = (p.kappa_max - p.kappa)/(25-1);
theta_step = (p.theta_max - p.theta)/(25-1);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K = ((1.064^0.25 - (1 - p.delta))/p.alpha/exp(p.mu_Z))^(1/(p.alpha-1));

handle = @(x) computeSteadyState(x,p);

x0 = [0.2 0.4 0.4 0.0655 0.02]'*K;
x1 = fsolve(handle,x0);

[~,steady] = handle(x1);

if steady.phi_shadow > p.phi_bar
    handle = @(x) computeSteadyState_with_CR(x,p);

    [x1,~,exitFlag] = fsolve(handle,x1);

    [~,steady] = handle(x1);
end

display(steady)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grd = struct;

scale_N_R_minus = 0.5;
scale_N_R_plus = 0.5;

scale_N_S_minus = 1 - p.exoInc*steady.K/steady.N_S;
scale_N_S_plus = 1 + p.exoInc*steady.K/steady.N_S;

scale_K_plus = 0.1;
scale_K_minus = 0.1;

grd.K_l = (1-scale_K_minus) * steady.K;
grd.K_u = (1+scale_K_plus) * steady.K;

grd.N_R_l = (1-scale_N_R_minus)*steady.N_R;
grd.N_R_u = (1+scale_N_R_plus)*steady.N_R;

% grd.N_S_l = p.exoInc*steady.K;
grd.N_S_l = (1-scale_N_S_minus)*steady.N_S;
grd.N_S_u = (1+scale_N_S_plus)*steady.N_S;  % /////

p.uncondStd = sqrt(p.sigma_Z^2/(1-p.rho_Z^2));
grd.Z_l = p.mu_Z - 3*p.uncondStd;
grd.Z_u = p.mu_Z + 3*p.uncondStd;

orig_min = [grd.N_R_l grd.N_S_l grd.K_l grd.Z_l];
orig_max = [grd.N_R_u grd.N_S_u grd.K_u grd.Z_u];

unit_min = -ones(size(orig_min));
unit_max = ones(size(orig_max));

clear K_l K_u N_H_l N_H_u N_R_l N_R_u N_S_l N_S_u Z_l Z_u

% 1 Smolyak approximation level
approx_level = [4 4 4 3];
max_approx_level = max(approx_level);
nDims = length(approx_level);

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states,1);
vec_N_R = states(:,1);
vec_N_S = states(:,2);
vec_K = states(:,3);
vec_logZ = states(:,4);

grd.n_Z = 9;
[grd.eps_Z,grd.Z_prob] = qnwnorm(grd.n_Z,0,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fileName = strcat('./Results/',reg,'_',run,'/Results_GammaR_',num2str(1/p.phi_bar),'.mat');

if exist('initialGuess.mat','file') == 2 || exist(fileName,'file') == 2
    p.theta = p.theta_max;
    p.kappa = p.kappa_max;
    
    if exist(fileName,'file') == 2
        load(fileName,'pols')
    else
        load('initialGuess.mat')
    end
    % Additional Policy Functions
    vec_K_H_prime = pols.K_H_prime;
    vec_K_R_prime = pols.K_R_prime;
    vec_K_S_prime = pols.K_S_prime; 
    
    % Value Functions
    vec_C_H = pols.C_H;
    vec_V_R = pols.V_R;
    vec_V_S = pols.V_S;  
    vec_Q   = pols.Q;
    
    % Aggregate LoMs
    N_R_prime_new = pols.N_R_prime;
    N_S_prime_new = pols.N_S_prime;
    vec_K_prime_new = pols.K_prime;
else
    % Additional Policy Functions
    vec_K_H_prime = ones(n_total,1);
    vec_K_R_prime = ones(n_total,1);
    vec_K_S_prime = max((p.theta-1)/p.theta*vec_K,0); 
    vec_Y = exp(vec_logZ).*vec_K.^p.alpha;
    
    % Value Functions
    vec_C_H = vec_Y;
    vec_V_R = zeros(size(vec_N_R));
    vec_V_S = zeros(size(vec_N_S));  
    vec_Q = max(ones(n_total,1) - p.theta,0);
    
    % Aggregate LoMs
    N_R_prime_new = p.exoInc*ones(n_total,grd.n_Z);
    N_S_prime_new = p.exoInc*ones(n_total,grd.n_Z);
    vec_K_prime_new = vec_K_S_prime;
end

vec_I = vec_K_prime_new - (1-p.delta)*vec_K;
vec_R_D_prime = ones(n_total,1);  
vec_R_B_prime = ones(n_total,1);
vec_D_prime   = ones(n_total,1);
vec_B_prime   = ones(n_total,1);

N_R_prime_old = N_R_prime_new;
N_S_prime_old = N_S_prime_new;
vec_K_prime_old = vec_K_prime_new;    

% Value Functions
vec_C_H_old = vec_C_H;
vec_V_R_old = vec_V_R;
vec_V_S_old = vec_V_S; 
vec_Q_old = vec_Q;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funs = struct;

% interpolated values
funs.C_H = base_poly\vec_C_H; 
funs.V_R = base_poly\vec_V_R; 
funs.V_S = base_poly\vec_V_S;  
funs.Q = base_poly\vec_Q;    

% help
funs.orig_min = orig_min;
funs.orig_max = orig_max;
funs.unit_min = unit_min;
funs.unit_max = unit_max;
funs.approx_level = approx_level;
funs.max_approx_level = max_approx_level;
funs.elem = elem_aniso;
funs.nDims = nDims;
funs.base_poly = base_poly;

clear orig_min orig_max unit_min unit_max approx_level max_approx_level ...
    elem nDims base_poly elem_iso elem_aniso unit_grid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vError = 10;
nError = 10;
iter = 1;
maxIter =  250;

options = optimset('Display','off','MaxIter',1e3,'MaxFunEvals',1e3);

% preallocation
vec_phi_R = ones(n_total,1);
vec_phi_R_tilde = ones(n_total,1);
vec_Q_tilde = ones(n_total,1);
vec_phi_S = ones(n_total,1);
vec_E_R_K_R_prime = ones(n_total,1);
vec_E_R_K_S_prime = ones(n_total,1);
vec_zero_C_S_cond = ones(n_total,1);
Z_prob_ext = grd.Z_prob;


while (vError > 1e-5 || nError > 1e-5) && iter <= maxIter
    tic
    for ii = 1:n_total
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Aggregate LoM for Z'
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states(ii,4) + p.sigma_Z*grd.eps_Z;              
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Next Period Value Functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        nextStates = [N_R_prime_old(ii,:)' N_S_prime_old(ii,:)' ...
            vec_K_prime_old(ii)*ones(size(logZ_prime)) logZ_prime];
        
        [ nextVals ] = computeExpectations( nextStates,funs,steady );
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the System of FOC without binding CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x_prime) FOC(p,x_prime, ...
            states(ii,:),nextVals,nextStates,Z_prob_ext');

        x0 = [vec_K_H_prime(ii,1);
                vec_K_R_prime(ii,1);
                vec_K_S_prime(ii,1)
                vec_C_H(ii,1)];
                    
        [x_prime_new,~,exitflag] = fsolve(handle,x0,options);
                
        [~,additionalVars] = handle(x_prime_new);
        
        vec_phi_R_tilde(ii,1) = additionalVars(1);
        vec_Q_tilde(ii,1)     = additionalVars(5);
        if vec_phi_R_tilde(ii,1) > p.phi_bar*vec_Q_tilde(ii,1)^p.phi_Q
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Solve the System of FOC without binding CR
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            handle = @(x_prime) FOC_with_CR_dynamic(p,x_prime, ...
                states(ii,:),nextVals,nextStates,Z_prob_ext');
            
            x0 = x_prime_new;

            [x_prime_new,~,exitflag] = fsolve(handle,x0,options);
                        
            [~,additionalVars] = handle(x_prime_new);
            
        end
        
        if exitflag ~= 1
            display(states(ii,:)./[steady.N_R steady.N_S steady.K p.mu_Z])
            error('No Solution found')
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Store Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vec_K_H_prime(ii,1) = x_prime_new(1);
        vec_K_R_prime(ii,1) = x_prime_new(2);
        vec_K_S_prime(ii,1) = x_prime_new(3);           
        
        vec_phi_R(ii,1)     = additionalVars(1);
        vec_phi_S(ii,1)     = additionalVars(2);
        vec_C_H(ii,1)       = additionalVars(3);
        vec_B_prime(ii,1)   = additionalVars(4);      
        vec_Q(ii,1)         = additionalVars(5);
        vec_R_D_prime(ii,1) = additionalVars(6);
        vec_R_B_prime(ii,1) = additionalVars(7);
        vec_E_R_K_R_prime(ii,1) = additionalVars(8);
        vec_E_R_K_S_prime(ii,1) = additionalVars(9);
        
        vec_K_prime_new(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
            + vec_K_S_prime(ii,1);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Update the Net Worth
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [N_R_prime_new(ii,:),N_S_prime_new(ii,:)] = ...
            N_prime(p, states(ii,:), x_prime_new, additionalVars, ...
            nextStates, nextVals);
    end
    elapsedTime = toc;

    N_R_prime_new = bound(N_R_prime_new,0,funs.orig_max(1));
    N_S_prime_new = bound(N_S_prime_new,0,funs.orig_max(2));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Other Variables not needed in the Solution Algorithm
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_I = vec_K_prime_new - (1-p.delta)*vec_K;
    vec_D_prime = (vec_phi_R-1).*vec_N_R + (1-p.gamma)*vec_B_prime;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    vec_V_R = (p.sigma_R + (1-p.sigma_R)*p.psi*vec_phi_R_tilde).*(vec_N_R - p.exoInc*vec_K)/(1-p.sigma_R);
    vec_V_S = (p.sigma_S + (1-p.sigma_S)*p.psi*(p.omega*vec_phi_S + ...
                (1-p.omega))).*(vec_N_S - p.exoInc*vec_K)/(1-p.sigma_S);   
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check Convergence Criteria
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    vError = norm([vec_C_H; vec_V_R; vec_V_S] - [vec_C_H_old; vec_V_R_old; vec_V_S_old],'inf');
    kError = norm(vec_K_prime_new - vec_K_prime_old,'inf');
    nError = norm(vec([N_R_prime_new; N_S_prime_new]) - vec([N_R_prime_old; N_S_prime_old]),'inf');
    
    if mod(iter,10) == 0  || iter == 1
        fprintf('Iteration %d: \n',iter);
        fprintf('Elapsed Time: %6.2f Seconds \n',elapsedTime);
        fprintf('Value Function Error: %6.6f \n',vError)
        fprintf('Net Worth Error %6.6f \n \n',max(kError,nError))
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Attenuation (Smoothing)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    weight_N = 0.5;
    weight_V = 1;
    
    vec_K_prime_old = weight_N*vec_K_prime_new + (1-weight_N)*vec_K_prime_old;
    N_R_prime_old = weight_N*N_R_prime_new + (1-weight_N)*N_R_prime_old;
    N_S_prime_old = weight_N*N_S_prime_new + (1-weight_N)*N_S_prime_old;      
    vec_Q_old = weight_N*vec_Q + (1-weight_N)*vec_Q_old;
    
    vec_C_H_old = weight_V*vec_C_H + (1-weight_V)*vec_C_H_old;
    vec_V_R_old = weight_V*vec_V_R + (1-weight_V)*vec_V_R_old;
    vec_V_S_old = weight_V*vec_V_S + (1-weight_V)*vec_V_S_old;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Interpolate Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%s
    funs.C_H = funs.base_poly\vec_C_H; 
    funs.V_R = funs.base_poly\vec_V_R; 
    funs.V_S = funs.base_poly\vec_V_S;  
    funs.Q = funs.base_poly\vec_Q;  
    
    if iter > 50        
        p.theta = min(p.theta + theta_step,p.theta_max);
        p.kappa = min(p.kappa + kappa_step,p.kappa_max);
    end
    iter = iter + 1;
end

%% Check the State Space Bounds
minVals = [min(vec(N_R_prime_new)) min(vec(N_S_prime_new)) min(vec_K_prime_new)];
maxVals = [max(vec(N_R_prime_new)) max(vec(N_S_prime_new)) max(vec_K_prime_new)];

fprintf('Lower Bounds (<1 means violation): \n')
display(minVals./funs.orig_min(1:3))

fprintf('Upper Bounds (<1 means violation): \n')
display(funs.orig_max(1:3)./maxVals)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Store Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pols = struct;
pols.K_H_prime  = vec_K_H_prime;
pols.K_R_prime  = vec_K_R_prime;
pols.K_S_prime  = vec_K_S_prime;
pols.K_prime    = vec_K_prime_new;
pols.I          = vec_I;
pols.D_prime    = vec_D_prime;
pols.B_prime    = vec_B_prime;
pols.phi_R      = vec_phi_R;
pols.phi_S      = vec_phi_S;

pols.R_D_prime  = vec_R_D_prime;
pols.R_B_prime  = vec_R_B_prime;
pols.Q          = vec_Q;
pols.E_R_K_R_prime = vec_E_R_K_R_prime;
pols.E_R_K_S_prime = vec_E_R_K_S_prime;

pols.N_R_prime  = N_R_prime_new;
pols.N_S_prime  = N_S_prime_new;
pols.C_H        = vec_C_H;
pols.V_R        = vec_V_R;
pols.V_S        = vec_V_S;

%%

fileName_short = strcat('./Results/','No_Run_Solution.mat');
% fileName = strcat('./Results/',date,'_No_Run_Solution_','theta_',num2str(p.theta),'_kappa_',num2str(p.kappa),'.mat');

save('initialGuess.mat','pols')
save(fileName_short,'pols','funs','grd','p','steady','states')
% save(fileName,'pols','funs','grd','p','steady','states')
