function [pols,pols_run,funs,funs_run,grd,steady,states_no_run,states_run] = function_HH_RB_SB_anti(p,reg,run)

p.theta = p.theta_max;
p.kappa = p.kappa_max;
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K = ((1.064^0.25 - (1 - p.delta))/p.alpha/exp(p.mu_Z))^(1/(p.alpha-1));

handle = @(x) computeSteadyState(x,p);

x0 = [0.2 0.4 0.4 0.0655 0.02]'*K;
x1 = fsolve(handle,x0);

[~,steady] = handle(x1);

if steady.phi_shadow > p.phi_bar
    handle = @(x) computeSteadyState_with_CR(x,p);

    x1 = fsolve(handle,x1);

    [~,steady] = handle(x1);
end

display(steady)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
grd = struct;

scale_N_R_minus = 0.5;
scale_N_R_plus = 0.5;

scale_N_S_minus = 1 - p.exoInc*steady.K/steady.N_S;
scale_N_S_plus = 1 + p.exoInc*steady.K/steady.N_S;

scale_K_plus = 0.1;
scale_K_minus = 0.1;

grd.K_l = (1-scale_K_minus) * steady.K;
grd.K_u = (1+scale_K_plus) * steady.K;

grd.N_R_l = (1-scale_N_R_minus)*steady.N_R;
grd.N_R_u = (1+scale_N_R_plus)*steady.N_R;

grd.N_S_l = (1-scale_N_S_minus)*steady.N_S;
grd.N_S_u = (1+scale_N_S_plus)*steady.N_S;  % /////

p.uncondStd = sqrt(p.sigma_Z^2/(1-p.rho_Z^2));
grd.Z_l = p.mu_Z - 3*p.uncondStd;
grd.Z_u = p.mu_Z + 3*p.uncondStd;

clear K_l K_u N_H_l N_H_u N_R_l N_R_u N_S_l N_S_u Z_l Z_u

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% No Run Grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
orig_min = [grd.N_R_l grd.N_S_l grd.K_l grd.Z_l];
orig_max = [grd.N_R_u grd.N_S_u grd.K_u grd.Z_u];

unit_min = -ones(size(orig_min));
unit_max = ones(size(orig_max));

% 1 Smolyak approximation level
approx_level = [4 4 4 3];
max_approx_level = max(approx_level);
nDims = length(approx_level);

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states_no_run = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total = size(states_no_run,1);
vec_N_R = states_no_run(:,1);
vec_N_S = states_no_run(:,2);
vec_K = states_no_run(:,3);

grd.n_Z = 9;
grd.n_Xi = 9;
[grd.eps_Z,grd.Z_prob] = qnwnorm(grd.n_Z,0,1);
[grd.eps_Xi,grd.Xi_prob] = qnwnorm(grd.n_Xi,0,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess, No Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fileName = strcat('./Results/',reg,'_',run,'/Results_GammaR_',num2str(1/p.phi_bar),'.mat');
load(fileName,'pols','pols_run')

% load('./Results/No_Run_Solution.mat','pols')
% load('./Results/Run_Solution.mat','pols_run')

% Additional Policy Functions
vec_K_H_prime = pols.K_H_prime;
vec_K_R_prime = pols.K_R_prime;
vec_K_S_prime = pols.K_S_prime; 

% Value Functions
vec_C_H = pols.C_H;
vec_V_R = pols.V_R;
vec_V_S = pols.V_S;  
vec_Q   = pols.Q;

% Aggregate LoMs
N_R_prime_n2r = pols.N_R_prime_n2r;
N_R_prime_n2n = pols.N_R_prime_n2n;
N_S_prime_n2r = pols.N_S_prime_n2r;
N_S_prime_n2n = pols.N_S_prime_n2n;
vec_K_prime = pols.K_prime;
p_prime           = pols.p_prime;
x_prime           = pols.x_prime;

vec_I = vec_K_prime - (1-p.delta)*vec_K;
vec_R_D_prime = ones(n_total,1);  
vec_R_B_prime = ones(n_total,1);
vec_D_prime   = ones(n_total,1);
vec_B_prime   = ones(n_total,1);

N_R_prime_n2r_old = N_R_prime_n2r;
N_R_prime_n2n_old = N_R_prime_n2n;
N_S_prime_n2r_old = N_S_prime_n2r;
N_S_prime_n2n_old = N_S_prime_n2n;
p_prime_old       = p_prime;
x_prime_old       = x_prime;

vec_K_prime_old   = vec_K_prime;    

% Value Functions
vec_C_H_old = vec_C_H;
vec_V_R_old = vec_V_R;
vec_V_S_old = vec_V_S; 
vec_Q_old = vec_Q;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation, No Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
funs = struct;

% interpolated values
funs.C_H = base_poly\vec_C_H; 
funs.V_R = base_poly\vec_V_R; 
funs.V_S = base_poly\vec_V_S;  
funs.Q = base_poly\vec_Q;    

% help
funs.orig_min = orig_min;
funs.orig_max = orig_max;
funs.unit_min = unit_min;
funs.unit_max = unit_max;
funs.approx_level = approx_level;
funs.max_approx_level = max_approx_level;
funs.elem = elem_aniso;
funs.nDims = nDims;
funs.base_poly = base_poly;

clear orig_min orig_max unit_min unit_max approx_level max_approx_level ...
    elem nDims base_poly elem_iso elem_aniso unit_grid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run Grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unit_min = funs.unit_min([1 3 4]);
unit_max = funs.unit_max([1 3 4]);
orig_min = funs.orig_min([1 3 4]);
orig_max = funs.orig_max([1 3 4]);

% 1 Smolyak approximation level
approx_level = [4 4 3];
max_approx_level = max(approx_level);
nDims = length(approx_level);

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(nDims,max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,approx_level);

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(nDims,max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
base_poly_run = Smolyak_Polynomial(unit_grid,nDims,max_approx_level,elem_aniso);

% original smolyak grid
states_run = Rescale(unit_grid,orig_min,orig_max,unit_min,unit_max);
n_total_run = size(states_run,1);
vec_N_R_star = states_run(:,1);
vec_K_star = states_run(:,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess, Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% Additional Policy Functions
vec_K_H_prime_star      = pols_run.K_H_prime;
vec_K_R_prime_star      = pols_run.K_R_prime;

% Value Functions
vec_C_H_star       = pols_run.C_H;
vec_V_R_star       = pols_run.V_R;
vec_Q_star         = pols_run.Q;

% Aggregate LoMs
N_R_prime_r2n      = pols_run.N_R_prime;
N_R_prime_r2r      = pols_run.N_R_prime_star;
N_S_prime_r2n      = p.exoInc*repmat(vec_K,1,grd.n_Z);
vec_K_prime_star   = pols_run.K_prime;

vec_R_D_prime_star  = ones(n_total_run,1);
vec_R_B_prime_star  = ones(n_total_run,1);
vec_phi_R_star      = ones(n_total_run,1); 
vec_phi_R_tilde_star = ones(n_total_run,1); 
vec_Q_tilde_star    = ones(n_total_run,1);

vec_C_H_star_old       = vec_C_H_star;
vec_V_R_star_old       = vec_V_R_star;
vec_Q_star_old         = vec_Q_star;

N_R_prime_r2n_old = N_R_prime_r2n;
N_R_prime_r2r_old = N_R_prime_r2r;
vec_K_prime_star_old = vec_K_prime_star;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation, Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funs_run = struct;
funs_run.C_H = base_poly_run\vec_C_H_star; 
funs_run.V_R = base_poly_run\vec_V_R_star;
funs_run.Q   = base_poly_run\vec_Q_star;  

% help
funs_run.orig_min = orig_min;
funs_run.orig_max = orig_max;
funs_run.unit_min = unit_min;
funs_run.unit_max = unit_max;
funs_run.approx_level = approx_level;
funs_run.max_approx_level = max_approx_level;
funs_run.elem = elem_aniso;
funs_run.nDims = nDims;
funs_run.base_poly = base_poly_run;

clear orig_min orig_max unit_min unit_max approx_level max_approx_level ...
    elem nDims base_poly elem_iso elem_aniso unit_grid

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vError = 10;
nError = 10;
iter = 1;
maxIter =  250;

options = optimset('Display','off','MaxIter',1e3,'MaxFunEvals',1e3);

% preallocation
vec_phi_R       = ones(n_total,1);
vec_phi_R_tilde = ones(n_total,1);
vec_Q_tilde 	= ones(n_total,1);
vec_phi_S       = ones(n_total,1);
vec_E_R_K_prime = ones(n_total,1);
Z_prob_ext      = kron(grd.Xi_prob,grd.Z_prob);

while (vError > 1e-5 || nError > 1e-5) && iter <= maxIter
    tic
    
    % find the no run policy functions
    for ii = 1:n_total     
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Aggregate LoM for Z', Xi'
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states_no_run(ii,4) + p.sigma_Z*repmat(grd.eps_Z,grd.n_Xi,1);              
        logXi_prime = p.sigma_xi*kron(grd.eps_Xi,ones(grd.n_Z,1));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Next Period Value Functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        % n2n
        nextStates_n2n = [N_R_prime_n2n_old(ii,:)' N_S_prime_n2n_old(ii,:)' ...
            vec_K_prime_old(ii)*ones(size(logZ_prime)) logZ_prime];
        
        [ nextVals_n2n ] = computeExpectations( nextStates_n2n,funs,steady );
        
        % n2r
        nextStates_n2r = [N_R_prime_n2r_old(ii,:)' ...
            vec_K_prime_old(ii)*ones(size(logZ_prime)) logZ_prime];
        
        [ nextVals_n2r ] = computeExpectations_run( nextStates_n2r,funs_run );
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the System of FOC without binding CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x) FOC_anti(p,x, ...
            states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
            nextStates_n2n,nextStates_n2r,Z_prob_ext',min(exp(logXi_prime').*x_prime_old(ii,:),1),p_prime_old(ii,:));

        x0 = [vec_K_H_prime(ii,1);
                vec_K_R_prime(ii,1);
                vec_K_S_prime(ii,1);
                vec_C_H(ii,1)];
                    
        [x1,~,exitflag] = fsolve(handle,x0,options);
                
        [~,additionalVars] = handle(x1);
        
        vec_phi_R_tilde(ii,1) = additionalVars(1);
        vec_Q_tilde(ii,1)     = additionalVars(5);
        
        if vec_phi_R_tilde(ii,1) > p.phi_bar*vec_Q_tilde(ii,1)^p.phi_Q
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Solve the System of FOC with binding CR
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            handle = @(x) FOC_anti_with_CR_dynamic(p,x, ...
            states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
            nextStates_n2n,nextStates_n2r,Z_prob_ext',min(exp(logXi_prime').*x_prime_old(ii,:),1),p_prime_old(ii,:));
            
            x0 = x1;

            [x1,~,exitflag] = fsolve(handle,x0,options);
                        
            [~,additionalVars] = handle(x1);        
        end
        
%         if exitflag ~= 1
%             display(states_no_run(ii,:)./[steady.N_R steady.N_S steady.K p.mu_Z])
%             options = optimset('Display','iter','MaxIter',1e3,'MaxFunEvals',1e3);
%             error('No Solution found')
%         end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Store Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vec_K_H_prime(ii,1) = x1(1);
        vec_K_R_prime(ii,1) = x1(2);
        vec_K_S_prime(ii,1) = x1(3);           
        
        vec_phi_R(ii,1)     = additionalVars(1);
        vec_phi_S(ii,1)     = additionalVars(2);
        vec_C_H(ii,1)       = additionalVars(3);
        vec_B_prime(ii,1)   = additionalVars(4);      
        vec_Q(ii,1)         = additionalVars(5);
        vec_R_D_prime(ii,1) = additionalVars(6);
        vec_R_B_prime(ii,1) = additionalVars(7);
        vec_E_R_K_prime(ii,1) = additionalVars(8);
        
        vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
            + vec_K_S_prime(ii,1);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Update the Net Worth
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [N_R_prime_n2n(ii,:),N_S_prime_n2n(ii,:), ...
            N_R_prime_n2r(ii,:),N_S_prime_n2r(ii,:),x_prime(ii,:),p_prime(ii,:)] = ...
            N_prime_both(p, states_no_run(ii,:), x1, additionalVars, ...
            nextStates_n2n, nextVals_n2n, nextVals_n2r, logXi_prime');
    end

    for ii = 1:n_total_run      
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Aggregate LoM for Z'
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states_run(ii,3) + p.sigma_Z*grd.eps_Z; % no adjustment necessary, since no shadow banks
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Next Period Value Functions
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
        nextStates_r2n = [N_R_prime_r2n_old(ii,:)' N_S_prime_r2n(ii,:)' ...
            vec_K_prime_star_old(ii,1)*ones(size(logZ_prime)) logZ_prime];            
        
        nextStates_r2r    = [N_R_prime_r2r_old(ii,:)' ...
            vec_K_prime_star_old(ii,1)*ones(size(logZ_prime)) logZ_prime];
        
        nextVals_r2n = computeExpectations(nextStates_r2n,funs);
        
        nextVals_r2r = computeExpectations_run(nextStates_r2r,funs_run);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the System of FOC
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x) FOC_run(p,x, ...
            states_run(ii,:),nextVals_r2n,nextVals_r2r,nextStates_r2n,nextStates_r2r,grd.Z_prob');        

        x0 = [vec_K_H_prime_star(ii,1);
            vec_K_R_prime_star(ii,1);
            vec_C_H_star(ii,1)];

        [x1,~,~] = fsolve(handle,x0,options);      
                
        [~,additionalVars_run] = handle(x1);
        
        vec_phi_R_tilde_star(ii,1) = additionalVars_run(1);
        vec_Q_tilde_star(ii,1)     = additionalVars_run(3);
        
        if vec_phi_R_tilde_star(ii,1) > p.phi_bar*vec_Q_tilde_star(ii,1)^p.phi_Q && ...
                strcmp(reg,'run_exempt') ~= 1
            handle = @(x) FOC_run_with_CR_dynamic(p,x, ...
            states_run(ii,:),nextVals_r2n,nextVals_r2r,nextStates_r2n,nextStates_r2r,grd.Z_prob');        

            x0 = x1;

            [x1,~,~] = fsolve(handle,x0,options);      

            [~,additionalVars_run] = handle(x1);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Store Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vec_K_H_prime_star(ii,1) = x1(1);
        vec_K_R_prime_star(ii,1) = x1(2);
        
        vec_phi_R_star(ii,1)        = additionalVars_run(1);
        vec_C_H_star(ii,1)          = additionalVars_run(2);
        vec_Q_star(ii,1)            = additionalVars_run(3);
        vec_R_D_prime_star(ii,1)    = additionalVars_run(4);
        vec_R_B_prime_star(ii,1)    = additionalVars_run(5)	;

        vec_K_prime_star(ii,1) = vec_K_H_prime_star(ii,1) + vec_K_R_prime_star(ii,1);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Update the Net Worth
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [N_R_prime_r2n(ii,:),N_R_prime_r2r(ii,:)] = ...
             N_prime_run(p,states_run,x1,additionalVars_run,nextStates_r2n, ...
             nextVals_r2n,nextVals_r2r);
    end
    elapsedTime = toc;

    N_R_prime_n2n = bound(N_R_prime_n2n,0,1.5*funs.orig_max(1));
    N_R_prime_n2r = bound(N_R_prime_n2r,0,1.5*funs.orig_max(1));
    N_R_prime_r2n = bound(N_R_prime_r2n,0,1.5*funs.orig_max(1));
    N_R_prime_r2r = bound(N_R_prime_r2r,0,1.5*funs.orig_max(1));
    
    N_S_prime_n2n = bound(N_S_prime_n2n,0,funs.orig_max(2));
    N_S_prime_n2r = bound(N_S_prime_n2r,0,funs.orig_max(2));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Other Variables not needed in the Solution Algorithm
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_I = vec_K_prime - (1-p.delta)*vec_K;
    vec_D_prime = (vec_phi_R-1).*vec_N_R + (1-p.gamma)*vec_B_prime;
    vec_I_star = vec_K_prime_star - (1-p.delta)*vec_K_star;
    vec_D_prime_star = (vec_phi_R_star-1).*vec_N_R_star;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    vec_V_R = (p.sigma_R + (1-p.sigma_R)*p.psi*vec_phi_R_tilde).*(vec_N_R - p.exoInc*vec_K)/(1-p.sigma_R);
    vec_V_R_star = (p.sigma_R + (1-p.sigma_R)*p.psi*vec_phi_R_tilde_star).*(vec_N_R_star - p.exoInc*vec_K_star)/(1-p.sigma_R);
    
    vec_V_S = (p.sigma_S + (1-p.sigma_S)*p.psi*(p.omega*vec_phi_S + ...
                (1-p.omega))).*(vec_N_S - p.exoInc*vec_K)/(1-p.sigma_S);   
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check Convergence Criteria
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    valueDiff = [vec_C_H; vec_V_R; vec_V_S; vec_C_H_star; vec_V_R_star] - ...
        [vec_C_H_old; vec_V_R_old; vec_V_S_old; vec_C_H_star_old; vec_V_R_star_old];
    
    vError = norm(valueDiff,'inf');
    
    kError = norm(vec_K_prime - vec_K_prime_old,'inf');
    nError = norm(vec([N_R_prime_n2n; N_S_prime_n2n]) - vec([N_R_prime_n2n_old; N_S_prime_n2n_old]),'inf');
    
    if mod(iter,10) == 0  || iter == 1
        fprintf('Iteration %d: \n',iter);
        fprintf('Elapsed Time: %6.2f Seconds \n',elapsedTime);
        fprintf('Value Function Error: %6.6f \n',vError)
        fprintf('Net Worth Error %6.6f \n \n',max(kError,nError))
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Attenuation (Smoothing)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    weight_N = 0.5;
    weight_V = 1;
    
    vec_K_prime_old = weight_N*vec_K_prime + (1-weight_N)*vec_K_prime_old;
    vec_K_prime_star_old = weight_N*vec_K_prime_star + (1-weight_N)*vec_K_prime_star_old;
    
    N_R_prime_n2n_old = weight_N*N_R_prime_n2n + (1-weight_N)*N_R_prime_n2n_old;
    N_R_prime_n2r_old = weight_N*N_R_prime_n2r + (1-weight_N)*N_R_prime_n2r_old;
    N_R_prime_r2n_old = weight_N*N_R_prime_r2n + (1-weight_N)*N_R_prime_r2n_old;
    N_R_prime_r2r_old = weight_N*N_R_prime_r2r + (1-weight_N)*N_R_prime_r2r_old;
    
    N_S_prime_n2n_old = weight_N*N_S_prime_n2n + (1-weight_N)*N_S_prime_n2n_old; 
    N_S_prime_n2r_old = weight_N*N_S_prime_n2r + (1-weight_N)*N_S_prime_n2r_old; 

    vec_Q_old         = weight_N*vec_Q + (1-weight_N)*vec_Q_old;
    vec_Q_star_old    = weight_N*vec_Q_star + (1-weight_N)*vec_Q_star_old;
    
    p_prime_old = weight_N*p_prime + (1-weight_N)*p_prime_old;
    x_prime_old = weight_N*x_prime + (1-weight_N)*x_prime_old;
    
    vec_C_H_old = weight_V*vec_C_H + (1-weight_V)*vec_C_H_old;
    vec_V_R_old = weight_V*vec_V_R + (1-weight_V)*vec_V_R_old;
    vec_V_S_old = weight_V*vec_V_S + (1-weight_V)*vec_V_S_old;
    
    vec_C_H_star_old       = weight_V*vec_C_H_star + (1-weight_V)*vec_C_H_star_old;
    vec_V_R_star_old       = weight_V*vec_V_R_star + (1-weight_V)*vec_V_R_star_old;
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Interpolate Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%s
    funs.C_H    = funs.base_poly\vec_C_H; 
    funs.V_R    = funs.base_poly\vec_V_R; 
    funs.V_S    = funs.base_poly\vec_V_S;  
    funs.Q      = funs.base_poly\vec_Q;
    
    funs_run.C_H = funs_run.base_poly\vec_C_H_star_old;  
    funs_run.V_R = funs_run.base_poly\vec_V_R_star_old; 
    funs_run.Q   = funs_run.base_poly\vec_Q_star_old;  
    
    iter = iter + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Store Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pols = struct;
pols.K_H_prime  = vec_K_H_prime;
pols.K_R_prime  = vec_K_R_prime;
pols.K_S_prime  = vec_K_S_prime;
pols.K_prime    = vec_K_prime;
pols.I          = vec_I;
pols.D_prime    = vec_D_prime;
pols.B_prime    = vec_B_prime;
pols.phi_R      = vec_phi_R;
pols.phi_S      = vec_phi_S;
pols.R_D_prime  = vec_R_D_prime;
pols.R_B_prime  = vec_R_B_prime;
pols.Q          = vec_Q;
pols.N_R_prime_n2n  = N_R_prime_n2n;
pols.N_R_prime_n2r  = N_R_prime_n2r;
pols.N_S_prime_n2n  = N_S_prime_n2n;
pols.N_S_prime_n2r  = N_S_prime_n2r;
pols.C_H        = vec_C_H;
pols.V_R        = vec_V_R;
pols.V_S        = vec_V_S;
pols.p_prime    = p_prime;
pols.x_prime    = x_prime;

% additional policies to ensure backwards compatibility of the code
pols.N_R_prime  = N_R_prime_n2n(:,1:grd.n_Z);
pols.N_S_prime  = N_S_prime_n2n(:,1:grd.n_Z);

pols_run = struct;
pols_run.K_H_prime  = vec_K_H_prime_star;
pols_run.K_R_prime  = vec_K_R_prime_star;
pols_run.K_prime    = vec_K_prime_star;
pols_run.I          = vec_I_star;
pols_run.phi_R      = vec_phi_R_star;
pols_run.R_D_prime  = vec_R_D_prime_star;
pols_run.R_B_prime  = vec_R_B_prime_star;
pols_run.D_prime    = vec_D_prime_star;
pols_run.N_R_prime_r2n  = N_R_prime_r2n;
pols_run.N_R_prime_r2r = N_R_prime_r2r;
pols_run.C_H        = vec_C_H_star;
pols_run.V_R        = vec_V_R_star;
pols_run.Q          = vec_Q_star;

% additional policies to ensure backwards compatibility of the code
pols_run.N_R_prime  = N_R_prime_r2n;
pols_run.N_R_prime_star = N_R_prime_r2r;

%%
fileName_short = strcat('./Results/','Anti_Run_Solution.mat');
save(fileName_short,'pols','pols_run','funs','funs_run','grd','p','steady','states_no_run')

% save('initialGuess.mat','pols')
% save('initialGuess_Run.mat','pols_run')