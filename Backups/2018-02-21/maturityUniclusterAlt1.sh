#!/bin/sh
# Die erste Zeile legt den Kommandointerpreter fest. Statt der Default-Shell sh,
# die mit der Bourne Again Shell bash identisch ist, koennen Sie hier auch eine
# andere eintragen: ksh, tcsh, zsh. Sogar /usr/bin/perl ist erlaubt.

# Zeilen die mit #MSUB beginnen, enthalten Optionen für das msub

# smallest node
#MSUB -q singlenode

# give name
#MSUB -N maturityUnicluster

# Zahl der Knoten und Zahl der Cores (processors per node)
#MSUB -l nodes=1:ppn=16

# Obergrenze fuer die Laufzeit des Jobs
#MSUB -l walltime=04:00:00

# Send mail when job begins, aborts and ends
#MSUB -m bae

# Ausgabe von Informationen:
echo Running on host `hostname`
echo This job is running on  $SLURM_NPROCS processors
echo with $SLURM_MEM_PER_NODE of memory each.

# Wechsle in das Verzeichnis, in dem das msub Kommando ausgeführt wurde
cd $MOAB_SUBMITDIR

# Lade Software Module

module avail math/matlab
module load math/matlab

# Run application.
matlab -nodisplay -singleCompThread -r MainFile > maturityUnicluster.out 2>&1
 
