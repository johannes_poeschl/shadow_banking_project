clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Gamma_R_Vals = [1e-4 0.01:0.01:0.19];
nVals = length(Gamma_R_Vals);

is_reg = 'static';
is_run = 'with_runs';

load(strcat('./Results/',is_reg,'_',is_run,'/Results_GammaR_',num2str(Gamma_R_Vals(1)),'.mat'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

welfare_SS = zeros(nVals,4);

% compute the steady state welfare levels
for kVal = 1:nVals
    handle_no_CR = @(x) computeSteadyState(x,p);
    
    p.phi_bar = 1./Gamma_R_Vals(kVal);
        
    x0 = [0.2 0.4 0.4 0.0655 0.02]'*10;
    
    [x1,~,exitFlag] = fsolve(handle_no_CR,x0);

    [~,steady] = handle_no_CR(x1);
    
    if steady.phi_shadow > p.phi_bar
        handle_CR = @(x) computeSteadyState_with_CR(x,p);
        
        [x1,~,exitFlag] = fsolve(handle_CR,x1);

        [~,steady] = handle_CR(x1);
        resid = handle_no_CR(x1);
    end
    
    C_R = p.sigma_R/(1-p.sigma_R) * (steady.N_R - p.exoInc*steady.K);
    C_S = p.sigma_S/(1-p.sigma_S) * (steady.N_S - p.exoInc*steady.K);
    
    welfare_SS(kVal,1) = steady.C_H;
    welfare_SS(kVal,2) = C_R;
    welfare_SS(kVal,3) = C_S;
    welfare_SS(kVal,4) = log(steady.C_H)/(1-p.beta) + ...
        C_R/(1-p.beta*(1-p.sigma_R)) + ...
        C_S/(1-p.beta*(1-p.sigma_S));
end

% load the simulation, no run case
is_reg = 'static';
is_run = 'no_runs';

stored_Tables_no_runs = cell(nVals,4);
for kVal = 1:nVals
    load(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
        'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')
    
    stored_Tables_no_runs(kVal,1) = {Business_Cycle_Statistics};
    stored_Tables_no_runs(kVal,2) = {Bank_Run_Statistics};
    stored_Tables_no_runs(kVal,3) = {SS_Bounds};
    stored_Tables_no_runs(kVal,4) = {Welfare_Table};
end

% load the simulation, run case
is_reg = 'run_exempt';
is_run = 'with_runs';

stored_Tables = cell(nVals,4);
for kVal = 1:nVals
    load(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
        'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')
    
    stored_Tables(kVal,1) = {Business_Cycle_Statistics};
    stored_Tables(kVal,2) = {Bank_Run_Statistics};
    stored_Tables(kVal,3) = {SS_Bounds};
    stored_Tables(kVal,4) = {Welfare_Table};
end  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'$1/\bar{\phi}$'};

varSelect = [1 2 3];
iter = 0;

yVals = cell(length(varSelect),1);
yVals_no_runs = cell(length(varSelect),1);

yVals(:) = {ones(nVals,1)};
yVals_no_runs(:) = {ones(nVals,1)};

for kVal = 1:nVals
    stored_Tables{kVal, 4}.Welfare(2) = (1-p.beta*(1-p.sigma_R))*stored_Tables{kVal, 4}.Welfare(2);
    stored_Tables{kVal, 4}.Welfare(3) = (1-p.beta*(1-p.sigma_S))*stored_Tables{kVal, 4}.Welfare(3);
    
    stored_Tables_no_runs{kVal, 4}.Welfare(2) = (1-p.beta*(1-p.sigma_R))*stored_Tables_no_runs{kVal, 4}.Welfare(2);
    stored_Tables_no_runs{kVal, 4}.Welfare(3) = (1-p.beta*(1-p.sigma_S))*stored_Tables_no_runs{kVal, 4}.Welfare(3);
end
% yNames = {'Welfare, HH','Welfare, RB','Welfare, SB','Bank Run Prob'};
yNames = {'(a) Welfare, HH','(b) Welfare, RB','(c) Welfare, SB','(d) Utilitarian Welfare Function'};
for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
        yVals{iter,1}(kVal) = stored_Tables{kVal, 4}.Welfare(jRow);
        yVals_no_runs{iter,1}(kVal) = stored_Tables_no_runs{kVal, 4}.Welfare(jRow);
    end
end

for kVal = 1:nVals
    yVals{4,1}(kVal) = log(stored_Tables{kVal, 4}.Welfare(1))/(1-p.beta) + ...
        stored_Tables{kVal, 4}.Welfare(2)/(1-p.beta*(1-p.sigma_R)) + ...
        stored_Tables{kVal, 4}.Welfare(3)/(1-p.beta*(1-p.sigma_S));
    yVals_no_runs{4,1}(kVal) = log(stored_Tables_no_runs{kVal, 4}.Welfare(1))/(1-p.beta) + ...
        stored_Tables_no_runs{kVal, 4}.Welfare(2)/(1-p.beta*(1-p.sigma_R)) + ...
        stored_Tables_no_runs{kVal, 4}.Welfare(3)/(1-p.beta*(1-p.sigma_S));
%     yVals{4,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
%     yVals_no_runs{4,1}(kVal) = stored_Tables_no_runs{kVal, 2}.Mean(7);
end

nor = @(x,jRow) 100*(sign(x(1))*(x - x(1))/x(1));
% nor = @(x,jRow) x/welfare_SS(1,jRow)*(welfare_SS(1,jRow) > 0) + ...
%     welfare_SS(1,jRow)./x*(welfare_SS(1,jRow) <= 0);
% nor = @(x,jRow) x;
figure
for jRow = 1:4
    subplot(2,2,jRow)
%     if jRow < 4
        plot(xVals,nor(yVals{jRow,1},jRow),'b','LineWidth',2);
        hold
        plot(xVals,nor(yVals_no_runs{jRow,1},jRow),'r:','LineWidth',2);
        ylabel('% \Delta vs Baseline')
        plot(xVals,nor(welfare_SS(:,jRow),jRow),'k--');
        if jRow == 1
            hl = legend('With Runs','No Runs','Steady State');
            set(hl,'Location','SouthWest')
        end
%     else
%         plot(xVals,100*yVals{jRow,1},'b','LineWidth',2)
%         hold
%         plot(xVals,100*yVals_no_runs{jRow,1},'r:','LineWidth',2)
%         ylabel('%')
%     end
    title(yNames{jRow})
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,'Interpreter','latex')
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Welfare_alt_',is_reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Frequency and Severity of Runs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'$1/\bar{\phi}$'};

yVals = cell(length(varSelect),1);
yVals(:) = {ones(nVals,1)};

yNames = {'(a) E[p_t]','(b) E[Q_t^*]','(c) StDev(Q_t^*)'};
yLabels = {'%','% \Delta vs Baseline','% \Delta vs Baseline'};
for kVal = 1:nVals
    yVals{1,1}(kVal) = 100*stored_Tables{kVal, 2}.Mean(7);
    yVals{2,1}(kVal) = stored_Tables{kVal, 2}.Mean(2);
    yVals{3,1}(kVal) = stored_Tables{kVal, 2}.StDev(2);
end

nor = @(x) 100*(x/x(1) - 1);

figure
for jRow = 1:3
    subplot(1,3,jRow)
    if jRow == 1
        plot(xVals,yVals{jRow,1},'b','LineWidth',2)
    else
        plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
    end
    ylabel(yLabels{jRow})
    title(yNames{jRow})
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,'Interpreter','latex')  
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/4];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Bank_Run_Prob_',is_reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))