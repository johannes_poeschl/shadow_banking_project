% plot the bank run probability and expected future capital price as a
% function of the deposit level.

clear

reg_val = 0.0001;

reg = 'run_exempt';

load(strcat('./Results/',reg,'_with_runs/Results_GammaR_0.0001.mat'))

steady1 = steady;

load(strcat('./Results/',reg,'_with_runs/Results_GammaR_',num2str(reg_val),'.mat'))

steady = steady1;

p.theta = p.theta_max;

uncondStd = p.sigma_Z/sqrt((1-p.rho_Z^2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set States
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set a current state by fixing K^H, K^R, K^S, B, D
logZ = p.mu_Z - 0*uncondStd;
N_R = steady.N_R;
N_S = steady.N_S;
K   = steady.K;

N_R_old = N_R;
N_S_old = N_S;
nError = 10;

tol = 1e-6;
iter = 1;
maxIter = 100;
while nError > tol && iter <= maxIter
    states = [N_R N_S K logZ];   
    states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
    states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);
    
    Q = states_poly*funs.Q;
    r_K = p.alpha*exp(logZ).*K.^(p.alpha-1);
    R_K = r_K + (1-p.delta)*Q;
    
    N_R = (R_K*steady.K_R + steady.R_B*steady.B - steady.R_D*steady.D)*(1-p.sigma_R) + p.exoInc*K;
    N_S = (R_K*steady.K_S - steady.R_B*steady.B)*(1-p.sigma_S) + p.exoInc*K;
    
    nError = norm(vec(N_R - N_R_old));
    
    fprintf('Iteration %d, nError %6.4f \n',iter,nError);
    
    N_R_old = N_R;
    N_S_old = N_S;
    iter = iter+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% interpolate the policy for B'
coeffs_phi_R = funs.base_poly\pols.phi_R;
coeffs_K_H     = funs.base_poly\pols.K_H_prime;
coeffs_K_R     = funs.base_poly\pols.K_R_prime;
coeffs_K_S     = funs.base_poly\pols.K_S_prime;
coeffs_C_H     = funs.base_poly\pols.C_H;
coeffs_Q       = funs.base_poly\pols.Q;

act = struct;
act.K_prime = states_poly*(coeffs_K_H + coeffs_K_R + coeffs_K_S);
act.K_H_prime = states_poly*coeffs_K_H;
act.K_R_prime = states_poly*coeffs_K_R;
act.K_S_prime = states_poly*coeffs_K_S;
act.C_H = states_poly*coeffs_C_H;

Q = states_poly*funs.Q;

coeffs_K_H_star = funs_run.base_poly\pols_run.K_H_prime;
coeffs_K_R_star = funs_run.base_poly\pols_run.K_R_prime;
coeffs_Q_star   = funs_run.base_poly\pols_run.Q;
coeffs_C_H_star     = funs_run.base_poly\pols_run.C_H;

states_run = [N_R K logZ];   
states_resc = Rescale(states_run,funs_run.unit_min,funs_run.unit_max,funs_run.orig_min,funs_run.orig_max);
states_poly = Smolyak_Polynomial(states_resc,funs_run.nDims,funs_run.max_approx_level,funs_run.elem);

act.K_prime_run = states_poly*(coeffs_K_H_star + coeffs_K_R_star);
act.K_H_prime_run = states_poly*coeffs_K_H_star;
act.K_R_prime_run = states_poly*coeffs_K_R_star;
act.C_H_run = states_poly*coeffs_C_H_star;

Q_star = states_poly*funs_run.Q;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set price space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Q_space = 0.95:0.001:1.025;

nQ = length(Q_space);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Expectations and LoMs Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set nodes for Z' and xi' to compute the expectations
nNodes = 9;
[eps,trans] = qnwnorm(nNodes,0,1);

[eps_Z,eps_Xi] = ndgrid(eps,eps);
eps_Z = vec(eps_Z);
eps_Xi = vec(eps_Xi);

Z_prob_ext = kron(trans,trans);

% set up a grid for future net worth as a function of B', Z', xi'
% no run case
N_R_noRun_to_noRun_old  = repmat(pols.N_R_prime(1,:),nQ,nNodes);
N_S_noRun_to_noRun_old  = repmat(pols.N_S_prime(1,:),nQ,nNodes);
N_R_noRun_to_Run_old    = steady.N_R*ones(nQ,nNodes^2);
N_S_noRun_to_Run_old    = steady.N_S*ones(nQ,nNodes^2);

% run case
N_S_Run_to_noRun_old    = repmat(p.exoInc*states(1,3),nQ,nNodes^2);
N_R_Run_to_noRun_old    = repmat(pols_run.N_R_prime(1,:),nQ,nNodes);
N_R_Run_to_Run_old      = repmat(pols_run.N_R_prime(1,:),nQ,nNodes);

N_R_noRun_to_noRun_new  = N_R_noRun_to_noRun_old;
N_S_noRun_to_noRun_new  = N_S_noRun_to_noRun_old;
N_R_noRun_to_Run_new    = steady.N_R*ones(nQ,nNodes^2);

N_R_Run_to_noRun_new    = N_R_Run_to_noRun_old;
N_R_Run_to_Run_new      = N_R_Run_to_Run_old;

p_prime                 = zeros(nQ,nNodes^2);
p_prime_old             = p_prime;

x_prime                 = 1.05*ones(nQ,nNodes^2);
x_prime_old             = x_prime;

K_prime_demand   = repmat(pols.K_prime(1),nQ,nNodes^2);
K_prime_demand_run = repmat(pols_run.K_prime(1),nQ,nNodes^2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solve for the optimal investment level for each value of Q
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tol = 1e-6;
maxIter = 100;
    
iter = 1;
nError = 10;

jj = 1;

options = optimset('Display','off');

while iter <= maxIter && nError > tol
% compute for each B the solution to the rest of the equilibrium
    
for qq=1:nQ    
        
    logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states(jj,4) + p.sigma_Z*eps_Z;
    logXi_prime = p.sigma_xi*eps_Xi;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % No-Run FOC
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % first, collect the future state variables
    nextStates = [N_R_noRun_to_noRun_old(qq,:)' N_S_noRun_to_noRun_old(qq,:)' K_prime_demand(qq,:)' logZ_prime];
    nextStates_Run = [N_R_noRun_to_Run_old(qq,:)' K_prime_demand(qq,:)' logZ_prime];
        
    % second, compute the continuation values
    nextVals = computeExpectations(nextStates,funs,steady);
    nextVals_Run = computeExpectations_run(nextStates_Run,funs_run);
    
    % third, solve the FOC
    handle = @(x) FOC_anti_Q(p,x, ...
        states(jj,:),nextVals,nextVals_Run, ...
        nextStates,nextStates_Run,Z_prob_ext', ...
        min(exp(logXi_prime').*x_prime(qq,:),1),p_prime(qq,:),Q_space(qq));

    x0 = [act.K_H_prime act.K_R_prime act.K_S_prime act.C_H]';

    [x1_noRun,~,exitflag] = fsolve(handle,x0,options);

    [~,additionalVars] = handle(x1_noRun);
    
    vec_phi_R_tilde = additionalVars(1);
    vec_Q_tilde     = additionalVars(5);

    if vec_phi_R_tilde > p.phi_bar*vec_Q_tilde^p.phi_Q
        handle = @(x) FOC_anti_with_CR_dynamic_Q(p,x, ...
        states(jj,:),nextVals,nextVals_Run, ...
        nextStates,nextStates_Run,Z_prob_ext', ...
        min(exp(logXi_prime').*x_prime(qq,:),1),p_prime(qq,:),Q_space(qq));

%         x0 = x1_noRun;

        [x1_noRun,~,exitflag] = fsolve(handle,x0,options);

        [~,additionalVars] = handle(x1_noRun);        
    end  
    
    % fourth, update the net worth for the next iteration
    [N_R_noRun_to_noRun_new(qq,:),N_S_noRun_to_noRun_new(qq,:),N_R_noRun_to_Run_new(qq,:),~,x_prime(qq,:),p_prime(qq,:)] = ...
        N_prime_both(p,states(jj,:),x1_noRun,additionalVars,nextStates,nextVals,nextVals_Run,logXi_prime');
    
    K_prime_demand(qq,:) = sum(x1_noRun(1:3))*ones(1,nNodes^2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Run FOC
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % first, collect the future state variables
    nextStates = [N_R_Run_to_noRun_old(qq,:)' N_S_Run_to_noRun_old(qq,:)' K_prime_demand_run(qq,:)' logZ_prime];
    nextStates_Run = [N_R_Run_to_Run_old(qq,:)' K_prime_demand_run(qq,:)' logZ_prime];
        
    % second, compute the continuation values
    nextVals = computeExpectations(nextStates,funs,steady);
    nextVals_Run = computeExpectations_run(nextStates_Run,funs_run);
    
    % third, solve the FOC
    handle = @(x_prime) FOC_run_Q(p,x_prime, ...
        states_run(jj,:),nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob_ext',Q_space(qq));        

    x0 = [act.K_H_prime_run act.K_R_prime_run act.C_H_run]';

    [x1_Run,~,~] = fsolve(handle,x0,options);      

    [~,additionalVars] = handle(x1_Run);
    
    vec_phi_R_tilde_star = additionalVars(1);
    vec_Q_tilde_star     = additionalVars(3);
    
    if vec_phi_R_tilde_star > p.phi_bar*vec_Q_tilde_star^p.phi_Q && ...
            strcmp(reg,'run_exempt') ~= 1
        handle = @(x_prime) FOC_run_with_CR_dynamic_Q(p,x_prime, ...
        states_run(jj,:),nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob_ext',Q_space(qq));        

%         x0 = x1_Run;

        [x1_Run,~,~] = fsolve(handle,x0,options);      

        [~,additionalVars] = handle(x1_Run);
    end
    
    % fourth, update the net worth for the next iteration
    [N_R_Run_to_noRun_new(qq,:),N_R_Run_to_Run_new(qq,:)] = ...
             N_prime_run(p,states_run,x1_Run,additionalVars,nextStates, ...
             nextVals,nextVals_Run);
         
     K_prime_demand_run(qq,:) = sum(x1_Run(1:2))*ones(1,nNodes^2);
end

N_R_noRun_to_noRun_new = bound(N_R_noRun_to_noRun_new,0,2*steady.N_R);
N_S_noRun_to_noRun_new = bound(N_S_noRun_to_noRun_new,0,2*steady.N_S);
N_R_noRun_to_Run_new   = bound(N_R_noRun_to_Run_new,0,2*steady.N_R);

nError = norm(vec(N_R_noRun_to_noRun_new - N_R_noRun_to_noRun_old),'inf');

weight = 0.5;
N_R_noRun_to_noRun_old  = weight*N_R_noRun_to_noRun_old + (1-weight)*N_R_noRun_to_noRun_new;
N_R_noRun_to_Run_old    = weight*N_R_noRun_to_Run_old + (1-weight)*N_R_noRun_to_Run_new;
N_S_noRun_to_noRun_old  = weight*N_S_noRun_to_noRun_old + (1-weight)*N_S_noRun_to_noRun_new;

N_R_Run_to_noRun_old  = weight*N_R_Run_to_noRun_old + (1-weight)*N_R_Run_to_noRun_new;
N_R_Run_to_Run_old  = weight*N_R_Run_to_Run_old + (1-weight)*N_R_Run_to_Run_new;

p_prime                 = weight*p_prime_old + (1-weight)*p_prime;
x_prime                 = weight*x_prime_old + (1-weight)*x_prime;

p_prime_old = p_prime;
x_prime_old = x_prime;

fprintf('iteration %d, nError %6.4f \n',iter,nError)
iter = iter+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xiName = 'one_eq';
figureVersion = 'slides';

if strcmp(xiName,'two_eq') == 1
    xiVal = 0.955;
elseif strcmp(xiName,'one_eq') == 1
    xiVal = 0.98;
end

I_prime_supply = ((Q_space - 1) + p.theta*p.delta)*states(:,3)/p.theta;
I_prime_demand = K_prime_demand(:,1) - (1-p.delta)*states(:,3);
I_prime_demand_run = K_prime_demand_run(:,1) - (1-p.delta)*states(:,3);

I_prime_demand1 = I_prime_demand;
I_prime_demand_run1 = I_prime_demand_run;

Q_cutoff = 1/xiVal*(steady.R_B*steady.B/steady.K_S - p.alpha*exp(logZ)*K.^(p.alpha-1))/(1-p.delta);

I_prime_demand(Q_space < Q_cutoff) = nan;
I_prime_demand_run(Q_space >= Q_cutoff) = nan;

if exist(strcat('investment_demand_',num2str(0.0001),'.mat'),'file') == 2 && reg_val ~= 0.0001
    
    load(strcat('investment_demand_',num2str(0.0001),'.mat'))
    
    I_noreg_demand1 = I_noreg_demand;
    I_noreg_demand_run1 = I_noreg_demand_run;

    I_noreg_demand(Q_space < Q_cutoff) = nan;
    I_noreg_demand_run(Q_space >= Q_cutoff) = nan;
    
    gray = [0.65 0.65 0.65];
    
    figure
    p1 = plot(I_prime_demand,Q_space,'b','LineWidth',2);
    hold
    p2 = plot(I_prime_supply,Q_space,'r','LineWidth',2);
    plot(I_prime_demand_run,Q_space,'b','LineWidth',2)
    plot(I_prime_demand1,Q_space,'b:','LineWidth',1)
    plot(I_prime_demand_run1,Q_space,'b:','LineWidth',1)
    p4 = plot(I_noreg_demand,Q_space,'Color',gray,'LineWidth',2);
    plot(I_noreg_demand_run,Q_space,'Color',gray,'LineWidth',2)
    plot(I_noreg_demand1,Q_space,':','Color',gray,'LineWidth',1)
    plot(I_noreg_demand_run1,Q_space,':','Color',gray,'LineWidth',1)
    ylim([min(Q_space) max(Q_space)])
    xLim = [0.2 0.28];
    p3 = line(xLim,[Q_cutoff Q_cutoff]);
    xlim(xLim)
    set(p3,'Color','k')
    title('Capital Market Equilibrium')
    xlabel('Investment Quantity I_t')
    ylabel('Capital Price Q_t')
    hl = legend([p1 p4 p2 p3], ...
        ['Investment' newline 'Demand,' newline 'Regulation'], ...
        ['Investment' newline 'Demand,' newline 'no Regulation'], ...
        ['Investment' newline 'Supply'], ...
        ['Bank Run' newline 'Cutoff'] ...
        );
    set(hl,'Location','southoutside','Orientation','horizontal')
    legend('boxoff')
    
    figureName = strcat('./Figures/equilibrium_plot_',xiName,'_',reg);
else
    figure
    p1 = plot(I_prime_demand,Q_space,'b','LineWidth',2);
    hold
    p2 = plot(I_prime_supply,Q_space,'r','LineWidth',2);
    plot(I_prime_demand_run,Q_space,'b','LineWidth',2)
    plot(I_prime_demand1,Q_space,'b:','LineWidth',1)
    plot(I_prime_demand_run1,Q_space,'b:','LineWidth',1)
    ylim([min(Q_space) max(Q_space)])
    xLim = [0.2 0.28];
    p3 = line(xLim,[Q_cutoff Q_cutoff]);
    xlim(xLim)
    set(p3,'Color','k')
    title('Capital Market Equilibrium')
    xlabel('Investment Quantity I_t')
    ylabel('Capital Price Q_t')
    hl = legend([p1 p2 p3],'Investment Demand','Investment Supply','Bank Run Cutoff');
    set(hl,'Location','southoutside','Orientation','horizontal')
    legend('boxoff')
    
    I_noreg_demand = I_prime_demand1;
    I_noreg_demand_run = I_prime_demand_run1;
    
    save(strcat('investment_demand_',num2str(0.0001),'.mat'), ...
        'I_noreg_demand','I_noreg_demand_run')
    
    figureName = strcat('./Figures/equilibrium_plot_',xiName);
end

set(gca,'xtick',[])
set(gca,'xticklabel',[])

set(gca,'ytick',[])
set(gca,'yticklabel',[])

hRim = 0;
vRim = 0;
set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    paperDim = [12.8 9.6];
else
    paperDim = [21 29.7/2];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))