% plot the bank run probability and expected future capital price as a
% function of the deposit level.

clear

load('./Results/static_with_runs/Results_GammaR_0.0001.mat')
% load('./Results/run_exempt_with_runs_theta_10/Results_GammaR_0.0001.mat')

p.theta = p.theta_max;

uncondStd = p.sigma_Z/sqrt((1-p.rho_Z^2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set a current state by fixing K^H, K^R, K^S, B, D
logZ = [p.mu_Z - 2*uncondStd;
    p.mu_Z + 2*uncondStd];

N_R = steady.N_R*ones(2,1);
N_S = steady.N_S*ones(2,1);
K   = steady.K*ones(2,1);

N_R_old = N_R;
N_S_old = N_S;
nError = 10;
tol = 1e-6;
iter = 1;
maxIter = 100;
while nError > tol && iter <= maxIter
    states = [N_R N_S K logZ];   
    states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
    states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);
    
    Q = states_poly*funs.Q;
    r_K = p.alpha*exp(logZ).*K.^(p.alpha-1);
    R_K = r_K + (1-p.delta)*Q;
    
    N_R = (R_K*steady.K_R + steady.R_B*steady.B - steady.R_D*steady.D)*(1-p.sigma_R) + p.exoInc*K;
    N_S = (R_K*steady.K_S - steady.R_B*steady.B)*(1-p.sigma_S) + p.exoInc*K;
    
    nError = norm(vec(N_R - N_R_old));
    
    fprintf('Iteration %d, nError %6.4f \n',iter,nError);
    
    N_R_old = N_R;
    N_S_old = N_S;
    iter = iter+1;
end
% interpolate the policy for B'
coeffs_phi_R = funs.base_poly\pols.phi_R;
coeffs_K_H     = funs.base_poly\pols.K_H_prime;
coeffs_K_R     = funs.base_poly\pols.K_R_prime;
coeffs_K_S     = funs.base_poly\pols.K_S_prime;

coeffs_K_H_star = funs_run.base_poly\pols_run.K_H_prime;
coeffs_K_R_star = funs_run.base_poly\pols_run.K_R_prime;

% set a grid for B around the actual policy chosen in equilibrium
nD = 50;
phi_R_act = states_poly*coeffs_phi_R;

% phi_R_inv_space = sort([1./phi_R_act' linspace(1/phi_R_act(2),1,nD)]);
phi_R_inv_space = sort([1./phi_R_act' linspace(0.75*1/phi_R_act(1),0.25,nD)]);
phi_R_inv_space = unique(phi_R_inv_space);
nD = length(phi_R_inv_space);

% set nodes for Z' and xi' to compute the expectations
nNodes = 9;
[eps,trans] = qnwnorm(nNodes,0,1);

[eps_Z,eps_Xi] = ndgrid(eps,eps);
eps_Z = vec(eps_Z);
eps_Xi = vec(eps_Xi);

transAll = kron(trans,trans);

% set up a grid for future net worth as a function of B', Z', xi'
% no run case
N_R_noRun_to_noRun_old = repmat(pols.N_R_prime(1,:),nD,nNodes);
N_S_noRun_to_noRun_old = repmat(pols.N_S_prime(1,:),nD,nNodes);
N_S_Run_to_noRun_old   = pols.N_S_prime(1,:);
N_R_Run_to_noRun_old   = pols.N_R_prime(1,:);

K_prime1   = repmat(pols.K_prime(1),nD,nNodes^2);

N_R_noRun_to_noRun_new = N_R_noRun_to_noRun_old;
N_S_noRun_to_noRun_new = N_S_noRun_to_noRun_old;

% run case
N_R_noRun_to_Run_old    = steady.N_R*ones(nD,nNodes^2);
N_S_noRun_to_Run_old    = steady.N_S*ones(nD,nNodes^2);
N_R_Run_to_Run_old      = pols.N_R_prime(1,:);
p_prime                 = zeros(nD,nNodes^2);
x_prime                 = 1.05*ones(nD,nNodes^2);
Q_prime_star            = zeros(nD,nNodes^2);
N_R_noRun_to_Run_new    = steady.N_R*ones(nD,nNodes^2);
K_H_prime               = zeros(nD,nNodes^2);
K_R_prime               = zeros(nD,nNodes^2);
K_H_prime_star          = zeros(nD,nNodes^2);
K_R_prime_star          = zeros(nD,nNodes^2);

options = optimset('Display','off');

E_Q_prime       = zeros(nD,size(states,1));
E_p_prime       = zeros(nD,size(states,1));
E_Q_prime_star  = zeros(nD,size(states,1));
sd_Q_prime      = zeros(nD,size(states,1));
sd_Q_prime_star = zeros(nD,size(states,1));
RK_RB_ratio     = zeros(nD,size(states,1));
KS_BS_ratio     = zeros(nD,size(states,1));
K_prime         = zeros(nD,size(states,1));
E_K_H_pp        = zeros(nD,size(states,1));
E_K_R_pp        = zeros(nD,size(states,1));
E_K_S_pp        = zeros(nD,size(states,1));
E_I             = zeros(nD,size(states,1));
E_I_star        = zeros(nD,size(states,1));
I               = zeros(nD,size(states,1));

tol = 1e-6;
maxIter = 100;

Q = ones(nD,1);

for jj = 1:size(states,1)
    
iter = 1;
nError = 10;

while iter <= maxIter && nError > tol
% compute for each B the solution to the rest of the equilibrium
    
for dd=1:nD
    logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states(jj,4) + p.sigma_Z*eps_Z;
    logXi_prime = p.sigma_xi*eps_Xi;
    
    % first, collect the future state variables
    nextStates = [N_R_noRun_to_noRun_old(dd,:)' N_S_noRun_to_noRun_old(dd,:)' K_prime1(dd,:)' logZ_prime];
    nextStates_Run = [N_R_noRun_to_Run_old(dd,:)' K_prime1(dd,:)' logZ_prime];
        
    % second, compute the continuation values
    nextVals = computeExpectations(nextStates,funs,steady);
    nextVals_Run = computeExpectations_run(nextStates_Run,funs_run);
    
    % third, compute the FOC for all other variables
    handle = @(x0) FOC_D(p,x0,1/phi_R_inv_space(dd),states(jj,:),nextVals, ...
        nextStates,nextVals_Run,nextStates_Run,transAll', ...
        min(exp(logXi_prime').*x_prime(dd,:),1),p_prime(dd,:));
    
%     handle = @(x0) FOC_D(p,x0,1/phi_R_inv_space(dd),states(jj,:),nextVals, ...
%         nextStates,nextVals_Run,nextStates_Run,transAll');
    
    x0 = [pols.K_H_prime(1) pols.K_R_prime(1) pols.K_S_prime(1) pols.C_H(1)]';
    [x1,~,exitflag] = fsolve(handle,x0,options);
    
    if exitflag ~= 1
        error('No Solution found')
    end
    
    [~,additionalVars] = handle(x1);

    % fourth, update the net worth for the next iteration
    [N_R_noRun_to_noRun_new(dd,:),N_S_noRun_to_noRun_new(dd,:),N_R_noRun_to_Run_new(dd,:),~,x_prime(dd,:),p_prime(dd,:)] = ...
        N_prime_both(p,states(jj,:),x1,additionalVars,nextStates,nextVals,nextVals_Run,logXi_prime');
    K_prime1(dd,:) = sum(x1(1:3))*ones(1,nNodes^2);
    
    Q_prime_star(dd,:)      = nextVals_Run{3};
   
    % Compute the statistics of interest
    E_Q_prime(dd,jj)        = sum(transAll'.*(p_prime(dd,:).*nextVals_Run{3} + ...
        (1-p_prime(dd,:)).*nextVals{4}));
    E_Q_prime_star(dd,jj)   = sum(transAll'.*nextVals_Run{3});
    
    sd_Q_prime(dd,jj)       = sum(transAll'.*(p_prime(dd,:).*(nextVals_Run{3} - E_Q_prime(dd,jj)).^2 + ...
        (1-p_prime(dd,:)).*(nextVals{4} - E_Q_prime(dd,jj)).^2));
    
    sd_Q_prime_star(dd,jj)       = sum(transAll'.*(nextVals_Run{3} - E_Q_prime(dd,jj)).^2);
    
    E_p_prime(dd,jj)        = sum(transAll'.*p_prime(dd,:));
    K_prime(dd,jj)          = K_prime1(dd,1);
    
    nextStates_Resc = Rescale(nextStates,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    nextStates_Run_Resc = Rescale(nextStates_Run,funs_run.unit_min,funs_run.unit_max, ...
        funs_run.orig_min,funs_run.orig_max);
    
    nextPoly_Resc = Smolyak_Polynomial(nextStates_Resc,funs.nDims, ...
        funs.max_approx_level,funs.elem);
    nextPoly_Run_Resc = Smolyak_Polynomial(nextStates_Run_Resc,funs_run.nDims, ...
        funs_run.max_approx_level,funs_run.elem);
    
    K_H_prime(dd,:) = (nextPoly_Resc*coeffs_K_H)';
    K_H_prime_star(dd,:) = (nextPoly_Run_Resc*coeffs_K_H_star)';
    
    K_R_prime(dd,:) = (nextPoly_Resc*coeffs_K_R)';
    K_R_prime_star(dd,:) = (nextPoly_Run_Resc*coeffs_K_R_star)';
    
    K_S_prime = (nextPoly_Resc*coeffs_K_S)';
    
    E_K_H_pp(dd,jj) = sum(transAll'.*(p_prime(dd,:).*K_H_prime_star(dd,:) + ...
        (1-p_prime(dd,:)).*K_H_prime(dd,:)));
    E_K_R_pp(dd,jj) = sum(transAll'.*(p_prime(dd,:).*K_R_prime_star(dd,:) + ...
        (1-p_prime(dd,:)).*K_R_prime(dd,:)));
    E_K_S_pp(dd,jj) = sum(transAll'.*(1-p_prime(dd,:)).*K_S_prime);
    
    I(dd,jj) = sum(x1(1:3)) - (1-p.delta)*steady.K;
    
    E_I(dd,jj) = sum(transAll'.*(p_prime(dd,:).*(K_H_prime_star(dd,:) + ...
        K_R_prime_star(dd,:)) + ...
        (1-p_prime(dd,:)).*(K_H_prime(dd,:) + K_R_prime(dd,:) + K_S_prime))) - ...
        (1-p.delta)*K_prime(dd,jj);
    
    E_I_star(dd,jj) = sum(transAll'.*((K_H_prime_star(dd,:) + ...
        K_R_prime_star(dd,:)))) - ...
        (1-p.delta)*K_prime(dd,jj);
    
    Q(dd,jj)           = additionalVars(5);
    KS_BS_ratio(dd,jj) = x1(3)/additionalVars(4);
    RK_RB_ratio(dd,jj) = additionalVars(10)/additionalVars(7);

end

N_R_noRun_to_noRun_new = bound(N_R_noRun_to_noRun_new,0,2*funs.orig_max(1));
N_S_noRun_to_noRun_new = bound(N_S_noRun_to_noRun_new,0,2*funs.orig_max(2));
N_R_noRun_to_Run_new   = bound(N_R_noRun_to_Run_new,0,2*funs.orig_max(1));

nError = norm(vec(N_R_noRun_to_noRun_new - N_R_noRun_to_noRun_old),'inf');

weight = 0.75;
N_R_noRun_to_noRun_old  = weight*N_R_noRun_to_noRun_old + (1-weight)*N_R_noRun_to_noRun_new;
N_R_noRun_to_Run_old    = weight*N_R_noRun_to_Run_old + (1-weight)*N_R_noRun_to_Run_new;
N_S_noRun_to_noRun_old  = weight*N_S_noRun_to_noRun_old + (1-weight)*N_S_noRun_to_noRun_new;

fprintf('iteration %d, nError %6.4f \n',iter,nError)
iter = iter+1;
end
end
%% Future Capital Price and Liquidation Price

% nor = @(x) x;

yVals = cell(4,1);
yVals(1) = {E_Q_prime};
yVals(2) = {E_Q_prime_star};
yVals(3) = {Q};
yVals(4) = {sd_Q_prime_star};

yNames = {'(a) E_t[Q_{t+1}]','(b) E_t[Q^*_{t+1}]', ...
    '(c) Q_{t}','(d) StDev[Q^*_{t+1}]'};
yLabels = {'% \Delta vs actual','% \Delta vs actual', ...
    '% \Delta vs actual','% \Delta vs actual'};

figure
for aa = 1:3
    subplot(2,2,aa)
    nor = @(x) 100*(x/x((phi_R_inv_space == 1/phi_R_act(1))) - 1);
    yVals{aa}(phi_R_inv_space < 1/phi_R_act(1),1) = NaN;
    yVals{aa}(phi_R_inv_space < 1/phi_R_act(2),2) = NaN;
    plot(phi_R_inv_space,nor(yVals{aa}(:,1)),'LineWidth',2)
    hold
    nor = @(x) 100*(x/x((phi_R_inv_space == 1/phi_R_act(2))) - 1);
    plot(phi_R_inv_space,nor(yVals{aa}(:,2)),'r--','LineWidth',2)
    yLim = ylim;
    hl1 = line([1/phi_R_act(1) 1/phi_R_act(1)],yLim);
    hl2 = line([1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    ylabel(yLabels{aa})
    xlabel('1/\phi^R_t')
    title(yNames{aa})
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
end
hl = legend('Low Z','High Z');
set(hl,'Location','NorthEast')

nor = @(x) 100*x;

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/4];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Q_against_phi_R');
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% Future Bank Run Probability

yVals = cell(3,1);
yVals(1) = {E_p_prime};
yVals(2) = {RK_RB_ratio};
yVals(3) = {KS_BS_ratio};

yNames = {'(a) E_t[p_{t+1}]','(b) R^{K *}_{t+1}/R^B_{t+1}', ...
    '(c) K^S_{t+1}/B^S_{t+1}'};
yLabels = {'%','% \Delta','% \Delta'};

figure
for aa = 1:3
    if aa == 1
        nor = @(x) 100*x;
    else
        nor = @(x) 100*(x/x((phi_R_inv_space == 1/phi_R_act(1))) - 1);
    end
    subplot(1,3,aa)
    yVals{aa}(phi_R_inv_space < 1/phi_R_act(1),1) = NaN;
    yVals{aa}(phi_R_inv_space < 1/phi_R_act(2),2) = NaN;
    plot(phi_R_inv_space,nor(yVals{aa}(:,1)),'LineWidth',2)
    hold
    if aa ~= 1
        nor = @(x) 100*(x/x((phi_R_inv_space == 1/phi_R_act(2))) - 1);
    end
    plot(phi_R_inv_space,nor(yVals{aa}(:,2)),'r:','LineWidth',2)
    yLim = ylim;
    hl1 = line([1/phi_R_act(1) 1/phi_R_act(1)],yLim);
    hl2 = line([1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
    ylabel(yLabels{aa})
    xlabel('1/\phi^R_t')
    title(yNames{aa})
end
legend('Low Z','High Z')

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/4];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/p_against_phi_R');
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% Future Capital Stock and Investment Level

yVals = cell(4,1);
yVals(1) = {E_I};
yVals(2) = {E_I_star};
yVals(3) = {I};

yNames = {'(a) E_t [I_{t+1}]','(b) E_t [I^*_{t+1}]', ...
    '(c) I_{t}','(d) E_t [K^S_{t+2}]'};
yLabels = {'% \Delta vs actual','% \Delta vs actual','% \Delta vs actual' ...
    ,'% \Delta vs actual'};

figure
for aa = 1:3
    nor = @(x) 100*(x/x((phi_R_inv_space == 1/phi_R_act(1))) - 1);
    yVals{aa}(phi_R_inv_space < 1/phi_R_act(1),1) = NaN;
    yVals{aa}(phi_R_inv_space < 1/phi_R_act(2),2) = NaN;
    subplot(2,2,aa)
    plot(phi_R_inv_space,nor(yVals{aa}(:,1)),'LineWidth',2)
    hold
    nor = @(x) 100*(x/x((phi_R_inv_space == 1/phi_R_act(2))) - 1);
    plot(phi_R_inv_space,nor(yVals{aa}(:,2)),'r:','LineWidth',2)
    yLim = ylim;
    hl1 = line([1/phi_R_act(1) 1/phi_R_act(1)],yLim);
    hl2 = line([1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
    ylabel(yLabels{aa})
    xlabel('1/\phi^R_t')
    title(yNames{aa})
end
legend('Low Z','High Z')

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/K_against_phi_R');
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))