% plot the bank run probability and expected future capital price as a
% function of the deposit level.

clear

reg = 'run_exempt';

load('./Results/static_with_runs/Results_GammaR_0.0001.mat')

p.theta = p.theta_max;

uncondStd = p.sigma_Z/sqrt((1-p.rho_Z^2));

figureVersion = 'paper';
scalingFactor = 1.25;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set a current state by fixing K^H, K^R, K^S, B, D
logZ = [p.mu_Z - 2*uncondStd;
    p.mu_Z + 2*uncondStd];

N_R = steady.N_R*ones(2,1);
N_S = steady.N_S*ones(2,1);
K   = steady.K*ones(2,1);

N_R_old = N_R;
N_S_old = N_S;
nError = 10;
tol = 1e-6;
iter = 1;
maxIter = 100;
while nError > tol && iter <= maxIter
    states = [N_R N_S K logZ];   
    states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
    states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);
    
    Q = states_poly*funs.Q;
    r_K = p.alpha*exp(logZ).*K.^(p.alpha-1);
    R_K = r_K + (1-p.delta)*Q;
    
    N_R = (R_K*steady.K_R + steady.R_B*steady.B - steady.R_D*steady.D)*(1-p.sigma_R) + p.exoInc*K;
    N_S = (R_K*steady.K_S - steady.R_B*steady.B)*(1-p.sigma_S) + p.exoInc*K;
    
    nError = norm(vec(N_R - N_R_old));
    
    fprintf('Iteration %d, nError %6.4f \n',iter,nError);
    
    N_R_old = N_R;
    N_S_old = N_S;
    iter = iter+1;
end

% interpolate the policy for B'
coeffs_phi_R = funs.base_poly\pols.phi_R;
coeffs_K_H     = funs.base_poly\pols.K_H_prime;
coeffs_K_R     = funs.base_poly\pols.K_R_prime;
coeffs_K_S     = funs.base_poly\pols.K_S_prime;

coeffs_K_H_star = funs_run.base_poly\pols_run.K_H_prime;
coeffs_K_R_star = funs_run.base_poly\pols_run.K_R_prime;

% set a grid for B around the actual policy chosen in equilibrium
phi_R_act = states_poly*coeffs_phi_R;

% phi_R_inv_space = sort([1./phi_R_act' linspace(1/phi_R_act(2),1,nD)]);
phi_R_inv_space = [0.0001 0.01:0.01:0.2]';
nD = length(phi_R_inv_space);

% set nodes for Z' and xi' to compute the expectations
nNodes = 9;
[eps,trans] = qnwnorm(nNodes,0,1);

[eps_Z,eps_Xi] = ndgrid(eps,eps);
eps_Z = vec(eps_Z);
eps_Xi = vec(eps_Xi);

Z_prob_ext = kron(trans,trans);

% set up a grid for future net worth as a function of B', Z', xi'
% no run case
N_R_noRun_to_noRun_old = repmat(pols.N_R_prime(1,:),nD,nNodes);
N_S_noRun_to_noRun_old = repmat(pols.N_S_prime(1,:),nD,nNodes);
N_S_Run_to_noRun_old   = pols.N_S_prime(1,:);
N_R_Run_to_noRun_old   = pols.N_R_prime(1,:);

K_prime1   = repmat(pols.K_prime(1),nD,nNodes^2);

N_R_noRun_to_noRun_new = N_R_noRun_to_noRun_old;
N_S_noRun_to_noRun_new = N_S_noRun_to_noRun_old;

% run case
N_R_noRun_to_Run_old    = steady.N_R*ones(nD,nNodes^2);
N_S_noRun_to_Run_old    = steady.N_S*ones(nD,nNodes^2);
N_R_Run_to_Run_old      = pols.N_R_prime(1,:);
p_prime                 = zeros(nD,nNodes^2);
p_prime_old             = zeros(nD,nNodes^2);
x_prime                 = 1.05*ones(nD,nNodes^2);
x_prime_old             = 1.05*ones(nD,nNodes^2);
Q_prime_star            = zeros(nD,nNodes^2);
N_R_noRun_to_Run_new    = steady.N_R*ones(nD,nNodes^2);
K_H_prime               = zeros(nD,nNodes^2);
K_R_prime               = zeros(nD,nNodes^2);
K_H_prime_star          = zeros(nD,nNodes^2);
K_R_prime_star          = zeros(nD,nNodes^2);

options = optimset('Display','off');

E_Q_prime       = zeros(nD,size(states,1));
E_p_prime       = zeros(nD,size(states,1));
E_Q_prime_star  = zeros(nD,size(states,1));
sd_Q_prime      = zeros(nD,size(states,1));
sd_Q_prime_star = zeros(nD,size(states,1));
RK_RB_ratio     = zeros(nD,size(states,1));
KS_BS_ratio     = zeros(nD,size(states,1));
K_prime         = zeros(nD,size(states,1));
E_K_H_pp        = zeros(nD,size(states,1));
E_K_R_pp        = zeros(nD,size(states,1));
E_K_S_pp        = zeros(nD,size(states,1));
E_I             = zeros(nD,size(states,1));
E_I_star        = zeros(nD,size(states,1));
I               = zeros(nD,size(states,1));

tol = 1e-6;
maxIter = 100;

Q = ones(nD,1);

stored_funs = cell(length(phi_R_inv_space),4);

for dd=1:nD   
    load(strcat('./Results/',reg,'_with_runs/Results_GammaR_', ...
        num2str(phi_R_inv_space(dd)),'.mat'),'funs','funs_run')
    
    stored_funs(dd,1) = {funs};
    stored_funs(dd,2) = {funs_run};
    stored_funs(dd,3) = {pols};
    stored_funs(dd,4) = {pols_run};
end

for jj = 1:size(states,1)
    
iter = 1;
nError = 10;

while iter <= maxIter && nError > tol
% compute for each B the solution to the rest of the equilibrium
    
for dd=1:nD    
    funs        = stored_funs{dd,1};
    funs_run    = stored_funs{dd,2};
    pols        = stored_funs{dd,3};
    pols_run    = stored_funs{dd,4};
    
    p.phi_bar   = phi_R_inv_space(dd);
    
    logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states(jj,4) + p.sigma_Z*eps_Z;
    logXi_prime = p.sigma_xi*eps_Xi;
    
    % first, collect the future state variables
    nextStates = [N_R_noRun_to_noRun_old(dd,:)' N_S_noRun_to_noRun_old(dd,:)' K_prime1(dd,:)' logZ_prime];
    nextStates_Run = [N_R_noRun_to_Run_old(dd,:)' K_prime1(dd,:)' logZ_prime];
        
    % second, compute the continuation values
    nextVals = computeExpectations(nextStates,funs,steady);
    nextVals_Run = computeExpectations_run(nextStates_Run,funs_run);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Solve the System of FOC without binding CR
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    handle = @(x) FOC_anti(p,x, ...
        states(jj,:),nextVals,nextVals_Run, ...
        nextStates,nextStates_Run,Z_prob_ext', ...
        min(exp(logXi_prime').*x_prime(dd,:),1),p_prime(dd,:));

    x0 = [pols.K_H_prime(1) pols.K_R_prime(1) pols.K_S_prime(1) pols.C_H(1)]';

    [x1,~,exitflag] = fsolve(handle,x0,options);

    [~,additionalVars] = handle(x1);

    vec_phi_R_tilde = additionalVars(1);
    vec_Q_tilde     = additionalVars(5);

    if vec_phi_R_tilde > p.phi_bar*vec_Q_tilde^p.phi_Q
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the System of FOC with binding CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x) FOC_anti_with_CR_dynamic(p,x, ...
        states(jj,:),nextVals,nextVals_Run, ...
        nextStates,nextStates_Run,Z_prob_ext', ...
        min(exp(logXi_prime').*x_prime(dd,:),1),p_prime(dd,:));

        x0 = x1;

        [x1,~,exitflag] = fsolve(handle,x0,options);

        [~,additionalVars] = handle(x1);        
    end

    % fourth, update the net worth for the next iteration
    [N_R_noRun_to_noRun_new(dd,:),N_S_noRun_to_noRun_new(dd,:),N_R_noRun_to_Run_new(dd,:),~,x_prime(dd,:),p_prime(dd,:)] = ...
        N_prime_both(p,states(jj,:),x1,additionalVars,nextStates,nextVals,nextVals_Run,logXi_prime');
    K_prime1(dd,:) = sum(x1(1:3))*ones(1,nNodes^2);
    
    Q_prime_star(dd,:)      = nextVals_Run{3};
   
    % Compute the statistics of interest
    E_Q_prime(dd,jj)        = sum(Z_prob_ext'.*(p_prime(dd,:).*nextVals_Run{3} + ...
        (1-p_prime(dd,:)).*nextVals{4}));
    E_Q_prime_star(dd,jj)   = sum(Z_prob_ext'.*nextVals_Run{3});
    
    sd_Q_prime(dd,jj)       = sum(Z_prob_ext'.*(p_prime(dd,:).*(nextVals_Run{3} - E_Q_prime(dd,jj)).^2 + ...
        (1-p_prime(dd,:)).*(nextVals{4} - E_Q_prime(dd,jj)).^2));
    
    sd_Q_prime_star(dd,jj)       = sum(Z_prob_ext'.*(nextVals_Run{3} - E_Q_prime(dd,jj)).^2);
    
    E_p_prime(dd,jj)        = sum(Z_prob_ext'.*p_prime(dd,:));
    K_prime(dd,jj)          = K_prime1(dd,1);
    
    nextStates_Resc = Rescale(nextStates,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    nextStates_Run_Resc = Rescale(nextStates_Run,funs_run.unit_min,funs_run.unit_max, ...
        funs_run.orig_min,funs_run.orig_max);
    
    coeffs_K_H     = funs.base_poly\pols.K_H_prime;
    coeffs_K_R     = funs.base_poly\pols.K_R_prime;
    coeffs_K_S     = funs.base_poly\pols.K_S_prime;

    coeffs_K_H_star = funs_run.base_poly\pols_run.K_H_prime;
    coeffs_K_R_star = funs_run.base_poly\pols_run.K_R_prime;
    
    nextPoly_Resc = Smolyak_Polynomial(nextStates_Resc,funs.nDims, ...
        funs.max_approx_level,funs.elem);
    nextPoly_Run_Resc = Smolyak_Polynomial(nextStates_Run_Resc,funs_run.nDims, ...
        funs_run.max_approx_level,funs_run.elem);
    
    K_H_prime(dd,:) = (nextPoly_Resc*coeffs_K_H)';
    K_H_prime_star(dd,:) = (nextPoly_Run_Resc*coeffs_K_H_star)';
    
    K_R_prime(dd,:) = (nextPoly_Resc*coeffs_K_R)';
    K_R_prime_star(dd,:) = (nextPoly_Run_Resc*coeffs_K_R_star)';
    
    K_S_prime = (nextPoly_Resc*coeffs_K_S)';
    
    E_K_H_pp(dd,jj) = sum(Z_prob_ext'.*(p_prime(dd,:).*K_H_prime_star(dd,:) + ...
        (1-p_prime(dd,:)).*K_H_prime(dd,:)));
    E_K_R_pp(dd,jj) = sum(Z_prob_ext'.*(p_prime(dd,:).*K_R_prime_star(dd,:) + ...
        (1-p_prime(dd,:)).*K_R_prime(dd,:)));
    E_K_S_pp(dd,jj) = sum(Z_prob_ext'.*(1-p_prime(dd,:)).*K_S_prime);
    
    I(dd,jj) = sum(x1(1:3)) - (1-p.delta)*steady.K;
    
    E_I(dd,jj) = sum(Z_prob_ext'.*(p_prime(dd,:).*(K_H_prime_star(dd,:) + ...
        K_R_prime_star(dd,:)) + ...
        (1-p_prime(dd,:)).*(K_H_prime(dd,:) + K_R_prime(dd,:) + K_S_prime))) - ...
        (1-p.delta)*K_prime(dd,jj);
    
    E_I_star(dd,jj) = sum(Z_prob_ext'.*((K_H_prime_star(dd,:) + ...
        K_R_prime_star(dd,:)))) - ...
        (1-p.delta)*K_prime(dd,jj);
    
    Q(dd,jj)           = additionalVars(5);
    KS_BS_ratio(dd,jj) = x1(3)/additionalVars(4);
    RK_RB_ratio(dd,jj) = additionalVars(10)/additionalVars(7);
end

% N_R_noRun_to_noRun_new = bound(N_R_noRun_to_noRun_new,0,2*funs.orig_max(1));
% N_S_noRun_to_noRun_new = bound(N_S_noRun_to_noRun_new,0,2*funs.orig_max(2));
% N_R_noRun_to_Run_new   = bound(N_R_noRun_to_Run_new,0,2*funs.orig_max(1));

N_R_noRun_to_noRun_new = bound(N_R_noRun_to_noRun_new,0,2*steady.N_R);
N_S_noRun_to_noRun_new = bound(N_S_noRun_to_noRun_new,0,2*steady.N_S);
N_R_noRun_to_Run_new   = bound(N_R_noRun_to_Run_new,0,2*steady.N_R);

nError = norm(vec(N_R_noRun_to_noRun_new - N_R_noRun_to_noRun_old),'inf');

weight = 0.5;
N_R_noRun_to_noRun_old  = weight*N_R_noRun_to_noRun_old + (1-weight)*N_R_noRun_to_noRun_new;
N_R_noRun_to_Run_old    = weight*N_R_noRun_to_Run_old + (1-weight)*N_R_noRun_to_Run_new;
N_S_noRun_to_noRun_old  = weight*N_S_noRun_to_noRun_old + (1-weight)*N_S_noRun_to_noRun_new;
p_prime                 = weight*p_prime_old + (1-weight)*p_prime;
x_prime                 = weight*x_prime_old + (1-weight)*x_prime;

p_prime_old = p_prime;
x_prime_old = x_prime;

fprintf('iteration %d, nError %6.4f \n',iter,nError)
iter = iter+1;
end
end
%% Future Capital Price and Liquidation Price

yVals = cell(4,1);
yVals(1) = {E_Q_prime};
yVals(2) = {E_Q_prime_star};
yVals(3) = {Q};
yVals(4) = {sd_Q_prime_star};

yNames = {'(a) E_t[Q_{t+1}]','(b) E_t[Q^*_{t+1}]', ...
    '(c) Q_{t}','(d) StDev[Q^*_{t+1}]'};
yLabels = {'% \Delta vs actual','% \Delta vs actual', ...
    '% \Delta vs actual','% \Delta vs actual'};

figure
for aa = 1:2
    subplot(1,2,aa)
    nor = @(x) 100*(x/x(1) - 1);
    plot(phi_R_inv_space,nor(yVals{aa}(:,1)),'b','LineWidth',2)
    hold
    nor = @(x) 100*(x/x(1) - 1);
    plot(phi_R_inv_space,nor(yVals{aa}(:,2)),'r--','LineWidth',2)
    yLim = ylim;
    hl1 = line([1/phi_R_act(1) 1/phi_R_act(1)],yLim,'Color','b');
    hl2 = line([1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    ylabel(yLabels{aa})
    xlabel('$$1/\bar{\phi}^R_t$$','Interpreter','latex')
    title(yNames{aa})
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
end
hl = legend('Low Z','High Z');
if strcmp(reg,'static') == 1
    set(hl,'Location','SouthWest')
else
    set(hl,'Location','NorthWest')
end

nor = @(x) 100*x;

set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    hRim = scalingFactor*1;
    vRim = 0.25;
    paperDim = scalingFactor*[12.8 9.6];
else
    hRim = 0.25;
    vRim = 0.25;
    paperDim = [21 29.7/4];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/Q_against_phi_R_',reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% Future Bank Run Probability

yVals = cell(4,1);
yVals(1) = {E_p_prime};
yVals(2) = {E_Q_prime_star};
yVals(3) = {RK_RB_ratio};
yVals(4) = {KS_BS_ratio};

yNames = {'(a) E_t[p_{t+1}]','(b) E_t[Q^*_{t+1}]','(c) E_t[R^{K *}_{t+1}/R^B_{t+1}]', ...
    '(d) K^S_{t+1}/B^S_{t+1}'};
yLabels = {'%','% \Delta vs actual','% \Delta','% \Delta'};

figure
for aa = 1:4
    if aa == 1
        nor = @(x) 100*x;
    else
        nor = @(x) 100*(x/x(1) - 1);
    end
    subplot(2,2,aa)
    plot(phi_R_inv_space,nor(yVals{aa}(:,1)),'b','LineWidth',2)
    hold
    if aa ~= 1
        nor = @(x) 100*(x/x(1) - 1);
    end
    plot(phi_R_inv_space,nor(yVals{aa}(:,2)),'r:','LineWidth',2)
    yLim = ylim;
    hl1 = line([1/phi_R_act(1) 1/phi_R_act(1)],yLim,'Color','b');
    hl2 = line([1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
    ylabel(yLabels{aa})
    xlabel('$$1/\bar{\phi}^R_t$$','Interpreter','latex')
    title(yNames{aa})
end
hl = legend('Low Z','High Z');

if strcmp(reg,'static') == 1
    set(hl,'Location','NorthWest')
else
    set(hl,'Location','SouthWest')
end

vRim = 0.25;
hRim = 0.25;
set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    paperDim = scalingFactor*[12.8 9.6];
else
    paperDim = [21 29.7/2];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/p_against_phi_R_',reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% Future Capital Stock and Investment Level

yVals = cell(4,1);
yVals(1) = {E_I};
yVals(2) = {E_I_star};
yVals(3) = {I};

yNames = {'(a) E_t [I_{t+1}]','(b) E_t [I^*_{t+1}]', ...
    '(c) I_{t}','(d) E_t [K^S_{t+2}]'};
yLabels = {'% \Delta vs actual','% \Delta vs actual','% \Delta vs actual' ...
    ,'% \Delta vs actual'};

figure
for aa = 1:2
    nor = @(x) 100*(x/x(1) - 1);
    subplot(1,2,aa)
    plot(phi_R_inv_space,nor(yVals{aa}(:,1)),'b','LineWidth',2)
    hold
    nor = @(x) 100*(x/x(1) - 1);
    plot(phi_R_inv_space,nor(yVals{aa}(:,2)),'r:','LineWidth',2)
    yLim = ylim;
    hl1 = line([1/phi_R_act(1) 1/phi_R_act(1)],yLim,'Color','b');
    hl2 = line([1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
    ylabel(yLabels{aa})
    xlabel('$$1/\bar{\phi}^R_t$$','Interpreter','latex')
    title(yNames{aa})
end
legend('Low Z','High Z')

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    paperDim = [12.8 9.6];
else
    paperDim = [21 29.7/2];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/K_against_phi_R_',reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% Future Bank Run  - Slides

yVals = cell(2,1);
yVals(1) = {E_p_prime};
yVals(2) = {E_Q_prime_star};

yNames = {'(a) p_{t+1}','(b) E_t[Q^*_{t+1}]'};
yLabels = {'Bank Run Probability (\%)','Liquidation Price (\% $\Delta$)'};

figure
for aa = 1:2
    if aa == 1
        nor = @(x) 100*x;
    else
        nor = @(x) 100*(x/x(1) - 1);
    end
    subplot(1,2,aa)
    hl0 = plot(100*phi_R_inv_space,nor(yVals{aa}(:,1)),'b','LineWidth',2);
    hold
    if aa ~= 1
        nor = @(x) 100*(x/x(1) - 1);
    end
    hl1 = plot(100*phi_R_inv_space,nor(yVals{aa}(:,2)),'r:','LineWidth',2);
    yLim = ylim;
    hl3 = line(100*[1/phi_R_act(1) 1/phi_R_act(1)],yLim,'Color','b');
    hl4 = line(100*[1/phi_R_act(2) 1/phi_R_act(2)],yLim,'LineStyle','--','Color','r');
    hl5 = line(100*[min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    xlim(100*[min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
    ylabel(yLabels{aa},'Interpreter','latex')
    xlabel('Capital Requirement $$1/\bar{\phi}^R_t$$ (\%)','Interpreter','latex')
    title(yNames{aa})
end
hl = legend([hl0 hl1],'Recession','Expansion');

if strcmp(reg,'static') == 1
    set(hl,'Location','SouthWest')
else
    set(hl,'Location','NorthWest')
end

vRim = 0.25;
hRim = 0.25;
set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    paperDim = 1.35*[12.8 0.5*9.6];
else
    paperDim = [21 29.7/2];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

% figureName = strcat('./Figures/p_against_phi_R_',reg);
% print(figureName,'-dpdf')
% saveas(gcf,strcat(figureName,'.fig'))