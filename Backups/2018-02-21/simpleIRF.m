% set simulation parameters
nBurnin = 500;
nT = 40;
nSimulations = nBurnin + nT;  % number of simulated periods 
nPanels = 2; %%%%% number of simulations for each capital requirement level
bankRun = 1;

% draw shocks
seq_epsZ = zeros(nPanels,nSimulations);
seq_epsZ(1,nBurnin+10) = -2*p.sigma_Z;
seq_epsZ(2,nBurnin+10) = -2*p.sigma_Z;
seq_logZ = p.mu_Z*ones(nPanels,nSimulations);
n_Z = 25;
[Z_grid,Z_prob] = tauchen(n_Z,0,p.rho_Z,p.sigma_Z,3);

for tt = 2:nSimulations  %%%%% for each simulation 
    seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + seq_epsZ(:,tt);
end

shocks.logZ = seq_logZ;
shocks.nu = 1.5 + 0*normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
shocks.pi = 2 + 0*normrnd(0,1,nPanels,nSimulations);
if bankRun == 1
    shocks.nu(1,nBurnin+10) = 0.95;
    shocks.pi(1,nBurnin+11:nBurnin+20) = 0;
end

shocks.burnin = nBurnin;

% [TS,TS_add] = simulateModel_noRuns(p,shocks,funs,pols,steady);
[TS,TS_add] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

%% Plots - presentation - landscape

time = [1:nT]-10;

plotVals = cell(7,1);
plotVals(1) = {TS.logZ};
plotVals(2) = {TS_add.Y};
plotVals(3) = {TS_add.I};
plotVals(4) = {TS_add.C_H};
plotVals(5) = {TS.Q};
plotVals(6) = {TS.N_R};
plotVals(7) = {TS.N_S};
plotNames = {'log(Z)','Y','I','C_H','Q','N_R','N_S'};

figure
for ii = 1:length(plotVals)
    subplot(3,3,ii)
    yVal = plotVals{ii};
    if ii ~= 1 && ii ~= 8
        plot(time,yVal(1,:)./yVal(1,1) - 1,':','LineWidth',2)
        hold
        plot(time,yVal(2,:)./yVal(2,1)-1,'r','LineWidth',2)
        maxVal = max(max(yVal(1,:)./yVal(1,1) - 1),max(yVal(2,:)./yVal(2,1) - 1));
        minVal = min(min(yVal(1,:)./yVal(1,1) - 1),min(yVal(2,:)./yVal(2,1) - 1));       
        ylim([(1-sign(minVal)*0.2)*minVal (1+sign(maxVal)*0.2)*maxVal])
    else
        plot(time,yVal(1,:) - yVal(1,1),':','LineWidth',2)
        hold
        plot(time,yVal(2,:) - yVal(2,1),'r','LineWidth',2)
        maxVal = max(max(yVal(1,:) - yVal(1,1)),max(yVal(2,:) - yVal(2,1)));
        minVal = min(min(yVal(1,:) - yVal(1,1)),min(yVal(2,:) - yVal(2,1)));
        ylim([(1-sign(minVal)*0.2)*minVal (1+sign(maxVal)*0.2)*maxVal])
    end
    title(plotNames{ii})
    xlabel('Time (Quarters)')
    ylabel('% \Delta from SS')
    xlim([time(1) time(end)])
end
hl = legend('Run','No Run');
set(hl,'Position',[0.730 0.375 0.077 0.072],...
    'FontSize',14,'Orientation','Vertical')

%     legend('Run','No Run')

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/IRF_landscape');
print(figureName,'-dpdf')

%% Plots - presentation - portrait

time = [1:length(TS.logZ)]-10;

plotVals = cell(7,1);
plotVals(1) = {TS.logZ};
plotVals(2) = {TS_add.Y};
plotVals(3) = {TS_add.I};
plotVals(4) = {TS_add.C_H};
plotVals(5) = {TS.Q};
plotVals(6) = {TS.N_R};
plotVals(7) = {TS.N_S};
plotNames = {'log(Z)','Y','I','C_H','Q','N_R','N_S'};

figure
for ii = 1:length(plotVals)
    subplot(4,2,ii)
    yVal = plotVals{ii};
    if ii ~= 1 && ii ~= 8
        plot(time,yVal(1,:)./yVal(1,1) - 1,':','LineWidth',2)
        hold
        plot(time,yVal(2,:)./yVal(2,1) - 1,'r','LineWidth',2)
        maxVal = max(max(yVal(1,:)./yVal(1,1) - 1),max(yVal(2,:)./yVal(2,1) - 1));
        minVal = min(min(yVal(1,:)./yVal(1,1) - 1),min(yVal(2,:)./yVal(2,1) - 1));       
        ylim([(1-sign(minVal)*0.2)*minVal (1+sign(maxVal)*0.2)*maxVal])
    else
        plot(time,yVal(1,:) - yVal(1,1),':','LineWidth',2)
        hold
        plot(time,yVal(2,:) - yVal(2,1),'r','LineWidth',2)
        maxVal = max(max(yVal(1,:) - yVal(1,1)),max(yVal(2,:) - yVal(2,1)));
        minVal = min(min(yVal(1,:) - yVal(1,1)),min(yVal(2,:) - yVal(2,1)));
        ylim([(1-sign(minVal)*0.2)*minVal (1+sign(maxVal)*0.2)*maxVal])
    end
    title(plotNames{ii})
    ylabel('% \Delta from SS')
    xlim([time(1) time(end)])
end
xlabel('Time (Quarters)')
hl = legend('Run','No Run');
set(hl,'Position',[0.562 0.110 0.185 0.125],...
    'FontSize',14,'Orientation','Vertical')

%     legend('Run','No Run')

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [29.7/2 21];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/IRF_portrait');
print(figureName,'-dpdf')