function [plot_vals,cell_grid] = smolyak_plot(smol_vals,funs,sz_array)
% smolyak_plot interpolates smolyak coefficients on a rectangular array

nDims = length(sz_array);

% construct the grid vectors
cell_grid = cell(nDims,1);
for ii = 1:nDims
    if sz_array(ii) > 1
        cell_grid(ii) = {linspace(funs.orig_min(ii),funs.orig_max(ii),sz_array(ii))};
    elseif sz_array(ii) == 1 && ii == 3
        cell_grid(ii) = {(funs.orig_min(ii) + funs.orig_max(ii))/2};
%         cell_grid(ii) = {funs.orig_min(ii)};
%         cell_grid(ii) = {funs.orig_max(ii)};
    else
        cell_grid(ii) = {(funs.orig_min(ii) + funs.orig_max(ii))/2};
%         cell_grid(ii) = {funs.orig_min(ii)};
    end
end

if nDims == 4
    % construct the nd-grid
    [nd_nr,nd_ns,nd_k,nd_z] = ndgrid(cell_grid{1},cell_grid{2},cell_grid{3},cell_grid{4});

    % construct the grid matrix
    states = [vec(nd_nr) vec(nd_ns) vec(nd_k) vec(nd_z)];
elseif nDims == 3
    % construct the nd-grid
    [nd_nr,nd_k,nd_z] = ndgrid(cell_grid{1},cell_grid{2},cell_grid{3});

    % construct the grid matrix
    states = [vec(nd_nr) vec(nd_k) vec(nd_z)];
end

states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);

% compute the smolyak coefficients
coeffs = funs.base_poly\smol_vals;

% compute the vectorized smolyak coefficients
vec_plot_vals = states_poly*coeffs;

% reshape into nd output format
plot_vals = squeeze(reshape(vec_plot_vals,sz_array));