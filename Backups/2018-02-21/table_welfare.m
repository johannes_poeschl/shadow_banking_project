clear

load('./Results/static_no_runs/Moments_GammaR_0.0001.mat')

Welfare_no_runs = Welfare_Table;
Business_Cycle_no_runs = Business_Cycle_Statistics;

load('./Results/static_with_runs/Moments_GammaR_0.0001.mat')

Welfare_with_runs = Welfare_Table;
Business_Cycle_with_runs = Business_Cycle_Statistics;

RowNames = {'$U^H$','$U^R$','$U^S$'};

Stats_no_runs = Welfare_no_runs.Welfare;
Stats_with_runs = Welfare_with_runs.Welfare;
PctDiff = 100*(Stats_no_runs./Stats_with_runs-1);

t = table(Stats_no_runs, ...
    Stats_with_runs, ...
    PctDiff,'RowNames',RowNames)

input.data = table2array(t);
input.tablePositioning = 'h';
input.booktabs = 1;
input.tableCaption = 'Cost of Runs.';
input.dataFormat = {'%.3f'};
input.tableColLabels = {'Baseline, No Runs','Baseline, With Runs','Difference'};
input.tableRowLabels = RowNames;

char(latexTable(input))


