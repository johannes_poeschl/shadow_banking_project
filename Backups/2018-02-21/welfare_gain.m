% Computes the welfare gain from eliminating bank runs and puts it into a
% table.

% set simulation parameters
nBurnin = 200; 
nSimulations = nBurnin + 8000;  % number of simulated periods 
nPanels = 100; %%%%% number of simulations for each capital requirement level
bankRun = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Draw Shocks (except sunspot)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
seq_logZ = ones(nPanels,nSimulations)*p.mu_Z;
seq_epsZ = randn(nPanels,nSimulations);

for tt = 2:nSimulations  %%%%% for each simulation    
    seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + p.sigma_Z*seq_epsZ(:,tt);
end

shocks.logZ = seq_logZ;
shocks.pi = normrnd(0,1,nPanels,nSimulations);
shocks.burnin = nBurnin;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation without Runs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
shocks.nu = 1 + 0*normrnd(0,1,nPanels,nSimulations);

% simulation
[TS_no_run,TS_add_no_run] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation with Runs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p.sigma_xi = 0.01325;
p.mu_xi = 0 - p.sigma_Z^2/2;

shocks.nu = exp(normrnd(p.mu_xi,p.sigma_xi,nPanels,nSimulations));

% simulation
[TS_run,TS_add_run] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Welfare Statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tt = 1:(nSimulations - nBurnin);

disc_H = p.beta.^tt;
disc_R = (p.beta*(1-p.sigma_R)).^tt;
disc_S = (p.beta*(1-p.sigma_S)).^tt;

% without runs
util_H = sum(repmat(disc_H,nPanels,1).*TS_add_no_run.C_H,2);
ex_ante_util_H = mean(exp((1-p.beta)*util_H));

util_R = sum(repmat(disc_R,nPanels,1).*TS_add_no_run.C_R,2);
ex_ante_util_R = mean(util_R);

util_S = sum(repmat(disc_S,nPanels,1).*TS_add_no_run.C_S,2);
ex_ante_util_S = mean(util_S);

welfare_no_runs = [ex_ante_util_H; ex_ante_util_R; ex_ante_util_S];

% with runs
util_H = sum(repmat(disc_H,nPanels,1).*TS_add_run.C_H,2);
ex_ante_util_H = mean(exp((1-p.beta)*util_H));

util_R = sum(repmat(disc_R,nPanels,1).*TS_add_run.C_R,2);
ex_ante_util_R = mean(util_R);

util_S = sum(repmat(disc_S,nPanels,1).*TS_add_run.C_S,2);
ex_ante_util_S = mean(util_S);

welfare_runs = [ex_ante_util_H; ex_ante_util_R; ex_ante_util_S];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('Probability of Runs: %6.4f percent per year \n',400*mean(vec(TS_run.run_starts)))

RowNames = {'HH','RB','SB'};

Welfare_Table = table(100*(welfare_no_runs./welfare_runs - 1), ...
    'RowNames',RowNames,'VariableNames',{'Welfare'});

display(Welfare_Table)

input = struct;
input.data = table2array(Welfare_Table);
input.tableRowLabels = RowNames;
input.booktabs = 1;
lTable = latexTable(input);