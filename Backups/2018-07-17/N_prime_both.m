function [N_R_prime,N_S_prime,N_R_prime_star,N_S_prime_star,x_prime,p_prime] = N_prime_both(p, states, x0, additionalVars, ...
    nextStates, nextVals, nextVals_run, logXi_prime)
% HH_RB_SB_compute_N_prime computes the next period net worth

N_R = states(1,1);
N_S = states(1,2);
K   = states(1,3);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);

phi_R = additionalVars(1);
phi_S = additionalVars(2);
B_prime = additionalVars(4);
Q       = additionalVars(5);
R_D_prime = additionalVars(6);
R_B_prime = additionalVars(7);

K_prime = K_H_prime + K_R_prime + K_S_prime;
D_prime = (Q + p.eta_R*K_R_prime./K).*K_R_prime + B_prime - N_R;
logZ_prime = nextStates(:,4)';

Q_prime = nextVals{4};
Q_prime_star = nextVals_run{3};

R_K_prime = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime;
R_K_prime_star = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha - 1) + (1 - p.delta)*Q_prime_star;

x_prime = R_K_prime_star.*K_S_prime./(R_B_prime.*B_prime);

if min(x_prime) < 0
    error('negative xPrime!')
end

p_prime = normcdf(log(1./(exp(logXi_prime).*x_prime)),0,0.0025);

% N'
N_R_prime = (R_K_prime.*K_R_prime + R_B_prime.*B_prime - R_D_prime.*D_prime)*(1-p.sigma_R) + ...
    p.exoInc*K_prime;  % /////
N_S_prime = (R_K_prime.*K_S_prime - R_B_prime.*B_prime)*(1-p.sigma_S) + p.exoInc*K_prime;  % /////

N_R_prime_star = (R_K_prime_star.*K_R_prime + min(exp(logXi_prime).*x_prime,1).*R_B_prime.*B_prime - R_D_prime.*D_prime)*(1-p.sigma_R) + ...
    p.exoInc*K_prime;  % /////
N_S_prime_star = zeros(size(N_S_prime));  % /////

