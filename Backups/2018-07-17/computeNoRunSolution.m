function [pols,vals] = computeNoRunSolution(p,steady,grd,states_no_run,funs,funs_run, ...
    pols_old,options)

Z_prob_ext      = kron(grd.Xi_prob,grd.Z_prob);

vec_N_R = states_no_run(:,1);
vec_N_S = states_no_run(:,2);
vec_K   = states_no_run(:,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preallocate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_phi_R_tilde = zeros(grd.n_total,1);
vec_Q_tilde     = zeros(grd.n_total,1);
vec_K_H_prime   = zeros(grd.n_total,1);
vec_K_R_prime   = zeros(grd.n_total,1);
vec_K_S_prime   = zeros(grd.n_total,1);
vec_phi_R       = zeros(grd.n_total,1);
vec_phi_S       = zeros(grd.n_total,1);
vec_C_H         = zeros(grd.n_total,1);
vec_B_prime     = zeros(grd.n_total,1);
vec_Q           = zeros(grd.n_total,1);
vec_R_D_prime   = zeros(grd.n_total,1);
vec_R_B_prime   = zeros(grd.n_total,1);
vec_E_R_K_prime = zeros(grd.n_total,1);
vec_K_prime     = zeros(grd.n_total,1);

vec_N_R_prime_n2n   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_N_S_prime_n2n   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_N_R_prime_n2r   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_N_S_prime_n2r   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_x_prime         = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_p_prime         = zeros(grd.n_total,grd.n_Z*grd.n_Xi);

for ii = 1:grd.n_total     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Aggregate LoM for Z', Xi'
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states_no_run(ii,4) + p.sigma_Z*repmat(grd.eps_Z,grd.n_Xi,1);              
    logXi_prime = p.sigma_xi*kron(grd.eps_Xi,ones(grd.n_Z,1));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Next Period Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    % n2n
    nextStates_n2n = [pols_old.N_R_prime_n2n(ii,:)' pols_old.N_S_prime_n2n(ii,:)' ...
        pols_old.K_prime(ii)*ones(size(logZ_prime)) logZ_prime];

    [ nextVals_n2n ] = computeExpectations( nextStates_n2n,funs,steady );

    % n2r
    nextStates_n2r = [pols_old.N_R_prime_n2r(ii,:)' ...
        pols_old.K_prime(ii)*ones(size(logZ_prime)) logZ_prime];

    [ nextVals_n2r ] = computeExpectations_run( nextStates_n2r,funs_run );

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Solve the System of FOC without binding CR
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    handle = @(x) FOC_anti(p,x, ...
        states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
        nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
        min(exp(logXi_prime').*pols_old.x_prime(ii,:),1),pols_old.p_prime(ii,:));

    x0 = [pols_old.K_H_prime(ii,1);
            pols_old.K_R_prime(ii,1);
            pols_old.K_S_prime(ii,1);
            pols_old.C_H(ii,1)];

    x1 = fsolve(handle,x0,options);

    [~,additionalVars] = handle(x1);

    vec_phi_R_tilde(ii,1) = additionalVars(1);
    vec_Q_tilde(ii,1)     = additionalVars(5);

    if vec_phi_R_tilde(ii,1) > p.phi_bar*vec_Q_tilde(ii,1)^p.phi_Q
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Solve the System of FOC with binding CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x) FOC_anti_with_CR_dynamic(p,x, ...
        states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
        nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
        min(exp(logXi_prime').*pols_old.x_prime(ii,:),1),pols_old.p_prime(ii,:));

        x0 = x1;

        x1 = fsolve(handle,x0,options);

        [~,additionalVars] = handle(x1);        
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Store Solution
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_K_H_prime(ii,1) = x1(1);
    vec_K_R_prime(ii,1) = x1(2);
    vec_K_S_prime(ii,1) = x1(3);           

    vec_phi_R(ii,1)     = additionalVars(1);
    vec_phi_S(ii,1)     = additionalVars(2);
    vec_C_H(ii,1)       = additionalVars(3);
    vec_B_prime(ii,1)   = additionalVars(4);      
    vec_Q(ii,1)         = additionalVars(5);
    vec_R_D_prime(ii,1) = additionalVars(6);
    vec_R_B_prime(ii,1) = additionalVars(7);
    vec_E_R_K_prime(ii,1) = additionalVars(8);

    vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
        + vec_K_S_prime(ii,1);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the Net Worth
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [vec_N_R_prime_n2n(ii,:),vec_N_S_prime_n2n(ii,:), ...
        vec_N_R_prime_n2r(ii,:),vec_N_S_prime_n2r(ii,:),vec_x_prime(ii,:),vec_p_prime(ii,:)] = ...
        N_prime_both(p, states_no_run(ii,:), x1, additionalVars, ...
        nextStates_n2n, nextVals_n2n, nextVals_n2r, logXi_prime');
end
    
vec_N_R_prime_n2n = bound(vec_N_R_prime_n2n,0,1.5*funs.orig_max(1));
vec_N_R_prime_n2r = bound(vec_N_R_prime_n2r,0,1.5*funs.orig_max(1));
    
vec_N_S_prime_n2n = bound(vec_N_S_prime_n2n,0,funs.orig_max(2));
vec_N_S_prime_n2r = bound(vec_N_S_prime_n2r,0,funs.orig_max(2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pols = struct;

pols.phi_R_tilde = vec_phi_R_tilde;
pols.Q_tilde     = vec_Q_tilde;
pols.K_H_prime   = vec_K_H_prime;
pols.K_R_prime   = vec_K_R_prime;
pols.K_S_prime   = vec_K_S_prime;
pols.phi_R       = vec_phi_R;
pols.phi_S       = vec_phi_S;
pols.C_H         = vec_C_H;
pols.B_prime     = vec_B_prime;
pols.Q           = vec_Q;
pols.R_D_prime   = vec_R_D_prime;
pols.R_B_prime   = vec_R_B_prime;
pols.E_R_K_prime = vec_E_R_K_prime;
pols.K_prime     = vec_K_prime;

pols.N_R_prime_n2n   = vec_N_R_prime_n2n;
pols.N_S_prime_n2n   = vec_N_S_prime_n2n;
pols.N_R_prime_n2r   = vec_N_R_prime_n2r;
pols.N_S_prime_n2r   = vec_N_S_prime_n2r;
pols.x_prime         = vec_x_prime;
pols.p_prime         = vec_p_prime;

pols.I = pols.K_prime - (1-p.delta)*vec_K;
pols.D_prime = (pols.phi_R-1).*vec_N_R + (1-p.gamma)*pols.B_prime;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Value Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vals = struct;
vals.C_H = pols.C_H;
vals.V_R = (p.sigma_R + (1-p.sigma_R)*p.psi*pols.phi_R_tilde).*(vec_N_R - p.exoInc*vec_K)/(1-p.sigma_R);    
vals.V_S = (p.sigma_S + (1-p.sigma_S)*p.psi*(p.omega*vec_phi_S + ...
            (1-p.omega))).*(vec_N_S - p.exoInc*vec_K)/(1-p.sigma_S);
vals.Q   = pols.Q;