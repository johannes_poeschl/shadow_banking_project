% comparative statics with respect to theta and kappa
clear
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
solveModel = 1;
simulModel = 1;
anticipated_runs = 1;

reg_order = {'static','run_exempt'};
run_order = {'with_runs','with_runs'};
Gamma_R_Vals = [1e-4 0.01:0.01:0.2];

yNames = {'Static Capital Requirement','Dynamic Capital Requirement'};

nVals = length(Gamma_R_Vals);

fontOptions = {'fontSize',20};

for xx = 1:length(reg_order)
    
    % types of capital requirements: static, run_exempt, dynamic
    is_reg = reg_order{xx};

    % types of runs: no_runs, with_runs
    is_run = run_order{xx};

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Plots
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    stored_Tables = cell(nVals,4);
    for kVal = 1:nVals
        load(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
            'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')

        stored_Tables(kVal,1) = {Business_Cycle_Statistics};
        stored_Tables(kVal,2) = {Bank_Run_Statistics};
        stored_Tables(kVal,3) = {SS_Bounds};
        stored_Tables(kVal,4) = {Welfare_Table};
    end  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Bank Run Prob Only - Two Different Capital Requirements
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    xVals = Gamma_R_Vals;
    nVals = length(xVals);
    xNames = {'Capital Requirement (%)'};

    yVals = cell(1,1);
    yVals(:) = {ones(nVals,1)};

    for kVal = 1:nVals
        yVals{1,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
    end

    subplot(1,2,xx)
    plot(xVals,400*yVals{1,1},'b','LineWidth',2)
    set(gca,fontOptions{:})
    if xx == 1
        ylabel('Bank Runs per 100 years',fontOptions{:})
    end
    title(yNames{xx},fontOptions{:})
    hl = line([0.08 0.08],[0 6.5]);
    set(hl,'Color','k')
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,fontOptions{:})
    ylim([0 6.5])
    xticks([0 0.05 0.08 0.15 0.2])
    yticks(0:6)

end

hRim = 0;
vRim = 0;
set(gcf,'Units','centimeters')
paperDim = [12.8 0.5*9.6];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Bank_Run_Prob_',is_reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))