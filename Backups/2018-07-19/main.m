% solves the unemployment model

clear
close

addpath('./Funktionen')
addpath('./Funktionen/Smolyak_Anisotropic_JMMV_2014')
addpath('./Funktionen/CompEcon Toolbox/CEtools')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt = struct;

opt.solveModel = 0;
opt.simulModel = 1;
opt.newGuess   = 0;

opt.reg = {'retail','shadow'}; % retail, shadow, none
opt.run = {'with_runs'}; % with_runs, no_runs
opt.modelVersion = 'baseline';

Gamma_R_Vals = 0.1;

nReg = length(opt.reg);
nGam = length(Gamma_R_Vals);

[iReg,iGam] = ndgrid(1:nReg,1:nGam);

iReg = vec(iReg);
iGam = vec(iGam);

nXX = nReg*nGam;

for xx = 1:nXX
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    p = setParams(opt.reg{iReg(xx)},opt.run{1},opt.modelVersion,Gamma_R_Vals(iGam(xx)));

    if opt.solveModel == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Model Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if opt.newGuess == 1
            delete('initialGuess.mat')
        end

        if exist(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{1}),'dir') == 0
            mkdir(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{1}))
        end

        [pols,pols_run,funs,funs_run,grd,steady,states_no_run,states_run] = solveModel(p,opt.reg{iReg(xx)},opt.run{1});

        save(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{1},'/Results_GammaR_',num2str(Gamma_R_Vals(iGam(xx))),'.mat'), ...
            'pols','funs','pols_run','funs_run','p','steady')
    end

    if opt.simulModel == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Model Simulation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        load(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{1},'/Results_GammaR_',num2str(Gamma_R_Vals(iGam(xx))),'.mat'))

        Tables = function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt.run{1});

        save(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{1},'/Moments_GammaR_',num2str(Gamma_R_Vals(iGam(xx))),'.mat'), ...
            'Tables')
    end

end

delete('shocks.mat')
