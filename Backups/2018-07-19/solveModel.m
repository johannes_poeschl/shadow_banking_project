function [pols,pols_run,funs,funs_run,grd,steady,states_no_run,states_run] = solveModel(p,reg,run)

p.theta = p.theta_max;
p.kappa = p.kappa_max;
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K = ((1.064^0.25 - (1 - p.delta))/p.alpha/exp(p.mu_Z))^(1/(p.alpha-1));

handle = @(x) computeSteadyState(x,p);

x0 = [0.2 0.4 0.4 0.0655 0.02]'*K;
x1 = fsolve(handle,x0);

[~,steady] = handle(x1);

if steady.phi_shadow > p.phi_bar
    handle = @(x) computeSteadyState_with_CR(x,p);

    x1 = fsolve(handle,x1);

    [~,steady] = handle(x1);
end

display(steady)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid and State Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[grd,states_no_run,states_run,funs,funs_run] = setupGrid(p,steady);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial Guess
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[pols,pols_run,vals,vals_run] = setInitialGuess(p,steady,grd,states_no_run,states_run);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[funs,funs_run] = interpolFuns(funs,funs_run,vals,vals_run);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vError = 10;
nError = 10;
iter = 1;
maxIter =  250;

options = optimset('Display','off','MaxIter',1e3,'MaxFunEvals',1e3);

while (vError > 1e-5 || nError > 1e-5) && iter <= maxIter
    tic

    pols_old        = pols;
    pols_run_old    = pols_run;

    vals_old        = vals;
    vals_run_old    = vals_run;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % find the no run policy functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [pols,vals] = computeNoRunSolution(p,steady,grd,states_no_run,funs,funs_run, ...
        pols_old,options,reg);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % find the run policy functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [pols_run,vals_run] = computeRunSolution(p,grd,states_run,funs,funs_run, ...
        pols_run_old,options,reg);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Convergence Criterium and Output
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    valueDiff = [vals.C_H; vals.V_R; vals.V_S; vals_run.C_H; vals_run.V_R] - ...
        [vals_old.C_H; vals_old.V_R; vals_old.V_S; vals_run_old.C_H; vals_run_old.V_R];
    
    vError = norm(valueDiff,'inf');
    
    kError = norm(pols.K_prime - pols_old.K_prime,'inf');
    nError = norm(vec([pols.N_R_prime_n2n; pols.N_S_prime_n2n]) - ...
        vec([pols_old.N_R_prime_n2n; pols_old.N_S_prime_n2n]),'inf');
    
    elapsedTime = toc;
    
%     if mod(iter,10) == 0  || iter == 1
        fprintf('Iteration %d: \n',iter);
        fprintf('Elapsed Time: %6.2f Seconds \n',elapsedTime);
        fprintf('Value Function Error: %6.6f \n',vError)
        fprintf('Net Worth Error %6.6f \n \n',max(kError,nError))
%     end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Attenuation (Smoothing)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    weight_N = 0.5;
    weight_V = 1;
    
    pols.K_prime                = weight_N*pols.K_prime + (1-weight_N)*pols_old.K_prime; 
    pols.N_R_prime_n2n          = weight_N*pols.N_R_prime_n2n + (1-weight_N)*pols_old.N_R_prime_n2n;
    pols.N_R_prime_n2r          = weight_N*pols.N_R_prime_n2r + (1-weight_N)*pols_old.N_R_prime_n2r;
    pols.N_S_prime_n2n          = weight_N*pols.N_S_prime_n2n + (1-weight_N)*pols_old.N_S_prime_n2n; 
    pols.N_S_prime_n2r          = weight_N*pols.N_S_prime_n2r + (1-weight_N)*pols_old.N_S_prime_n2r;
    pols.p_prime                = weight_N*pols.p_prime + (1-weight_N)*pols_old.p_prime;
    pols.x_prime                = weight_N*pols.x_prime + (1-weight_N)*pols_old.x_prime;
    pols.Q                      = weight_N*pols.Q + (1-weight_N)*pols_old.Q;

    pols_run.K_prime            = weight_N*pols_run.K_prime + (1-weight_N)*pols_run_old.K_prime;
    pols_run.N_R_prime_r2n      = weight_N*pols_run.N_R_prime_r2n + (1-weight_N)*pols_run_old.N_R_prime_r2n;
    pols_run.N_R_prime_r2r      = weight_N*pols_run.N_R_prime_r2r + (1-weight_N)*pols_run_old.N_R_prime_r2r;
    pols_run.Q                  = weight_N*pols_run.Q + (1-weight_N)*pols_run_old.Q;
    
    vals.C_H            = weight_V*vals.C_H + (1-weight_V)*vals_old.C_H;
    vals.V_R            = weight_V*vals.V_R + (1-weight_V)*vals_old.V_R;
    vals.V_S            = weight_V*vals.V_S + (1-weight_V)*vals_old.V_S;
    vals.Q              = weight_N*vals.Q + (1-weight_N)*vals_old.Q;

    vals_run.C_H       = weight_V*vals_run.C_H + (1-weight_V)*vals_run_old.C_H;
    vals_run.V_R       = weight_V*vals_run.V_R + (1-weight_V)*vals_run_old.V_R;
    vals_run.Q         = weight_N*vals_run.Q + (1-weight_N)*vals_run_old.Q;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Interpolation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%s
    
    [funs,funs_run] = interpolFuns(funs,funs_run,vals,vals_run);
    
    iter = iter + 1;
end

%%
fileName_short = strcat('./Results/','Anti_Run_Solution.mat');
save(fileName_short,'pols','pols_run','funs','funs_run','grd','p','steady','states_no_run')

save('initialGuess.mat','pols','pols_run','vals','vals_run')
