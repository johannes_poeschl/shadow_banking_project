function [foc_residual,add,expc] = FOC_run(p, x0, states, ...
    nextVals, nextVals_run, nextStates, nextStates_run, Z_prob)
% HH_RB_SB_compute_residual_run computes the F.O.C. residuals in a run.

N_R = states(1,1);
K   = states(1,2);

K_H_prime = x0(1);
C_H_star  = x0(3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations and additional variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

add = computeAdditionalVariables_Run(p, x0, states, ...
    nextStates);

expc = computeExpectationsFOC_Run(p, x0, states, ...
    nextVals, nextVals_run, nextStates, nextStates_run, Z_prob, add);

foc_residual = zeros(3,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(1) = C_H_star^(-p.sigma)*(add.Q + p.eta_H*K_H_prime/K) - ...
    p.beta * expc.EV_K_H;

foc_residual(2) = C_H_star - (add.Y - add.I - add.adj_cost);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(3) = p.psi*add.phi_R*N_R - p.beta*expc.EV_R;

