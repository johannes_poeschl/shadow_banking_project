function add = computeAdditionalVariables_Run(p, x0, states, ...
    nextStates)

N_R = states(1,1);
K   = states(1,2);
logZ = states(1,3);

K_H_prime = x0(1);
K_R_prime = x0(2);

K_prime = K_H_prime + K_R_prime;
logZ_prime = nextStates(:,4)';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I           = K_prime - (1-p.delta)*K;

if p.kappa == 0
    Q_star          = 1 + p.theta*(I/K - p.delta);
    adj_cost       	= p.theta/2*(I/K - p.delta)^2*K;
else
    Q_star          = 1 + p.theta/p.kappa*(1 - exp(-p.kappa*(I/K - p.delta)));
    adj_cost        = p.theta/p.kappa^2*(exp(-p.kappa*(I/K - p.delta)) + ...
        p.kappa*(I/K - p.delta) - 1)*K;
end

phi_R_star       = ((Q_star + p.eta_R*K_R_prime/K)*K_R_prime)/N_R;

Y           = exp(logZ)*K^p.alpha + p.exoInc*K ...
    - p.eta_H/2*(K_H_prime/K)^2*K ...
    - p.eta_R/2*(K_R_prime/K)^2*K;

r_K_prime   = p.alpha*exp(logZ_prime).*K_prime.^(p.alpha-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Collect
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
add = struct;

add.I = I;
add.Q = Q_star;
add.adj_cost = adj_cost;
add.phi_R = phi_R_star;
add.Y = Y;
add.r_K_prime = r_K_prime;