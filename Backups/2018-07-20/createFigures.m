% comparative statics with respect to theta and kappa
clear
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
solveModel = 1;
simulModel = 1;
anticipated_runs = 1;

reg_order = {'static','run_exempt'};
run_order = {'with_runs','with_runs'};
Gamma_R_Vals = [1e-4 0.01:0.01:0.2];

nVals = length(Gamma_R_Vals);

for xx = 1:length(reg_order)
    
% types of capital requirements: static, run_exempt, dynamic
is_reg = reg_order{xx};

% types of runs: no_runs, with_runs
is_run = run_order{xx};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

stored_Tables = cell(nVals,4);
for kVal = 1:nVals
    load(strcat('./Results/',is_reg,'_',is_run,'/Moments_GammaR_',num2str(Gamma_R_Vals(kVal)),'.mat'), ...
        'Business_Cycle_Statistics','Bank_Run_Statistics','SS_Bounds','Welfare_Table')
    
    stored_Tables(kVal,1) = {Business_Cycle_Statistics};
    stored_Tables(kVal,2) = {Bank_Run_Statistics};
    stored_Tables(kVal,3) = {SS_Bounds};
    stored_Tables(kVal,4) = {Welfare_Table};
end  

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'\Gamma^R'};

varSelect = [1 2 3 4 5 6];
iter = 0;

yVals = cell(length(varSelect),4);
yVals(:) = {ones(nVals,1)};

yNames = {'Q','Q*','x','\phi_R','\phi_S','I'};

for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
    yVals{iter,1}(kVal) = stored_Tables{kVal, 2}.Mean(jRow);
    yVals{iter,2}(kVal) = stored_Tables{kVal, 2}.Median(jRow);
    yVals{iter,3}(kVal) = stored_Tables{kVal, 2}.p05(jRow);
    yVals{iter,4}(kVal) = stored_Tables{kVal, 2}.p95(jRow);
    end
end

% nor = @(x) (x/x(1));
nor = @(x) x;

figure
for jRow = 1:length(varSelect)
    subplot(2,3,jRow)
    plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
    hold
    plot(xVals,nor(yVals{jRow,2}),'r','LineWidth',2)
    plot(xVals,nor(yVals{jRow,3}),'g','LineWidth',2)
    plot(xVals,nor(yVals{jRow,4}),'k','LineWidth',2)
    legend('Mean','p50','p05','p95')
    title(yNames{jRow})
    xlabel(xNames)
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

if exist(strcat('./Figures/',is_reg,'_',is_run),'dir') == 0
    mkdir(strcat('./Figures/',is_reg,'_',is_run))
end

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Comparative_Statics');
print(figureName,'-dpdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'$1/\bar{\phi}$'};

varSelect = [1 2 3];
iter = 0;

yVals = cell(length(varSelect),1);
yVals(:) = {ones(nVals,1)};

yNames = {'Welfare, HH','Welfare, RB','Welfare, SB','Bank Run Prob'};

for jRow = varSelect
    iter = iter + 1;
    for kVal = 1:nVals
        yVals{iter,1}(kVal) = stored_Tables{kVal, 4}.Welfare(jRow);
    end
end

for kVal = 1:nVals
    yVals{4,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
end

nor = @(x) 100*(x/x(1) - 1);
% nor = @(x) x;

for jRow = 1:4
    subplot(2,2,jRow)
    if jRow < 4
        plot(xVals,nor(yVals{jRow,1}),'b','LineWidth',2)
        ylabel('% \Delta vs Baseline')
    else
        plot(xVals,100*yVals{jRow,1},'b','LineWidth',2)
        ylabel('%')
    end
    title(yNames{jRow})
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,'Interpreter','latex')  
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Welfare');
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bank Run Prob Only - Two Different Capital Requirements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xVals = Gamma_R_Vals;
nVals = length(xVals);
xNames = {'$1/\bar{\phi}$'};

yVals = cell(length(varSelect),1);
yVals(:) = {ones(nVals,1)};

yNames = {'Bank Run Probability E[p_t]'};

for kVal = 1:nVals
    yVals{1,1}(kVal) = stored_Tables{kVal, 2}.Mean(7);
end

figure
for jRow = 1:1
    plot(xVals,100*yVals{jRow,1},'b','LineWidth',2)
    ylabel('%')
    title(yNames{jRow})
    xlim([xVals(1) xVals(end)])
    xlabel(xNames,'Interpreter','latex')  
end

hRim = 1;
vRim = 1;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/',is_reg,'_',is_run,'/Bank_Run_Prob_',is_reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

%% "Optimal" Capital Requirements
close

welf = struct;
welf.HH = yVals{1};
welf.RB = yVals{2};
welf.SB = yVals{3};

% if regulator only cares about households
[val_HH_1,pos_HH_1] = max(welf.HH);

welf.opt_CR_1 = Gamma_R_Vals(pos_HH_1);
welf.change_HH_1 = 100*(val_HH_1/welf.HH(1) - 1);
welf.change_RB_1 = 100*(welf.RB(pos_HH_1)/welf.RB(1) - 1);
welf.change_SB_1 = 100*(welf.SB(pos_HH_1)/welf.SB(1) - 1);

% if regulator cares about all agents equally
[val_HH_2,pos_HH_2] = max(welf.HH + welf.RB + welf.SB);

welf.opt_CR_2 = Gamma_R_Vals(pos_HH_2);
welf.change_HH_2 = 100*(val_HH_2/welf.HH(1) - 1);
welf.change_RB_2 = 100*(welf.RB(pos_HH_2)/welf.RB(1) - 1);
welf.change_SB_2 = 100*(welf.SB(pos_HH_2)/welf.SB(1) - 1);
end