% This file computes the steady state of the new model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('./Smolyak_Anisotropic_JMMV_2014')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

parName = 'phi_bar';
% parName_TeX = strcat('\',parName);
parName_TeX = strcat('$$ 1/\bar{\phi}^R $$');

xLow = 0.0001;
xHigh  = 0.25;

parSpace = linspace(xLow,xHigh,250);
nPars = length(parSpace);

vec_D       = zeros(nPars,1);
vec_B       = zeros(nPars,1);
vec_K_H     = zeros(nPars,1);
vec_K_R     = zeros(nPars,1);
vec_K_S     = zeros(nPars,1);
vec_K       = zeros(nPars,1);
vec_phi_R   = zeros(nPars,1);
vec_phi_R_tilde   = zeros(nPars,1);
vec_phi_S   = zeros(nPars,1);
vec_C_H     = zeros(nPars,1);
vec_C_R     = zeros(nPars,1);
vec_C_S     = zeros(nPars,1);
vec_Y       = zeros(nPars,1);
vec_R_K_spread = zeros(nPars,1);
vec_R_B_spread = zeros(nPars,1);
vec_R_B     = zeros(nPars,1);
vec_R_D     = zeros(nPars,1);
vec_R_K     = zeros(nPars,1);
vec_D_S     = zeros(nPars,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parameters chosen to match steady state properties of the model
p.beta      = 0.9902;               % Household Discount Factor
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers

% Parameters chosen to match dynamics
p.rho_Z     = 0.8;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.theta_max = 10;                    % Capital Adjustment Cost
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.kappa_max = 0;                    % Asymmetry, Capital Adjustment Cost

% Standard Parameters
p.delta     = 0.025;                % Depreciation Rate
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.36;                 % Production Function Curvature

% Bank Run Parameters
p.nu        = 0.025;                % Probability of Bank Run Starting
p.pi        = 0;                  % Probability of Bank Run Continuing

p.phi_bar   = 1/0.08;
p.gamma_bar = p.gamma;
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling

p_baseline = p;

positiveK  = zeros(length(parSpace),1);
zeroK      = zeros(length(parSpace),1);

for ii = 1:length(parSpace)
    
%     eval(strcat('p.',parName,'=',num2str(parSpace(ii)),'*','p_baseline.',parName,';'))
    p.phi_bar = 1/parSpace(ii);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Steady State
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    handle = @(x) computeSteadyState(x,p);
        
%     if ii > 1
%         x0 = x1;
%     else
        x0 = [0.2 0.4 0.4 0.0655 0.02]'*10;
%     end
    
    [x1,~,exitFlag] = fsolve(handle,x0);

    [~,steady] = handle(x1);
    
    if steady.phi_shadow > p.phi_bar
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Binding CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        positiveK(ii) = 1;
        handle = @(x) computeSteadyState_with_CR(x,p);
        
        [x1_CR,~,exitFlag] = fsolve(handle,x1);
        
        [~,steady] = handle(x1_CR);
        
        if x1_CR(2) < 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Specialized Retail Banks
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            positiveK(ii) = 0;
            zeroK(ii)     = 1;
            x0_tilde = [x1_CR; steady.R_B];
            
            handle = @(x) computeSteadyState_with_CR_zero_KR(x,p);
            
            [x1,~,exitFlag] = fsolve(handle,x0_tilde);
            
            [~,steady] = handle(x1);
        end
        
        if steady.R_B > (1-p.omega)*steady.R_K + p.omega*steady.R_D            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Shadow Banks Use Deposits
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            x0_tilde = [x0; 5];
            
            handle = @(x) computeSteadyState_with_CR_SB_deposits(x,p);
            
            [x1,~,exitFlag] = fsolve(handle,x0_tilde);
            
            [~,steady] = handle(x1);
            
            if steady.B <= 0               
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Shadow Lend On Interbank Market
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                x0_tilde = x0;

                handle = @(x) computeSteadyState_with_CR_no_interbank_market(x,p);

                [x1,~,exitFlag] = fsolve(handle,x0_tilde);

                [~,steady] = handle(x1);
            
            end
        end
        
    end
         
    if exitFlag ~= 1
        error('Could Not Find a Steady State')
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Store
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    vec_D_S(ii) = steady.D_S;
    vec_D(ii)   = steady.D;
    vec_B(ii)   = steady.B;
    vec_K_H(ii) = steady.K_H/steady.K;
    vec_K_R(ii) = steady.K_R/steady.K;
    vec_K_S(ii) = steady.K_S/steady.K;
    vec_K(ii)   = steady.K;
    vec_phi_R(ii) = steady.phi_R;
    vec_phi_S(ii) = steady.phi_S;
    vec_phi_R_tilde(ii) = steady.phi_shadow;
    vec_C_H(ii) = steady.C_H;
    vec_C_R(ii) = p.sigma_R*steady.N_R;
    vec_C_S(ii) = p.sigma_S*steady.N_S;
    vec_Y(ii)   = steady.Y;
    vec_R_K_spread(ii) = steady.R_K^4 - steady.R_B^4;
    vec_R_B_spread(ii) = steady.R_B^4 - steady.R_D^4;
    vec_R_B(ii) = steady.R_B;
    vec_R_D(ii) = steady.R_D;
    vec_R_K(ii) = steady.R_K;
end

%% Plots

xVals1 = 100*parSpace;
zero_deposit_cond = (vec_R_K - vec_R_B)./(vec_R_K - vec_R_D) -p.omega; % benefit of an additional unit of deposit funding
zero_interbank_cond = (vec_R_K - vec_R_B)./(vec_R_K - vec_R_D) - (1-p.gamma);

figure
plot(xVals1,zero_deposit_cond,'b')
hold
plot(xVals1,zero_interbank_cond,'r')
hl = line([min(xVals1) max(xVals1)],[0,0]);
set(hl,'Color','k')
legend('>0: SB use only wholesale funding','>0: SB use some wholesale funding')
title('Steady State Conditions')

figureName = strcat('./Figures/steady/Optimality_Conditions_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')

%% Bank Run Variables

c1 = find(positiveK == 1,1,'first');
c2 = find(zeroK == 1,1,'first');
c2 = c1;

plotVals = [vec_phi_R vec_phi_S 100*vec_R_K.*vec_K_S.*vec_K./(vec_R_B.*vec_B + vec_R_D.*vec_D_S) 100*vec_R_K_spread];
plotNames = {'(a) \phi^R','(b) \phi^S','(c) Coverage Ratio','(d) R^K - R^B'};
fontOptions = {'FontSize',12};
fontOptions_Alt = {'FontSize',10};
yLabel = {'Level','Level','%','%, annualized'};

figure
for ii = 1:size(plotVals,2)
   subplot(2,2,ii)
   plot(xVals1,plotVals(:,ii),'b','LineWidth',2)
   if ii == 1 && p.gamma ~= p.gamma_bar
       hold
       plot(xVals1,vec_phi_R_tilde,'r:','LineWidth',2)
   end
   hold
   set(gca,fontOptions_Alt{:})
   title(plotNames{ii},fontOptions{:})
   xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
   xlim([min(xVals1) max(xVals1)])
   yLim = ylim;
   ylim([yLim(1)-1e-4 yLim(2)+1e-4])
   line([min(xVals1) max(xVals1)],[1 1]*plotVals(1,ii),'Color','k','LineStyle',':')
   line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
   line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
   ylabel(yLabel{ii})
end

figureName = strcat('./Figures/steady/Comparative_Statics_run_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')

%% Plots - Slides

plotVals = [100*vec_K_H 100*vec_K_R 100*vec_K_S vec_K];
plotNames = {'K^H/K','K^R/K','K^S/K','K'};
fontOptions = {'FontSize',12};
fontOptions_Alt = {'FontSize',10};
parName_TeX = 'Capital Requirement $$1/\bar{\phi}$$ (\%)';

figure
subplot(1,2,1)
l1 = plot(xVals1,plotVals(:,1),'b','LineWidth',2);
hold
l2 = plot(xVals1,plotVals(:,2),'r:','LineWidth',2);
l3 = plot(xVals1,plotVals(:,3),'k--','LineWidth',2);
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
title('(a) Capital Allocation')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
ylabel('Capital Share (%)')
hl = legend([l1 l2 l3],plotNames{1:3});
set(hl,'Location','East')

subplot(1,2,2)
plot(xVals1,100*(plotVals(:,4)/plotVals(1,4)-1),'b','LineWidth',2)
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
ylabel('Capital Level (% \Delta vs Baseline)')
title('(b) Aggregate Capital Stock')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')

figureName = strcat('./Figures/steady/Comparative_Statics_K_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
% paperDim = [21 29.7/4];
paperDim = 1.4*[12.8 0.5*9.6];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')

%% Plots - Slides

vec_C = vec_C_H + vec_C_R + vec_C_S;

plotVals = 100*([vec_C_H/vec_C_H(1) vec_C_R/vec_C_R(1) vec_C_S/vec_C_S(1) vec_C/vec_C(1)]-1);
plotNames = {'(a) C^H','(b) C^R','(c) C^S','(d) C'};
fontOptions = {'FontSize',12};
fontOptions_Alt = {'FontSize',10};
parName_TeX = '$$1/\bar{\phi}$$';

figure
for qq = 1:4
    subplot(1,4,qq)
    plot(xVals1,plotVals(:,qq),'b','LineWidth',2)
    set(gca,fontOptions_Alt{:})
    xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
    ylabel('% \Delta vs Baseline')
    xlim([min(xVals1) max(xVals1)])
    line([min(xVals1) max(xVals1)],[0 0],'Color','k','LineStyle',':')
    yLim = ylim;
    ylim([yLim(1)-1e-4 yLim(2)+1e-4])
    line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
    line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
    title(plotNames{qq})
end

figureName = strcat('./Figures/steady/Comparative_Statics_C_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/4];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')

%% Plots - Slides

plotVals = [100*vec_K_H 100*vec_K_R 100*vec_K_S vec_K];
plotNames = {'K^H/K','K^R/K','K^S/K','K'};
fontOptions = {'FontSize',12};
fontOptions_Alt = {'FontSize',10};
parName_TeX = 'Capital Requirement $$1/\bar{\phi}$$ (\%)';

figure
subplot(3,2,1)
l1 = plot(xVals1,plotVals(:,1),'b','LineWidth',2);
hold
l2 = plot(xVals1,plotVals(:,2),'r:','LineWidth',2);
l3 = plot(xVals1,plotVals(:,3),'k--','LineWidth',2);
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
title('(a) Capital Allocation')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
ylabel('Capital Share (%)')
hl = legend([l1 l2 l3],plotNames{1:3});
set(hl,'Location','East')

subplot(3,2,2)
plot(xVals1,100*(plotVals(:,4)/plotVals(1,4)-1),'b','LineWidth',2)
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
ylabel('Capital Level (% \Delta vs Baseline)')
title('(b) Aggregate Capital Stock')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')

subplot(3,2,3)
l4 = plot(xVals1,-vec_B,'r:','LineWidth',2);
hold
l5 = plot(xVals1,vec_B,'k--','LineWidth',2);
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
ylabel('Wholesale Borrowing')
title('Wholesale Borrowing')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
legend([l4 l5],{'Retail Banks','Shadow Banks'})

subplot(3,2,4)
l4 = plot(xVals1,vec_D,'r:','LineWidth',2);
hold
l5 = plot(xVals1,vec_D_S,'k--','LineWidth',2);
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
ylabel('Deposit Borrowing')
title('Deposit Borrowing')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
legend([l4 l5],{'Retail Banks','Shadow Banks'})

subplot(3,2,5)
l4 = plot(xVals1,vec_phi_R,'r:','LineWidth',2);
hold
l5 = plot(xVals1,vec_phi_S,'k--','LineWidth',2);
set(gca,fontOptions_Alt{:})
xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
xlim([min(xVals1) max(xVals1)])
yLim = ylim;
ylim([yLim(1)-1e-4 yLim(2)+1e-4])
ylabel('Leverage')
title('Leverage')
line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
legend([l4 l5],{'Retail Banks','Shadow Banks'})


figureName = strcat('./Figures/steady/Comparative_Statics_K_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
% paperDim = [21 29.7/4];
paperDim = 1.4*[12.8 0.5*9.6];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')