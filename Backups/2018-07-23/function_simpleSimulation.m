function Tables = ...
    function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,is_run)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set simulation parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBurnin = 500; 
nSimulations = nBurnin + 1000;  % number of simulated periods 
nPanels = 5000; %%%%% number of simulations for each capital requirement level

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% draw shocks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if exist('shocks.mat','file') == 2
    load('shocks.mat')
else
    seq_logZ = p.mu_Z*ones(nPanels,nSimulations);
    seq_epsZ = randn(nPanels,nSimulations);

    for tt = 2:nSimulations  %%%%% for each simulation    
        seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + p.sigma_Z*seq_epsZ(:,tt);
    end

    shocks.logZ = seq_logZ;
    if strcmp(is_run,'no_runs') == 1
        shocks.xi = 1.25+0*normrnd(0,1,nPanels,nSimulations); %%%%% generage a sequence of random numbers ~N(0,1) 
    else
        shocks.xi = exp(p.sigma_xi*normrnd(0,1,nPanels,nSimulations)); %%%%% generage a sequence of random numbers ~N(0,1) 
    end
    shocks.pi = normrnd(0,1,nPanels,nSimulations);
    shocks.burnin = nBurnin;
    
    save('shocks.mat','shocks')
end

[TS,TS_add] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady);

Tables = struct;

%% Simulation Boundaries
% check the simulation boundaries

RowNames = {'N_R','N_S','K'};

Min_Sim = [min(vec(TS.N_R));  min(vec(TS.N_S)); min(vec(TS.K))];
Lower_Bound_SS = funs.orig_min(1:3)';
freq_below_bound = 100*[mean(vec(TS.N_R) <= funs.orig_min(1)); ...
        mean(vec(TS.N_S) <= funs.orig_min(2)); ...
        mean(vec(TS.K) <= funs.orig_min(3))];

Max_Sim = [max(vec(TS.N_R));  max(vec(TS.N_S)); max(vec(TS.K))];
Upper_Bound_SS = funs.orig_max(1:3)';
freq_above_bound = 100*[mean(vec(TS.N_R) >= funs.orig_max(1)); ...
        mean(vec(TS.N_S) >= funs.orig_max(2)); ...
        mean(vec(TS.K) >= funs.orig_max(3))];

Tables.SS_Bounds  = table(Min_Sim,Lower_Bound_SS,freq_below_bound, ...
    Max_Sim,Upper_Bound_SS,freq_above_bound,'RowNames',RowNames);
display(Tables.SS_Bounds)

%% Welfare Statistics

[ex_ante_util_H,ex_ante_util_R,ex_ante_util_S] = computeWelfare(TS_add,p);

RowNames = {'HH','RB','SB'};

Tables.Welfare_Table = table([ex_ante_util_H; ex_ante_util_R; ex_ante_util_S], ...
    'RowNames',RowNames,'VariableNames',{'Welfare'});

display(Tables.Welfare_Table)

%% Bank Run Statistics

RowNames = {'Q','Q*','x','phi_R','phi_S','I','p(x < 1)','p(I < 0)'};

mean_Q  = mean(vec(TS.Q));
median_Q = quantile(vec(TS.Q),0.5);
p05_Q = quantile(vec(TS.Q),0.05);
p95_Q = quantile(vec(TS.Q),0.95);
std_Q = std(vec(TS.Q));

mean_Q_star = mean(vec(TS.Q_star));
median_Q_star = quantile(vec(TS.Q_star),0.5);
p05_Q_star = quantile(vec(TS.Q_star),0.05);
p95_Q_star = quantile(vec(TS.Q_star),0.95);
std_Q_star = std(vec(TS.Q_star));

mean_recov = mean(vec(TS_add.recov));
median_recov = quantile(vec(TS_add.recov),0.5);
p05_recov = quantile(vec(TS_add.recov),0.05);
p95_recov = quantile(vec(TS_add.recov),0.95);
std_recov = std(vec(TS_add.recov));

mean_phi_R = mean(vec(TS_add.phi_R));
median_phi_R = quantile(vec(TS_add.phi_R),0.5);
p05_phi_R = quantile(vec(TS_add.phi_R),0.05);
p95_phi_R = quantile(vec(TS_add.phi_R),0.95);
std_phi_R = std(vec(TS_add.phi_R));

mean_phi_S = mean(vec(TS_add.phi_S));
median_phi_S = quantile(vec(TS_add.phi_S),0.5);
p05_phi_S = quantile(vec(TS_add.phi_S),0.05);
p95_phi_S = quantile(vec(TS_add.phi_S),0.95);
std_phi_S = std(vec(TS_add.phi_S));

mean_I = mean(vec(TS_add.I));
median_I = quantile(vec(TS_add.I),0.5);
p05_phi_I = quantile(vec(TS_add.I),0.05);
p95_phi_I = quantile(vec(TS_add.I),0.95);
std_phi_I = std(vec(TS_add.I));

prob_run = mean(vec(TS_add.recov < 1));
prob_neg_inv = mean(vec(TS_add.I./TS.K < p.delta - 1e-3));

mean_Vals = [mean_Q; mean_Q_star; mean_recov; mean_phi_R; mean_phi_S; mean_I; prob_run; prob_neg_inv];
median_Vals = [median_Q; median_Q_star; median_recov; median_phi_R; median_phi_S; median_I; 0; 0];
p05_Vals = [p05_Q; p05_Q_star; p05_recov; p05_phi_R; p05_phi_S; p05_phi_I; 0; 0];
p95_Vals = [p95_Q; p95_Q_star; p95_recov; p95_phi_R; p95_phi_S; p95_phi_I; 0; 0];
std_Vals = [std_Q; std_Q_star; std_recov; std_phi_R; std_phi_S; std_phi_I; 0; 0];

Tables.Bank_Run_Statistics = table(mean_Vals,median_Vals,p05_Vals,p95_Vals,std_Vals, ...
    'RowNames',RowNames, ...
    'VariableNames',{'Mean','Median','p05','p95','StDev'});

display(Tables.Bank_Run_Statistics)

%% Business Cycle Statistics

steady.C_H = steady.Y - p.delta*steady.K;

pct_dev_Y = TS_add.Y/steady.Y - 1;
pct_dev_C_H = TS_add.C_H/steady.C_H - 1;
pct_dev_I = TS_add.I/(steady.K*p.delta) - 1;
pct_dev_D_prime = TS_add.D_prime/steady.D - 1;
pct_dev_B_prime = TS_add.B_prime/steady.B - 1;
dev_R_D = TS_add.R_D.^4 - steady.R_D.^4;
dev_R_B = TS_add.R_B.^4 - steady.R_B.^4;
dev_R_K = TS_add.R_K.^4 - steady.R_K.^4;

% standard deviations
sd_Y = std(vec(pct_dev_Y));
sd_I = std(vec(pct_dev_I));
sd_C_H = std(vec(pct_dev_C_H));
sd_D_prime = std(vec(pct_dev_D_prime));
sd_B_prime = std(vec(pct_dev_B_prime));
sd_R_D_prime = std(vec(dev_R_D));
sd_R_B_prime = std(vec(dev_R_B));
sd_R_K_prime = std(vec(dev_R_K));

corr_C_H = corr(vec(pct_dev_Y),vec(pct_dev_C_H));
corr_I = corr(vec(pct_dev_Y),vec(pct_dev_I));
corr_D_prime = corr(vec(pct_dev_Y),vec(pct_dev_D_prime));
corr_B_prime = corr(vec(pct_dev_Y),vec(pct_dev_B_prime));
corr_R_D_prime = corr(vec(pct_dev_Y),vec(dev_R_D));
corr_R_B_prime = corr(vec(pct_dev_Y),vec(dev_R_B));
corr_R_K_prime = corr(vec(pct_dev_Y),vec(dev_R_K));

autoCorr_Y = corr(vec(pct_dev_Y(:,2:end)),vec(pct_dev_Y(:,1:end-1)));
autoCorr_C_H = corr(vec(pct_dev_C_H(:,2:end)),vec(pct_dev_C_H(:,1:end-1)));
autoCorr_I = corr(vec(pct_dev_I(:,2:end)),vec(pct_dev_I(:,1:end-1)));
autoCorr_D_prime = corr(vec(pct_dev_D_prime(:,2:end)),vec(pct_dev_D_prime(:,1:end-1)));
autoCorr_B_prime = corr(vec(pct_dev_B_prime(:,2:end)),vec(pct_dev_B_prime(:,1:end-1)));
autoCorr_R_D_prime = corr(vec(dev_R_D(:,2:end)),vec(dev_R_D(:,1:end-1)));
autoCorr_R_B_prime = corr(vec(dev_R_B(:,2:end)),vec(dev_R_B(:,1:end-1)));
autoCorr_R_K_prime = corr(vec(dev_R_K(:,2:end)),vec(dev_R_K(:,1:end-1)));

StDev = [sd_Y; sd_C_H/sd_Y; sd_I/sd_Y; sd_D_prime/sd_Y; sd_B_prime/sd_Y; ...
    sd_R_D_prime/sd_Y; sd_R_B_prime/sd_Y; sd_R_K_prime/sd_Y;];

CorrWithGDP = [1; corr_C_H; corr_I; corr_D_prime; corr_B_prime; ...
    corr_R_D_prime; corr_R_B_prime; corr_R_K_prime];

AutoCorr = [autoCorr_Y; autoCorr_C_H; autoCorr_I; autoCorr_D_prime; autoCorr_B_prime; ...
    autoCorr_R_D_prime; autoCorr_R_B_prime; autoCorr_R_K_prime];

RowNames = {'Output (Y)','Consumption (C_H)','Investment (I)','Deposits (D'')','Repo Lending (B'')', ...
    'Deposit Rate R^D','Wholesale Rate R^B','Lending Rate R^K/Q_{-1}'};

Tables.Business_Cycle_Statistics  = table(StDev,CorrWithGDP,AutoCorr,'RowNames',RowNames);
display(Tables.Business_Cycle_Statistics)

save(strcat('./Results/Moments_','theta_',num2str(p.theta_max),'_kappa_',num2str(p.kappa_max),'.mat'),'Tables')

%% We
