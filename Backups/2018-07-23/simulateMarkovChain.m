function markovChain = simulateMarkovChain(initialValue,valueRange,...
    transitionMatrix,nSimulations)
% simulate a scalar markov chain with arbitrarily many states

% testing values
% [valueRange,transitionMatrix] = markov_ar(0,1,2,0.66,10);
% initialValue = valueRange(5,1);
% nSimulations = 20;

markovChain = zeros(nSimulations,1);
markovChain(1,1) = initialValue;
transitionShock = rand(nSimulations,1);

% check that the initial value lies on the grid
if sum(valueRange == initialValue) == 0
    error('Initial value is not on the grid')
end

for iSim = 2:nSimulations 
    % check at which point in the range the markov chain currently is
    currentState = valueRange - markovChain(iSim-1,1) == 0;
    
    % pick the corresponding transition vector, get the transition cdf
    transitionVector = cumsum(transitionMatrix(currentState,:));
    
    % select the next value according to the realization of the
    % transitionShock
    
    newState = transitionVector - transitionShock(iSim,1) >= 0;    
    
    markovChain(iSim) = min(valueRange(newState'));
end

end