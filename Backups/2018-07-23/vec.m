function vector = vec(array)
% A simple function that reshapes an N-dimensional array into a column vector
%
%   Input: 
%           array, a double array of arbitrary dimension.
%   Output: 
%           vector, a vector of dimension numel(array)x1

vector = reshape(array,numel(array),1);