function [funs,funs_run] = interpolFuns(funs,funs_run,vals,vals_run)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation, No Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% interpolated values
funs.C_H = funs.base_poly\vals.C_H; 
funs.V_R = funs.base_poly\vals.V_R; 
funs.V_S = funs.base_poly\vals.V_S;  
funs.Q   = funs.base_poly\vals.Q;    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interpolation, Run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

funs_run.C_H = funs_run.base_poly\vals_run.C_H; 
funs_run.V_R = funs_run.base_poly\vals_run.V_R;
funs_run.Q   = funs_run.base_poly\vals_run.Q;  
