% solves the unemployment model

clear
close

addpath('./Functions')
addpath('./Functions/Smolyak_Anisotropic_JMMV_2014')
addpath('./Functions/CompEcon Toolbox/CEtools')
addpath('./Solution_Files')
addpath('./Simulation_Files')
addpath('./Steady')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt = struct;

opt.solveModel      = 1;
opt.simulModel      = 1;
opt.newGuess        = 0;
opt.createTables    = 1;

opt.reg = {'retail'}; % retail, shadow, none
opt.run = {'with_runs'}; % with_runs, no_runs
opt.modelVersion = 'baseline';
opt.capReqVals = 0:0.05:0.15;

% opt.reg = {'retail'}; % retail, shadow, none
% opt.run = {'with_runs','no_runs'}; % with_runs, no_runs
% opt.modelVersion = 'baseline';
% opt.capReqVals = 0;

nReg = length(opt.reg);
nGam = length(opt.capReqVals);
nRun = length(opt.run);

[iGam,iReg,iRun] = ndgrid(1:nGam,1:nReg,1:nRun);

iReg = vec(iReg);
iGam = vec(iGam);
iRun = vec(iRun);

nXX = nReg*nGam*nRun;

for xx = 1:nXX
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    p = setParams(opt.reg{iReg(xx)},opt.run{iRun(xx)},opt.capReqVals(iGam(xx)));

    if opt.solveModel == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Model Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if opt.newGuess == 1 && iGam(xx) == 1
            delete('initialGuess.mat')
        end

        if exist(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{iRun(xx)}),'dir') == 0
            mkdir(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{iRun(xx)}))
        end

        [pols,pols_run,funs,funs_run,grd,steady,states_no_run,states_run] = solveModel(p,opt.reg{iReg(xx)},opt.run{iRun(xx)});

        save(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{iRun(xx)},'/Results_GammaR_',num2str(opt.capReqVals(iGam(xx))),'.mat'), ...
            'pols','funs','pols_run','funs_run','p','steady')
    end

    if opt.simulModel == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Model Simulation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        load(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{iRun(xx)},'/Results_GammaR_',num2str(opt.capReqVals(iGam(xx))),'.mat'))

        Tables = function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt.run{iRun(xx)});

        save(strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{iRun(xx)},'/Moments_GammaR_',num2str(opt.capReqVals(iGam(xx))),'.mat'), ...
            'Tables')
    end

end

if opt.createTables == 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Tables
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    latexTables = createTables(opt);
        
end

delete('shocks.mat')
