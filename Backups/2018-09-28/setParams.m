function p = setParams(reg,run,CR_val_retail,CR_val_shadow)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Baseline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p = struct;

% Parameters chosen to match steady state properties of the model
p.beta      = 0.9902;               % Household Discount Factor
p.gamma     = 0.6676;               % Relative Divertable Share of Interbank Loans
p.eta_H     = 0.0286;               % Household Capital Holding Cost
p.eta_R     = 0.0071;               % Retail Bank Capital Holding Cost
p.sigma_R   = 0.0521;               % Retail Bank Exit Probability
p.sigma_S   = 0.1273;               % Shadow Bank Exit Probability
p.psi       = 0.2154;               % Divertable Asset Share, Retail Banks
p.omega     = 0.5130;               % Non-Divertable Share of Debt-Financed Assets, Shadow Banks
p.exoInc    = 0.001;                % Exogenous Income of Bankers

% Parameters chosen to match dynamics
p.rho_Z     = 0.9;                  % Persistence, TFP
p.sigma_Z   = 0.01;                 % Volatility, TFP
p.theta     = 0.25;                 % Initial Capital Adjustment Cost
p.theta_max = 10;                   % Capital Adjustment Cost
p.kappa     = 0;                    % Initial Asymmetry, Capital Adjustment Cost
p.kappa_max = 0;                    % Asymmetry, Capital Adjustment Cost

% Standard Parameters
p.delta     = 0.025;                % Depreciation Rate
p.sigma     = 1;                    % HH risk aversion
p.alpha     = 0.36;                 % Production Function Curvature

% regulation parameters
p.gamma_bar = p.gamma;
p.phi_Q = 0;

% Bank Run Parameters
p.pi        = 12/13;                % Probability of Bank Run Continuing
p.tau       = 0;                    % Output loss during bank run
p.mu_Z      = log((1.064^(1/4) - 1 + p.delta)/p.alpha/10^(p.alpha-1)); % Productivity Scaling
p.sigma_xi  = 0.01875;
p.mu_xi     = -p.sigma_xi^2/2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Variants
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(run,'with_runs') == 1
    p.pi_start  = 1;
elseif strcmp(run,'no_runs') == 1
    p.pi_start  = 0;
end

if strcmp(reg,'retail') == 1 || strcmp(reg,'both') == 1
    p.phi_R_bar = 1/0.1;
else
    p.phi_R_bar = inf;
end

if strcmp(reg,'shadow') == 1 || strcmp(reg,'both') == 1
    p.phi_S_bar = 1/0.05;
else
    p.phi_S_bar = inf;
end
