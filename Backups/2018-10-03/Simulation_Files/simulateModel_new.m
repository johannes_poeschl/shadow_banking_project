function [TS,TS_add] = simulateModel_new(p,shocks,funs,funs_run,pols,pols_run,steady)
% simulateModel simulates model time series
%
% Inputs:
%   - p:        struct array containing the parameters
%   - shocks:   struct array containing the shocks
%   - funs:     struct array of interpolation accessories
%   - pols:     struct array for the policy functions
%   - steady:   struct array containing the steady state
%
% Outputs:
%   - TS:       time series structure of the endogenous states

[nPanels,nSimulations] = size(shocks.logZ);

seq_logZ = shocks.logZ;
seq_Z    = exp(seq_logZ);

p.nu = 0;
pi_cutoff = norminv(p.pi);

grd.n_Z = 9;
grd.n_Xi = 9;
[grd.eps_Z,grd.Z_prob]   = qnwnorm(grd.n_Z,0,1);
[grd.eps_Xi,grd.Xi_prob] = qnwnorm(grd.n_Xi,0,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - no run case -
sz_array = [20 20 20 20];
exo_array = [9 9];

[nd_K_H_prime,~,states_poly] = smolyak_plot(pols.K_H_prime,funs,sz_array);
nd_K_R_prime = smolyak_plot(pols.K_R_prime,funs,sz_array,states_poly);
nd_K_S_prime = smolyak_plot(pols.K_S_prime,funs,sz_array,states_poly);
nd_R_D_prime = smolyak_plot(pols.R_D_prime,funs,sz_array,states_poly);
nd_R_B_prime = smolyak_plot(pols.R_B_prime,funs,sz_array,states_poly);
[nd_Q,cellVals] = smolyak_plot(pols.Q,funs,sz_array,states_poly);

nd_N_R_prime_n2n = zeros([sz_array prod(exo_array)]);
nd_N_R_prime_n2r = zeros([sz_array prod(exo_array)]);
nd_N_S_prime_n2n = zeros([sz_array prod(exo_array)]);

for ii = 1:prod(exo_array)
   
    nd_N_R_prime_n2n(:,:,:,:,ii) = smolyak_plot(pols.N_R_prime_n2n(:,ii),funs,sz_array,states_poly);
    nd_N_R_prime_n2r(:,:,:,:,ii) = smolyak_plot(pols.N_R_prime_n2r(:,ii),funs,sz_array,states_poly);
    nd_N_S_prime_n2n(:,:,:,:,ii) = smolyak_plot(pols.N_S_prime_n2n(:,ii),funs,sz_array,states_poly);

end

nd_N_R_prime_n2n = reshape(nd_N_R_prime_n2n,[sz_array exo_array]);
nd_N_R_prime_n2r = reshape(nd_N_R_prime_n2r,[sz_array exo_array]);
nd_N_S_prime_n2n = reshape(nd_N_S_prime_n2n,[sz_array exo_array]);

cell_grid = {cellVals{1},cellVals{2},cellVals{3},cellVals{4}};
cell_grid_next = {cellVals{1},cellVals{2},cellVals{3},cellVals{4},grd.eps_Z,grd.eps_Xi};

int = struct;
int.K_H_prime = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime = griddedInterpolant(cell_grid,nd_K_R_prime);
int.K_S_prime = griddedInterpolant(cell_grid,nd_K_S_prime);
int.R_D_prime = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q       = griddedInterpolant(cell_grid,nd_Q,'linear','nearest');

int.N_R_prime_n2n = griddedInterpolant(cell_grid_next,nd_N_R_prime_n2n);
int.N_R_prime_n2r = griddedInterpolant(cell_grid_next,nd_N_R_prime_n2r);
int.N_S_prime_n2n = griddedInterpolant(cell_grid_next,nd_N_S_prime_n2n);

% - run case -
sz_array = [20 20 10];
[nd_K_H_prime,~,states_poly] = smolyak_plot(pols_run.K_H_prime,funs_run,sz_array);
nd_K_R_prime = smolyak_plot(pols_run.K_R_prime,funs_run,sz_array,states_poly);
nd_R_D_prime = smolyak_plot(pols_run.R_D_prime,funs_run,sz_array,states_poly);
nd_R_B_prime = smolyak_plot(pols_run.R_B_prime,funs_run,sz_array,states_poly);
[nd_Q,cellVals] = smolyak_plot(pols_run.Q,funs_run,sz_array,states_poly);

nd_N_R_prime_r2n = zeros([sz_array exo_array(1)]);
nd_N_R_prime_r2r = zeros([sz_array exo_array(1)]);
nd_N_S_prime_r2n = zeros([sz_array exo_array(1)]);

for ii = 1:exo_array(1)
   
    nd_N_R_prime_r2n(:,:,:,ii) = smolyak_plot(pols_run.N_R_prime_r2n(:,ii),funs_run,sz_array,states_poly);
    nd_N_R_prime_r2r(:,:,:,ii) = smolyak_plot(pols_run.N_R_prime_r2r(:,ii),funs_run,sz_array,states_poly);
    nd_N_S_prime_r2n(:,:,:,ii) = smolyak_plot(pols_run.N_S_prime(:,ii),funs_run,sz_array,states_poly);

end

nd_N_R_prime_r2n = reshape(nd_N_R_prime_r2n,[sz_array exo_array(1)]);
nd_N_R_prime_r2r = reshape(nd_N_R_prime_r2r,[sz_array exo_array(1)]);
nd_N_S_prime_r2n = reshape(nd_N_S_prime_r2n,[sz_array exo_array(1)]);

cell_grid = {cellVals{1},cellVals{2},cellVals{3}};
cell_grid_next = {cellVals{1},cellVals{2},cellVals{3},grd.eps_Z};

int.K_H_prime_star = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime_star = griddedInterpolant(cell_grid,nd_K_R_prime);
int.R_D_prime_star = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime_star = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q_star         = griddedInterpolant(cell_grid,nd_Q);

int.N_R_prime_r2n = griddedInterpolant(cell_grid_next,nd_N_R_prime_r2n);
int.N_R_prime_r2r = griddedInterpolant(cell_grid_next,nd_N_R_prime_r2r);
int.N_S_prime_r2n = griddedInterpolant(cell_grid_next,nd_N_S_prime_r2n);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial Conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - states, no run -
seq_N_R = ones(nPanels, nSimulations)*steady.N_R;  % state
seq_N_S = ones(nPanels, nSimulations)*steady.N_S;  % state 
seq_K   = ones(nPanels, nSimulations)*steady.K;    % state

% - states, run -
seq_N_R_star = ones(nPanels, nSimulations)*steady.N_R;    % state
seq_N_S_star = zeros(nPanels, nSimulations);  % state

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preallocation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice
seq_K_prime   = ones(nPanels, nSimulations);
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_I         = ones(nPanels, nSimulations);  % choice
seq_phi_R     = ones(nPanels, nSimulations);  % choice
seq_phi_S     = ones(nPanels, nSimulations);  % choice

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_star     = ones(nPanels, nSimulations);  % price, run

seq_R_K_star = ones(nPanels, nSimulations);

seq_run           = zeros(nPanels, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_state  = zeros(nPanels, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(nPanels, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(nPanels, nSimulations);   %%%%% whether a run starts in period iii
seq_recovery      = ones(nPanels, nSimulations);
    
for tt = 1: nSimulations

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % determine whether the run condition is met
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if tt > 1
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % determine the counterfactual liquidation price of capital 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        eval_point = [seq_N_R_star(:,tt) seq_K(:,tt) seq_logZ(:,tt)];

        seq_Q_star(:,tt) = int.Q_star(eval_point);
        
        seq_R_K_star(:,tt) = p.alpha*seq_Z(:, tt).*seq_K(:, tt).^(p.alpha-1) ...
                      + (1-p.delta)*seq_Q_star(:,tt);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % determine the recovery value 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        seq_recovery(:,tt) = shocks.xi(:,tt).*seq_R_K_star(:,tt).*seq_K_S_prime(:,tt-1)./ ...
            (seq_R_B_prime(:,tt-1).*seq_B_prime(:,tt-1));

        seq_run(:,tt) = p.pi_start*(seq_recovery(:,tt) < 1);

        seq_recovery(seq_B_prime(:,tt-1) == 0,tt) = 1;
        
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % determine whether the economy is in a bank run
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    A = seq_run(:,tt);  % bankrun condition is met and coordination fails
    B = seq_run_persist(:, tt) == 1; % bankrun continues

    seq_run_state(:,tt) =  1 - (1-A).*(1-B); %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
    seq_run_triggered(:,tt) = A.*(1-B);    

    seq_N_R(: ,tt) = bound(seq_N_R_star(:,tt).*seq_run_state(:,tt) + ...
                    seq_N_R(:,tt).*(1-seq_run_state(:,tt)),p.exoInc*seq_K(:,tt),funs.orig_max(1));
    seq_N_S(:, tt) = bound(seq_N_S_star(:,tt).*seq_run_state(:,tt) + ...
                    seq_N_S(:,tt).*(1-seq_run_state(:,tt)),p.exoInc*seq_K(:,tt),funs.orig_max(2));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % bank run policy functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    eval_point = [seq_N_R(:,tt) seq_K(:,tt) seq_logZ(:,tt)];
    
    
    eval_point = eval_point(seq_run_state(:,tt)==1,:);
    
    seq_Q(seq_run_state(:,tt)==1,tt)         = int.Q_star(eval_point);
    seq_K_H_prime(seq_run_state(:,tt)==1,tt) = int.K_H_prime_star(eval_point);
    seq_K_R_prime(seq_run_state(:,tt)==1,tt) = int.K_R_prime_star(eval_point);
    seq_K_S_prime(seq_run_state(:,tt)==1,tt) = 0;
    seq_R_D_prime(seq_run_state(:,tt)==1,tt) = int.R_D_prime_star(eval_point);
    seq_R_B_prime(seq_run_state(:,tt)==1,tt) = int.R_B_prime_star(eval_point);                         

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % no bank run policy functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    eval_point = [seq_N_R(:,tt) seq_N_S(:, tt) seq_K(:,tt)  seq_logZ(:,tt)];
    eval_point = eval_point(~seq_run_state(:,tt),:);
    
    seq_Q(~seq_run_state(:,tt),tt)         = int.Q(eval_point);
    seq_K_H_prime(~seq_run_state(:,tt),tt) = int.K_H_prime(eval_point);
    seq_K_R_prime(~seq_run_state(:,tt),tt) = int.K_R_prime(eval_point);
    seq_K_S_prime(~seq_run_state(:,tt),tt) = int.K_S_prime(eval_point);           
    seq_R_D_prime(~seq_run_state(:,tt),tt) = int.R_D_prime(eval_point);
    seq_R_B_prime(~seq_run_state(:,tt),tt) = int.R_B_prime(eval_point);          

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % other stuff
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    seq_phi_S(:,tt)     = max((seq_K_S_prime(:,tt).*seq_Q(:,tt))./seq_N_S(:,tt),1);

    seq_B_prime(:,tt)   = (seq_phi_S(:,tt) - 1).*seq_N_S(:,tt);

    seq_phi_R(:,tt)     = ((seq_Q(:,tt) + ...
        p.eta_R*seq_K_R_prime(:,tt)./seq_K(:,tt)).*seq_K_R_prime(:,tt) ...
        + p.gamma*seq_B_prime(:,tt))./seq_N_R(:,tt);

    seq_D_prime(:,tt)    = (seq_phi_R(:,tt) - 1).*seq_N_R(:,tt) + ...
        (1-p.gamma)*seq_B_prime(:,tt);       

    seq_K_prime(:,tt) = seq_K_H_prime(:, tt) + seq_K_R_prime(:, tt) + seq_K_S_prime(:, tt);
    seq_I(:,tt)    =  seq_K_prime(:,tt) - (1-p.delta)*seq_K(:,tt);  

    if tt == nSimulations
        break
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Other stuff
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    seq_run_persist(:, tt+1) = seq_run_state(:,tt).*(shocks.pi(:,tt+1) < pi_cutoff); %%%%% whether bankrun persists to the next period

    seq_K(:, tt+1) = seq_K_prime(:,tt);

    seq_Y_prime = seq_Z(:, tt + 1) .* seq_K(:, tt+1).^p.alpha;

    seq_W_prime(:, tt)  = (1-p.alpha) * seq_Y_prime;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update the state variables, no run case
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    eval_point = [seq_N_R(:,tt) seq_N_S(:, tt) seq_K(:,tt)  seq_logZ(:,tt) ...
        shocks.eps_Z(:,tt+1) shocks.eps_Xi(:,tt+1)];

    seq_N_R_n2n = int.N_R_prime_n2n(eval_point);
    seq_N_R_n2r = int.N_R_prime_n2r(eval_point);
    seq_N_S_n2n = int.N_S_prime_n2n(eval_point);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update the state variables, run case
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    eval_point = [seq_N_R(:,tt) seq_K(:,tt)  seq_logZ(:,tt) ...
        shocks.eps_Z(:,tt+1)];

    seq_N_R_r2n = int.N_R_prime_r2n(eval_point);
    seq_N_R_r2r = int.N_R_prime_r2r(eval_point);
    seq_N_S_r2n = int.N_S_prime_r2n(eval_point);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update the state variables, run case
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    seq_N_R(:,tt+1) = seq_N_R_n2n.*(1-seq_run_state(:,tt)) + ...
        seq_N_R_r2n.*seq_run_state(:,tt);
    seq_N_S(:,tt+1) = seq_N_S_n2n.*(1-seq_run_state(:,tt)) + ...
        seq_N_S_r2n.*seq_run_state(:,tt);
    seq_N_R_star(:,tt+1) = seq_N_R_n2r.*(1-seq_run_state(:,tt)) + ...
        seq_N_R_r2r.*seq_run_state(:,tt);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq_R_K_prime = seq_R_K_prime.*(1-seq_run_state) + ...
    seq_R_K_star.*seq_run_state;

seq_Y = exp(seq_logZ).*seq_K.^(p.alpha) ...
    - p.eta_H/2*seq_K_H_prime.^2./seq_K ...
    - p.eta_R/2*seq_K_R_prime.^2./seq_K;

seq_I_tilde = seq_I + p.theta/2*(seq_I./seq_K - p.delta).^2.*seq_K;

seq_C_R = p.sigma_R*(seq_N_R - p.exoInc*seq_K)/(1 - p.sigma_R);

seq_C_S = p.sigma_S*(seq_N_S - p.exoInc*seq_K)/(1 - p.sigma_S);

seq_C_H = seq_Y - seq_I_tilde;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TS = struct;
TS.logZ     = seq_logZ(:,shocks.burnin+1:end);
TS.N_R      = seq_N_R(:,shocks.burnin+1:end);
TS.N_S      = seq_N_S(:,shocks.burnin+1:end);
TS.K        = seq_K(:,shocks.burnin+1:end);
TS.Q        = seq_Q(:,shocks.burnin+1:end);
TS.Q_star   = seq_Q_star(:,shocks.burnin+1:end);
TS.run_starts      = seq_run_triggered(:,shocks.burnin+1:end);
TS.run_state       = seq_run_state(:,shocks.burnin+1:end);

TS_add = struct;
TS_add.Y            =  seq_Y(:,shocks.burnin+1:end);
TS_add.I            =  seq_I_tilde(:,shocks.burnin+1:end);
TS_add.C_H          =  seq_C_H(:,shocks.burnin+1:end);
TS_add.C_R          =  seq_C_R(:,shocks.burnin+1:end);
TS_add.C_S          =  seq_C_S(:,shocks.burnin+1:end);
TS_add.D_prime      =  seq_D_prime(:,shocks.burnin+1:end);
TS_add.B_prime      =  seq_B_prime(:,shocks.burnin+1:end);
TS_add.R_D          =  seq_R_D_prime(:,shocks.burnin+1:end);
TS_add.R_B          =  seq_R_B_prime(:,shocks.burnin+1:end);
TS_add.phi_R        =  seq_phi_R(:,shocks.burnin+1:end);
TS_add.phi_S        =  seq_phi_S(:,shocks.burnin+1:end);
TS_add.K_H_prime    =  seq_K_H_prime(:,shocks.burnin+1:end);
TS_add.K_R_prime    =  seq_K_R_prime(:,shocks.burnin+1:end);
TS_add.K_S_prime    =  seq_K_S_prime(:,shocks.burnin+1:end);
TS_add.recov        =  seq_recovery(:,shocks.burnin+1:end);
TS_add.R_K          =  seq_R_K_prime(:,shocks.burnin:end-1)./seq_Q(:,shocks.burnin-1:end-2);