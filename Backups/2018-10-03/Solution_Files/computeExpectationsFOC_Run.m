function expc = computeExpectationsFOC_Run(p, x0, states, ...
    nextVals, nextVals_run, nextStates, nextStates_run, Z_prob, add)

N_R = states(1,1);
K   = states(1,2);

K_H_prime = x0(1);
K_R_prime = x0(2);
C_H_star  = x0(3);

N_R_prime = nextStates(:,1)';
N_R_prime_star = nextStates_run(:,1)';
K_prime = K_H_prime + K_R_prime;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C_H_prime = nextVals{1};
V_R_prime = nextVals{2};
Q_prime   = nextVals{4};

C_H_prime_star = nextVals_run{1};
V_R_prime_star = nextVals_run{2};
Q_prime_star   = nextVals_run{3};

R_K_prime       = add.r_K_prime + (1-p.delta)*Q_prime;
R_K_prime_star  = add.r_K_prime + (1-p.delta)*Q_prime_star;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Lambda          = (C_H_prime/C_H_star).^(-p.sigma);
Lambda_star     = (C_H_prime_star/C_H_star).^(-p.sigma);

EV_K_H = Z_prob* ...
    ( ...
        (1-p.pi)*C_H_prime.^(-p.sigma).*R_K_prime + ...
        p.pi*C_H_prime_star.^(-p.sigma).*R_K_prime_star ...
    )';

EC_H = Z_prob*( ...
            (1-p.pi)*C_H_prime.^(-p.sigma) + ...
            p.pi*C_H_prime_star.^(-p.sigma) ...
        )';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EV_R = Z_prob* ...
    ( ...
        (1-p.pi)*Lambda.*V_R_prime + ...
        p.pi*Lambda_star.*V_R_prime_star ...
    )';

n_R_prime = (N_R_prime - p.exoInc*K_prime)/(1-p.sigma_R);
n_R_prime_star = (N_R_prime_star - p.exoInc*K_prime)/(1-p.sigma_R);

Omega_R_prime = V_R_prime./n_R_prime;
Omega_R_prime_star = V_R_prime_star./n_R_prime_star;

Omega_R_prime(n_R_prime <= 0) = 1;
Omega_R_prime_star(n_R_prime_star <= 0) = 1;

E_Omega = Z_prob* ...
    ( ...
        (1-p.pi)*Lambda.*Omega_R_prime + ...
        p.pi*Lambda_star.*Omega_R_prime_star ...
    )';

E_R_K_R_prime = Z_prob* ...
    ( ...
        (1-p.pi)*Lambda.*Omega_R_prime.*R_K_prime + ...
        p.pi*Lambda_star.*Omega_R_prime_star.*R_K_prime_star ...
    )' ...
    /(add.Q + p.eta_R*K_R_prime/K);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

R_D_prime_star = C_H_star^(-p.sigma)/(p.beta * EC_H);
R_B_prime_star = E_R_K_R_prime*p.gamma./E_Omega + R_D_prime_star*(1-p.gamma);

Omega_R = p.beta*EV_R/N_R;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Collect
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
expc = struct;

% household
expc.EV_K_H = EV_K_H;

% retail bank
expc.EV_R = EV_R;
expc.Omega_R = Omega_R;

% prices
expc.R_D_prime = R_D_prime_star;
expc.R_B_prime = R_B_prime_star;
expc.E_R_K_R_prime = E_R_K_R_prime;