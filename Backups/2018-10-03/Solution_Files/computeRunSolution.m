function [pols_run,vals_run] = computeRunSolution(p,grd,states_run,funs,funs_run, ...
    pols_run_old,options,reg)

vec_N_R = states_run(:,1);
vec_K   = states_run(:,2);
vec_logZ = states_run(:,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preallocate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_K_H_prime_star  = zeros(grd.n_total_run,1);
vec_K_R_prime_star  = zeros(grd.n_total_run,1);
vec_phi_R_star      = zeros(grd.n_total_run,1);
vec_C_H_star        = zeros(grd.n_total_run,1);
vec_Q_star          = zeros(grd.n_total_run,1);
vec_R_D_prime_star  = zeros(grd.n_total_run,1);
vec_R_B_prime_star  = zeros(grd.n_total_run,1);
vec_K_prime_star    = zeros(grd.n_total_run,1);
N_R_prime_r2n       = zeros(grd.n_total_run,grd.n_Z);
N_R_prime_r2r       = zeros(grd.n_total_run,grd.n_Z);
N_S_prime           = zeros(grd.n_total_run,grd.n_Z);
vec_phi_R_tilde_star = zeros(grd.n_total_run,1);
vec_phi_R_bar = zeros(grd.n_total_run,1);
vec_Q_tilde_star    = zeros(grd.n_total_run,1);

for ii = 1:grd.n_total_run      
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Aggregate LoM for Z'
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*vec_logZ(ii) + p.sigma_Z*grd.eps_Z; % no adjustment necessary, since no shadow banks

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Next Period Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
    nextStates_r2n = [pols_run_old.N_R_prime_r2n(ii,:)' pols_run_old.N_S_prime(ii,:)' ...
        pols_run_old.K_prime(ii,1)*ones(size(logZ_prime)) logZ_prime];            

    nextStates_r2r    = [pols_run_old.N_R_prime_r2r(ii,:)' ...
        pols_run_old.K_prime(ii,1)*ones(size(logZ_prime)) logZ_prime];

    nextVals_r2n = computeExpectations(nextStates_r2n,funs);

    nextVals_r2r = computeExpectations_run(nextStates_r2r,funs_run);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Solve the System of FOC
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    handle = @(x) FOC_run(p,x, ...
        states_run(ii,:),nextVals_r2n,nextVals_r2r,nextStates_r2n,nextStates_r2r,grd.Z_prob');        

    x0 = [pols_run_old.K_H_prime(ii,1);
        pols_run_old.K_R_prime(ii,1);
        pols_run_old.C_H(ii,1)];

    [x1,~,~] = fsolve(handle,x0,options);      

    [~,add,expc] = handle(x1);

    vec_phi_R_tilde_star(ii,1) = add.phi_R;

%     if add.phi_R > add.phi_R_bar
%         
%         handle = @(x) FOC_run_retail(p,x, ...
%         states_run(ii,:),nextVals_r2n,nextVals_r2r,nextStates_r2n,nextStates_r2r,grd.Z_prob');        
% 
%         x0 = x1;
% 
%         [x1,~,~] = fsolve(handle,x0,options);      
% 
%         [~,add,expc] = handle(x1);
%         
%     end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Store Solution
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_K_H_prime_star(ii,1)    = x1(1);
    vec_K_R_prime_star(ii,1)    = x1(2);
    vec_C_H_star(ii,1)          = x1(3);
    
    vec_phi_R_star(ii,1)        = add.phi_R;
    vec_phi_R_bar(ii,1)   = add.phi_R_bar;
    vec_Q_star(ii,1)            = add.Q;
    vec_R_D_prime_star(ii,1)    = expc.R_D_prime;
    vec_R_B_prime_star(ii,1)    = expc.R_B_prime;

    vec_K_prime_star(ii,1) = vec_K_H_prime_star(ii,1) + vec_K_R_prime_star(ii,1);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the Net Worth
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [N_R_prime_r2n(ii,:),N_R_prime_r2r(ii,:),N_S_prime(ii,:)] = ...
         N_prime_run(p,states_run,x1,add,expc,nextStates_r2n, ...
         nextVals_r2n,nextVals_r2r);
end
    
N_R_prime_r2n = bound(N_R_prime_r2n,0,1.5*funs.orig_max(1));
N_R_prime_r2r = bound(N_R_prime_r2r,0,1.5*funs.orig_max(1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Other Variables not needed in the Solution Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_I_star = vec_K_prime_star - (1-p.delta)*vec_K;
vec_D_prime_star = (vec_phi_R_star-1).*vec_N_R;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pols_run = struct;
pols_run.K_H_prime      = vec_K_H_prime_star;
pols_run.K_R_prime      = vec_K_R_prime_star;
pols_run.K_prime        = vec_K_prime_star;
pols_run.I              = vec_I_star;
pols_run.phi_R          = vec_phi_R_star;
pols_run.phi_R_tilde    = vec_phi_R_tilde_star;
pols_run.phi_R_bar      = vec_phi_R_bar;
pols_run.R_D_prime      = vec_R_D_prime_star;
pols_run.R_B_prime      = vec_R_B_prime_star;
pols_run.D_prime        = vec_D_prime_star;
pols_run.N_R_prime_r2n  = N_R_prime_r2n;
pols_run.N_R_prime_r2r  = N_R_prime_r2r;
pols_run.N_S_prime      = N_S_prime;
pols_run.C_H            = vec_C_H_star;
pols_run.Q              = vec_Q_star;

% additional policies to ensure backwards compatibility of the code
pols_run.N_R_prime      = N_R_prime_r2n;
pols_run.N_R_prime_star = N_R_prime_r2r;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Value Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vals_run = struct;

vals_run.C_H    = pols_run.C_H;
vals_run.V_R    = (p.sigma_R + (1-p.sigma_R)*p.psi*vec_phi_R_tilde_star).*(vec_N_R - p.exoInc*vec_K)/(1-p.sigma_R);
vals_run.Q      = pols_run.Q;