function ex_ante_util_H = computeWelfare(C_H,p)

[nPanels,nPeriods] = size(C_H);

tauVals = 0:nPeriods-1;
        
disc_H = p.beta.^tauVals;
util_H = sum(repmat(disc_H,nPanels,1).*log(C_H),2);

ex_ante_util_H = mean(vec(exp((1-p.beta)*util_H)));
