% solves the unemployment model

clear
close

addpath('./Functions')
addpath('./Functions/Smolyak_Anisotropic_JMMV_2014')
addpath('./Functions/CompEcon Toolbox/CEtools')
addpath('./Solution_Files')
addpath('./Simulation_Files')
addpath('./Steady')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt = struct;

opt.solveModel      = 0;
opt.simulModel      = 1;
opt.newGuess        = 0;
opt.createTables    = 0;
opt.overwrite_initial_guess = 0;

opt.reg             = {'none','retail_dynamic','shadow_dynamic'}; 
% none, retail, shadow, both, retail_dynamic, shadow_dynamic, both_dynamic
opt.run             = {'with_runs','no_runs'};
% with_runs, no_runs
opt.modelVersion    = 'baseline';
opt.seed = rng;

nReg = length(opt.reg);
nRun = length(opt.run);

[iReg,iRun] = ndgrid(1:nReg,1:nRun);

iReg = vec(iReg);
iRun = vec(iRun);

nII = nReg*nRun;

for ii = 1:nII
    
opt.capReqVals   = setPolicy(opt.reg{iReg(ii)});

nXX = size(opt.capReqVals,1);

    for xx = 1:nXX

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Parameters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        p = setParams(opt.reg{iReg(ii)},opt.run{iRun(ii)},opt.capReqVals(xx,:));

        if opt.solveModel == 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Model Solution
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            if opt.newGuess == 1 && xx == 1
                delete('initialGuess.mat')
            end

            if exist(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)}),'dir') == 0
                mkdir(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)}))
            end

            try

                [pols,pols_run,funs,funs_run,vals,vals_run,grd,steady,states_no_run,states_run,out] =  ...
                    solveModel(p,opt.reg{iReg(ii)},opt.run{iRun(ii)});

                save(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)},'/Results_GammaR_',num2str(xx),'.mat'), ...
                    'pols','funs','pols_run','funs_run','p','steady','out')

                if opt.overwrite_initial_guess == 1
                    save('initialGuess.mat','pols','pols_run','vals','vals_run')
                end

            catch

                fprintf('Could not solve model %d \n',xx)

            end


        end

        if opt.simulModel == 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Model Simulation
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            try 

                load(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)},'/Results_GammaR_',num2str(xx),'.mat'))

                Tables = function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt);

                save(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)},'/Moments_GammaR_',num2str(xx),'.mat'), ...
                    'Tables')

            catch

                fprintf('Could not simulate model %d \n',xx)

            end
        end
    end
end

if opt.createTables == 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Tables
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    latexTables = createTables(opt);
        
end
