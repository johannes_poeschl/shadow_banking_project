clear

%% initialize

opt.reg             = {'none'}; % none, retail, shadow, both, retail_dynamic, both_dynamic
opt.run             = {'with_runs'}; % with_runs, no_runs
opt.modelVersion    = 'baseline';

filename = strcat('./Results/',opt.reg{1},'_',opt.run{1},'/Moments_GammaR_',num2str(1),'.mat');

load(filename)

welfare_baseline = Tables.Welfare_Table.Welfare(1);

%%

opt.reg             = {'shadow_dynamic'}; % none, retail, shadow, both, retail_dynamic, both_dynamic
opt.run             = {'with_runs'}; % with_runs, no_runs
opt.modelVersion    = 'baseline';
opt.capReqVals      = setPolicy(opt.reg{1});

nReg = length(opt.reg);
nGam = size(opt.capReqVals,1);
nRun = length(opt.run);

[iGam,iReg,iRun] = ndgrid(1:nGam,1:nReg,1:nRun);

iReg = vec(iReg);
iGam = vec(iGam);
iRun = vec(iRun);

nXX = nReg*nGam*nRun;

welfare = zeros(nXX,1);
retail_CR = zeros(nXX,1);
shadow_CR = zeros(nXX,1);
run_frequency = zeros(nXX,1);
cons_vola = zeros(nXX,1);
cons_drop = zeros(nXX,1);
output_vola = zeros(nXX,1);
output_drop = zeros(nXX,1);
phi_R_mean = zeros(nXX,1);
phi_S_mean = zeros(nXX,1);

for xx = 1:nXX
    
    p = setParams(opt.reg{iReg(xx)},opt.run{iRun(xx)},opt.capReqVals(iGam(xx),:));
    
    retail_CR(xx) = p.nu_R;
    shadow_CR(xx) = p.nu_S;
    
    filename = strcat('./Results/',opt.reg{iReg(xx)},'_',opt.run{iRun(xx)},'/Moments_GammaR_',num2str(xx),'.mat');
        
    if exist(filename,'file') == 2
        
        load(filename)
        
        welfare(xx) = Tables.Welfare_Table.Welfare(1);
        run_frequency(xx) = Tables.paper.varVals(end-1);
        cons_vola(xx) = Tables.paper.varVals(5);
        output_vola(xx) = Tables.paper.varVals(3); 
        cons_drop(xx) = Tables.paper.varVals(10);
        output_drop(xx) = Tables.paper.varVals(7);
        phi_R_mean(xx) = Tables.Bank_Run_Statistics.Mean(4);
        phi_S_mean(xx) = Tables.Bank_Run_Statistics.Mean(5);
        
    else
        
        welfare(xx) = nan;
        run_frequency(xx) = nan;
        cons_vola(xx) = nan;
        cons_drop(xx) = nan;
        output_vola(xx) = nan;
        output_drop(xx) = nan;
        
    end

end

welfare_normalized = 100*(welfare/welfare_baseline-1);

[maxVal,maxPos] = max(welfare_normalized);

fprintf('Welfare Gain: %6.4f \n',maxVal)
fprintf('Retail: %6.4f, Shadow: %6.4f',retail_CR(maxPos),shadow_CR(maxPos))

%%

% welfare_normalized = reshape(welfare_normalized,11,11);
% run_frequency = reshape(run_frequency,11,11);
% cons_vola = reshape(cons_vola,11,11);
% cons_drop = reshape(cons_drop,11,11);
% retail_CR = reshape(retail_CR,11,11);
% shadow_CR = reshape(shadow_CR,11,11);
% 
% varVals{1} = welfare_normalized;
% varVals{2} = run_frequency;
% varVals{3} = cons_vola;
% varVals{4} = cons_drop;
% 
% varNames = {'Welfare','Run Frequency','Volatility, Consumption','Fall in Q in a Run'};
% 
% figure
% for ii = 1:length(varVals)
%     subplot(2,2,ii)
%     surf(retail_CR(:,1),shadow_CR(1,:),varVals{ii});
%     xlabel('shadow CR')
%     ylabel('retail CR')
%     title(varNames{ii})
% end
