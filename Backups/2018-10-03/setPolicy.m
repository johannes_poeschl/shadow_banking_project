function policyVals = setPolicy(reg)
%SETPOLICY Summary of this function goes here
%   Detailed explanation goes here

% none, retail, shadow, both, retail_dynamic, both_dynamic

sens_vals = -25:1:1;
% sens_vals = -1:0.2:1;

if strcmp(reg,'none') == 1
    CR_retail   = 0;
    CR_shadow   = 0;
    sens_retail = 0;
    sens_shadow = 0;
elseif strcmp(reg,'retail') == 1
    CR_retail   = 0:0.02:0.2;
    CR_shadow   = 0;
    sens_retail = 0;
    sens_shadow = 0;
elseif strcmp(reg,'shadow') == 1
    CR_retail   = 0;
    CR_shadow   = 0:0.02:0.2;
    sens_retail = 0;
    sens_shadow = 0;
elseif strcmp(reg,'both') == 1
    CR_retail   = 0:0.02:0.2;
    CR_shadow   = 0:0.02:0.2;
    sens_retail = 0;
    sens_shadow = 0;
elseif strcmp(reg,'retail_dynamic') == 1
    CR_retail   = 0.1;
    CR_shadow   = 0;
    sens_retail = sens_vals;
    sens_shadow = 0;
elseif strcmp(reg,'shadow_dynamic') == 1
    CR_retail   = 0;
    CR_shadow   = 0.05;
    sens_retail = 0;
    sens_shadow = sens_vals;
elseif strcmp(reg,'both_dynamic') == 1
    CR_retail   = 0.1;
    CR_shadow   = 0.05;
    sens_retail = sens_vals;
    sens_shadow = sens_vals;
end
    

[nd_CR_retail,nd_CR_shadow,nd_sens_retail,nd_sens_shadow] =  ...
    ndgrid(CR_retail,CR_shadow,sens_retail,sens_shadow);

policyVals = [vec(nd_CR_retail) vec(nd_CR_shadow) vec(nd_sens_retail) vec(nd_sens_shadow)];


end

