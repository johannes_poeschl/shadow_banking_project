% This file computes the steady state of the new model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear

addpath('./Functions')
addpath('./Steady')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameter Space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt.reg = 'retail';
opt.run = 'with_runs';
parName = 'phi_bar';
parName_TeX = strcat('$$ 1/\bar{\phi}^R $$');

xLow = 0;
xHigh  = 1;

parSpace = linspace(xLow,xHigh,250);
nPars = length(parSpace);

vec_D       = zeros(nPars,1);
vec_B       = zeros(nPars,1);
vec_K_H     = zeros(nPars,1);
vec_K_R     = zeros(nPars,1);
vec_K_S     = zeros(nPars,1);
vec_K       = zeros(nPars,1);
vec_phi_R   = zeros(nPars,1);
vec_phi_R_tilde   = zeros(nPars,1);
vec_phi_S   = zeros(nPars,1);
vec_C_H     = zeros(nPars,1);
vec_C_R     = zeros(nPars,1);
vec_C_S     = zeros(nPars,1);
vec_Y       = zeros(nPars,1);
vec_R_K_spread = zeros(nPars,1);
vec_R_B_spread = zeros(nPars,1);
vec_R_B     = zeros(nPars,1);
vec_R_D     = zeros(nPars,1);
vec_R_K     = zeros(nPars,1);
vec_D_S     = zeros(nPars,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

positiveK  = zeros(length(parSpace),1);
zeroK      = zeros(length(parSpace),1);

for ii = 1:length(parSpace)
    
    p = setParams(opt.reg,opt.run,[parSpace(ii) 0 0 0]);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Steady State
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    handle = @(x) computeSteadyState(x,p);
        
%     if ii > 1
%         x0 = x1;
%     else
        x0 = [0.2 0.4 0.4 0.0655 0.02]'*10;
%     end
    
    [x1,~,exitFlag] = fsolve(handle,x0);

    [~,steady] = handle(x1);
    
    if (steady.phi_R > p.phi_R_bar && strcmp(opt.reg,'retail') == 1) || ...
        (steady.phi_R > p.phi_R_bar && steady.phi_S < p.phi_S_bar && strcmp(opt.reg,'both') == 1)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Binding CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        positiveK(ii) = 1;
%         handle = @(x) computeSteadyState_with_CR(x,p);
        handle = @(x) computeSteadyState_with_CR_adjusting_equity(x,p,steady.K_R + steady.B);
        
        [x1_CR,~,exitFlag] = fsolve(handle,[x1; p.exoInc*steady.K]);
        
        [~,steady] = handle(x1_CR);
        
        if x1_CR(2) < 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Specialized Retail Banks
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            positiveK(ii) = 0;
            zeroK(ii)     = 1;
            x0_tilde = [x1_CR; steady.R_B];
            
            handle = @(x) computeSteadyState_with_CR_zero_KR(x,p);
            
            [x1,~,exitFlag] = fsolve(handle,x0_tilde);
            
            [~,steady] = handle(x1);
        end
        
    elseif steady.phi_S > p.phi_S_bar && strcmp(opt.reg,'shadow') == 1 || ...
        (steady.phi_S > p.phi_S_bar && steady.phi_R < p.phi_R_bar && strcmp(opt.reg,'both') == 1)
        
        positiveK(ii) = 1;
        handle = @(x) computeSteadyState_with_shadow_CR(x,p);
        
        [x1_CR,~,exitFlag] = fsolve(handle,x1);
        
        [~,steady] = handle(x1_CR);
        
        if x1_CR(2) < 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Specialized Retail Banks
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            positiveK(ii) = 0;
            zeroK(ii)     = 1;
            x0_tilde = [x1_CR; steady.R_B];
            
            handle = @(x) computeSteadyState_with_CR_zero_KR(x,p);
            
            [x1,~,exitFlag] = fsolve(handle,x0_tilde);
            
            [~,steady] = handle(x1);
        end
        
    elseif steady.phi_S > p.phi_S_bar && steady.phi_R > p.phi_R_bar && strcmp(opt.reg,'both') == 1
        
        positiveK(ii) = 1;
        handle = @(x) computeSteadyState_with_both_CR(x,p);
        
        [x1_CR,~,exitFlag] = fsolve(handle,x1);
        
        [~,steady] = handle(x1_CR);
        
    end
         
    if exitFlag ~= 1
        error('Could Not Find a Steady State')
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Store
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    vec_D_S(ii) = steady.D_S;
    vec_D(ii)   = steady.D;
    vec_B(ii)   = steady.B;
    vec_K_H(ii) = steady.K_H/steady.K;
    vec_K_R(ii) = steady.K_R/steady.K;
    vec_K_S(ii) = steady.K_S/steady.K;
    vec_K(ii)   = steady.K;
    vec_phi_R(ii) = steady.phi_R;
    vec_phi_S(ii) = steady.phi_S;
    vec_C_H(ii) = steady.C_H;
    vec_C_R(ii) = p.sigma_R*steady.N_R;
    vec_C_S(ii) = p.sigma_S*steady.N_S;
    vec_Y(ii)   = steady.Y;
    vec_R_K_spread(ii) = steady.R_K^4 - steady.R_B^4;
    vec_R_B_spread(ii) = steady.R_B^4 - steady.R_D^4;
    vec_R_B(ii) = steady.R_B;
    vec_R_D(ii) = steady.R_D;
    vec_R_K(ii) = steady.R_K;
end

c1 = find(positiveK == 1,1,'first');
c2 = find(zeroK == 1,1,'first');

if isempty(c1)
    c1 = 1;
end

if isempty(c2)
    c2 = c1;
end

%% Plots - Check the optimality of the equilibrium

xVals1 = 100*parSpace;
zero_deposit_cond = (vec_R_K - vec_R_B)./(vec_R_K - vec_R_D) -p.omega; % benefit of an additional unit of deposit funding
zero_interbank_cond = (vec_R_K - vec_R_B)./(vec_R_K - vec_R_D) - (1-p.gamma);

figure
plot(xVals1,zero_deposit_cond,'b')
hold
plot(xVals1,zero_interbank_cond,'r')
hl = line([min(xVals1) max(xVals1)],[0,0]);
set(hl,'Color','k')
legend('>0: SB use only wholesale funding','>0: SB use some wholesale funding')
title('Steady State Conditions')

figureName = strcat('./Figures/steady/Optimality_Conditions_',parName);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7/2];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots - 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plotVals    = [100*vec_K_H  vec_K -vec_B vec_D vec_phi_S vec_phi_R];
plotVals2   = [100*vec_K_R, nan(nPars,1) vec_B vec_D_S nan(nPars,2)];
plotVals3   = [100*vec_K_S nan(nPars,5)];

plotNames   = {'K^H/K','K','B^R','D^R','\phi^R','\phi^S'};
plotNames2  = {'K^R/K','','B^S','D^S','',''};
plotNames3  = {'K^S/K','','','','',''};

yLabels = {'$K^J/K$','$K$','$B^J$','$D^J$','$\phi^R$','$\phi^S$'};

plotTitles = {'(a) Capital Allocation', ...
    '(b) Aggregate Capital Stock', ...
    '(c) Wholesale Borrowing', ...
    '(d) Retail Borrowing', ...
    '(e) Shadow Bank Leverage', ...
    '(f) Retail Bank Leverage'};

fontOptions = {'FontSize',12};
fontOptions_Alt = {'FontSize',10};

if strcmp(opt.reg,'retail') == 1
    parName_TeX = 'Capital Requirement $$1/\bar{\phi}^R$$ (\%)';
elseif strcmp(opt.reg,'shadow') == 1
    parName_TeX = 'Capital Requirement $$1/\bar{\phi}^S$$ (\%)';
end

figure
for jj = 1:size(plotVals,2)
    
    subplot(3,2,jj)
    
    l1 = plot(xVals1,plotVals(:,jj),'b','LineWidth',2);
    
    legendVals = l1;
    legendNames = plotNames(jj);
    
    if jj == 1
        hold
        l2 = plot(xVals1,plotVals2(:,jj),'r:','LineWidth',2);
        l3 = plot(xVals1,plotVals3(:,jj),'k--','LineWidth',2);
        
        legendVals = [l1 l2 l3];
        legendNames = [plotNames(jj) plotNames2(jj) plotNames3(jj)];
    elseif jj == 3 || jj == 4
        hold
        l2 = plot(xVals1,plotVals2(:,jj),'r:','LineWidth',2);
        
        legendVals = [l1 l2];
        legendNames = [plotNames(jj) plotNames2(jj)];
    end
    
    set(gca,fontOptions_Alt{:})
    
    xlabel(parName_TeX,'Interpreter','latex',fontOptions{:})
    ylabel(yLabels{jj},'Interpreter','latex',fontOptions{:})
    
    xlim([min(xVals1) max(xVals1)])
    yLim = ylim;
    ylim([yLim(1)-1e-4 yLim(2)+1e-4])
    
    title(plotTitles{jj},'Interpreter','latex',fontOptions{:})
    
    line([xVals1(c1) xVals1(c1)],yLim,'Color','k')
    line([xVals1(c2) xVals1(c2)],yLim,'Color','k')
    
    
    
    if length(legendNames) > 1
        hl = legend(legendVals,legendNames);
        set(hl,'Location','NorthEast')
    end
    
end

figureName = strcat('./Figures/steady/Comparative_Statics_',opt.reg);
saveas(gcf,strcat(figureName,'.fig'))

hRim = 0.25;
vRim = 0.25;
set(gcf,'Units','centimeters')
paperDim = [21 29.7];
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])
print(figureName,'-dpdf')