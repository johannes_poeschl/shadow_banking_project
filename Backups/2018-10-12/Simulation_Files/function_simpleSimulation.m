function Tables = ...
    function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set simulation parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBurnin = 1e3; 
nSimulations = nBurnin + 1e3;  % number of simulated periods 
nPanels = 1e3; %%%%% number of simulations for each capital requirement level

rng(opt.seed);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% draw shocks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq_logZ = p.mu_Z*ones(nPanels,nSimulations);
seq_epsZ    = randn(nPanels,nSimulations);
seq_epsXi   = randn(nPanels,nSimulations);

for tt = 2:nSimulations  %%%%% for each simulation    
    seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + p.sigma_Z*seq_epsZ(:,tt);
end

shocks.eps_Z     = seq_epsZ;
shocks.eps_Xi    = seq_epsXi;
shocks.logZ     = seq_logZ;
shocks.xi       = exp(p.mu_xi + p.sigma_xi*seq_epsXi); %%%%% generage a sequence of random numbers ~N(0,1) 
shocks.pi       = normrnd(0,1,nPanels,nSimulations);
shocks.burnin   = nBurnin;

[TS,TS_add] = simulateModel_new(p,shocks,funs,funs_run,pols,pols_run,steady);

Tables = struct;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation Boundaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check the simulation boundaries

RowNames = {'N_R','N_S','K'};

Min_Sim = [min(vec(TS.N_R));  min(vec(TS.N_S)); min(vec(TS.K))];
Lower_Bound_SS = funs.orig_min(1:3)';
freq_below_bound = 100*[mean(vec(TS.N_R) <= funs.orig_min(1)); ...
        mean(vec(TS.N_S) <= funs.orig_min(2)); ...
        mean(vec(TS.K) <= funs.orig_min(3))];

Max_Sim = [max(vec(TS.N_R));  max(vec(TS.N_S)); max(vec(TS.K))];
Upper_Bound_SS = funs.orig_max(1:3)';
freq_above_bound = 100*[mean(vec(TS.N_R) >= funs.orig_max(1)); ...
        mean(vec(TS.N_S) >= funs.orig_max(2)); ...
        mean(vec(TS.K) >= funs.orig_max(3))];

Tables.SS_Bounds  = table(Min_Sim,Lower_Bound_SS,freq_below_bound, ...
    Max_Sim,Upper_Bound_SS,freq_above_bound,'RowNames',RowNames);
display(Tables.SS_Bounds)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Welfare Statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nPeriods = 100;

ex_ante_util_H = zeros(nPeriods,1);

for ii = 1:nPeriods
    ex_ante_util_H(ii) = computeWelfare(TS_add.C_H(:,ii:end),p);
end

RowNames = {'HH'};

Tables.Welfare_Table = table(mean(ex_ante_util_H), ...
    'RowNames',RowNames,'VariableNames',{'Welfare'});

display(Tables.Welfare_Table)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bank Run Statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RowNames = {'Q','Q*','x','phi_R','phi_S','I','p(x < 1)','p(I < 0)'};

mean_Q  = mean(vec(TS.Q));
std_Q = std(vec(TS.Q));

mean_Q_star = mean(vec(TS.Q_star));
std_Q_star = std(vec(TS.Q_star));

mean_recov = mean(vec(TS_add.recov));
std_recov = std(vec(TS_add.recov));

mean_phi_R = mean(vec(TS_add.phi_R));
std_phi_R = std(vec(TS_add.phi_R));

mean_phi_S = mean(vec(TS_add.phi_S));
std_phi_S = std(vec(TS_add.phi_S));

mean_I = mean(vec(TS_add.I));
std_phi_I = std(vec(TS_add.I));

prob_run = mean(vec(TS.run_starts == 1));
prob_neg_inv = mean(vec(TS_add.I./TS.K < p.delta - 1e-3));

mean_Vals = [mean_Q; mean_Q_star; mean_recov; mean_phi_R; mean_phi_S; mean_I; prob_run; prob_neg_inv];
std_Vals = [std_Q; std_Q_star; std_recov; std_phi_R; std_phi_S; std_phi_I; 0; 0];

Tables.Bank_Run_Statistics = table(mean_Vals,std_Vals, ...
    'RowNames',RowNames, ...
    'VariableNames',{'Mean','StDev'});

display(Tables.Bank_Run_Statistics)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stochastic Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RowNames = {'Output (Y)', ...
    'Consumption (C_H)', ...
    'Investment (I)', ...
    'Deposits (D'')', ...
    'Repo Lending (B'')', ...
    'Deposit Rate R^D''', ...
    'Wholesale Spread R^B'' - R^D''', ...
    'Retail Spread R^K''/Q - R^D''', ...
    'Leverage, Retail Banks (\phi^R)', ...
    'Leverage, Shadow Banks (\phi^S)', ...
    'Credit-to-GDP Ratio (Q(K^R + K^S)/Y)', ...
    'Shadow Credit-to-GDP Ratio (Q K^S/Y)'};

varVals{1} = TS_add.Y;
varVals{2} = TS_add.C_H;
varVals{3} = TS_add.I;
varVals{4} = TS_add.D_prime;
varVals{5} = TS_add.B_prime;
varVals{6} = 100*(TS_add.R_D.^4);
varVals{7} = 100*(TS_add.R_B.^4 - TS_add.R_D.^4);
varVals{8} = 100*(TS_add.R_K.^4 - TS_add.R_D.^4);
varVals{9} = TS_add.phi_R;
varVals{10} = TS_add.phi_S;
varVals{11} = TS.Q.*(TS_add.K_R_prime + TS_add.K_S_prime)./TS_add.Y;
varVals{12} = TS.Q.*TS_add.K_S_prime./TS_add.Y;

nVars = length(varVals);

stochastic_SS_vals = zeros(nVars,1);

for ii = 1:nVars
    stochastic_SS_vals(ii) = mean(vec(varVals{ii}));
end

deterministic_SS_vals = [steady.Y; ...
    steady.C_H; ...
    p.delta*steady.K; ...
    steady.D; ...
    steady.B; ...
    100*steady.R_D.^4; ...
    100*(steady.R_B.^4 - steady.R_D.^4); ...
    100*(steady.R_K.^4 - steady.R_D.^4); ...
    steady.phi_R; ...
    steady.phi_S; ...
    (steady.K_R + steady.K_S)/steady.Y; ...
    steady.K_S/steady.Y];

Tables.Stochastic_Steady_State  = table(deterministic_SS_vals,stochastic_SS_vals, ...
    'RowNames',RowNames);
display(Tables.Stochastic_Steady_State)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Business Cycle Statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pct_dev = cell(nVars,1);

for ii = 1:nVars
    pct_dev{ii} = varVals{ii}/stochastic_SS_vals(ii) - 1;
end

% standard deviations
stdev = zeros(nVars,1);

for ii = 1:nVars
    stdev(ii) = std(vec(pct_dev{ii}));
end

% correlations
corr_with_gdp = zeros(nVars,1);

for ii = 1:nVars
    corr_with_gdp(ii) = corr(vec(pct_dev{1}),vec(pct_dev{ii}));
end

% autocorrelations
autocorr = zeros(nVars,1);

for ii = 1:nVars
    autocorr(ii) = corr(vec(pct_dev{ii}(:,2:end)),vec(pct_dev{ii}(:,1:end-1)));
end

Tables.Business_Cycle_Statistics  = table(stdev,corr_with_gdp,autocorr,'RowNames',RowNames);
display(Tables.Business_Cycle_Statistics)

save(strcat('./Results/Moments_','theta_',num2str(p.theta_max),'_kappa_',num2str(p.kappa_max),'.mat'),'Tables')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event study
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tStart = 4;
tStop  = 20;

% locate all runs
[runN,runT] = find(TS.run_starts == 1);

if isempty(runN) == 0

    runN = runN(runT > tStart & runT <= nSimulations-nBurnin-tStop);
    runT = runT(runT > tStart & runT <= nSimulations-nBurnin-tStop);

    nRuns = size(runN,1);

    % put all run time events in a sequence

    allRuns = zeros(nRuns,tStart+tStop+1);
    avgRun = zeros(length(RowNames),tStart+tStop+1);

    for jj = 1:length(RowNames)
        for tt = 1:nRuns

            allRuns(tt,:,jj) = varVals{jj}(runN(tt),runT(tt)-tStart:runT(tt)+tStop);

        end

    avgRun(jj,:) = mean(allRuns(:,:,jj) - stochastic_SS_vals(jj),1);

    end

    
else
    
    avgRun = nan(length(RowNames),tStart+tStop+1);
    
end

    Tables.eventStudy = table(avgRun,'RowNames',RowNames);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Table for the paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% stdevs, variable change in run, bank run frequency, all in one column

varNames = {'Mean($\phi^R$)','Mean($\phi^S$)','StDev(log(Y))','StDev(log(I))','StDev(log(C))','StDev(Q)', ...
    '$\Delta$ log(Y) $|$ Run','$\Delta$ log(I) $|$ Run','$\Delta$ log(C) $|$ Run','$\Delta$ Credit-to-GDP $|$ Run', ...
    'Runs per 100 Years','Welfare'}';

varVals = [mean_phi_R; mean_phi_S; ...
    100*stdev(1); 100*stdev(3); 100*stdev(2); 100*std_Q; ...
    100*(avgRun([1 2 3 11],tStart+1) - avgRun([1 2 3 11],tStart)); ...
    400*prob_run; mean(ex_ante_util_H)];

Tables.section_welfare = table(varVals,'rowNames',varNames);

disp(Tables.section_welfare)

%% SchuTa-result

% schularick & taylor experiment

varNames = {'$\log(Y)$','$\log(I)$','$\log((Q+f^R)K^R + QK^S)$'}';

dataVals = [-2.02 -4.46 -6.3;
    -3.45 -12.45 -19.9;
    -1.89 -6.98 -7.7];

modelVals = [100*(avgRun([1 2 11],tStart+4) - avgRun([1 2 11],tStart)) ...
    100*(avgRun([1 2 11],tStart+8) - avgRun([1 2 11],tStart)) ...
    100*(avgRun([1 2 11],tStart+12) - avgRun([1 2 11],tStart))];

Tables.section_event_study = table(dataVals,modelVals,'rowNames',varNames);

disp(Tables.section_event_study)

input = struct;

input.dataFormat = {'%.3f \\%%'};
input.booktabs = 1;
input.tablePlacement= 'htbp';

input.data = [dataVals modelVals];
input.tableRowLabels = Tables.section_event_study.Properties.RowNames;
input.tableColLabels = {'Data','Data','Data','Model','Model','Model'};
input.tableCaption = 'Financial Crisis Moments';
input.tableLabel = 'bla';
output = latexTable(input);

%% Business Cycle Statistics
data1 = 100*0.027*[1; 0.929; 4.368; 2.449; 10.379];
data2 = [1; 0.916; 0.943; 0.759; 0.187];
data3 = [0.898; 0.887; 0.886; 0.891; 0.853];

varNames = {'Output ($Y$)','Consumption ($C$)','Investment ($\tilde{I}$)','Deposits ($D''$)','Wholesale Lending ($B''$)'};

model1 = stdev(1:5);
model2 = corr_with_gdp(1:5);
model3 = autocorr(1:5);

Tables.section_business_cycles = table(data1,100*model1,data2,model2,data3,model3,'rowNames',varNames);

input = struct;

input.dataFormat = {'%.3f'};
input.booktabs = 1;
input.tablePlacement= 'htbp';

input.data = table2array(Tables.section_business_cycles);
input.tableRowLabels = Tables.section_business_cycles.Properties.RowNames;
input.tableColLabels = {'Data','Model','Data','Model','Data','Model'};
input.tableCaption = 'Business Cycle Moments';
input.tableLabel = 'bla';
output = latexTable(input);
