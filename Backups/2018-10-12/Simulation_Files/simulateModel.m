function [TS,TS_add] = simulateModel(p,shocks,funs,funs_run,pols,pols_run,steady)
% simulateModel simulates model time series
%
% Inputs:
%   - p:        struct array containing the parameters
%   - shocks:   struct array containing the shocks
%   - funs:     struct array of interpolation accessories
%   - pols:     struct array for the policy functions
%   - steady:   struct array containing the steady state
%
% Outputs:
%   - TS:       time series structure of the endogenous states

[nPanels,nSimulations] = size(shocks.logZ);

seq_logZ = shocks.logZ;
seq_Z    = exp(seq_logZ);

p.nu = 0;
pi_cutoff = norminv(p.pi);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - no run case -
sz_array = [20 20 20 20];
nd_K_H_prime = smolyak_plot(pols.K_H_prime,funs,sz_array);
nd_K_R_prime = smolyak_plot(pols.K_R_prime,funs,sz_array);
nd_K_S_prime = smolyak_plot(pols.K_S_prime,funs,sz_array);
nd_R_D_prime = smolyak_plot(pols.R_D_prime,funs,sz_array);
nd_R_B_prime = smolyak_plot(pols.R_B_prime,funs,sz_array);
[nd_Q,cellVals] = smolyak_plot(pols.Q,funs,sz_array);

cell_grid = {cellVals{1},cellVals{2},cellVals{3},cellVals{4}};

int = struct;
int.K_H_prime = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime = griddedInterpolant(cell_grid,nd_K_R_prime);
int.K_S_prime = griddedInterpolant(cell_grid,nd_K_S_prime);
int.R_D_prime = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q       = griddedInterpolant(cell_grid,nd_Q,'linear','nearest');

% - run case -
sz_array = [20 20 10];
nd_K_H_prime = smolyak_plot(pols_run.K_H_prime,funs_run,sz_array);
nd_K_R_prime = smolyak_plot(pols_run.K_R_prime,funs_run,sz_array);
nd_R_D_prime = smolyak_plot(pols_run.R_D_prime,funs_run,sz_array);
nd_R_B_prime = smolyak_plot(pols_run.R_B_prime,funs_run,sz_array);
[nd_Q,cellVals] = smolyak_plot(pols_run.Q,funs_run,sz_array);

cell_grid = {cellVals{1},cellVals{2},cellVals{3}};

int.K_H_prime_star = griddedInterpolant(cell_grid,nd_K_H_prime);
int.K_R_prime_star = griddedInterpolant(cell_grid,nd_K_R_prime);
int.R_D_prime_star = griddedInterpolant(cell_grid,nd_R_D_prime);
int.R_B_prime_star = griddedInterpolant(cell_grid,nd_R_B_prime);
int.Q_star         = griddedInterpolant(cell_grid,nd_Q);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial Conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - states, no run -
seq_N_R = ones(nPanels, nSimulations)*steady.N_R;  % state
seq_N_S = ones(nPanels, nSimulations)*steady.N_S;  % state 
seq_K   = ones(nPanels, nSimulations)*steady.K;    % state

% - states, run -
seq_N_R_star = ones(nPanels, nSimulations)*steady.N_R;    % state
seq_N_S_star = seq_K*p.exoInc;  % state

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preallocation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
seq_K_H_prime = ones(nPanels, nSimulations);  % choice
seq_K_R_prime = ones(nPanels, nSimulations);  % choice
seq_K_S_prime = ones(nPanels, nSimulations);  % choice
seq_K_prime   = ones(nPanels, nSimulations);
seq_D_prime   = ones(nPanels, nSimulations);  % choice
seq_B_prime   = ones(nPanels, nSimulations);  % choice
seq_I         = ones(nPanels, nSimulations);  % choice
seq_phi_R     = ones(nPanels, nSimulations);  % choice
seq_phi_S     = ones(nPanels, nSimulations);  % choice

seq_R_D_prime = ones(nPanels, nSimulations);  % price 
seq_R_B_prime = ones(nPanels, nSimulations);  % price 
seq_R_K_prime = ones(nPanels, nSimulations);  % price
seq_W_prime   = ones(nPanels, nSimulations);  % price

seq_Q          = ones(nPanels, nSimulations);  % price, no run
seq_Q_star     = ones(nPanels, nSimulations);  % price, run

seq_R_K_prime_star = ones(nPanels, nSimulations);

seq_run           = zeros(nPanels, nSimulations);   %%%%% whether bankrun condition is met in period iii
seq_run_state  = zeros(nPanels, nSimulations);   %%%%% whether period iii is in bankrun
seq_run_persist   = zeros(nPanels, nSimulations);   %%%%% whether a run in last period continues in period iii
seq_run_triggered = zeros(nPanels, nSimulations);   %%%%% whether a run starts in period iii
seq_recovery      = ones(nPanels, nSimulations);
    
for tt = 1: nSimulations

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % determine whether the run condition is met
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if tt > shocks.burnin        
        seq_recovery(:,tt) = shocks.xi(:,tt).*seq_R_K_prime_star(:,tt-1).*seq_K_S_prime(:,tt-1)./ ...
            (seq_R_B_prime(:,tt-1).*seq_B_prime(:,tt-1));

        seq_run(:,tt) = p.pi_start*(seq_recovery(:,tt) < 1);

        seq_recovery(seq_B_prime(:,tt-1) == 0,tt) = 1;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % determine whether the economy is in a bank run
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    A = seq_run(:,tt);  % bankrun condition is met and coordination fails
    B = seq_run_persist(:, tt) == 1; % bankrun continues

    seq_run_state(:,tt) =  1 - (1-A).*(1-B); %%%%% if bankrun condition is met and coordination fails or if bankrun contiunes from last period
    seq_run_triggered(:,tt) = A.*(1-B);

    seq_N_R(: ,tt) = bound(seq_N_R_star(:,tt).*seq_run_state(:,tt) + ...
                    seq_N_R(:,tt).*(1-seq_run_state(:,tt)),p.exoInc*seq_K(:,tt),funs.orig_max(1));
    seq_N_S(:, tt) = bound(seq_N_S_star(:,tt).*seq_run_state(:,tt) + ...
                    seq_N_S(:,tt).*(1-seq_run_state(:,tt)),p.exoInc*seq_K(:,tt),funs.orig_max(2));
    seq_Q(:,tt)    = seq_Q_star(:,tt).*seq_run_state(:,tt) + ...
                    seq_Q(:,tt).*(1-seq_run_state(:,tt));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % bank run policy functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    eval_point = [seq_N_R(:,tt) seq_K(:,tt) seq_logZ(:,tt)];
    eval_point = eval_point(seq_run_state(:,tt)==1,:);

    seq_K_H_prime(seq_run_state(:,tt)==1,tt) = int.K_H_prime_star(eval_point);
    seq_K_R_prime(seq_run_state(:,tt)==1,tt) = int.K_R_prime_star(eval_point);
    seq_K_S_prime(seq_run_state(:,tt)==1,tt) = 0;
    seq_R_D_prime(seq_run_state(:,tt)==1,tt) = int.R_D_prime_star(eval_point);
    seq_R_B_prime(seq_run_state(:,tt)==1,tt) = int.R_B_prime_star(eval_point);                         

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % no bank run policy functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    eval_point = [seq_N_R(:,tt) seq_N_S(:, tt) seq_K(:,tt)  seq_logZ(:,tt)];
    eval_point = eval_point(~seq_run_state(:,tt),:);

    seq_K_H_prime(~seq_run_state(:,tt),tt) = int.K_H_prime(eval_point);
    seq_K_R_prime(~seq_run_state(:,tt),tt) = int.K_R_prime(eval_point);
    seq_K_S_prime(~seq_run_state(:,tt),tt) = int.K_S_prime(eval_point);           
    seq_R_D_prime(~seq_run_state(:,tt),tt) = int.R_D_prime(eval_point);
    seq_R_B_prime(~seq_run_state(:,tt),tt) = int.R_B_prime(eval_point);          

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % other stuff
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    seq_phi_S(:,tt)     = max((seq_K_S_prime(:,tt).*seq_Q(:,tt))./seq_N_S(:,tt),1);

    seq_B_prime(:,tt)   = (seq_phi_S(:,tt) - 1).*seq_N_S(:,tt);

    seq_phi_R(:,tt)     = ((seq_Q(:,tt) + ...
        p.eta_R*seq_K_R_prime(:,tt)./seq_K(:,tt)).*seq_K_R_prime(:,tt) ...
        + p.gamma*seq_B_prime(:,tt))./seq_N_R(:,tt);

    seq_D_prime(:,tt)    = (seq_phi_R(:,tt) - 1).*seq_N_R(:,tt) + ...
        (1-p.gamma)*seq_B_prime(:,tt);       

    seq_K_prime(:,tt) = seq_K_H_prime(:, tt) + seq_K_R_prime(:, tt) + seq_K_S_prime(:, tt);
    seq_I(:,tt)    =  seq_K_prime(:,tt) - (1-p.delta)*seq_K(:,tt);  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % determine the counterfactual liquidation price of capital 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    eval_point = [seq_N_R_star(:,tt) seq_K(:,tt) seq_logZ(:,tt)];

    seq_Q_star(:,tt) = int.Q_star(eval_point);

    if tt == nSimulations
        break
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update the state variables
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    seq_run_persist(:, tt+1) = seq_run_state(:,tt).*(shocks.pi(:,tt+1) < pi_cutoff); %%%%% whether bankrun persists to the next period

    seq_K(:, tt+1) = seq_K_prime(:,tt);

    seq_Y_prime = seq_Z(:, tt + 1) .* seq_K(:, tt+1).^p.alpha;

    seq_W_prime(:, tt)  = (1-p.alpha) * seq_Y_prime;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % iteration options
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tol = 1e-6;
    maxIter = 100;
    weight = 0.5;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % iterate on the future net worth and capital price, no runs
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    nError = 10;
    iter = 1;

    N_R_prime_old = seq_N_R(:,tt+1);
    N_S_prime_old = seq_N_S(:,tt+1);

    while nError > tol && iter <= maxIter
        % compute Q' given N^R' and N^S'
        eval_point = [N_R_prime_old N_S_prime_old seq_K(:,tt+1)  seq_logZ(:,tt+1)];
        Q_prime        = int.Q(eval_point);

        seq_R_K_prime(:, tt) = p.alpha * seq_Y_prime./seq_K(:, tt+1)...
                          + (1-p.delta)*Q_prime;

        N_R_prime_new =  (seq_R_K_prime(:, tt).*seq_K_R_prime(:, tt) ...
                  + seq_R_B_prime(:,tt).*seq_B_prime(:,tt) ...
                  - seq_R_D_prime(:, tt).*seq_D_prime(:, tt))*(1-p.sigma_R) ...
                  + p.exoInc*seq_K(:,tt+1);

        N_S_prime_new =  (seq_R_K_prime(:, tt).*seq_K_S_prime(:, tt) ...
                            - seq_R_B_prime(:, tt).*seq_B_prime(:, tt))*(1-p.sigma_S) ...
                            + p.exoInc*seq_K(:,tt+1);

        nError = norm(N_R_prime_new - N_R_prime_old,'inf');
        iter = iter + 1;

        N_R_prime_old = weight*N_R_prime_old + (1-weight)*N_R_prime_new;
        N_S_prime_old = weight*N_S_prime_old + (1-weight)*N_S_prime_new;
    end

    seq_N_R(:, tt+1) = N_R_prime_old;
    seq_N_S(:, tt+1) = N_S_prime_old;
    seq_Q(:,tt+1)    = Q_prime;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % iterate on the future net worth and capital price, runs
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    nError = 10;
    iter = 1;

    N_R_prime_old = seq_N_R_star(:,tt+1);

    while nError > tol && iter <= maxIter
        % compute Q' given N^R' and N^S'
        eval_point = [N_R_prime_old seq_K(:,tt+1)  seq_logZ(:,tt+1)];
        Q_prime_star        = int.Q_star(eval_point);

        seq_R_K_prime_star(:,tt) = p.alpha * seq_Y_prime./seq_K(:, tt+1)...
                      + (1-p.delta)*Q_prime_star;

        recov = min(shocks.xi(:,tt+1).*seq_R_K_prime_star(:,tt).*seq_K_S_prime(:,tt)./ ...
            (seq_R_B_prime(:,tt).*seq_B_prime(:,tt)),1);

        N_R_prime_new =  (seq_R_K_prime_star(:, tt).*seq_K_R_prime(:, tt) ...
                          + recov.*seq_R_B_prime(:,tt).*seq_B_prime(:,tt) ...
                          - seq_R_D_prime(:, tt).*seq_D_prime(:, tt))*(1-p.sigma_R) ...
                          + p.exoInc*seq_K(:,tt);

        nError = norm(N_R_prime_new - N_R_prime_old,'inf');
        iter = iter + 1;

        N_R_prime_old = weight*N_R_prime_old + (1-weight)*N_R_prime_new;                        
    end

    seq_N_R_star(:, tt+1) = N_R_prime_old;
    seq_N_S_star(:, tt+1) = p.exoInc*seq_K(:,tt);
    seq_Q_star(:,tt+1)     = Q_prime_star;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Additional Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq_R_K_prime = seq_R_K_prime.*(1-seq_run_state) + ...
    seq_R_K_prime_star.*seq_run_state;

seq_Y = exp(seq_logZ).*seq_K.^(p.alpha) ...
    - p.eta_H/2*seq_K_H_prime.^2./seq_K_prime ...
    - p.eta_R/2*seq_K_R_prime.^2./seq_K_prime;

seq_I_tilde = seq_I + p.theta/2*(seq_I./seq_K - p.delta).^2.*seq_K;

seq_C_R = p.sigma_R*(seq_N_R - p.exoInc*seq_K)/(1 - p.sigma_R);

seq_C_S = p.sigma_S*(seq_N_S - p.exoInc*seq_K)/(1 - p.sigma_S);

seq_C_H = seq_Y - seq_I_tilde;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TS = struct;
TS.logZ     = seq_logZ(:,shocks.burnin+1:end);
TS.N_R      = seq_N_R(:,shocks.burnin+1:end);
TS.N_S      = seq_N_S(:,shocks.burnin+1:end);
TS.K        = seq_K(:,shocks.burnin+1:end);
TS.Q        = seq_Q(:,shocks.burnin+1:end);
TS.Q_star   = seq_Q_star(:,shocks.burnin+1:end);
TS.run_starts      = seq_run_triggered(:,shocks.burnin+1:end);
TS.run_state       = seq_run_state(:,shocks.burnin+1:end);

TS_add = struct;
TS_add.Y            =  seq_Y(:,shocks.burnin+1:end);
TS_add.I            =  seq_I_tilde(:,shocks.burnin+1:end);
TS_add.C_H          =  seq_C_H(:,shocks.burnin+1:end);
TS_add.C_R          =  seq_C_R(:,shocks.burnin+1:end);
TS_add.C_S          =  seq_C_S(:,shocks.burnin+1:end);
TS_add.D_prime      =  seq_D_prime(:,shocks.burnin+1:end);
TS_add.B_prime      =  seq_B_prime(:,shocks.burnin+1:end);
TS_add.R_D          =  seq_R_D_prime(:,shocks.burnin+1:end);
TS_add.R_B          =  seq_R_B_prime(:,shocks.burnin+1:end);
TS_add.phi_R        =  seq_phi_R(:,shocks.burnin+1:end);
TS_add.phi_S        =  seq_phi_S(:,shocks.burnin+1:end);
TS_add.K_H_prime    =  seq_K_H_prime(:,shocks.burnin+1:end);
TS_add.K_R_prime    =  seq_K_R_prime(:,shocks.burnin+1:end);
TS_add.K_S_prime    =  seq_K_S_prime(:,shocks.burnin+1:end);
TS_add.recov        =  seq_recovery(:,shocks.burnin+1:end);
TS_add.R_K          =  seq_R_K_prime(:,shocks.burnin:end-1)./seq_Q(:,shocks.burnin-1:end-2);