function [ nextVals ] = computeExpectations( nextStates,funs,steady )

n_Z = size(nextStates,1);

% adjust the evaluation point to lie within the grid
nextStates_int = nextStates;
for ii = 1:size(nextStates,2)
    nextStates_int(:,ii) = bound(nextStates(:,ii),funs.orig_min(ii),funs.orig_max(ii));
end

nextStates_unit =  Rescale(nextStates_int,funs.unit_min,funs.unit_max, ...
    funs.orig_min,funs.orig_max);
nextStates_poly = Smolyak_Polynomial(nextStates_unit,funs.nDims, ...
    funs.max_approx_level,funs.elem);

% evaluate the interpolated functions
C_H_1 = (nextStates_poly*funs.C_H);
V_R_1 = (nextStates_poly*funs.V_R);
V_S_1 = (nextStates_poly*funs.V_S);
Q_1 = (nextStates_poly*funs.Q);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR EXTRAPOLATION OFF THE GRID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(abs(vec(nextStates - nextStates_int))) > 0
    df = 1e-2;
    
    nDims = 3;
    
    aux = eye(nDims);
    aux1 = kron(aux,ones(n_Z,1));
    
    eval_point_plus = repmat(nextStates_int,nDims,1) + df*[aux1 zeros(length(aux1),1)];
    
    eval_point_rescaled_minus =  Rescale(eval_point_plus,funs.unit_min,funs.unit_max, ...
        funs.orig_min,funs.orig_max);
    
    eval_poly_plus = Smolyak_Polynomial(eval_point_rescaled_minus,funs.nDims, ...
        funs.max_approx_level,funs.elem);

    C_H_1_plus = (eval_poly_plus*funs.C_H);
    V_R_1_plus = (eval_poly_plus*funs.V_R);
    V_S_1_plus = (eval_poly_plus*funs.V_S);
    Q_1_plus   = (eval_poly_plus*funs.Q);
    
    C_H_1_plus = reshape(C_H_1_plus,n_Z,nDims);
    V_R_1_plus = reshape(V_R_1_plus,n_Z,nDims);
    V_S_1_plus = reshape(V_S_1_plus,n_Z,nDims);
    Q_1_plus = reshape(Q_1_plus,n_Z,nDims);
    
    C_H_1_diff = [(C_H_1_plus - repmat(C_H_1,1,nDims))./df zeros(n_Z,1)];
    V_R_1_diff = [(V_R_1_plus - repmat(V_R_1,1,nDims))./df zeros(n_Z,1)];
    V_S_1_diff = [(V_S_1_plus - repmat(V_S_1,1,nDims))./df zeros(n_Z,1)];
    Q_1_diff = [(Q_1_plus - repmat(Q_1,1,nDims))./df zeros(n_Z,1)];
    
    C_H_1 = C_H_1 + sum(C_H_1_diff.*(nextStates - nextStates_int),2);
    V_R_1 = V_R_1 + sum(V_R_1_diff.*(nextStates - nextStates_int),2);
    V_S_1 = V_S_1 + sum(V_S_1_diff.*(nextStates - nextStates_int),2);
    Q_1 = Q_1 + sum(Q_1_diff.*(nextStates - nextStates_int),2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C_H_prime = max(C_H_1',1e-3);
V_R_prime = max(V_R_1',0);
V_S_prime = max(V_S_1',0);
Q_prime = max(Q_1',0);

nextVals = cell(4,1);
nextVals(1) = {C_H_prime};
nextVals(2) = {V_R_prime};
nextVals(3) = {V_S_prime};
nextVals(4) = {Q_prime};

