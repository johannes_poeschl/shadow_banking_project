function [resid,steady] = computeSteadyState_with_shadow_CR(x,p)

K_H = x(1);
K_R = x(2);
K_S = x(3);
N_R = x(4);
N_S = x(5);

% Market Clearing
K = K_H + K_R + K_S;
exoInc = p.exoInc*K;

% Final Goods Producer
r_K = p.alpha*exp(p.mu_Z)*K^(p.alpha-1);
W = (1-p.alpha)*exp(p.mu_Z)*K^p.alpha;

% Returns
R_D     = 1/p.beta;
R_K     = r_K + 1 - p.delta;
R_K_H   = R_K/(1 + p.eta_H*K_H/K);
R_K_R   = R_K/(1 + p.eta_R*K_R/K);

% Shadow Bank
phi_S   = p.phi_S_bar;
B       = (phi_S - 1)*N_S;

% Retail Bank
f_R     = p.eta_R*K_R/K;
n_R     = (N_R - exoInc)/(1-p.sigma_R);
phi_R   = p.beta*p.sigma_R*n_R/(p.psi*(N_R-p.beta*(1-p.sigma_R)*n_R));

% capital requirement does not bind
R_B     = p.gamma_bar*R_K_R + (1-p.gamma_bar)*R_D;
D       = (phi_R - 1)*N_R + (1 - p.gamma_bar)*B;

% Household
N_H = (r_K + 1 - p.delta)*K_H + R_D*D + W;
Y = exp(p.mu_Z)*K^p.alpha - p.eta_H/2*K_H^2/K - p.eta_R/2*K_R^2/K;
C_H = Y - p.delta*K;

resid    = zeros(5,1);
resid(1) = p.beta*R_K_H - 1;
resid(2) = phi_R*N_R - (1 + f_R)*K_R - p.gamma_bar*B;
resid(3) = phi_S*N_S - K_S;
resid(4) = N_S - ((R_K - R_B)*phi_S + R_B)*N_S*(1-p.sigma_S) - exoInc;
resid(5) = N_R - (((R_K_R - R_D)*phi_R + R_D)*N_R ...
    + ((R_B - R_D) - p.gamma_bar*(R_K_R - R_D))*B)*(1-p.sigma_R) - exoInc;

% collect all steady state values in a struct array
steady      = struct;
steady.R_D  = R_D;
steady.R_B  = R_B;
steady.R_K  = R_K;
steady.K_H  = K_H;
steady.K_R  = K_R;
steady.K_S  = K_S;
steady.K    = K;
steady.B    = B;
steady.D    = D;
steady.W    = W;
steady.N_H  = N_H;
steady.N_R  = N_R;
steady.N_S  = N_S;
steady.C_H  = C_H;
steady.phi_R = phi_R;
steady.phi_S = phi_S;
steady.Y    = Y;
steady.D_S = 0;