function a_new = ar_lom(mu_a,rho_a,sigma_a,a_old,eps_old)

a_new = (1-rho_a)*mu_a + rho_a*a_old + sigma_a*eps_old;