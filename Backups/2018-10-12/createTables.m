function output = createTables(opt)

nRun = length(opt.run);
nReg = length(opt.reg);
nCapReqVals = length(opt.capReqVals);

input = struct;

input.dataFormat = {'%.1f \\%%'};
input.booktabs = 1;
input.tablePlacement= 'ht';


if nRun == 1
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Regulation Experiments
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    input.tableColumnAlignment = 'lrrrr';
    
    for ii = 1:nReg 
        
        for jj = 1:nCapReqVals

            load(strcat('./Results/',opt.reg{ii},'_',opt.run{1},'/Moments_GammaR_',num2str(opt.capReqVals(jj)),'.mat'), ...
                        'Tables')

            tableVals(:,jj) = table2array(Tables.paper);

        end

        % compute the welfare gain relative to baseline
        tableVals(end,:) = 100*(tableVals(end,:)/tableVals(end,1) - 1);

        input.data = tableVals;
        input.tableRowLabels = Tables.paper.Properties.RowNames;
        input.tableColLabels = num2cell(opt.capReqVals);
        input.tableCaption = opt.reg{ii};
        input.tableLabel = char(strcat('regulation_',opt.reg(ii)'));
        output(:,ii) = latexTable(input);

    end
else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Removing Runs Experiment
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    input.tableColumnAlignment = 'lrr';
    
    for kk = 1:nRun

        load(strcat('./Results/',opt.reg{1},'_',opt.run{kk},'/Moments_GammaR_',num2str(opt.capReqVals),'.mat'), ...
                    'Tables')

        tableVals(:,kk) = table2array(Tables.paper);

    end

    % compute the welfare gain relative to baseline
    tableVals(end,:) = 100*(tableVals(end,:)/tableVals(end,1) - 1);

    input.data = tableVals;
    input.tableRowLabels = Tables.paper.Properties.RowNames;
    input.tableColLabels = opt.run;
    input.tableCaption = 'removing runs';
    input.tableLabel = 'removing_runs';
    output = latexTable(input);
end