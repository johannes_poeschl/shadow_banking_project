function pols_new = interpolate_on_new_grid( pols,funs,states )

states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);

% extract the information on the policies from the structure
field_names = fieldnames(pols);

nFields = length(field_names);

% interpolate all policies
pols_new = struct;
for jj = 1:nFields

    eval(strcat('old_values=pols.',field_names{jj},';'))

    coeffs = funs.base_poly\old_values;

    new_values = states_poly*coeffs;

    eval(strcat('pols_new.',field_names{jj},'=new_values;'))
end