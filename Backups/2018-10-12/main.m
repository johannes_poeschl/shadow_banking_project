% solves the unemployment model

clear
close

addpath('./Functions')
addpath('./Functions/Smolyak_Anisotropic_JMMV_2014')
addpath('./Functions/CompEcon Toolbox/CEtools')
addpath('./Solution_Files')
addpath('./Simulation_Files')
addpath('./Steady')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt = struct;

opt.solveModel      = 0;
opt.simulModel      = 1;
opt.newGuess        = 0;
opt.createTables    = 1;
opt.overwrite_initial_guess = 0;
opt.use_old_solution  = 1;

opt.reg             = {'none','retail_dynamic'}; 
% none, retail, shadow, both, retail_dynamic, shadow_dynamic, both_dynamic
opt.run             = {'with_runs'};
% with_runs, no_runs
opt.modelVersion    = 'baseline';
opt.seed = rng;

nReg = length(opt.reg);
nRun = length(opt.run);

[iReg,iRun] = ndgrid(1:nReg,1:nRun);

iReg = vec(iReg);
iRun = vec(iRun);

nII = nReg*nRun;

for ii = 1:nII
    
opt.capReqVals   = setPolicy(opt.reg{iReg(ii)});

nXX = size(opt.capReqVals,1);

    for xx = 1:nXX

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Parameters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        p = setParams(opt.reg{iReg(ii)},opt.run{iRun(ii)},opt.capReqVals(xx,:));
        
        foldername = strcat('./Results/', ...
                        opt.reg{iReg(ii)},'_', ...
                        opt.run{iRun(ii)});
                    
        filename_solu = strcat('Results_', ...
           'GamR_',num2str(1/p.phi_R_bar), ...
           '_GamS_',num2str(1/p.phi_S_bar), ...
           '_nuR_',num2str(p.nu_R), ...
           '_nuS_',num2str(p.nu_S), ...
           '.mat');
       
       filename_simu = strcat('Moments_', ...
           'GamR_',num2str(1/p.phi_R_bar), ...
           '_GamS_',num2str(1/p.phi_S_bar), ...
           '_nuR_',num2str(p.nu_R), ...
           '_nuS_',num2str(p.nu_S), ...
           '.mat');

        if opt.solveModel == 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Model Solution
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            if opt.newGuess == 1 && xx == 1
                delete('initialGuess.mat')
            end

            if exist(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)}),'dir') == 0
                mkdir(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)}))
            end
            
            try
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % START Quick and dirty - get the old results as initial guess
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                if opt.use_old_solution == 1
                    
                    load(strcat(foldername,'/',filename_solu), ...
                        'pols','funs','pols_run','funs_run')

                    vals.C_H = funs.base_poly*funs.C_H;
                    vals.V_R = funs.base_poly*funs.V_R;
                    vals.V_S = funs.base_poly*funs.V_S;
                    vals.Q = funs.base_poly*funs.Q;

                    vals_run.C_H = funs_run.base_poly*funs_run.C_H;
                    vals_run.V_R = funs_run.base_poly*funs_run.V_R;
                    vals_run.Q = funs_run.base_poly*funs_run.Q;

                    save('initialGuess.mat','pols','pols_run','vals','vals_run','funs','funs_run')
                    
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % END Quick and dirty - get the old results as initial guess
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                [pols,pols_run,funs,funs_run,vals,vals_run,grd,steady,states_no_run,states_run,out] =  ...
                    solveModel(p,opt.reg{iReg(ii)},opt.run{iRun(ii)});

                save(strcat(foldername,'/',filename_solu), ...
                    'pols','funs','vals','pols_run','funs_run','vals_run','p','steady','out')

                if opt.overwrite_initial_guess == 1
                    save('initialGuess.mat','pols','pols_run','vals','vals_run')
                end

            catch

                fprintf('Could not solve model %d \n',xx)

            end

        end

        if opt.simulModel == 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Model Simulation
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            try 

                load(strcat(foldername,'/',filename_solu))

                Tables = function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt);

                save(strcat(foldername,'/',filename_simu),'Tables')

            catch

                fprintf('Could not simulate model %d \n',xx)

            end
        end
    end
end

if opt.createTables == 1
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Tables
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%     latexTables = createTables(opt);
    
    for ii = 1:nII
    
        Summary_Tables{ii} = plotWelfare(opt.reg(iReg(ii)),opt.run(iRun(ii)));
    
    end

        
end
