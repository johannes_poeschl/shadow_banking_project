function Summary_Tables = plotWelfare(reg,run)

%% initialize

opt.reg             = {'none'}; % none, retail, shadow, both, retail_dynamic, both_dynamic
opt.run             = run; % with_runs, no_runs
opt.modelVersion    = 'baseline';
opt.capReqVals      = setPolicy(opt.reg{1});

foldername = strcat('./Results/', ...
                        opt.reg{1},'_', ...
                        opt.run{1});
                    
p = setParams(opt.reg{1},opt.run{1},opt.capReqVals(1,:));
       
filename = strcat('Moments_', ...
   'GamR_',num2str(1/p.phi_R_bar), ...
   '_GamS_',num2str(1/p.phi_S_bar), ...
   '_nuR_',num2str(p.nu_R), ...
   '_nuS_',num2str(p.nu_S), ...
   '.mat');

load(strcat(foldername,'/',filename))

welfare_baseline = Tables.Welfare_Table.Welfare(1);
run_frequency_baseline = Tables.section_welfare.varVals(end-1);
mean_vals_baseline     = Tables.Stochastic_Steady_State.stochastic_SS_vals;
stdev_vals_baseline    = Tables.Business_Cycle_Statistics.stdev;
corr_vals_baseline     = Tables.Business_Cycle_Statistics.corr_with_gdp;
autocorr_vals_baseline = Tables.Business_Cycle_Statistics.autocorr;
impact_effect_baseline = Tables.eventStudy.avgRun(:,5);
cum_effect_baseline    = cumsum(Tables.eventStudy.avgRun(:,end),2);

%%

opt.reg             = reg; % none, retail, shadow, both, retail_dynamic, both_dynamic
opt.modelVersion    = 'baseline';
opt.capReqVals      = setPolicy(opt.reg{1});

foldername = strcat('./Results/', ...
                        opt.reg{1},'_', ...
                        opt.run{1});

nReg = length(opt.reg);
nGam = size(opt.capReqVals,1);
nRun = length(opt.run);

[iGam,iReg,iRun] = ndgrid(1:nGam,1:nReg,1:nRun);

iReg = vec(iReg);
iGam = vec(iGam);
iRun = vec(iRun);

nXX = nReg*nGam*nRun;

RowNames = {'Output (Y)', ...
    'Consumption (C_H)', ...
    'Investment (I)', ...
    'Deposits (D'')', ...
    'Repo Lending (B'')', ...
    'Deposit Rate R^D''', ...
    'Wholesale Spread R^B'' - R^D''', ...
    'Retail Spread R^K''/Q - R^D''', ...
    'Leverage, Retail Banks (\phi^R)', ...
    'Leverage, Shadow Banks (\phi^S)', ...
    'Credit-to-GDP Ratio (Q(K^R + K^S)/Y)', ...
    'Shadow Credit-to-GDP Ratio (Q K^S/Y)'};

nVars = length(RowNames);

gammaS          = zeros(1,nXX);
gammaR          = zeros(1,nXX);
nuS             = zeros(1,nXX);
nuR             = zeros(1,nXX);
welfare         = zeros(1,nXX);
run_frequency   = zeros(1,nXX);
mean_vals       = zeros(nVars,nXX);
stdev_vals      = zeros(nVars,nXX);
corr_vals       = zeros(nVars,nXX);
autocorr_vals   = zeros(nVars,nXX);
impact_effect   = zeros(nVars,nXX);
cum_effect      = zeros(nVars,nXX);

for xx = 1:nXX
    
    p = setParams(opt.reg{iReg(xx)},opt.run{iRun(xx)},opt.capReqVals(iGam(xx),:));
    
    gammaR(xx) = 1/p.phi_R_bar;
    gammaS(xx) = 1/p.phi_S_bar;
    nuR(xx) = p.nu_R;
    nuS(xx) = p.nu_S;
    
    filename = strcat('Moments_', ...
           'GamR_',num2str(1/p.phi_R_bar), ...
           '_GamS_',num2str(1/p.phi_S_bar), ...
           '_nuR_',num2str(p.nu_R), ...
           '_nuS_',num2str(p.nu_S), ...
           '.mat');
     
    if exist(strcat(foldername,'/',filename),'file') == 2
        
        load(strcat(foldername,'/',filename)) 
        
        welfare(xx)         = Tables.Welfare_Table.Welfare(1);
        run_frequency(xx)   = Tables.section_welfare.varVals(end-1);
        mean_vals(:,xx)     = Tables.Stochastic_Steady_State.stochastic_SS_vals;
        stdev_vals(:,xx)    = Tables.Business_Cycle_Statistics.stdev;
        corr_vals(:,xx)     = Tables.Business_Cycle_Statistics.corr_with_gdp;
        autocorr_vals(:,xx) = Tables.Business_Cycle_Statistics.autocorr;
        impact_effect(:,xx) = Tables.eventStudy.avgRun(:,5);
        cum_effect(:,xx)    = cumsum(Tables.eventStudy.avgRun(:,end),2);
        
    else
        
        welfare(xx)         = nan;
        run_frequency(xx)   = nan;
        mean_vals(:,xx)     = nan(nVars,1);
        stdev_vals(:,xx)    = nan(nVars,1);
        corr_vals(:,xx)     = nan(nVars,1);
        autocorr_vals(:,xx) = nan(nVars,1);
        impact_effect(:,xx) = nan(nVars,1);
        cum_effect(:,xx)    = nan(nVars,1);
        
    end

end

welfare_gain = 100*(welfare/welfare_baseline-1);
run_frequency_reduction = run_frequency_baseline - run_frequency;
consumption_volatility_change = ...
    100*(stdev_vals(2,:)/stdev_vals_baseline(2) - 1);
consumption_mean_change = ...
    100*(mean_vals(2,:)/mean_vals_baseline(2) - 1);

[maxVal,maxPos] = max(welfare_gain);

fprintf(strcat(run{1},' \n'))
fprintf('Welfare Gain: %6.4f \n',maxVal)
fprintf('Run Frequency Reduction: %6.4f \n',run_frequency_reduction(maxPos))
fprintf('Consumption Volatility Change: %6.4f \n',consumption_volatility_change(maxPos))
fprintf('Consumption Mean Change: %6.4f \n',consumption_mean_change(maxPos))
fprintf('gammaR: %6.4f, gammaS: %6.4f, nuR: %6.4f, nuS: %6.4f \n \n',...
    gammaR(maxPos),gammaS(maxPos),nuR(maxPos),nuS(maxPos))

%%

Summary_Tables = struct;
Summary_Tables.welfare          = table(welfare_gain);
Summary_Tables.run_frequency    = table(run_frequency);
Summary_Tables.mean_vals        = table(mean_vals,'RowNames',RowNames);
Summary_Tables.stdev_vals       = table(stdev_vals,'RowNames',RowNames);
Summary_Tables.corr_vals        = table(corr_vals,'RowNames',RowNames);
Summary_Tables.autocorr_vals    = table(autocorr_vals,'RowNames',RowNames);
Summary_Tables.impact_effect    = table(impact_effect,'RowNames',RowNames);
Summary_Tables.cum_effect       = table(cum_effect,'RowNames',RowNames);
