clear all

alpha=0:.01:2;
t=1;
a=0;
div=1;

while a<2
    a=alpha(t);
    g0=[1 1 -0.5 0; -a 1 1 -0.5; 1 0 0 0; 0 1 0 0];
    g1=[0.5 0 0 0; 0 0.5 0 0; 0 0 1 0; 0 0 0 1];
    c=zeros(4,4);
    psi=[1 0 ;0 1 ; 0 0 ; 0 0];
    pi=[0 0 ;0 0;1 0; 0 1];
    [G1,C,impact,fmat,fwt,ywt,gev,eu]=gensys(g0,g1,c,psi,pi,div);
    if eu == [1;1]
        B(t)=alpha(t);
    else
        B(t)=0;
    end
    t=t+1;
end   
