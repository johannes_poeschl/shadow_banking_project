function annualizedYield = annualizeYield(yield,frequency)
% annualizeYield transforms a yield into annual frequency
% inputs:
%   - yield can be a matrix
%   - frequency in number of years. Quarterly frequency is for example 0.25

annualizedYield = (1 + yield).^(1/frequency) - 1;

end