% basic RBC model with perfectly elastic labor supply


clear
beta=0.99;      % dicsount factor
delta=0.025;    % depreciation rate
sigma=1;    % 1/sigma = interteporal elasticity of substitution
alpha=1/3; % capital share
rho=1;   % persistence in technology shocks
eta=1;
Phi=1;
sigma_e=.007;
%==========================================================================
nlead = 1;      % Number of leads in system 
nlag  = 1;      % Number of lags in system 
xnames = strvcat('c','y','k','L','i','z','z_');
               %   1   2   3   4      5
xnum = length(xnames); % Number of variables in system 
neq = xnum;                 % Number of equations (same)
cpos   =  1;               % consumption
ypos   =  2;               % output
kpos   =  3;               % capital
Lpos   =  4;               % labor
ipos   =  5;               % investment
zpos   =  6;
z_pos  =  7;

colzero = 0+nlag*xnum;      % Position counter for start of contemp. coefs 
collead = 0+nlag*xnum+xnum; % Position counter for start of lead coefs 
collag  = 0;                % Position counter for start of lag coefs  

czero   = colzero+cpos;
yzero   = colzero+ypos;
kzero   = colzero+kpos;
Lzero   = colzero+Lpos;
izero   = colzero+ipos;
zzero   = colzero+zpos;
z_zero  = colzero+z_pos;

clead   = collead+cpos;
ylead   = collead+ypos;
klead   = collead+kpos;
Llead   = collead+Lpos;
ilead   = collead+ipos;
zlead  = collead+zpos;
z_lead  = collead+z_pos;

clag   = collag+cpos;
ylag   = collag+ypos;
klag   = collag+kpos;
Llag   = collag+Lpos;
ilag   = collag+ipos;
zlag  = collag+zpos;
z_lag  = collag+z_pos;


% Now we have one vector with all of the leads, contemporaneous and 
% lags stacked in one column
% Determine number of total posible coefficients per equation: 
ncoef = neq*(nlag+nlead+1);
% Initialize the coeffienct matrix, where each 
% row is an equation of the model
cof = zeros(neq,ncoef);      % Coef matrix --- Each row is an equation
% Setup coefficients vectors for each equation: 
% ==============================================
% labor FOC
cof(1,czero) = sigma;
cof(1,yzero) = -1;
cof(1,Lzero) = (1+1/eta);

% euler equation
cof(2,clead)    = sigma;

coefA=1-beta*(1-delta);

cof(2,czero)    =  -sigma;
cof(2,klead)    =  -beta*Phi;
cof(2,klag)     =  -Phi;
cof(2,kzero)    =  Phi*(1+beta)+coefA;
cof(2,ylead)    =  -coefA;

% resource constraint
IYratio=delta*beta*alpha/(1-beta*(1-delta));

cof(3,yzero)   = -1;
cof(3,izero)   =  IYratio;
cof(3,czero)   =  1-IYratio;

% capital accumulation
cof(4,kzero)   =  -1;
cof(4,klag)    =  (1-delta);
cof(4,izero)   =  delta;

% ?????????????????????????????????
% note timing of capital


% production function
cof(5,yzero)    =  -1;
cof(5,klag)     =  alpha;
cof(5,Lzero)    =  1-alpha;
cof(5,zzero)    =  1;

% correlated error term: technology
cof(6,zzero)  =  -1;
cof(6,zlag)   =  rho;
cof(6,z_zero) =  1;

% 0 = SHOCKS
cof(7,z_zero) =  1;

%==========================================================================
% Use AIM procedure to solve model: 
uprbnd = 1+1e-8;    % Tolerance values for AIM program 
condn = 1e-8;
[cofb,rts,ia,nex,nnum,lgrts,mcode] = ...
       aim_eig(cof,neq,nlag,nlead,condn,uprbnd);

scof = obstruct(cof,cofb,neq,nlag,nlead);
s0 = scof(:,(neq*nlag+1):neq*(nlag+1)); %Contemp. coefs from obs.
                                        %structure
amat=zeros(neq*nlag,neq*nlag);   		% Initialize A matrix 
bmat=cofb(1:neq,((nlag-1)*neq+1):nlag*neq); % Lag 1 coefficients 
i=2;
while i<=nlag;
  bmat=[bmat cofb(1:neq,((nlag-i)*neq+1):(nlag-i+1)*neq)]; % Lag i coefs 
  i=i+1;
end;
amat(1:neq,:)=bmat;  				% Coefs for equations 
if nlag>1;
 amat((length(cofb(:,1))+1):length(amat(:,1)),...
     1:neq*(nlag-1))=eye(neq*(nlag-1));
end;
b = zeros(length(amat(:,1)),length(s0(1,:)));
b(1:length(s0(:,1)),1:length(s0(1,:))) = inv(s0);  % Store coefs 


%==========================================================================
% check unique/stable REE: mcode = 1
disp(['mcode = ' num2str(mcode)])

% ?????????????????????????????????
% unstable? non-unique?

% compute and plot impulse response
% use reduced form solution: x_t=amat*x_{t-1}+b*shock
T=100; % forecast horizon

shock=zeros(neq,1);

shock(z_pos)=1;
% ?????????????????????????????????
% how to introduce multiple shocks? 

imptech=b*shock;

for i=2:T
    imptech=[imptech amat*imptech(:,i-1)];
end


figure(1)
for i=1:6
subplot(3,2,i)
plot(1:T,imptech(i,:),'LineWidth',2)
title(xnames(i))
end

%%
% doubling method to compute covariances
error=10;
iter=1;
cofvar=eye(7).*.5;
while error > 1e-6 && iter < 1e4
    cofvarnew=amat*cofvar*amat'+b*sigma_e*b';
    error=norm(cofvarnew-cofvar);
    iter=iter+1;
    cofvar=cofvarnew;
end