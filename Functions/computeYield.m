function yield = computeYield(principal,coupon,bondPrice)

yield = 100*((principal + coupon)*bondPrice.^(-1) - principal);