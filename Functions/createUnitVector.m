function unitVector = createUnitVector(n,dim)
% constrcts the n-th unit vector of length dim

unitVector = zeros(dim,1);
unitVector(n,1) = 1;