function [grid,trans] = markov_ar(muZ,sigE,lam,rhoZ,dim)

% testing values
% muZ = 0.5;
% sigE = 1;
% lam = 2;
% rhoZ = 0.8;
% dim = 20;

% compute the unconditional variance
sigZ = sigE/sqrt(1-rhoZ^2);

% generate the bounds of the grid
lZ = muZ - lam*sigZ;
uZ = muZ + lam*sigZ;

% generate the grid
gridZ = linspace(lZ,uZ,dim);

% generate the midpoints (of dimension dim-1)
gridM = (gridZ(1,2:end) + gridZ(1,1:end-1))/2;

% generate transition possibilities row i, column j: transition from state
% i to state j

% first, extend the grids

% starting points are constant across rows
matZ = repmat(gridZ',1,dim-1);

% endpoints are constant across columns
matM = repmat(gridM,dim,1);

% normalized end values
normM = (matM - (1-rhoZ)*muZ - rhoZ*matZ)/sigE;

% corresponding cdfs
cdfM = normcdf(normM);

% preallocation
trans = zeros(dim,dim);

% take differences across rows
trans(:,2:end-1) = cdfM(:,2:end) - cdfM(:,1:end-1);
trans(:,1) = cdfM(:,1);
trans(:,end) = 1-cdfM(:,end);

grid = gridZ';