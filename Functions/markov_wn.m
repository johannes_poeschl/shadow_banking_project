function [values,transition_probs] = markov_wn(I_mean,I_std,I_scale,gridsize)
%This function approximates a standard normal white noise process by a markov chain.

z_top = I_mean + I_scale*I_std;
z_bot = I_mean - I_scale*I_std;

z_space=linspace(z_bot,z_top,gridsize);
z_space=z_space';

z_midpoints=(z_space(2:end,1)+z_space(1:end-1,1))/2;
z_cdf=normcdf(z_midpoints,I_mean,I_std);

z_trans=zeros(gridsize,1);
z_trans(2:end-1,1)=z_cdf(2:end,1)-z_cdf(1:end-1,1);
z_trans(gridsize,1)=1-z_cdf(gridsize-1,1);
z_trans(1,1)=z_cdf(1,1);

values=z_space;
transition_probs=z_trans;