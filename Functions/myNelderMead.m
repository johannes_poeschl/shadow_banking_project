function [ xFinal,yFinal ] = myNelderMead(f,x0)
%myNelderMead Finds the arg min of a function using the Nelder-Mead method
%   Detailed explanation goes here

pAlpha = 1;
pGamma = 2;
pRho = -0.5;
pSigma = 0.5;

% create a grid of starting points
x1 = x0.*[1; 1.5];
x2 = x0.*[1.5; 1];

X = [x0 x1 x2];

nVars = size(X,1);
nPoints = nVars + 1;

Y = zeros(1,nPoints);
for ii = 1:nPoints
    Y(:,ii) = f(X(:,ii));
end

[Ysort,sortOrder] = sort(Y);
Xsort = X(:,sortOrder);
yFinal = Ysort(:,3);

errorVal = 10;
crit = 1e-10;
iter = 1;
maxIter = 500;

while errorVal > crit && iter < maxIter
    %centroid
    xo = 1/(nPoints - 1)*sum(Xsort(:,1:end-1),2);

    % worst point
    xworst = Xsort(:,end);
    yworst = Ysort(:,end);

    xr = xo + pAlpha*(xo - xworst);
    yr = f(xr);

    if Ysort(:,1) < yr && yr < Ysort(:,2)
        % Reflection
        fprintf('Function Value: %6.4f, Reflection \n',yFinal)
        X = [Xsort(:,1:end-1) xr];
        Y = [Ysort(:,1:end-1) yr];
    elseif yr < Ysort(:,1)
        % Expansion
        fprintf('Function Value: %6.4f, Expansion \n',yFinal)
        xe = xo + pGamma*(xo - xworst);
        ye = f(xe);

        if ye <= yr
            X = [Xsort(:,1:end-1) xe];
            Y = [Ysort(:,1:end-1) ye];
        else
            X = [Xsort(:,1:end-1) xr];
            Y = [Ysort(:,1:end-1) yr];
        end
    else
        xc = xo + pRho*(xo - xworst);
        yc = f(xc);

        if yc < yworst
           % Contraction
           fprintf('Function Value: %6.4f, Contraction \n',yFinal)
           X = [Xsort(:,1:end-1) xc];
           Y = [Ysort(:,1:end-1) yc];
        else
           % Reduction
           fprintf('Function Value: %6.4f, Reduction \n',yFinal)
           X = (1-pSigma)*repmat(Xsort(:,1),1,nPoints) + pSigma*Xsort;
           y0 = Ysort(:,1);
           y1 = f(X(:,2));
           y2 = f(X(:,3));
           Y = [y0 y1 y2];
        end
    end
    
    [Ysort,sortOrder] = sort(Y);
    Xsort = X(:,sortOrder);
    
    errorVal = norm(Ysort(:,3) - yFinal);
    iter = iter + 1;
    
    xFinal = Xsort(:,3);
    yFinal = Ysort(:,3);
end
end

