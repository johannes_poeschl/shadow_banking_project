function [acf,lags,bounds] = my_acf(data,numlags)
%autocorrelation function

numrows = length(data);
lags = (0:1:numlags)';
acf=ones(numlags+1,1);
bounds = [(1/numrows)^(.5)*2; -(1/numrows)^(.5)*2];

for j = 1:numlags
    lagmat=ones(numrows-j,j);
    for i=1:j
        lagmat(:,i) = data(j+1-i:numrows-i,1); 
    end
    X = [ones(numrows-j,1) lagmat(:,j)];
    beta = (X'*X)\(X'*data(j+1:numrows));
    acf(j+1,1) = beta(2,1);
end