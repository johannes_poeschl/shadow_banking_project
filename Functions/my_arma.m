function [beta,err_out] = my_arma(data,y,ylags,errors,elags,constant)
%       Vector-autoregression.
%
%   DESCRIPTION
%       the function takes a numobs x numvars matrix data, an integer 
%       number of lags and a constant indicator and estimates an
%       numvars*(numlags+1)x1 vector of coeffictients beta, its covariance 
%       matrix Sigma, the numobs x numvars matrix of error terms and the 
%       numvars x numvars matrix of the error covariances

% data=y(21:end,:);
% ylags=4;
% elags=2;
% constant=0;

%additional variables
[numobs,numvars]=size(data);
numlags=max(ylags,elags);

%preallocate
lagmat_y=zeros(numobs-numlags,numlags*numvars);
lagmat_e=zeros(numobs-numlags,numlags*numvars);

%construct lag-matrices
for j=1:numvars
   lagmat1_y=my_lagmatrix(y(:,j),numlags);
   lagmat_y=[lagmat_y(:,numlags+1:end) lagmat1_y];
end
for j=1:numvars
   lagmat1_e=my_lagmatrix(errors(:,j),numlags);
   lagmat_e=[lagmat_e(:,numlags+1:end) lagmat1_e];
end

%concatenate, add constant
lagmat=[lagmat_y(:,1:ylags) lagmat_e(:,1:elags)];
if constant == 1
    lagmat = [ones(numobs-numlags,1) lagmat];
end

X=lagmat;
Y=data(numlags+1:end,:);
beta=(X'*X)\(X'*Y);
err_out=Y-X*beta;