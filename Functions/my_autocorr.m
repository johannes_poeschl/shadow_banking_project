function [corr,cov]=my_autocorr(Data,lag)
%autocorrelation matrix of VAR data

%Data in rows, variables in columns;
Data1=Data(1:end-lag,:); %contemporary
Data2=Data(1+lag:end,:); %lagged
[rows,cols]=size(Data1);

mean1=1/rows.*sum(Data1,1);
mean2=1/rows.*sum(Data2,1);
stack1=mean1'*mean2;
stack2=zeros(cols,cols,rows);
%sum 
for j=1:rows
    stack2(:,:,j)=Data1(j,:)'*Data2(j,:);
end
stack3=1/rows.*sum(stack2,3);
cov=stack3-stack1;

[~,cov2]=my_corr(Data);
stdev=diag(cov2).^.5;
stack4 = stdev*stdev';

corr=cov./stack4;