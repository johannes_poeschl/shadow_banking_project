function y_bp = my_bandpass_2(pL,pU,K,y)
%bandpass filter

omegaU = 2*pi/pU;
omegaL = 2*pi/pL;

if pU >= K;
    omegaU = 0;
end

alpha_bp = zeros(K+1,1);
alpha_bp(1)=(omegaL-omegaU)/pi;
for j=2:K+1
    alpha_bp(j)=(sin(omegaL*(j-1))-sin(omegaU*(j-1)))/(pi*(j-1));
end

theta_bp = (-alpha_bp(1)-2*sum(alpha_bp(2:end)))/(2*K+1);
beta_bp = alpha_bp + ones(K+1,1)*theta_bp;
beta_bp = [flipdim(beta_bp(2:end)',2) beta_bp'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%build a forward & backward-looking kernel:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

kernel=zeros(length(y),2*K+1);

for i=K+1:length(y)-K
   kernel(i,:)=y(-K+i:K+i)';
end

y_bp = kernel*beta_bp';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Loop to get the boundary values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for k=1:K-1
K_b=K-k;

alpha_bp = zeros(K_b+1,1);
alpha_bp(1)=(omegaL-omegaU)/pi;
for j=2:K_b+1
    alpha_bp(j)=(sin(omegaL*(j-1))-sin(omegaU*(j-1)))/(pi*(j-1));
end

theta_bp = (-alpha_bp(1)-2*sum(alpha_bp(2:end)))/(2*K_b+1);
beta_bp = alpha_bp + ones(K_b+1,1)*theta_bp;
beta_bp = [flipdim(beta_bp(2:end)',2) beta_bp'];

kernel=zeros(length(y),2*K_b+1);

for i=K_b+1:length(y)-K_b
   kernel(i,:)=y(-K_b+i:K_b+i)';
end

y_tails=kernel*beta_bp';

y_bp(K_b+1)=y_tails(K_b+1);
y_bp(end-K_b)=y_tails(end-K_b);
end