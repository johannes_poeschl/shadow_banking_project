function IRF = my_btstrap(data,pos,numres,numdraws,numlags)
% bootstrap of impulse response bounds

% data=data1;
[numobs,numvars]=size(data);

c=1;

IRF=zeros(numvars,numres,numdraws);
[beta,~,errors]=my_var(data,numlags,c);
[Phi,alpha]=my_getphi(beta,numvars,numlags,c);

    for j=1:numdraws
        %reshuffle errors
        for i=1:numvars
            errors(:,i) = randsample(errors(:,i),length(errors));
        end
        %construct new dataset
        newdata=zeros(size(data));
        newdata(1:numlags,:)=data(1:numlags,:);
        datavec=reshape(newdata(1:numlags,:)',numlags*numvars,1);
        for i=1:numobs-numlags
            newdata(i+numlags,:)=(alpha+cell2mat(Phi')*datavec+errors(i,:)')';
            datavec=[newdata(i+numlags,:)';datavec(1:numvars*(numlags-1),:)];
        end
        %get irf from new dataset (note that this also does a cholesky
        %decomposition - a different identification scheme would have to be
        %added by hand!
        [beta,~,errors1]=my_var(newdata,numlags,c);
        
        % generate the shock and the identification matrix:
        Omega=1/(numobs-numlags)*(errors1'*errors1);
        rest=chol(Omega,'lower');
        shock=zeros(numvars,1);
        shock(pos,1)=sqrt(Omega(pos,pos));
        
        % generate the new irfs:
        Phi1=my_getphi(beta,numvars,numlags,c);
        Psi1=my_getpsi(Phi1,numres,numvars,numlags);
        for i=1:numres
            IRF(:,i,j)=Psi1{i}*(rest\shock);
        end
     end