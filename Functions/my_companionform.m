function Y = my_companionform(data,nlags);

%generate the companion form of a var

%format:
% Y(T)         Y(T-1)     ...  Y(nlags)
% Y(T-1)       Y(T-2)     ...  Y(nlags-1)
% ...          ...        ...  ...
% Y(T-nlags+1) Y(T-nlags) ...  Y(1)

%a regression is performed by taking away the last row for the regressand,
%the first row for the regressor and reshaping into a column vector.

[nobs,nvars]=size(data);

Y=zeros(nvars*nlags,nobs-nlags);

%in this loop, the companion form is created oldest to newest, NEEDS to be
%flipped to be constistent with the description!
for j=1:nobs-nlags+1
   Y1=data(j:j+nlags-1,:);
   Y2=flipud(Y1);
   Y3=Y2';
   Y4=reshape(Y3,nvars*nlags,1);
   Y(:,j)=Y4;
end

Y=fliplr(Y);