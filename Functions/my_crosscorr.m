function [corr,cov] =my_crosscorr(Data)
%contemporary autocorrelation matrix.

%Data in rows, variables in columns;
[rows,cols]=size(Data);
mean=1/rows.*sum(Data,1);
stack1=mean'*mean;
stack2=zeros(cols,cols,rows);
%sum 
for j=1:rows
    stack2(:,:,j)=Data(j,:)'*Data(j,:);
end
stack3=1/rows.*sum(stack2,3);
cov=stack3-stack1;

stdev=diag(cov).^.5;
stack4=stdev*stdev';

corr=cov./stack4;