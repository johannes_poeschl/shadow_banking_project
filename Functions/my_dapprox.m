function derivative = my_dapprox(coeffs,dim_cheby,w,eps)
%approximates the derivative of a function approximated by a tchebyshev
%polynomial (LOTS of approximations involved, be wary of the results)

wtop = w+eps;
wbot = w-eps;

polytop = my_tchebyshev(wtop,dim_cheby);
polybot = my_tchebyshev(wbot,dim_cheby);

ftop = polytop*coeffs;
fbot = polybot*coeffs;

derivative = (ftop-fbot)/(2*eps);