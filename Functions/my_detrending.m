function [cycle_component,trend_component]=my_detrending(data,DateNum,method,varargin)

%fit a quadratic time trend to the series, decompose into a trend and cycle
%component
nobs=length(data);
const=ones(nobs,1);
trend=cumsum(const);

if strcmp(method,'demean')==1
    regs= const;
elseif strcmp(method,'linear')==1
    regs= [const trend];
elseif strcmp(method,'quadratic')==1
    regs= [const trend trend.^2];
elseif strcmp(method,'cubic')==1
    regs= [const trend trend.^2 trend.^3];
else
    display('You did not select what you want me to do.')
end

deps=data;
coeffs=(regs'*regs)\regs'*deps;
trend_component=regs*coeffs;
cycle_component=data-regs*coeffs;

%make a picture
for ii=1:length(varargin)
    if strcmp(varargin{ii},'plots')==1
        plots=varargin{ii+1};
        if strcmp(plots,'on')==1
            figure
            subplot(2,1,1)
            plot(DateNum,data)
            hold on
            plot(DateNum,trend_component,'r')
            legend('Original','Trend')
            title('Detrending')
            datetick('x',17)
            hold off

            subplot(2,1,2)
            plot(DateNum,cycle_component)
            legend('Cycle')
            datetick('x',17)
        elseif strcmp(plots,'off')==1
            display('Plots are deactivated')
        else
            display('invalid input')
        end
    end
end