function der = my_findiff(fun,xval,deg)

% Default value for stepsize
xstep = 1e-4;

if deg == 1
    der = (fun(xval+xstep) - fun(xval))/xstep;
elseif deg == 2
    der = (fun(xval+xstep) + fun(xval-xstep) - 2*fun(xval))/(xstep^2);
else
    error('Degree of Derivative Not Specified')
end