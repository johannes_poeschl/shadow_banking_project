function derivative = my_finitediff(YY,XX)
 %calculates the derivative of YY as a function of XX using finite
 %differences.
 
 [nrows,ncols] = size(XX);
 
 derivative = zeros(nrows,ncols);
 
 %central differences
 derivative(2:end-1,:)=(YY(3:end,:)-YY(1:end-2,:))./(XX(3:end,:)-XX(1:end-2,:));
 
 %forward differences
 derivative(1,:) = (YY(2,:)-YY(1,:))/(XX(2,:)-XX(1,:));
 
 %backward differences
 derivative(end,:) = (YY(end,:)-YY(end-1,:))/(XX(end,:)-XX(end-1,:));