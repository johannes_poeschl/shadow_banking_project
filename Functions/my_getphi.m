function [Phi,const] = my_getphi(beta,numvars,numlags,c)
%       reduced form coefficients for VAR

%Correct for constant
if c == 1
    Phi2=reshape(beta,numvars*numlags+1,numvars);
    Phi_noconst=Phi2(2:end,:);
    const=Phi2(1,:)';
    beta=reshape(Phi_noconst,numvars^2*numlags,1);
else
    const=zeros(numvars,1);
end

%do some reshaping
Phi1=reshape(beta,numlags,numvars^2);
Phi=reshape(Phi1',numvars,numvars*numlags)';
PhiCell=cell(numlags,1);
for j=1:numlags
   iter=(j-1)*numvars;
   PhiCell(j)={Phi(1+iter:numvars+iter,:)};
end
Phi=PhiCell;