function Psi = my_getpsi(Phi,numres,numvars,numlags)
%       Wold representation coefficients for VAR.

Psistart = [eye(numvars) zeros(numvars,(numlags-1)*numvars)];
PsiCell=cell(numres,1);
PsiCell(1)={eye(numvars)};

for j=2:numres
Psi=Psistart*cell2mat(Phi);
Psistart = [Psi Psistart(:,1:(numlags-1)*numvars)];
PsiCell(j)={Psi};
end
Psi=PsiCell;