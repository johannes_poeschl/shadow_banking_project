function [irfu,irfl] = my_irfbounds(data,method,pos,numres,numdraws,numlags)
%       Impulse response bounds with either monte-carlo or bootstrapping

[~,numvars]=size(data);
[beta,Sigma]=my_var(data,numlags,1);

if strcmp(method,'mc') == 1
    IRF = my_montecarlo(beta,Sigma,pos);
elseif strcmp(method,'bt') == 1
    IRF = my_btstrap(data,pos,numres,numdraws,numlags);   
else
    display('no sampling method selected')
end
    
irfu=zeros(numvars,numres);
irfl=irfu;
irfstd=irfu;

for i=1:numvars
    test = squeeze(IRF(i,:,:))';
    irfu(i,:)=prctile(test,95,1);
    irfl(i,:)=prctile(test,5,1);
    irfstd(i,:)=std(test,1,1);
end