function [y_npf,x_npf,P_npf,x_cpf,P_cpf]=my_kalman(F,H,Q,R,y,x_start,P_start)

%define additional variables
numobs=length(y);

%preallocate
x_cpf=cell(numobs,1);
x_npf=x_cpf;
P_cpf=cell(numobs,1);
P_npf=P_cpf;
y_npf=cell(numobs,1);

%create first next-period forecast for the state variable
x_npf(1)={x_start};
P_npf(1)={P_start};

%create first npf for the observable
y_npf(1)={H'*x_npf{1}};

for j=1:numobs-1
%create current-period forecast for the state variable
x_cpf(j)={x_npf{j}+(P_npf{j}*H)/(H'*P_npf{j}*H+R)*(y(:,j)-H'*x_npf{j})};
P_cpf{j}=P_npf{j}-(P_npf{j}*H)/(H'*P_npf{j}*H+R)*(H'*P_npf{j});

%create subsequent next-period forecast based on current-period forecast
x_npf(j+1)={F*x_cpf{j}};
P_npf{j+1}=F*P_cpf{j}*F'+Q;

%create subsequent npf for the observable
y_npf(j+1)={H'*x_npf{j+1}};
end

j=numobs;
x_cpf(j)={x_npf{j}+(P_npf{j}*H)/(H'*P_npf{j}*H+R)*(y(:,j)-H'*x_npf{j})};