function [lagmatrix] = my_lagmatrix(data,numlags)
%       Creates a lagmatrix.

lagmatrix=zeros(length(data)-numlags,numlags);

for ii=1:numlags
   lagmatrix(:,ii)=data(numlags+1-ii:end-ii);
end