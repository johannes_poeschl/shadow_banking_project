function [AIC,BIC,lrtest]=my_lagselect(data1,maxlags)
%Gives a number of criteria to select the optimal lag-length in a VAR.

[numobs,numvars]=size(data1);

lklhd=zeros(maxlags,1);
AIC=lklhd;
BIC=lklhd;
for j=1:maxlags
   T=numobs-j;
   numparams=numvars*(numvars*j+1);
   [~,~,errors,lklhd(j)]=my_var(data1,j,1);
   %Omega=1/(numobs-j)*(errors'*errors);
   %AIC(j)=log(det(Omega))+2*numparams/T;
   %BIC(j)=log(det(Omega))+log(T)*numparams/T;
   AIC(j)=2*numparams-2*lklhd(j);
   BIC(j)=log(T)*numparams-2*lklhd(j);
end

test=2*(lklhd(2:end)-lklhd(1:end-1));
crit=ones(maxlags-1,1)*chi2inv(.95,3^2);
reject=test>crit;
lrtest=[test crit reject];
clear test crit reject