function [lagsAIC,lagsBIC]=my_lagselect2(data,maxlags)

[nobs,nvars]=size(data);

AIC=zeros(maxlags,1);
BIC=AIC;

for ii=1:maxlags
[~,~,Omega]=my_var2(data,ii);
k=nvars^2*ii;
T=nobs-ii;
AIC(ii,1)=log(det(Omega))+2*k/T;
BIC(ii,1)=log(det(Omega))+k*log(T)/T;
end

[minAIC,lagsAIC]=min(AIC);
[minBIC,lagsBIC]=min(BIC);
display('AIC')
display([minAIC lagsAIC])
display('BIC')
display([minBIC lagsBIC])
nlags=lagsBIC;