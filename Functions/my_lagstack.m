function lagmat = my_lagstack(data,numlags,varargin)

%define additional variables
[numobs,numvars]=size(data);
okargs={'IncludeContemp'};

if nargin > 2
    pname=varargin{1};
    pval=varargin{2};

    if strcmp(pname,okargs)==1 && pval==true
        %preallocate
        lagmat=zeros(numobs-numlags,(numlags+1)*numvars);
        %stack
        for j=1:numvars
            lagmat1=my_lagmatrix(data(:,j),numlags);
            lagmat1=[data(numlags+1:end,j),lagmat1];
            lagmat=[lagmat(:,numlags+2:end) lagmat1];   
        end
    end
else   
    %preallocate
    lagmat=zeros(numobs-numlags,numlags*numvars);
    %stack
    for j=1:numvars
        lagmat1=my_lagmatrix(data(:,j),numlags);
        lagmat=[lagmat(:,numlags+1:end) lagmat1];
    end
end


    