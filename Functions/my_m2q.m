function quarterly = my_m2q(monthly)

numobs = length(monthly)/3;

monthly = reshape(monthly,3,numobs);
quarterly = sum(monthly)./3;

quarterly = reshape(quarterly,numobs,1);