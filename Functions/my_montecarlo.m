function IRF = my_montecarlo(beta,Sigma,shock)
%       Monte-Carlo simulation of impulse response error bounds.
%       Subfunction of my_irfbounds.m

global numlags numsims numobs numdraws numvars c

IRF=zeros(numvars,numsims,numdraws);

for j=1:numdraws
    draws=beta+1/(numobs-numlags)^.5.*sqrtm(Sigma)'*randn(size(beta));
    Phi=my_getphi(draws,c);
    Psi=my_getpsi(Phi,numsims);
    for i=1:numsims
        IRF(:,i,j)=Psi{i}*shock;
    end
end