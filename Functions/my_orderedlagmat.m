function ordlagmat = my_orderedlagmat(data,numlags)
%generates "blocks" of lags for vector-autoregressions

%define additional variables
[numobs,numvars]=size(data);

%preallocate
ordlagmat=zeros(numobs-numlags,numvars*numlags);

%construct the lags in a loop
for j=1:numlags
    lagmatj=data(numlags+1-j:end-j,:);
    ordlagmat=[ordlagmat(:,numvars+1:end) lagmatj];
end