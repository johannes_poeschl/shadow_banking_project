function [State,Trans]=my_rouwenhorst(Rho,Sigma,N)
% Rouwenhorst method, as used in http://people.virginia.edu/~ey2d/markov.pdf
% Rho is the persistence of the AR(1)-process
% Sigma is the conditional volatility (!) of the AR(1)-process

SigmaZ = sqrt(Sigma^2/(1-Rho^2));

% Rho = 0.85;
% Sigma = 0.15;
% N = 20;

p = (1+Rho)/2;
q = (1+Rho)/2;

Pi2 = [p 1-p; 1-q q];
PiN = Pi2;

for ii=3:N
TL = zeros(ii);
TR = TL;
BL = TL;
BR = TL;

TL(1:ii-1,1:ii-1) = PiN;
TR(1:ii-1,2:ii) = PiN;
BL(2:ii,1:ii-1) = PiN;
BR(2:ii,2:ii) = PiN;

PiN = p*TL + (1-p)*TR + (1-q)*BL + q*BR;
PiN(2:end-1,:) = 0.5*PiN(2:end-1,:);
end

Psi = sqrt(N-1)*SigmaZ;

State = linspace(-Psi,Psi,N)';
Trans = PiN;