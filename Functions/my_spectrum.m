function spectrum = my_spectrum(data,M)
%   Spectrum of a time-series.
%
%M is the bandwidth of the kernel
%
omega=linspace(0,pi,1000);

cov=xcov(data,'unbiased');
T=length(data);
s=(-T+1:1:T-1)';

nwweight = zeros(length(s),1);

for j=1:length(s)
if abs(s(j)) <= M
    nwweight(j) = 1- abs(s(j))/(M+1);
end
end

expon = exp(-1i.*s*omega);

weighted_covs = nwweight.*cov;
spectrum = real((1/(2*pi).*(weighted_covs)'*expon))';