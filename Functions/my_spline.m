function newfval = my_spline(newgrid,oldgrid,oldfval)
%approximates a function by linear splines.

% newgrid=wnewgrid(500,1);
% oldgrid=grid_wold;
% oldfval=VdV_exp(:,20);

newdim=length(newgrid);
olddim=length(oldgrid);

if newgrid(1,1) < oldgrid(1,1) ||newgrid(end,1) > oldgrid(end,1)
    error('Can''t compute function for values outside the old grid, increase size of the old grid.')
end

newfval=zeros(newdim,1);

if newdim > 1
    for jj = 2:olddim
    w2 = oldgrid(jj,1);
    w1 = oldgrid(jj-1,1);
    vdv2 = oldfval(jj,1);
    vdv1 = oldfval(jj-1,1);

    newgridmat = newgrid(newgrid <= w2 & newgrid >= w1);
    num = length(newgridmat);

    w2mat = repmat(w2,num,1);
    w1mat = repmat(w1,num,1);
    vdv2mat = repmat(vdv2,num,1);
    vdv1mat = repmat(vdv1,num,1);
    One =ones(num,1);

    newfval(newgrid <= w2 & newgrid >= w1) = (One-(newgridmat-w1mat)./(w2mat-w1mat)).*vdv1mat+...
        ((newgridmat-w1mat)./(w2mat-w1mat)).*vdv2mat;
    end
elseif newdim == 1
    g = oldgrid - repmat(newgrid,olddim,1);
    [~,w1pos] = max(g(g<=0));
    w1 = oldgrid(w1pos,1);
    w2 = oldgrid(w1pos+1,1);
    vdv2 = oldfval(w1pos+1,1);
    vdv1 = oldfval(w1pos,1);
    newfval = (1-(newgrid-w1)/(w2-w1))*vdv1+((newgrid-w1)/(w2-w1))*vdv2;
end