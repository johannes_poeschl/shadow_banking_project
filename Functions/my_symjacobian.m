function [J,Jrank] = my_symjacobian(B)   
%       calculation the jacobian for a sufficient local identification 
%       condition
%
%   Requirements:
%       - Symbolic Toolbox
%       - my_vecdecomp

[rows, ~]=size(B);

Binv=B^(-1);

Omega2=sym('o',[rows rows]);
Omega2=sym(Omega2,'real');
Omega1=tril(Omega2);
Omega=Omega1+Omega1'-diag(diag(Omega1));

D_n1=dupmat(rows);
D_n=(D_n1'*D_n1)\D_n1';

Sigma=sym('sigma',[rows 1]);
Sigma=sym(Sigma,'real');
Sigma=diag(Sigma);

[~,S_B,~]=my_vecdecomp(B);
[~,S_Z,~]=my_vecdecomp(Sigma);

J=[-2.*D_n*(kron(Omega,Binv))*S_B D_n*(kron(Binv,Binv))*S_Z];

Jrank=rank(J);