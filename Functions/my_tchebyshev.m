function T = my_tchebyshev(input_vec,dim_cheby)
%generates a matrix of dim chebychev polynomials on the input domain

dim_input=length(input_vec);

T=ones(dim_input,dim_cheby);
T(:,2)=input_vec;

for ii=1:dim_cheby-2
    T(:,ii+2)=2*input_vec.*T(:,ii+1)-T(:,ii);
end