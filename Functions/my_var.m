function [beta,Sigma,errors,lklhd] = my_var(data,numlags,constant)
%       Vector-autoregression.
%
%   DESCRIPTION
%       the function takes a numobs x numvars matrix data, an integer 
%       number of lags and a constant indicator and estimates an
%       numvars*(numlags+1)x1 vector of coeffictients beta, its covariance 
%       matrix Sigma, the numobs x numvars matrix of error terms and the 
%       numvars x numvars matrix of the error covariances    

[numobs,numvars]=size(data);

lagmat=zeros(numobs-numlags,numlags*numvars);

for j=1:numvars
   lagmat1=my_lagmatrix(data(:,j),numlags);
   lagmat=[lagmat(:,numlags+1:end) lagmat1];
end

if constant == 1
    lagmat = [ones(numobs-numlags,1) lagmat];
end

X=kron(eye(numvars),lagmat);
Y=reshape(data(numlags+1:end,:),(numobs-numlags)*numvars,1);
beta=(X'*X)\(X'*Y);
errors1=Y-X*beta;
errors=reshape(errors1,numobs-numlags,numvars);

T=(numobs-numlags);
Omega=1/T.*errors'*errors;
Sigma=kron(Omega,T*(lagmat'*lagmat)^(-1));
lklhd= -T*numvars/2*log(2*pi)+T/2*log(det(inv(Omega)))-T*numvars/2;