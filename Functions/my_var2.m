function [coeffs,errors,Omega]=my_var2(data,nlags)

[~,nvars]=size(data);

%bring the data in companion form
data_com=my_companionform(data,nlags);
regs=data_com(:,2:end);
deps=data_com(:,1:end-1);
coeffs=(regs*regs')\(regs*deps');
errors=deps-coeffs'*regs;
error_variance=errors*errors';
Omega=error_variance(1:nvars,1:nvars);