function varshare = my_vardecomp(Psi,LTR,Sigma,select)
%Creates a variance decomposition

%varshare takes as input a numsims x 1 cell array Psi, a lower triangular
%matrix LTR, a structural error covariance matrix sigma and an indicator of
%the position of the shock of interest in the var and computes as output
%the numsims x numvars forecast errors

% LTR=P;
% Sigma=eye(3);
% select=3;

global numsims numvars
%Preallocating
Psistar=cell(numsims,1);
varshare=zeros(numvars,numsims);
num=varshare;
den=varshare;

factor1=zeros(numvars,1);
factor1(select,1)=Sigma(select,select);
factor2=diag(Sigma);

for j=1:numsims
    Psistar(j)={Psi{j}*LTR};
    num(:,j)=Psistar{j}.^2*factor1;
    den(:,j)=Psistar{j}.^2*factor2;
    varshare(:,j)=sum(num(:,1:j),2)./sum(den(:,1:j),2);
end