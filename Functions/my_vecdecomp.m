function [theta_R,S_R,s_r]=my_vecdecomp(R)
%creates the vectors necessary for the computation of the jacobian in my_symjacobian

index_R=find(R~=0 & R~= 1);
index_R2=find(R==1);
theta_R=R(index_R);
S_R=zeros(length(R)^2,length(index_R));
for j=1:length(index_R)
   S_R(index_R(j),j)=1; 
end
s_r=zeros(length(R)^2,1);
for j=1:length(index_R2)
   s_r(index_R2(j),j)=1;
end