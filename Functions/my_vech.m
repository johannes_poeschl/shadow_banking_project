function output=my_vech(input)

[rows,cols]=size(input);

if rows ~= cols
    display('Matrix must be symmetric')
end

vec=reshape(input,rows*cols,1);
dup=dupmat(rows);
output=dup\vec;