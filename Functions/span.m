function value = span(inputvector)

maximum=max(inputvector);
minimum=min(inputvector);
value=maximum-minimum;