% add the function folder to the search path

currentFolder = pwd;
display(currentFolder)

if strcmp(currentFolder(1:4),'/pfs') == 1
    addpath(genpath('/pfs/data2/home/ma/ma_ma/ma_jpoeschl/Funktionen'))
    display('Added the function folder to the search path')
else 
    display('Not on grid, nothing to do')
end