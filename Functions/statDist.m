function stationaryDistribution = statDist(transitionMatrix)
% This function computes the stationary distribution of a Markov process.

[nRows,~] = size(transitionMatrix);

% find the stationary distribution of the discretized process
oldDistribution = createUnitVector(1,nRows);
changeInDistribution = 10;

while changeInDistribution > 1e-5
    newDistribution = transitionMatrix'*oldDistribution;
    changeInDistribution = norm(newDistribution-oldDistribution);
    oldDistribution = newDistribution;
end

stationaryDistribution = oldDistribution;