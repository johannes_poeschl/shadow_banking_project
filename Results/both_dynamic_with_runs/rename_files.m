for xx = 1:118
   
    try
    
       filename_old = strcat('Results_GammaR_',num2str(xx),'.mat');

       load(filename_old)

       filename_new = strcat('Results_', ...
           'GamR_',num2str(1/p.phi_R_bar), ...
           '_GamS_',num2str(1/p.phi_S_bar), ...
           '_nuR_',num2str(p.nu_R), ...
           '_nuS_',num2str(p.nu_S), ...
           '.mat');

       save(filename_new)

       delete(filename_old)
       
    catch
        
        disp('File not found')
   
    end
    
end