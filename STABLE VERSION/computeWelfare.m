function [ex_ante_util_H,ex_ante_util_R,ex_ante_util_S] = computeWelfare(TS_add,p)

[nPanels,nPeriods] = size(TS_add.C_H);

tauVals = 0:nPeriods-1;
        
disc_H = p.beta.^tauVals;
util_H = sum(repmat(disc_H,nPanels,1).*log(TS_add.C_H),2);

disc_R = (p.beta*(1-p.sigma_R)).^tauVals;
util_R = sum(repmat(disc_R,nPanels,1).*TS_add.C_R,2);

disc_S = (p.beta*(1-p.sigma_S)).^tauVals;
util_S = sum(repmat(disc_S,nPanels,1).*TS_add.C_S,2);

ex_ante_util_H = mean(vec(exp((1-p.beta)*util_H)));
ex_ante_util_R = mean(vec(util_R));
ex_ante_util_S = mean(vec(util_S));