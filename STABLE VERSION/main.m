% solves the unemployment model

clear
close

addpath('./Funktionen')
addpath('./Funktionen/Smolyak_Anisotropic_JMMV_2014')
addpath('./Funktionen/CompEcon Toolbox/CEtools')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt = struct;

opt.solveModel = 1;
opt.simulModel = 1;
opt.newGuess   = 0;

opt.reg = {'static'};
opt.run = {'with_runs'};
opt.modelVersion = 'baseline';

Gamma_R_Vals = 0.08;

for xx = 1:length(opt.reg)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    p = setParams(opt.reg{xx},opt.run{xx},opt.modelVersion);

    if opt.solveModel == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Model Solution
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if opt.newGuess == 1
            delete('initialGuess.mat')
        end

        if exist(strcat('./Results/',opt.reg{xx},'_',opt.run{xx}),'dir') == 0
            mkdir(strcat('./Results/',opt.reg{xx},'_',opt.run{xx}))
        end

        [pols,pols_run,funs,funs_run,grd,steady,states_no_run,states_run] = solveModel(p,opt.reg{xx},opt.run{xx});

        save(strcat('./Results/',opt.reg{xx},'_',opt.run{xx},'/Results_GammaR_',num2str(Gamma_R_Vals(xx)),'.mat'), ...
            'pols','funs','pols_run','funs_run','p','steady')
    end

    if opt.simulModel == 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Model Simulation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        load(strcat('./Results/',opt.reg{xx},'_',opt.run{xx},'/Results_GammaR_',num2str(Gamma_R_Vals(xx)),'.mat'))

        Tables = function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt.run{xx});

        save(strcat('./Results/',opt.reg{xx},'_',opt.run{xx},'/Moments_GammaR_',num2str(Gamma_R_Vals(xx)),'.mat'), ...
            'Tables')
    end

end

delete('shocks.mat')
