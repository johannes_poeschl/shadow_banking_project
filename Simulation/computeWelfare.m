function avg_cons_eq_H = computeWelfare(C_H,p)

[nPanels,nPeriods] = size(C_H);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the discount factor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tauVals = 0:nPeriods-1;
        
disc_H = p.beta.^tauVals;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the utility from realized consumption
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if p.sigma == 1
    
    util_H = log(C_H);
    
else
    
    util_H = (C_H.^(1-p.sigma) - 1)./(1-p.sigma);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the ex ante utility from the realized consumption path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ex_ante_util_H = sum(repmat(disc_H,nPanels,1).*util_H,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the consumption equivalent from that consumption path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if p.sigma == 1
    
    flow_utility = (1-p.beta)/(1-p.beta^(nPeriods+1))*ex_ante_util_H;

    cons_eq_H = exp(flow_utility);
    
else
    
    flow_utility = (1-p.beta)/(1-p.beta^(nPeriods+1))*ex_ante_util_H;
    
    cons_eq_H = ((1-p.sigma)*flow_utility + 1).^(1/(1-p.sigma));
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute the consumption equivalent from that consumption path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

avg_cons_eq_H = mean(cons_eq_H);
