function [Tables,TS] = ...
    function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt)
% simulates the model and calculates statistics

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% set simulation parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nBurnin = opt.nBurnin; 
nPeriods = nBurnin + opt.nPeriods;  % number of simulated periods 
nEconomies = opt.nEconomies; %%%%% number of simulations for each capital requirement level

rng(opt.seed);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% draw shocks and initial conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

seq_logZ = p.mu_Z*ones(nEconomies,nPeriods);
seq_epsZ    = randn(nEconomies,nPeriods);
seq_epsXi   = randn(nEconomies,nPeriods);

for tt = 2:nPeriods  %%%%% for each simulation    
    seq_logZ(:,tt) = (1-p.rho_Z)*p.mu_Z + p.rho_Z*seq_logZ(:,tt-1) + p.sigma_Z*seq_epsZ(:,tt);
end

if opt.load_shocks == 0
    
    shocks          = struct;
    shocks.eps_Z    = seq_epsZ;
    shocks.eps_Xi   = seq_epsXi;
    shocks.logZ     = seq_logZ;
    shocks.xi       = exp(p.mu_xi + p.sigma_xi*seq_epsXi); %%%%% generage a sequence of random numbers ~N(0,1) 
    shocks.sunspot  = rand(nEconomies,nPeriods);
    shocks.pi       = rand(nEconomies,nPeriods);
    shocks.burnin   = nBurnin;
    
    init            = struct;
    init.N_R        = ones(nEconomies,1)*steady.N_R;
    init.N_S        = ones(nEconomies,1)*steady.N_S;
    init.K          = ones(nEconomies,1)*steady.K;
    init.run_persists = zeros(nEconomies,1);
    
end

TS = simulateModel_new(p,shocks,init,funs,funs_run,pols,pols_run,steady);

Tables = struct;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLES 1: Simulation Boundaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check the simulation boundaries

RowNames = {'N_R','N_S','K'};

Min_Sim = [min(vec(TS.N_R));  min(vec(TS.N_S)); min(vec(TS.K))];
Lower_Bound_SS = funs.orig_min(1:3)';
freq_below_bound = 100*[mean(vec(TS.N_R) <= funs.orig_min(1)); ...
        mean(vec(TS.N_S) <= funs.orig_min(2)); ...
        mean(vec(TS.K) <= funs.orig_min(3))];

Max_Sim = [max(vec(TS.N_R));  max(vec(TS.N_S)); max(vec(TS.K))];
Upper_Bound_SS = funs.orig_max(1:3)';
freq_above_bound = 100*[mean(vec(TS.N_R) >= funs.orig_max(1)); ...
        mean(vec(TS.N_S) >= funs.orig_max(2)); ...
        mean(vec(TS.K) >= funs.orig_max(3))];

Tables.SS_Bounds  = table(Min_Sim,Lower_Bound_SS,freq_below_bound, ...
    Max_Sim,Upper_Bound_SS,freq_above_bound,'RowNames',RowNames);
display(Tables.SS_Bounds)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TABLES 2: Welfare
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nT = 100;

ex_ante_util_H = zeros(nT,1);

for ii = 1:nT
    ex_ante_util_H(ii) = computeWelfare(TS.C_H(:,ii:end),p);
end

RowNames = {'HH'};

Tables.Welfare_Table = table(mean(ex_ante_util_H), ...
    'RowNames',RowNames,'VariableNames',{'Welfare'});

display(Tables.Welfare_Table)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extract variable names from the struct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
varNames    = fieldnames(TS);
nVars       = length(varNames);

varVals = cell(nVars,1);

for ii = 1:nVars
    
    currentField = strcat('TS.',varNames{ii});
   
    eval(strcat('varVals{ii} = ',currentField,';'))
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Steady State
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SteadyVal = [ ...
    steady.Y;
    steady.C_H;
    steady.I;
    steady.K_H;
    steady.K_R;
    steady.K_S;
    steady.N_R;
    steady.N_S;
    steady.K;
    steady.logZ;
    steady.xi;
    steady.D;
    steady.B;
    steady.phi_R;
    steady.phi_S;
    steady.Q;
    steady.Q_star;
    steady.R_D;
    steady.R_B;
    steady.R_K;
    0;
    0;
    steady.recov;
    0;
    0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MeanVal = zeros(nVars,1);

for ii = 1:nVars
    MeanVal(ii) = mean(vec(varVals{ii}),'omitnan');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Percentage deviations from stochastic steady state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for ii = 1:nVars
    varVals{ii} = sign(MeanVal(ii))*(varVals{ii} - MeanVal(ii))/MeanVal(ii);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Volatilites
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

StDev = zeros(nVars,1);

for ii = 1:nVars
    StDev(ii) = std(vec(varVals{ii}),'omitnan');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlations with GDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CorrWithGDP = zeros(nVars,1);

for ii = 1:nVars
    CorrWithGDP(ii) = corr(vec(varVals{1}),vec(varVals{ii}),'rows','complete');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auto-Correlations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

AutoCorr = zeros(nVars,1);

for ii = 1:nVars
    x1 = vec(varVals{ii}(:,2:end)');
    x2 = vec(varVals{ii}(:,1:end-1)');

    AutoCorr(ii) = corr(x1,x2,'rows','complete');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event study
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tStart = 20;
tStop  = 20;

% locate all runs
[runN,runT] = find(TS.run_starts == 1);

if isempty(runN) == 0

    runN = runN(runT > tStart & runT <= nPeriods-nBurnin-tStop);
    runT = runT(runT > tStart & runT <= nPeriods-nBurnin-tStop);
    
    nRuns = size(runN,1);
    
    % drop all economies which have been in a run state prior to the run
    no_prior_run = zeros(nRuns,1);
    
    for tt = 1:nRuns
        no_prior_run(tt) = TS.run_state(runN(tt),runT(tt)-1) == 0 & ...
            TS.run_state(runN(tt),runT(tt)-2) == 0 & ...
            TS.run_state(runN(tt),runT(tt)-3) == 0 & ...
            TS.run_state(runN(tt),runT(tt)-4) == 0;
    end

    runN = runN(no_prior_run == 1);
    runT = runT(no_prior_run == 1);
    nRuns = size(runN,1);
    
    % put all run time events in a sequence
    allRuns = zeros(nRuns,tStart+tStop+1);
    avgRun = zeros(length(RowNames),tStart+tStop+1);

    for ii = 1:nVars
        for tt = 1:nRuns

            allRuns(tt,:,ii) = varVals{ii}(runN(tt),runT(tt)-tStart:runT(tt)+tStop);

        end

    avgRun(ii,:) = 100*mean(allRuns(:,:,ii),1,'omitnan');

    end

    
else
    
    avgRun = nan(length(varNames),tStart+tStop+1);
    
end

Tables.eventStudy = table(avgRun,'RowNames',varNames);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event study counterfactual
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if opt.no_run_counterfactual == 1 && isempty(runN) == 0
    
    shocks1          = struct;
    shocks1.burnin   = 0;
    init1            = struct;
    
    shocks1.eps_Z    = zeros(nRuns,tStop+tStart+1);
    shocks1.eps_Xi   = zeros(nRuns,tStop+tStart+1);
    shocks1.logZ     = zeros(nRuns,tStop+tStart+1);
    shocks1.xi       = zeros(nRuns,tStop+tStart+1);
    shocks1.sunspot  = ones(nRuns,tStop+tStart+1);
    shocks1.pi       = zeros(nRuns,tStop+tStart+1);
    
    init1.N_R        = zeros(nRuns,1);
    init1.N_S        = zeros(nRuns,1);
    init1.K          = zeros(nRuns,1);
    
    runT1            = runT + nBurnin;
    
    for tt = 1:nRuns
    
        shocks1.eps_Z(tt,:)      = shocks.eps_Z(runN(tt),runT1(tt)-tStart:runT1(tt)+tStop);
        shocks1.eps_Xi(tt,:)     = shocks.eps_Xi(runN(tt),runT1(tt)-tStart:runT1(tt)+tStop);
        shocks1.logZ(tt,:)       = shocks.logZ(runN(tt),runT1(tt)-tStart:runT1(tt)+tStop);
        shocks1.xi(tt,:)         = shocks.xi(runN(tt),runT1(tt)-tStart:runT1(tt)+tStop); 
        shocks1.pi(tt,:)         = shocks.pi(runN(tt),runT1(tt)-tStart:runT1(tt)+tStop); 
       
        init1.N_R(tt,1)        = TS.N_R(runN(tt),runT(tt)-tStart);
        init1.N_S(tt,1)        = TS.N_S(runN(tt),runT(tt)-tStart);
        init1.K(tt,1)          = TS.K(runN(tt),runT(tt)-tStart);
        init1.run_persists(tt,1) = TS.run_persists(runN(tt),runT(tt)-tStart);
    end
     
    TS1 = simulateModel_new(p,shocks1,init1,funs,funs_run,pols,pols_run,steady);
    
    avgRun_counterfactual = zeros(nVars,tStop+tStart+1);  
    varVals1 = cell(nVars,1);
    
    for ii = 1:nVars
        
        currentField = strcat('TS1.',varNames{ii});
   
        eval(strcat('varVals1{ii} = ',currentField,';'))
        
        varVals1{ii} = sign(MeanVal(ii))*(varVals1{ii} - MeanVal(ii))/MeanVal(ii);
        
        avgRun_counterfactual(ii,:) = mean(varVals1{ii},1,'omitnan');
        
    end
      
else
    
    avgRun_counterfactual = nan(length(varNames),tStart+tStop+1);
    
end

Tables.eventStudyCounterfactual = table(avgRun_counterfactual,'RowNames',varNames);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Policy Counterfactuals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if opt.event_study_policies == 1 && isempty(runN) == 0
    
    for tt = 1:nRuns
    
        shocks1.sunspot(tt,:)         = shocks.sunspot(runN(tt),runT1(tt)-tStart:runT1(tt)+tStop); 

    end
    
%     load(strcat('./Results/retail_with_runs/Results_GamR_0.1_GamS_0_nuR_0_nuS_0.mat'),'funs','funs_run','pols','pols_run')
     load(strcat('./Results/shadow_with_runs/Results_GamR_0_GamS_0.066667_nuR_0_nuS_0.mat'),'funs','funs_run','pols','pols_run')
    
    TS_pol1 = simulateModel_new(p,shocks1,init1,funs,funs_run,pols,pols_run,steady);
    
    avgRun_counterfactual_pol1 = zeros(nVars,tStop+tStart+1);  
    varVals_pol = cell(nVars,1);
    
    for ii = 1:nVars
        
        currentField = strcat('TS_pol1.',varNames{ii});
   
        eval(strcat('varVals_pol{ii} = ',currentField,';'))
        
        varVals_pol{ii} = sign(MeanVal(ii))*(varVals_pol{ii} - MeanVal(ii))/MeanVal(ii);
        
        avgRun_counterfactual_pol1(ii,:) = 100*mean(varVals_pol{ii},1,'omitnan');
        
    end
    
%     load(strcat('./Results/retail_dynamic_with_runs/Results_GamR_0.1_GamS_0_nuR_-1_nuS_0.mat'),'funs','funs_run','pols','pols_run')
    load(strcat('./Results/shadow_dynamic_with_runs/Results_GamR_0_GamS_0.066667_nuR_0_nuS_-1.mat'),'funs','funs_run','pols','pols_run')
    
    TS_pol1 = simulateModel_new(p,shocks1,init1,funs,funs_run,pols,pols_run,steady);
    
    avgRun_counterfactual_pol2 = zeros(nVars,tStop+tStart+1);  
    varVals_pol = cell(nVars,1);
    
    for ii = 1:nVars
        
        currentField = strcat('TS_pol1.',varNames{ii});
   
        eval(strcat('varVals_pol{ii} = ',currentField,';'))
        
        varVals_pol{ii} = sign(MeanVal(ii))*(varVals_pol{ii} - MeanVal(ii))/MeanVal(ii);
        
        avgRun_counterfactual_pol2(ii,:) = 100*mean(varVals_pol{ii},1,'omitnan');
        
    end
      
else
    
    avgRun_counterfactual_pol1 = nan(length(varNames),tStart+tStop+1);  
    avgRun_counterfactual_pol2 = nan(length(varNames),tStart+tStop+1);
    
end

Tables.ev1 = table(avgRun_counterfactual_pol1,'RowNames',varNames);
Tables.ev2 = table(avgRun_counterfactual_pol2,'RowNames',varNames);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Tables.Moments = table(SteadyVal,MeanVal,StDev,CorrWithGDP,AutoCorr,'RowNames',varNames);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Print in LaTeX format
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

input = struct;

input.dataFormat = {'%.3f'};
input.booktabs = 1;
input.tablePlacement= 'ht';
input.tableColumnAlignment = 'c';

input.data = table2array(Tables.Moments);
input.tableRowLabels = Tables.Moments.Properties.RowNames;
input.tableColLabels = Tables.Moments.Properties.VariableNames;
input.tableCaption = 'Model Statistics (Raw)';

save(strcat('./Results/Moments.mat'),'Tables')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Table for the paper
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% stdevs, variable change in run, bank run frequency, all in one column

varNames = {
    'Mean, Output ($Y$)', ...
    'Mean, Consumption ($C^H$)', ...
    'Mean, Investment ($\tilde{I}$)', ...
    'Mean, Retail Bank Leverage ($\phi^R$)', ...
    'Mean, Retail Bank Net Worth ($N^R$)', ...
    'Mean, Deposits ($D''$)', ...
    'Mean, Shadow Bank Leverage ($\phi^S$)', ...
    'Mean, Shadow Bank Net Worth ($N^S$)', ...
    'Mean, Interbank Loans ($B''$)', ...
    'Mean, Capital Price ($Q$)', ...
    'Mean, Spread, Wholesale $(R^{B''} - R^{D''})$', ...
    'Mean, Spread, Retail $(E[R^{K''}/Q] - R^{D''})$', ...
    'St. Dev., Output ($Y$)', ...
    'St. Dev., Consumption ($C^H$)', ...
    'St. Dev., Investment ($\tilde{I}$)', ...
    'St. Dev., Retail Bank Leverage ($\phi^R$)', ...
    'St. Dev., Retail Bank Net Worth ($N^R$)', ...
    'St. Dev., Deposits ($D''$)', ...
    'St. Dev., Shadow Bank Leverage ($\phi^S$)', ...
    'St. Dev., Shadow Bank Net Worth ($N^S$)', ...
    'St. Dev., Interbank Loans ($B''$)', ...
    'St. Dev., Capital Price ($Q$)', ...
    'St. Dev., Spread, Wholesale $(R^{B''} - R^{D''})$', ...
    'St. Dev., Spread, Retail $(E[R^{K''}/Q] - R^{D''})$', ...
    'Runs per 100 Years', ...
    'Welfare'}';

varVals = [MeanVal([1 2 3 14 7 12 15 8 13 16]); MeanVal([19 20]).^4 - MeanVal(18).^4; ...
    100*StDev([1 2 3 14 7 12 15 8 13 16 19 20]); ...
    400*MeanVal(21); mean(ex_ante_util_H)];

Tables.section_welfare = table(varVals,'rowNames',varNames);

disp(Tables.section_welfare)

%% SchuTa-result

% schularick & taylor experiment

varNames = {'$\log(Y)$','$\log(I)$','$\log((Q+f^R)K^R + QK^S)$'}';

dataVals = [-2.02 -4.46 -6.3;
    -3.45 -12.45 -19.9;
    -1.89 -6.98 -7.7];

modelVals = [(avgRun([1 3 11],tStart+4) - avgRun([1 3 11],tStart)) ...
    (avgRun([1 3 11],tStart+8) - avgRun([1 3 11],tStart)) ...
    (avgRun([1 3 11],tStart+12) - avgRun([1 3 11],tStart))];

Tables.section_event_study = table(dataVals,modelVals,'rowNames',varNames);

disp(Tables.section_event_study)

input = struct;

input.dataFormat = {'%.3f \\%%'};
input.booktabs = 1;
input.tablePlacement= 'htbp';

input.data = [dataVals modelVals];
input.tableRowLabels = Tables.section_event_study.Properties.RowNames;
input.tableColLabels = {'Data','Data','Data','Model','Model','Model'};
input.tableCaption = 'Financial Crisis Moments';
input.tableLabel = 'bla';
output = latexTable(input);

%% Business Cycle Statistics
data1 = 100*0.027*[1; 0.929; 4.368; 2.449; 10.379];
data2 = [1; 0.916; 0.943; 0.759; 0.187];
data3 = [0.898; 0.887; 0.886; 0.891; 0.853];

varNames = {'Output ($Y$)','Consumption ($C$)','Investment ($\tilde{I}$)','Deposits ($D''$)','Wholesale Lending ($B''$)'};

model1 = StDev(1:5);
model2 = CorrWithGDP(1:5);
model3 = AutoCorr(1:5);

Tables.section_business_cycles = table(data1,100*model1,data2,model2,data3,model3,'rowNames',varNames);

input = struct;

input.dataFormat = {'%.3f'};
input.booktabs = 1;
input.tablePlacement= 'htbp';

input.data = table2array(Tables.section_business_cycles);
input.tableRowLabels = Tables.section_business_cycles.Properties.RowNames;
input.tableColLabels = {'Data','Model','Data','Model','Data','Model'};
input.tableCaption = 'Business Cycle Moments';
input.tableLabel = 'bla';
output = latexTable(input);
