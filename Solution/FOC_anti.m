function [foc_residual,add,expc] = FOC_anti(p, x0, states, ...
    nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob,x_prime,p_prime)
% HH_RB_SB_compute_residual computes the FOC residuals

N_R = states(1,1);
N_S = states(1,2);
K   = states(1,3);

K_H_prime = x0(1);
C_H       = x0(4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations and additional variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

add = computeAdditionalVariables(p, x0, states, ...
    nextStates);

expc = computeExpectationsFOC(p, x0, states, ...
    nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob,x_prime,p_prime,add);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

foc_residual = zeros(4,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(1) = C_H^(-p.sigma)*(add.Q + p.eta_H*K_H_prime/K) - ...
    p.beta * expc.EV_K_H;

foc_residual(2) = C_H - (add.Y - add.I - add.adj_cost);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(3) = p.psi*add.phi_R - expc.Omega_R;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(4) = p.psi*(add.phi_S*p.omega + (1-p.omega)) - expc.Omega_S;