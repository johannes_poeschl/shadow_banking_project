function [foc_residual,add,expc] = FOC_anti_shadow_zero_KH(p, x0, states, ...
    nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob,x_prime,p_prime)
% FOC_anti_shadow computes the FOC residuals

N_R = states(1,1);
K   = states(1,3);

K_H_prime = x0(1);
C_H       = x0(4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations and additional variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

add = computeAdditionalVariables(p, x0, states, ...
    nextStates);

expc = computeExpectationsFOC(p, x0, states, ...
    nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob,x_prime,p_prime,add);

foc_residual = zeros(4,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(1) = K_H_prime;

foc_residual(2) = C_H - (add.Y - add.I - add.adj_cost);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(3) = p.psi*add.phi_R - expc.Omega_R;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow Bank FOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
foc_residual(4) = add.phi_S - add.phi_S_bar;