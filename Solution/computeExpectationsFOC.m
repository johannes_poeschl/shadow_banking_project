function expc = computeExpectationsFOC(p, x0, states, ...
    nextVals,nextVals_Run,nextStates,nextStates_Run,Z_prob,x_prime,p_prime,add)

N_R = states(1,1);
N_S = states(1,2);
K   = states(1,3);

K_H_prime = x0(1);
K_R_prime = x0(2);
K_S_prime = x0(3);
C_H       = x0(4);

N_R_prime = nextStates(:,1)';
K_prime = K_H_prime + K_R_prime + K_S_prime;

N_R_prime_star = nextStates_Run(:,1)';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C_H_prime       = nextVals{1};
V_R_prime       = nextVals{2};
V_S_prime       = nextVals{3};
Q_prime         = nextVals{4};

C_H_prime_star  = nextVals_Run{1};
V_R_prime_star  = nextVals_Run{2};
Q_prime_star    = nextVals_Run{3};

R_K_prime = add.r_K_prime + (1-p.delta)*Q_prime;
R_K_prime_star = add.r_K_prime + (1-p.delta)*Q_prime_star;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Household Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Lambda          = (C_H_prime/C_H).^(-p.sigma);
Lambda_star     = (C_H_prime_star/C_H).^(-p.sigma);

EV_K_H = sum(Z_prob.* ...
    ( ...
        (1-p_prime).*C_H_prime.^(-p.sigma).*R_K_prime + ...
        p_prime.*C_H_prime_star.^(-p.sigma).*R_K_prime_star) ...
    );

EC_H = sum(Z_prob.* ...
    ( ...
        (1-p_prime).*C_H_prime.^(-p.sigma) + ...
        p_prime.*C_H_prime_star.^(-p.sigma)) ...
    );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retail Bank Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EV_R = sum(Z_prob.* ...
    ( ...
        p_prime.*Lambda_star.*V_R_prime_star + ...
        (1-p_prime).*Lambda.*V_R_prime) ...
    );

n_R_prime = (N_R_prime - p.exoInc*K_prime)/(1-p.sigma_R);
n_R_prime_star = (N_R_prime_star - p.exoInc*K_prime)/(1-p.sigma_R);

Omega_R_prime = V_R_prime./n_R_prime;
Omega_R_prime(n_R_prime <= 0) = 1;
Omega_R_prime_star = V_R_prime_star./n_R_prime_star;
Omega_R_prime_star(n_R_prime_star <= 0) = 1;

E_Omega_R = sum(Z_prob.* ...
    ( ...
        (1-p_prime).*Lambda.*Omega_R_prime + ...
        p_prime.*Lambda_star.*Omega_R_prime_star) ...
    );

E_x_prime = sum(Z_prob.* ...
    ( ...
        p_prime.*Lambda_star.*Omega_R_prime_star.*x_prime + ...
        (1-p_prime).*Lambda.*Omega_R_prime) ...
    );

E_R_K_R_prime = sum(Z_prob.* ...
    ( ...
        (1-p_prime).*Lambda.*Omega_R_prime.*R_K_prime + ...
        p_prime.*Lambda_star.*Omega_R_prime_star.*R_K_prime_star) ...
    ) ...
    /(add.Q + p.eta_R*K_R_prime/K);

E_R_K_prime = sum(Z_prob.* ...
    ( ...
        (1-p_prime).*R_K_prime + ...
        p_prime.*R_K_prime_star) ...
    ) ...
    /add.Q;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow Bank Expectations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EV_S = sum(Z_prob.*(1-p_prime).*Lambda.*V_S_prime);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

R_D_prime = C_H^(-p.sigma)/(p.beta * EC_H);
R_B_prime = (E_R_K_R_prime*p.gamma + R_D_prime.*E_Omega_R*(1-p.gamma))./E_x_prime;

Omega_S = max(p.beta*EV_S/N_S,1);
Omega_R = max(p.beta*EV_R/N_R,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Collect
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
expc = struct;

% household
expc.EV_K_H = EV_K_H;

% retail bank
expc.EV_R = EV_R;
expc.Omega_R = Omega_R;

% shadow bank
expc.EV_S = EV_S;
expc.Omega_S = Omega_S;

% prices
expc.R_D_prime = R_D_prime;
expc.R_B_prime = R_B_prime;
expc.E_R_K_R_prime = E_R_K_R_prime;
expc.E_R_K_S_prime = E_R_K_prime;