function [pols,vals] = computeNoRunSolution(p,steady,grd,states_no_run,funs,funs_run, ...
    pols_old,options,reg)

Z_prob_ext      = kron(grd.Xi_prob,grd.Z_prob);

vec_N_R     = states_no_run(:,1);
vec_N_S     = states_no_run(:,2);
vec_K       = states_no_run(:,3);
vec_logZ    = states_no_run(:,4);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preallocate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_phi_R_tilde = zeros(grd.n_total,1);
vec_phi_S_tilde = zeros(grd.n_total,1);
vec_Q_tilde     = zeros(grd.n_total,1);
vec_K_H_prime   = zeros(grd.n_total,1);
vec_K_R_prime   = zeros(grd.n_total,1);
vec_K_S_prime   = zeros(grd.n_total,1);
vec_phi_R       = zeros(grd.n_total,1);
vec_phi_S       = zeros(grd.n_total,1);
vec_phi_R_bar   = zeros(grd.n_total,1);
vec_phi_S_bar   = zeros(grd.n_total,1);
vec_C_H         = zeros(grd.n_total,1);
vec_B_prime     = zeros(grd.n_total,1);
vec_Q           = zeros(grd.n_total,1);
vec_R_D_prime   = zeros(grd.n_total,1);
vec_R_B_prime   = zeros(grd.n_total,1);
vec_E_R_K_R_prime = zeros(grd.n_total,1);
vec_E_R_K_S_prime = zeros(grd.n_total,1);
vec_K_prime     = zeros(grd.n_total,1);
vec_Omega_R     = zeros(grd.n_total,1);
vec_Omega_S     = zeros(grd.n_total,1);
vec_sunspot_prob = zeros(grd.n_total,grd.n_Z*grd.n_Xi);

vec_N_R_prime_n2n   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_N_S_prime_n2n   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_N_R_prime_n2r   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_N_S_prime_n2r   = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_x_prime         = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
vec_p_prime         = zeros(grd.n_total,grd.n_Z*grd.n_Xi);

parfor ii = 1:grd.n_total     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Aggregate LoM for Z', Xi'
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    logZ_prime  = (1-p.rho_Z)*p.mu_Z + p.rho_Z*vec_logZ(ii) + p.sigma_Z*repmat(grd.eps_Z,grd.n_Xi,1);              
    logXi_prime = p.mu_xi + p.sigma_xi*kron(grd.eps_Xi,ones(grd.n_Z,1));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Next Period Value Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    % n2n
    nextStates_n2n = [pols_old.N_R_prime_n2n(ii,:)' pols_old.N_S_prime_n2n(ii,:)' ...
        pols_old.K_prime(ii)*ones(size(logZ_prime)) logZ_prime];

    [ nextVals_n2n ] = computeExpectations( nextStates_n2n,funs,steady );

    % n2r
    nextStates_n2r = [pols_old.N_R_prime_n2r(ii,:)' ...
        pols_old.K_prime(ii)*ones(size(logZ_prime)) logZ_prime];

    [ nextVals_n2r ] = computeExpectations_run( nextStates_n2r,funs_run );

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Solve the System of FOC without binding CR
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    handle = @(x) FOC_anti(p,x, ...
        states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
        nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
        pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

    x0 = [pols_old.K_H_prime(ii,1);
            pols_old.K_R_prime(ii,1);
            pols_old.K_S_prime(ii,1);
            pols_old.C_H(ii,1)];

    [x1,add,expc] = findSolution(handle,x0,options);
    
    if x1(1) < 0
        handle = @(x) FOC_anti_zero_KH(p,x, ...
            states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
            nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
            pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));
        
        [x1,add,expc] = findSolution(handle,x1,options);
    end

    vec_phi_R_tilde(ii,1) = add.phi_R;
    vec_phi_S_tilde(ii,1) = add.phi_S;
    vec_Q_tilde(ii,1)     = add.Q;
    
    retail_violated = vec_phi_R_tilde(ii,1) > add.phi_R_bar;
    shadow_violated = vec_phi_S_tilde(ii,1) > add.phi_S_bar;

    if retail_violated && ~shadow_violated
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % CASE 1: Binding Retail CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        handle = @(x) FOC_anti_retail(p,x, ...
            states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
            nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
            pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

        [x1,add,expc] = findSolution(handle,x1,options);
        
        if x1(1) < 0
            handle = @(x) FOC_anti_retail_zero_KH(p,x, ...
                states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

            [x1,add,expc] = findSolution(handle,x1,options);
        end
        
        if x1(2) <= 0          
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % CASE 1a: Binding Retail CR, K^R = 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            handle = @(x) FOC_anti_retail_zero_KR(p,x, ...
                states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

            [x1,add,expc] = findSolution(handle,x1,options);
            
            if x1(1) < 0
                handle = @(x) FOC_anti_retail_zero_KR_zero_KH(p,x, ...
                    states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                    nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                    pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                [x1,add,expc] = findSolution(handle,x1,options);
            end
            
            if add.phi_R > add.phi_R_bar
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % CASE 1aa: Binding Retail CR, K^R = 0, non-binding shadow IC
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                handle = @(x) FOC_anti_retail_zero_KR_binding_retail(p,x, ...
                    states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                    nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                    pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                [x1,add,expc] = findSolution(handle,x1,options);
                
                if x1(1) < 0
                    handle = @(x) FOC_anti_retail_zero_KR_binding_retail_zero_KH(p,x, ...
                        states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                        nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                        pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                    [x1,add,expc] = findSolution(handle,x1,options);
                end          
            end   
        end
        
    elseif ~retail_violated && shadow_violated
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % CASE 2: Binding Shadow CR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        handle = @(x) FOC_anti_shadow(p,x, ...
            states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
            nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
            pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

        [x1,add,expc] = findSolution(handle,x1,options);
        
        if x1(1) < 0
            handle = @(x) FOC_anti_shadow_zero_KH(p,x, ...
                states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

            [x1,add,expc] = findSolution(handle,x1,options);
        end    
        
        if x1(2) <= 0          
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % CASE 2a: Binding Shadow CR, K^R = 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            handle = @(x) FOC_anti_shadow_zero_KR(p,x, ...
                states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

            [x1,add,expc] = findSolution(handle,x1,options);
            
            if x1(1) < 0
                handle = @(x) FOC_anti_shadow_zero_KR_zero_KH(p,x, ...
                    states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                    nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                    pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                [x1,add,expc] = findSolution(handle,x1,options);
            end   
            
            if add.phi_R > add.phi_R_market
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % CASE 2aa: Binding Retail CR, K^R = 0, non-binding shadow CR
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                handle = @(x) FOC_anti_shadow_zero_KR_binding_retail(p,x, ...
                    states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                    nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                    pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                [x1,add,expc] = findSolution(handle,x1,options);
                
                if x1(1) < 0
                    handle = @(x) FOC_anti_shadow_zero_KR_binding_retail_zero_KH(p,x, ...
                        states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                        nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                        pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                    [x1,add,expc] = findSolution(handle,x1,options);
                end             
            end
        end         
    end
    
    retail_violated = add.phi_R > add.phi_R_bar + 1e-3;
    shadow_violated = add.phi_S > add.phi_S_bar + 1e-3;
               
    if (retail_violated || shadow_violated)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % CASE 3: Both CR binding
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        handle = @(x) FOC_anti_both(p,x, ...
            states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
            nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
            pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

        [x1,add,expc] = findSolution(handle,x1,options);
        
        if x1(1) < 0
            handle = @(x) FOC_anti_both_zero_KH(p,x, ...
                states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

            [x1,add,expc] = findSolution(handle,x1,options);
        end   
        
        if x1(2) <= 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % CASE 3a: Both CR binding, KR = 0
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            handle = @(x) FOC_anti_both_zero_KR(p,x, ...
                states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));
            
            [x1,add,expc] = findSolution(handle,x1,options);
            
            if x1(1) < 0
                handle = @(x) FOC_anti_both_zero_KR_zero_KH(p,x, ...
                    states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                    nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                    pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                [x1,add,expc] = findSolution(handle,x1,options);
            end   
            
            if add.phi_R > add.phi_R_bar
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % CASE 3aa: Both CR binding, KR = 0, binding retail CR
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                handle = @(x) FOC_anti_both_zero_KR_binding_retail(p,x, ...
                    states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                    nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                    pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));
            
                [x1,add,expc] = findSolution(handle,x1,options)
                
                if x1(1) < 0
                    handle = @(x) FOC_anti_both_zero_KR_binding_retail_zero_KH(p,x, ...
                        states_no_run(ii,:),nextVals_n2n,nextVals_n2r, ...
                        nextStates_n2n,nextStates_n2r,Z_prob_ext', ...
                        pols_old.x_prime(ii,:),pols_old.p_prime(ii,:));

                    [x1,add,expc] = findSolution(handle,x1,options);
                end   
            end
        end     
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Store Solution
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_K_H_prime(ii,1)     = x1(1);
    vec_K_R_prime(ii,1)     = x1(2);
    vec_K_S_prime(ii,1)     = x1(3);           
    vec_C_H(ii,1)           = x1(4);
    
    vec_phi_R_bar(ii,1)     = add.phi_R_bar;
    vec_phi_S_bar(ii,1)     = add.phi_S_bar;
    vec_phi_R(ii,1)         = add.phi_R;
    vec_phi_S(ii,1)         = add.phi_S;
    vec_B_prime(ii,1)       = add.B_prime;      
    vec_Q(ii,1)             = add.Q;
    
    vec_R_D_prime(ii,1)     = expc.R_D_prime;
    vec_R_B_prime(ii,1)     = expc.R_B_prime;
    vec_E_R_K_R_prime(ii,1) = expc.E_R_K_R_prime;
    vec_E_R_K_S_prime(ii,1) = expc.E_R_K_S_prime; 
    vec_Omega_R(ii,1)       = expc.Omega_R;
    vec_Omega_S(ii,1)       = expc.Omega_S;
    
    vec_K_prime(ii,1) = vec_K_H_prime(ii,1) + vec_K_R_prime(ii,1) ...
        + vec_K_S_prime(ii,1);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Update the Net Worth
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [vec_N_R_prime_n2n(ii,:),vec_N_S_prime_n2n(ii,:), ...
        vec_N_R_prime_n2r(ii,:),vec_N_S_prime_n2r(ii,:),vec_x_prime(ii,:), ...
        vec_p_prime(ii,:),vec_sunspot_prob(ii,:)] = ...
        N_prime_both(p, states_no_run(ii,:), x1, add,expc, ...
        nextStates_n2n, nextVals_n2n, nextVals_n2r, logXi_prime', Z_prob_ext);
end
    
vec_N_R_prime_n2n = bound(vec_N_R_prime_n2n,funs.orig_min(1),funs.orig_max(1));
vec_N_R_prime_n2r = bound(vec_N_R_prime_n2r,funs.orig_min(1),funs.orig_max(1));
    
vec_N_S_prime_n2n = bound(vec_N_S_prime_n2n,funs.orig_min(2),funs.orig_max(2));
vec_N_S_prime_n2r = bound(vec_N_S_prime_n2r,funs.orig_min(2),funs.orig_max(2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store Solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pols = struct;

pols.phi_R_tilde = vec_phi_R_tilde;
pols.phi_S_tilde = vec_phi_S_tilde;
pols.phi_R_bar   = vec_phi_R_bar;
pols.phi_S_bar   = vec_phi_S_bar;
pols.Q_tilde     = vec_Q_tilde;
pols.K_H_prime   = vec_K_H_prime;
pols.K_R_prime   = vec_K_R_prime;
pols.K_S_prime   = vec_K_S_prime;
pols.phi_R       = vec_phi_R;
pols.phi_S       = vec_phi_S;
pols.C_H         = vec_C_H;
pols.B_prime     = vec_B_prime;
pols.Q           = vec_Q;
pols.R_D_prime   = vec_R_D_prime;
pols.R_B_prime   = vec_R_B_prime;
pols.E_R_K_R_prime = vec_E_R_K_R_prime;
pols.E_R_K_S_prime = vec_E_R_K_S_prime;
pols.K_prime     = vec_K_prime;
pols.sunspot_prob = vec_sunspot_prob;

pols.N_R_prime_n2n   = vec_N_R_prime_n2n;
pols.N_S_prime_n2n   = vec_N_S_prime_n2n;
pols.N_R_prime_n2r   = vec_N_R_prime_n2r;
pols.N_S_prime_n2r   = vec_N_S_prime_n2r;
pols.x_prime         = vec_x_prime;
pols.p_prime         = vec_p_prime;

pols.I = pols.K_prime - (1-p.delta)*vec_K;
pols.D_prime = (pols.phi_R-1).*vec_N_R + (1-p.gamma)*pols.B_prime;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the Value Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vals = struct;
vals.C_H = pols.C_H;
vals.V_R = (p.sigma_R + (1-p.sigma_R)*vec_Omega_R).*(vec_N_R - p.exoInc*vec_K)/(1-p.sigma_R);    
vals.V_S = (p.sigma_S + (1-p.sigma_S)*vec_Omega_S).*(vec_N_S - p.exoInc*vec_K)/(1-p.sigma_S);
vals.Q   = pols.Q;