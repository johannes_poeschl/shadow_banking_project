function [x1,add,expc] = findSolution(handle,x0,options)

x1 = fsolve(handle,max(x0,0),options);

[~,add,expc] = handle(x1);
