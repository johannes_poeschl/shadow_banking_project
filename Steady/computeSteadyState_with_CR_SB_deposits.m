function [resid,steady] = computeSteadyState_with_CR_SB_deposits(x,p)

K_H = x(1);
K_R = x(2);
K_S = x(3);
N_R = x(4);
N_S = x(5);
D_S = x(6);

% Market Clearing
K = K_H + K_R + K_S;
exoInc = p.exoInc*K;

% Final Goods Producer
r_K = p.alpha*exp(p.mu_Z)*K^(p.alpha-1);
W = (1-p.alpha)*exp(p.mu_Z)*K^p.alpha;

% Returns
R_D     = 1/p.beta;
R_K     = r_K + 1 - p.delta;
R_K_H   = R_K/(1 + p.eta_H*K_H/K);
R_K_R   = R_K/(1 + p.eta_R*K_R/K);

% Shadow Bank
n_S     = (N_S - exoInc)/(1-p.sigma_S);
phi_S   = K_S/N_S;
B       = (phi_S - 1)*N_S - D_S;

% Retail Bank
f_R     = p.eta_R*K_R/K;
n_R     = (N_R - exoInc)/(1-p.sigma_R);

phi_R = ((1 + f_R)*K_R + p.gamma*B)/N_R;

% capital requirement does not bind
phi_R_tilde     = p.phi_R_bar;
R_B             = p.gamma_bar*R_K_R + (1-p.gamma_bar)*R_D;
D_R             = (phi_R_tilde - 1)*N_R + (1 - p.gamma_bar)*B;

% Household
N_H = (r_K + 1 - p.delta)*K_H + R_D*(D_R + D_S) + W;
Y = exp(p.mu_Z)*K^p.alpha - p.eta_H/2*K_H^2/K - p.eta_R/2*K_R^2/K;
C_H = Y - p.delta*K;

resid    = zeros(5,1);
resid(1) = p.beta*R_K_H - 1;
resid(2) = phi_R_tilde*N_R - (1 + f_R)*K_R - p.gamma_bar*B;
resid(3) = p.psi*(K_S - (1-p.omega)*B) - ...
    p.beta*(p.sigma_S*n_S + (1-p.sigma_S)*p.psi*(K_S - (1-p.omega)*B)*n_S/N_S);
resid(4) = N_S - (R_K*K_S - R_B*B - R_D*D_S)*(1-p.sigma_S) - exoInc;
resid(5) = N_R - (R_K*K_R + R_B*B - R_D*D_R)*(1-p.sigma_R) - exoInc;
resid(6) = (1-p.omega)*R_K + p.omega*R_D - R_B;

% collect all steady state values in a struct array
steady          = struct;
steady.Y        = Y;
steady.C_H      = C_H;
steady.I        = p.delta*K;
steady.K_H      = K_H;
steady.K_R      = K_R;
steady.K_S      = K_S;
steady.N_R      = N_R;
steady.N_S      = N_S;
steady.K        = K;
steady.logZ     = p.mu_Z;
steady.xi   	= exp(p.mu_xi + p.sigma_xi^2/2);
steady.D        = D;
steady.B        = B;
steady.phi_R    = phi_R;
steady.phi_S    = phi_S;
steady.Q        = 1;
steady.Q_star   = 1;
steady.R_D      = R_D;
steady.R_B      = R_B;
steady.R_K      = R_K;
steady.recov    = R_K*K_S/(R_B*B);
steady.phi_shadow = phi_shadow;
steady.V_R = p.beta*p.sigma_R/(1-p.beta*(1-p.sigma_R))*n_R;
steady.V_S = p.beta*p.sigma_S/(1-p.beta*(1-p.sigma_S))*n_S;
steady.D_S = 0;