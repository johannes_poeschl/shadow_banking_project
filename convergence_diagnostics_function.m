field_names = fieldnames(pols);

nFields = length(field_names);

for jj = 1:nFields
    
    eval(strcat('pols_diff.',field_names{jj},'=','pols.',field_names{jj},'-pols_old.',field_names{jj},';'))
    
end

[max_val,max_pos] = max(abs(pols_diff.K_S_prime));

pols_at_max = cell(nFields,3);

for jj = 1:nFields
    
    pols_at_max{jj,1} = field_names{jj};
    
    eval(strcat('pols_at_max{jj,2}=','pols.',field_names{jj},'(max_pos);'))
    eval(strcat('pols_at_max{jj,3}=','pols_old.',field_names{jj},'(max_pos);'))
    
end

ii = max_pos;

n_error = pols.N_R_prime_n2n - pols_old.N_R_prime_n2n;

negative_phi_S = pols.phi_S <= 0;

for jj = 1:nFields
    
    eval(strcat('pols_negative_phi.',field_names{jj},'=','pols.',field_names{jj},'(negative_phi_S,:);'))
    
end