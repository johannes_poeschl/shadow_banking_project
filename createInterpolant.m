function [int_fun,cellVals,states_poly] = createInterpolant(smolyak_fun,funs,sz_array,exo_array,varargin)

if isempty(varargin) == 1
    
    [nd_fun,cellVals,states_poly] = smolyak_plot(smolyak_fun(:,1),funs,sz_array);

else
    
    states_poly = varargin{1};
    [nd_fun,cellVals] = smolyak_plot(smolyak_fun(:,1),funs,sz_array,states_poly);
    
end

if prod(exo_array > 1)
    
    nd_fun1 = nd_fun;
    
    nd_fun = zeros([sz_array prod(exo_array)]);
    
    nd_fun(:,:,:,:,1) = nd_fun1;

    for ii = 2:prod(exo_array)

        nd_fun(:,:,:,:,ii) = smolyak_plot(smolyak_fun(:,ii),funs,sz_array,states_poly);

    end

end

if prod(exo_array > 1)

    cell_grid = {cellVals{1},cellVals{2},cellVals{3},cellVals{4},grd.eps_Z,grd.eps_Xi};
    
else
    
    cell_grid = {cellVals{1},cellVals{2},cellVals{3},cellVals{4}};
    
end

int_fun = griddedInterpolant(cell_grid,nd_fun,'linear','none');