function int_fun = interpolate_on_exo_states(fun,grd,grd_old,run)

if run == 0

    fun_2D = reshape(fun,grd_old.n_Z,grd_old.n_Xi);

    fun_interpolant = griddedInterpolant({grd_old.eps_Z,grd_old.eps_Xi},fun_2D);

    int_fun = fun_interpolant({grd.eps_Z,grd.eps_Xi});

    int_fun = vec(int_fun);

    int_fun = int_fun';
    
else    

    fun_interpolant = griddedInterpolant({grd_old.eps_Z},fun);

    int_fun = fun_interpolant({grd.eps_Z});

    int_fun = int_fun';
    
end