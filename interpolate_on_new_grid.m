function pols_new = interpolate_on_new_grid( pols,funs,states,grd,run)

states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
states_resc = bound(states_resc,-1,1); % don't extrapolate!
states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extract the information on the policies from the structure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
field_names = fieldnames(pols);

nFields = length(field_names);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% interpolate all policies on the states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pols_new = struct;
for jj = 1:nFields

    eval(strcat('old_values=pols.',field_names{jj},';'))

    coeffs = funs.base_poly\old_values;

    new_values = states_poly*coeffs;

    eval(strcat('pols_new.',field_names{jj},'=new_values;'))
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% interpolate all future policies on the shocks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Temporary - specify the old future shock values here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
grd_old.n_Z = 9;
grd_old.n_Xi = 9;
[grd_old.eps_Z,grd_old.Z_prob] = qnwnorm(grd_old.n_Z,0,1);
[grd_old.eps_Xi,grd_old.Xi_prob] = qnwnorm(grd_old.n_Xi,0,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for jj = 1:nFields
    
    eval(strcat('old_values=pols_new.',field_names{jj},';'))
    
    [nStates,nExo] = size(old_values);

    if nExo > 1
        
        if run == 0
            new_values = zeros(nStates,grd.n_Z*grd.n_Xi);
        else
            new_values = zeros(nStates,grd.n_Z);
        end
        
       for ii = 1:nStates
          
           new_values(ii,:) = interpolate_on_exo_states(old_values(ii,:),grd,grd_old,run);
           
       end
       
       eval(strcat('pols_new.',field_names{jj},'=new_values;'))
        
    end
    
end