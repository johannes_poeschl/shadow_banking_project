% solves the unemployment model

clear
close

addpath('./Functions')
addpath('./Functions/Smolyak_Anisotropic_JMMV_2014')
addpath('./Functions/altmany-export_fig-5b3965b')
addpath('./Functions/eliduenisch-latexTable-5212622')
addpath('./Functions/CompEcon Toolbox/CEtools')
addpath('./Steady')
addpath('./Solution')
addpath('./Simulation')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

opt = struct;
opt.solveModel      = 1;
opt.simulModel      = 0;
opt.createTables    = 0;
opt.createFigures   = 0;
opt.reg             = {'both'};         % none, retail, shadow, both, retail_dynamic, shadow_dynamic, both_dynamic
opt.run             = {'with_runs'};    % with_runs, no_runs, unexpected_runs, unrealized_runs
opt.modelVersion    = 'baseline';

% Solution
opt.newGuess                = 0;
opt.overwrite_initial_guess = 0;
opt.use_old_solution        = 1;
opt.maxIter                 = 500;
opt.tol                     = 1e-5;
opt.weigth_N_new            = 0.5;

% Simulation
opt.seed        = rng;
opt.load_shocks = 0;
opt.nEconomies  = 1e3;
opt.nPeriods    = 1000;
opt.nBurnin     = 1000;
opt.no_run_counterfactual = 0;
opt.event_study_policies = 0;

nReg = length(opt.reg);
nRun = length(opt.run);

[iReg,iRun] = ndgrid(1:nReg,1:nRun);

iReg = vec(iReg);
iRun = vec(iRun);

nII = nReg*nRun;

for ii = 1:nII
    
opt.capReqVals   = setPolicy(opt.reg{iReg(ii)});

nXX = size(opt.capReqVals,1);

    for xx = 1:nXX

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% Parameters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        p = setParams(opt.reg{iReg(ii)},opt.run{iRun(ii)},opt.capReqVals(xx,:));
        
        foldername = strcat('./Results/', ...
                        opt.reg{iReg(ii)},'_', ...
                        opt.run{iRun(ii)});
                    
        filename_solu = strcat('Results_', ...
           'GamR_',num2str(1/p.phi_R_bar), ...
           '_GamS_',num2str(1/p.phi_S_bar), ...
           '_nuR_',num2str(p.nu_R), ...
           '_nuS_',num2str(p.nu_S), ...
           '.mat');
       
       filename_simu = strcat('Moments_', ...
           'GamR_',num2str(1/p.phi_R_bar), ...
           '_GamS_',num2str(1/p.phi_S_bar), ...
           '_nuR_',num2str(p.nu_R), ...
           '_nuS_',num2str(p.nu_S), ...
           '.mat');

        if opt.solveModel == 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Model Solution
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            if opt.newGuess == 1 && xx == 1
                delete('initialGuess.mat')
            end

            if exist(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)}),'dir') == 0
                mkdir(strcat('./Results/',opt.reg{iReg(ii)},'_',opt.run{iRun(ii)}))
            end
            
%             try
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % START Quick and dirty - get the old results as initial guess
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                if opt.use_old_solution == 1 && exist(strcat(foldername,'/',filename_solu),'file') == 2
                    
                    load(strcat(foldername,'/',filename_solu), ...
                        'pols','funs','pols_run','funs_run')

                    vals.C_H = funs.base_poly*funs.C_H;
                    vals.V_R = funs.base_poly*funs.V_R;
                    vals.V_S = funs.base_poly*funs.V_S;
                    vals.Q = funs.base_poly*funs.Q;

                    vals_run.C_H = funs_run.base_poly*funs_run.C_H;
                    vals_run.V_R = funs_run.base_poly*funs_run.V_R;
                    vals_run.Q = funs_run.base_poly*funs_run.Q;

                    save('initialGuess.mat','pols','pols_run','vals','vals_run','funs','funs_run')
                    
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % END Quick and dirty - get the old results as initial guess
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                [pols,pols_run,funs,funs_run,vals,vals_run,grd,steady,states_no_run,states_run,out] =  ...
                    solveModel(p,opt.reg{iReg(ii)},opt.run{iRun(ii)},opt);

                save(strcat(foldername,'/',filename_solu), ...
                    'pols','funs','vals','pols_run','funs_run','vals_run','p','steady','out')

                if opt.overwrite_initial_guess == 1
                    save('initialGuess.mat','pols','pols_run','vals','vals_run')
                end

%             catch
% 
%                 fprintf('Could not solve model %d \n',xx)
% 
%             end

        end

        if opt.simulModel == 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %% Model Simulation
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%             try 

                load(strcat(foldername,'/',filename_solu))

                [Tables,TS] = function_simpleSimulation(pols,funs,pols_run,funs_run,p,steady,opt);

                save(strcat(foldername,'/',filename_simu),'Tables')

%             catch
% 
%                 fprintf('Could not simulate model %d \n',xx)
% 
%             end
        end
    end
end

if opt.createTables == 1
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Tables
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
    Summary_Tables = createTables(opt);
    
    input = struct;

    input.dataFormat = {'%.3f'};
    input.booktabs = 1;
    input.tablePlacement= 'ht';
    input.tableColLabels = opt.run;
    input.data = table2array(Summary_Tables.section_welfare);
    input.tableRowLabels = Summary_Tables.section_welfare.Properties.RowNames;
    output = latexTable(input);  
        
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if opt.createFigures == 1

    time = -20:20;

    nVars = length(Tables.eventStudy.Properties.RowNames);

    hf3 = figure;
    count = 1;
    for ii = 1:nVars
        subplot(5,5,count)
        plot(time,Tables.eventStudy.avgRun(ii,:),'LineWidth',2)
        title(Tables.eventStudy.Properties.RowNames{ii})
        hold
%         plot(time,100*Tables.eventStudyCounterfactual.avgRun_counterfactual(ii,:),'r','LineWidth',2)
        plot(time,Tables.ev1.avgRun_counterfactual_pol1(ii,:),'k','LineWidth',2)
        plot(time,Tables.ev2.avgRun_counterfactual_pol2(ii,:),'c','LineWidth',2)
        yLim = ylim;
        hl = line([0 0],yLim);
        xLim = xlim;
        set(hl,'Color','r')
        count = count + 1;
%         dataVals = [Tables.eventStudy.avgRun(ii,:) 100*Tables.eventStudyCounterfactual.avgRun_counterfactual(ii,:)];
%         ylim([min(dataVals)-1e-6 max(dataVals)+1e-6])
        xlim([min(time) max(time)])
    end
    xlabel('Time (Quarters)','Interpreter','Latex')

    hf3.Units           = 'centimeters';
%     hf3.InnerPosition   = [1 1 21 29.7-5];

    %%
    
    varNames = {'Output ($Y$)', ...
        'Consumption ($C^H$)', ...
        'Investment ($\tilde{I}$)', ...
        'Retail Bank Leverage ($\phi^R$)', ...
        'Retail Bank Net Worth ($N^R$)', ...
        'Deposits ($D''$)', ...
        'Shadow Bank Leverage ($\phi^S$)', ...
        'Shadow Bank Net Worth ($N^S$)', ...
        'Interbank Loans ($B''$)', ...
        'Capital Price ($Q$)', ...
        'Spread, Wholesale $(R^{B''} - R^{D''})$', ...
        'Spread, Retail $(E[R^{K''}/Q] - R^{D''})$', ...
        'log(Productivity) ($\log(Z)$)', ...
        'Recovery Value Shock ($\xi$)', ...
        'Sunspot Probability (Pr$(\Xi = 1)$)'};
    
    varNorm = {'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'pctdev', ...
        'rate', ...
        'rate', ...
        'pctdev', ...
        'pctdev', ...
        'level'};

    nVars = length(Tables.eventStudy.Properties.RowNames);
    
    font_options = {'Interpreter','Latex'};

    hf3 = figure;
    count = 1;
    for ii = [1 2 3 14 7 12 15 8 13 16 19 20 10 11 24]
        subplot(5,3,count)
        if strcmp(varNorm{count},'pctdev') == 1
            
            varVals = Tables.eventStudy.avgRun(ii,:);
            varVals2 = 100*Tables.eventStudyCounterfactual.avgRun_counterfactual(ii,:);
            
            yLabelVal = 'Dev. from SS (Pct.)';
            
        elseif strcmp(varNorm{count},'rate') == 1
            
            depositRate = (1+Tables.eventStudy.avgRun(18,:)/100)*Tables.Moments.MeanVal(18);
            depositRate2 = (1+Tables.eventStudyCounterfactual.avgRun_counterfactual(18,:))*Tables.Moments.MeanVal(18);
            
            depositRate = 100*(depositRate.^4 - 1);
            depositRate2 = 100*(depositRate2.^4 - 1);

            varVals = (1+Tables.eventStudy.avgRun(ii,:)/100)*Tables.Moments.MeanVal(ii);
            varVals2 = (1+Tables.eventStudyCounterfactual.avgRun_counterfactual(ii,:))*Tables.Moments.MeanVal(ii);
            
            varVals = 100*(varVals.^4 - 1);
            varVals2 = 100*(varVals2.^4 - 1);
            
            varVals = varVals - depositRate;
            varVals2 = varVals2 - depositRate2;
            
            yLabelVal = 'Annualized Rate (Pct.)';
            
        else
            
            varVals = 100*(1+Tables.eventStudy.avgRun(ii,:)/100)*Tables.Moments.MeanVal(ii);
            varVals2 = 100*(1+Tables.eventStudyCounterfactual.avgRun_counterfactual(ii,:))*Tables.Moments.MeanVal(ii);
            
            yLabelVal = 'Level (Pct.)';
            
        end
        plot(time,varVals,'b-','LineWidth',2)
        hold
        plot(time,varVals2,'r:','LineWidth',2)
        title(varNames{count},font_options{:})
        ylabel(yLabelVal,font_options{:})
        yLim = ylim;
        hl = line([0 0],yLim);
        xLim = xlim;
        set(hl,'Color','k')
        count = count + 1;
        dataVals = [varVals varVals2];
        ylim([min(dataVals)-1e-6 max(dataVals)+1e-6])
        xlim([min(time) max(time)])
    end
    xlabel('Time (Quarters)',font_options{:})
    hl = legend('With Run','No Run');
    set(hl,font_options{:})

    hf3.Units           = 'centimeters';
    hf3.InnerPosition   = [1 1 21 29.7-5];
    
    export_fig('./Figures/Event_Study_Full','-pdf','-transparent')
    
end
