%%

clear
close

addpath('./Functions')
addpath('./Functions/Smolyak_Anisotropic_JMMV_2014')
addpath('./Functions/altmany-export_fig-5b3965b')
addpath('./Functions/eliduenisch-latexTable-5212622')
addpath('./Functions/CompEcon Toolbox/CEtools')
addpath('./Steady')
addpath('./Solution')
addpath('./Simulation')


%%

% interpolate policy functions on a rectangular grid

grd.n_Z = 9;
grd.n_Xi = 9;
[grd.eps_Z,grd.Z_prob] = qnwnorm(grd.n_Z,0,1);
[grd.eps_Xi,grd.Xi_prob] = qnwnorm(grd.n_Xi,0,1);

prob = kron(grd.Z_prob,grd.Xi_prob);

sz_array = [20 20 20 20];
exo_array = [1 1];

%% baseline

load('./Results/none_with_runs/Results_GamR_0_GamS_0_nuR_0_nuS_0.mat')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

field_names = fieldnames(pols);

field_names = field_names([28 17 9 10]);

nFields = length(field_names);

ii = 1;

eval(strcat('pols_fun = pols.',field_names{ii},';'))

[int_fun,cellVals,states_poly] = createInterpolant(pols_fun,funs,sz_array,exo_array);

eval(strcat('int.',field_names{ii},'=int_fun;'))

for ii = 2:length(field_names)
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int.',field_names{ii},'=int_fun;'))
    
end

%% policy 1

load('./Results/retail_with_runs/Results_GamR_0.1_GamS_0_nuR_0_nuS_0.mat','pols','funs')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

for ii = 1:nFields
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int2.',field_names{ii},'=int_fun;'))
    
end

%% policy 2

load('./Results/retail_with_runs/Results_GamR_0.13333_GamS_0_nuR_0_nuS_0.mat','pols','funs')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

for ii = 1:nFields
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int3.',field_names{ii},'=int_fun;'))
    
end

%% policy 3

load('./Results/shadow_with_runs/Results_GamR_0_GamS_0.05_nuR_0_nuS_0.mat','pols','funs')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

for ii = 1:nFields
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int4.',field_names{ii},'=int_fun;'))
    
end

%% policy 4

load('./Results/shadow_with_runs/Results_GamR_0_GamS_0.066667_nuR_0_nuS_0.mat','pols','funs')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

for ii = 1:nFields
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int5.',field_names{ii},'=int_fun;'))
    
end

%% policy 5

load('./Results/both_with_runs/Results_GamR_0.1_GamS_0.05_nuR_0_nuS_0.mat','pols','funs')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

for ii = 1:nFields
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int6.',field_names{ii},'=int_fun;'))
    
end

%% policy 6

load('./Results/both_with_runs/Results_GamR_0.11111_GamS_0.055556_nuR_0_nuS_0.mat','pols','funs')

pols.E_sunspot_prob = 400*pols.sunspot_prob*prob;

for ii = 1:nFields
    
    eval(strcat('pols_fun = pols.',field_names{ii},';'))

    int_fun = createInterpolant(pols_fun,funs,sz_array,exo_array,states_poly);

    eval(strcat('int7.',field_names{ii},'=int_fun;'))
    
end

%% Retail Policy Plot

plotNames = {'Exp. Sunspot Prob. $E_t [ Pr(\Xi_{t+1} = 1) ]$', ...
    'Exp. Return on Capital $E_t [ R_{t+1}^K/Q_t ]$', ...
    'Retail Bank Leverage $\phi_t^R$', ...
    'Shadow Bank Leverage $\phi_t^S$'};

close all

% reference values
N = 25;

N_R_val     = steady.N_R;
N_S_val     = steady.N_S;
K_val       = steady.K;
Z_val       = p.mu_Z;

N_R_grid = linspace(funs.orig_min(1),funs.orig_max(1),N);
N_S_grid = linspace(funs.orig_min(2),funs.orig_max(2),N);
K_grid   = linspace(funs.orig_min(3),funs.orig_max(3),N);
Z_grid   = linspace(funs.orig_min(4),funs.orig_max(4),N);

count = 1;

plotOptions1 = {'b','LineWidth',2};
plotOptions2 = {'r--','LineWidth',2};
plotOptions3 = {'k:','LineWidth',2};

textOptions = {'Interpreter','Latex'};

hf1 = figure;
for ii = 1:nFields
        
    eval(strcat('plot_fun = int.',field_names{ii},'(({N_R_grid,N_S_val,K_val,Z_val}));'))
    eval(strcat('plot_fun2 = int2.',field_names{ii},'(({N_R_grid,N_S_val,K_val,Z_val}));'))
    eval(strcat('plot_fun3 = int3.',field_names{ii},'(({N_R_grid,N_S_val,K_val,Z_val}));'))

    subplot(2,2,count)
    plot(N_R_grid,plot_fun,plotOptions1{:})
    hold
    plot(N_R_grid,plot_fun2,plotOptions2{:})
    plot(N_R_grid,plot_fun3,plotOptions3{:})
    xlabel('$N^R_t$',textOptions{:})
    title(plotNames{ii},textOptions{:})
    xlim([min(N_R_grid) max(N_R_grid)])
    
    count = count+1;

end
hl = legend('Baseline','$\bar{\phi}^R = 10$','$\bar{\phi}^R = 7.5$');
set(hl,textOptions{:})

hf1.Units           = 'centimeters';
pos1 = 2;
pos2 = 2;
hf1.InnerPosition   = [pos1 pos2 21 29.7/2];

%% Shadow Policy Plot

count = 1;

hf2 = figure;
for ii = 1:nFields
    eval(strcat('plot_fun = int.',field_names{ii},'(({N_R_val,N_S_grid,K_val,Z_val}));'))
    eval(strcat('plot_fun2 = int4.',field_names{ii},'(({N_R_val,N_S_grid,K_val,Z_val}));'))
    eval(strcat('plot_fun3 = int5.',field_names{ii},'(({N_R_val,N_S_grid,K_val,Z_val}));'))

    subplot(2,2,count)
    plot(N_S_grid,plot_fun,plotOptions1{:})
    hold
    plot(N_S_grid,plot_fun2,plotOptions2{:})
    plot(N_S_grid,plot_fun3,plotOptions3{:})
    xlabel('$N^S_t$',textOptions{:})
    title(plotNames{ii},textOptions{:})
    xlim([min(N_S_grid) max(N_S_grid)])
    
    count = count+1;
  
end
hl = legend('Baseline','$\bar{\phi}^S = 20$','$\bar{\phi}^S = 15$');
set(hl,textOptions{:});

hf2.Units           = 'centimeters';
pos1 = 2;
pos2 = 2;
hf2.InnerPosition   = [pos1 pos2 21 29.7/2];

%% Both Policies Plot - against N^R

plotNames = {'Exp. Sunspot Prob. $E_t [ Pr(\Xi_{t+1} = 1) ]$', ...
    'Exp. Return on Capital $E_t [ R_{t+1}^K/Q_t ]$', ...
    'Retail Bank Leverage $\phi_t^R$', ...
    'Shadow Bank Leverage $\phi_t^S$'};

close all

% reference values
N = 25;

N_R_val     = steady.N_R;
N_S_val     = steady.N_S;
K_val       = steady.K;
Z_val       = p.mu_Z;

N_R_grid = linspace(funs.orig_min(1),funs.orig_max(1),N);
N_S_grid = linspace(funs.orig_min(2),funs.orig_max(2),N);
K_grid   = linspace(funs.orig_min(3),funs.orig_max(3),N);
Z_grid   = linspace(funs.orig_min(4),funs.orig_max(4),N);

count = 1;

plotOptions1 = {'b','LineWidth',2};
plotOptions2 = {'r--','LineWidth',2};
plotOptions3 = {'k:','LineWidth',2};

textOptions = {'Interpreter','Latex'};

hf1 = figure;
for ii = 1:nFields
        
    eval(strcat('plot_fun = int.',field_names{ii},'(({N_R_grid,N_S_val,K_val,Z_val}));'))
    eval(strcat('plot_fun2 = int6.',field_names{ii},'(({N_R_grid,N_S_val,K_val,Z_val}));'))
    eval(strcat('plot_fun3 = int7.',field_names{ii},'(({N_R_grid,N_S_val,K_val,Z_val}));'))

    subplot(2,2,count)
    plot(N_R_grid,plot_fun,plotOptions1{:})
    hold
    plot(N_R_grid,plot_fun2,plotOptions2{:})
    plot(N_R_grid,plot_fun3,plotOptions3{:})
    xlabel('$N^R_t$',textOptions{:})
    title(plotNames{ii},textOptions{:})
    xlim([min(N_R_grid) max(N_R_grid)])
    
    count = count+1;

end
hl = legend('Baseline','$\bar{\phi}^R = 10$','$\bar{\phi}^R = 7.5$');
set(hl,textOptions{:})

hf1.Units           = 'centimeters';
pos1 = 2;
pos2 = 2;
hf1.InnerPosition   = [pos1 pos2 21 29.7/2];

%% Both Policies Plot - against N^S

count = 1;

hf2 = figure;
for ii = 1:nFields
    eval(strcat('plot_fun = int.',field_names{ii},'(({N_R_val,N_S_grid,K_val,Z_val}));'))
    eval(strcat('plot_fun2 = int6.',field_names{ii},'(({N_R_val,N_S_grid,K_val,Z_val}));'))
    eval(strcat('plot_fun3 = int7.',field_names{ii},'(({N_R_val,N_S_grid,K_val,Z_val}));'))

    subplot(2,2,count)
    plot(N_S_grid,plot_fun,plotOptions1{:})
    hold
    plot(N_S_grid,plot_fun2,plotOptions2{:})
    plot(N_S_grid,plot_fun3,plotOptions3{:})
    xlabel('$N^S_t$',textOptions{:})
    title(plotNames{ii},textOptions{:})
    xlim([min(N_S_grid) max(N_S_grid)])
    
    count = count+1;
  
end
hl = legend('Baseline','$\bar{\phi}^S = 20$','$\bar{\phi}^S = 15$');
set(hl,textOptions{:});

hf2.Units           = 'centimeters';
pos1 = 2;
pos2 = 2;
hf2.InnerPosition   = [pos1 pos2 21 29.7/2];

