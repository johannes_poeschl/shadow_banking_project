clear

time = 0:0.01:2*pi;

nuVals = [0 0.5 1 2];

market_leverage_SS = 10;
market_leverage = market_leverage_SS + sin(time);

lev_fun = @(phi,nu) market_leverage_SS*(phi/market_leverage_SS).^nu;

hf3 = figure;

fontOptions = {'Interpreter','Latex'};

for ii = 1:4
    
    regulatory_leverage = lev_fun(market_leverage,nuVals(ii));

    binding_leverage = market_leverage;
    binding_leverage(market_leverage >= regulatory_leverage) = ...
        regulatory_leverage(market_leverage >= regulatory_leverage);
    
    subplot(2,2,ii)
    plot(time,binding_leverage,'k','LineWidth',4);
    hold
    plot(time,market_leverage,'b:','LineWidth',2);
    plot(time,regulatory_leverage,'r--','LineWidth',2);
    xlim([min(time) max(time)])
%     ylim([8 12])
    xlabel('Time',fontOptions{:})
    ylabel('Leverage',fontOptions{:})
    title(strcat('$\nu = ',num2str(nuVals(ii)),'$'),fontOptions{:})
    
end

hl = legend('Binding LC','Market LC','Regulatory LC');
set(hl,fontOptions{:})

hf3.Units           = 'centimeters';
pos1 = 2;
pos2 = 2;
hf3.InnerPosition   = [pos1 pos2 21 29.7/2];

export_fig('./Figures/capital_requirement','-pdf','-transparent')