% plot the bank run probability and expected future capital price as a
% function of the deposit level.

clear

load(strcat('./Results/none_with_runs/Results_GamR_0_GamS_0_nuR_0_nuS_0.mat'))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set States
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set a current state by fixing K^H, K^R, K^S, B, D
logZ = p.mu_Z;
N_R = steady.N_R;
N_S = steady.N_S;
K   = steady.K;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set price space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Q_space = 0.95:0.001:1.025;
nQ = length(Q_space);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Solve the model for a given capital price
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[pols,vals] = computeNoRunSolution(p,steady,grd,states_no_run,funs,funs_run, ...
    pols_old,options,reg);

[pols_run,vals_run] = computeRunSolution(p,grd,states_run,funs,funs_run, ...
    pols_run_old,options,reg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xiName = 'one_eq';
figureVersion = 'slides';

if strcmp(xiName,'two_eq') == 1
    xiVal = 0.955;
elseif strcmp(xiName,'one_eq') == 1
    xiVal = 0.98;
end

I_prime_supply = ((Q_space - 1) + p.theta*p.delta)*states(:,3)/p.theta;
I_prime_demand = K_prime_demand(:,1) - (1-p.delta)*states(:,3);
I_prime_demand_run = K_prime_demand_run(:,1) - (1-p.delta)*states(:,3);

I_prime_demand1 = I_prime_demand;
I_prime_demand_run1 = I_prime_demand_run;

Q_cutoff = 1/xiVal*(steady.R_B*steady.B/steady.K_S - p.alpha*exp(logZ)*K.^(p.alpha-1))/(1-p.delta);

I_prime_demand(Q_space < Q_cutoff) = nan;
I_prime_demand_run(Q_space >= Q_cutoff) = nan;

if exist(strcat('investment_demand_',num2str(0.0001),'.mat'),'file') == 2 && reg_val ~= 0.0001
    
    load(strcat('investment_demand_',num2str(0.0001),'.mat'))
    
    I_noreg_demand1 = I_noreg_demand;
    I_noreg_demand_run1 = I_noreg_demand_run;

    I_noreg_demand(Q_space < Q_cutoff) = nan;
    I_noreg_demand_run(Q_space >= Q_cutoff) = nan;
    
    gray = [0.65 0.65 0.65];
    
    figure
    p1 = plot(I_prime_demand,Q_space,'b','LineWidth',2);
    hold
    p2 = plot(I_prime_supply,Q_space,'r','LineWidth',2);
    plot(I_prime_demand_run,Q_space,'b','LineWidth',2)
    plot(I_prime_demand1,Q_space,'b:','LineWidth',1)
    plot(I_prime_demand_run1,Q_space,'b:','LineWidth',1)
    p4 = plot(I_noreg_demand,Q_space,'Color',gray,'LineWidth',2);
    plot(I_noreg_demand_run,Q_space,'Color',gray,'LineWidth',2)
    plot(I_noreg_demand1,Q_space,':','Color',gray,'LineWidth',1)
    plot(I_noreg_demand_run1,Q_space,':','Color',gray,'LineWidth',1)
    ylim([min(Q_space) max(Q_space)])
    xLim = [0.2 0.28];
    p3 = line(xLim,[Q_cutoff Q_cutoff]);
    xlim(xLim)
    set(p3,'Color','k')
    title('Capital Market Equilibrium')
    xlabel('Investment Quantity I_t')
    ylabel('Capital Price Q_t')
    hl = legend([p1 p4 p2 p3], ...
        ['Investment' newline 'Demand,' newline 'Regulation'], ...
        ['Investment' newline 'Demand,' newline 'no Regulation'], ...
        ['Investment' newline 'Supply'], ...
        ['Bank Run' newline 'Cutoff'] ...
        );
    set(hl,'Location','southoutside','Orientation','horizontal')
    legend('boxoff')
    
    figureName = strcat('./Figures/equilibrium_plot_',xiName,'_',reg);
else
    figure
    p1 = plot(I_prime_demand,Q_space,'b','LineWidth',2);
    hold
    p2 = plot(I_prime_supply,Q_space,'r','LineWidth',2);
    plot(I_prime_demand_run,Q_space,'b','LineWidth',2)
    plot(I_prime_demand1,Q_space,'b:','LineWidth',1)
    plot(I_prime_demand_run1,Q_space,'b:','LineWidth',1)
    ylim([min(Q_space) max(Q_space)])
    xLim = [0.2 0.28];
    p3 = line(xLim,[Q_cutoff Q_cutoff]);
    xlim(xLim)
    set(p3,'Color','k')
    title('Capital Market Equilibrium')
    xlabel('Investment Quantity I_t')
    ylabel('Capital Price Q_t')
    hl = legend([p1 p2 p3],'Investment Demand','Investment Supply','Bank Run Cutoff');
    set(hl,'Location','southoutside','Orientation','horizontal')
    legend('boxoff')
    
    I_noreg_demand = I_prime_demand1;
    I_noreg_demand_run = I_prime_demand_run1;
    
    save(strcat('investment_demand_',num2str(0.0001),'.mat'), ...
        'I_noreg_demand','I_noreg_demand_run')
    
    figureName = strcat('./Figures/equilibrium_plot_',xiName);
end

set(gca,'xtick',[])
set(gca,'xticklabel',[])

set(gca,'ytick',[])
set(gca,'yticklabel',[])

hRim = 0;
vRim = 0;
set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    paperDim = [12.8 9.6];
else
    paperDim = [21 29.7/2];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))