% function output = plotPolicyFunctions(opt)
% plot the bank run probability and expected future capital price as a
% function of the deposit level.

clear

opt.run = 'with_runs';
opt.reg = 'retail';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up the grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(opt.reg,'retail') == 1
    phi_R_inv_space = [0.05:0.05:0.15]';
elseif strcmp(opt.reg,'shadow') == 1
    phi_R_inv_space = [0:0.05:0.1]';
end

nD = length(phi_R_inv_space);

filename_solu = strcat('Results_', ...
           'GamR_',num2str(0), ...
           '_GamS_',num2str(0), ...
           '_nuR_',num2str(0), ...
           '_nuS_',num2str(0), ...
           '.mat');

load(strcat('./Results/',opt.reg,'_',opt.run,'/',filename_solu))

uncondStd = p.sigma_Z/sqrt((1-p.rho_Z^2));

figureVersion = 'slides';
scalingFactor = 1.25;

grd.n_Z = 9;
grd.n_Xi = 9;
[grd.eps_Z,grd.Z_prob] = qnwnorm(grd.n_Z,0,1);
[grd.eps_Xi,grd.Xi_prob] = qnwnorm(grd.n_Xi,0,1);

prob = kron(grd.Xi_prob,grd.Z_prob);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set a current state by fixing K^H, K^R, K^S, B, D
logZ = [p.mu_Z - uncondStd;
    p.mu_Z + uncondStd];

N_R = steady.N_R*ones(2,1);
N_S = steady.N_S*ones(2,1);
K   = steady.K*ones(2,1);

N_R_old = N_R;
N_S_old = N_S;
nError = 10;
tol = 1e-6;
iter = 1;
maxIter = 100;

while nError > tol && iter <= maxIter
    states = [N_R N_S K logZ];   
    states_resc = Rescale(states,funs.unit_min,funs.unit_max,funs.orig_min,funs.orig_max);
    states_poly = Smolyak_Polynomial(states_resc,funs.nDims,funs.max_approx_level,funs.elem);
    
    Q = states_poly*funs.Q;
    r_K = p.alpha*exp(logZ).*K.^(p.alpha-1);
    R_K = r_K + (1-p.delta)*Q;
    
    N_R = (R_K*steady.K_R + steady.R_B*steady.B - steady.R_D*steady.D)*(1-p.sigma_R) + p.exoInc*K;
    N_S = (R_K*steady.K_S - steady.R_B*steady.B)*(1-p.sigma_S) + p.exoInc*K;
    
    nError = norm(vec(N_R - N_R_old));
    
    fprintf('Iteration %d, nError %6.4f \n',iter,nError);
    
    N_R_old = N_R;
    N_S_old = N_S;
    iter = iter+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preload the policy functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stored_funs = cell(length(phi_R_inv_space),4);

for dd=1:nD
    
    if strcmp(opt.reg,'retail') == 1
        filename_solu = strcat('Results_', ...
               'GamR_',num2str(phi_R_inv_space(dd)), ...
               '_GamS_',num2str(1/p.phi_S_bar), ...
               '_nuR_',num2str(p.nu_R), ...
               '_nuS_',num2str(p.nu_S), ...
               '.mat');
    elseif strcmp(opt.reg,'shadow') == 1
        filename_solu = strcat('Results_', ...
               'GamR_',num2str(1/p.phi_R_bar), ...
               '_GamS_',num2str(phi_R_inv_space(dd)), ...
               '_nuR_',num2str(p.nu_R), ...
               '_nuS_',num2str(p.nu_S), ...
               '.mat');
    end
        
    
    load(strcat('./Results/',opt.reg,'_',opt.run,'/',filename_solu), ...
        'pols','pols_run','funs','funs_run')
    
    stored_funs(dd,1) = {funs};
    stored_funs(dd,2) = {funs_run};
    stored_funs(dd,3) = {pols};
    stored_funs(dd,4) = {pols_run};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preallocation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vec_phi_S       = zeros(nD,2);
vec_phi_R       = zeros(nD,2);
vec_Q           = zeros(nD,2);
vec_p_prime     = zeros(nD,2);
vec_spread_prime = zeros(nD,2);
vec_E_Q_star_prime = zeros(nD,2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute the policy functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for jj = 1:size(states,1)
    
iter = 1;
nError = 10;

    for dd=1:nD    
        funs        = stored_funs{dd,1};
        funs_run    = stored_funs{dd,2};
        pols        = stored_funs{dd,3};
        pols_run    = stored_funs{dd,4};

        states_resc = Rescale(states(jj,:),funs.unit_min,funs.unit_max, ...
            funs.orig_min,funs.orig_max);
        states_poly = Smolyak_Polynomial(states_resc,funs.nDims, ...
            funs.max_approx_level,funs.elem);
        
        pols.E_p_prime  = pols.p_prime*prob;
        
        coeffs_spread_prime = funs.base_poly\(pols.E_R_K_S_prime./pols.R_B_prime);
        coeffs_phi_R    = funs.base_poly\pols.phi_R;
        coeffs_phi_S    = funs.base_poly\pols.phi_S;
        coeffs_Q        = funs.base_poly\pols.Q;
        coeffs_p_prime  = funs.base_poly\pols.E_p_prime;
        coeffs_N_R_prime_n2r      = funs.base_poly\pols.N_R_prime_n2r;
        
        coeffs_K_H      = funs.base_poly\pols.K_H_prime;
        coeffs_K_R      = funs.base_poly\pols.K_R_prime;
        coeffs_K_S      = funs.base_poly\pols.K_S_prime;

        coeffs_K_H_star = funs_run.base_poly\pols_run.K_H_prime;
        coeffs_K_R_star = funs_run.base_poly\pols_run.K_R_prime;
        
        N_R_prime_n2r = states_poly*coeffs_N_R_prime_n2r;
        K_prime = states_poly*(coeffs_K_H + coeffs_K_R + coeffs_K_S);
        logZ_prime = (1-p.rho_Z)*p.mu_Z + p.rho_Z*states(jj,4) + p.sigma_Z*repmat(grd.eps_Z,grd.n_Xi,1);
        
        nextStates = [N_R_prime_n2r' K_prime*ones(size(logZ_prime)) logZ_prime];
        nextStates_resc = Rescale(nextStates,funs_run.unit_min,funs_run.unit_max, ...
            funs_run.orig_min,funs_run.orig_max);
        nextStates_poly = Smolyak_Polynomial(nextStates_resc,funs_run.nDims, ...
            funs_run.max_approx_level,funs_run.elem);
        
        coeffs_Q_star = funs_run.base_poly\pols_run.Q;
        
        Q_star_prime = nextStates_poly*coeffs_Q_star;
        
        vec_E_Q_star_prime(dd,jj) = prob'*Q_star_prime;
        vec_phi_R(dd,jj) = states_poly*coeffs_phi_R;
        vec_phi_S(dd,jj) = states_poly*coeffs_phi_S;
        vec_Q(dd,jj)     = states_poly*coeffs_Q;
        vec_p_prime(dd,jj)  = max(400*states_poly*coeffs_p_prime,0);
        vec_spread_prime(dd,jj) = states_poly*coeffs_spread_prime;

    end
end

%% Future Bank Run Probability

yVals = cell(4,1);
yVals(1) = {vec_p_prime};
yVals(2) = {vec_E_Q_star_prime};
yVals(3) = {vec_phi_S};
yVals(4) = {vec_phi_R};

yNames = {'(a) Bank Run Probability','(b) Future Fire Sale Price','(c) Shadow Bank Leverage', ...
    '(d) Retail Bank Leverage'};
yLabels = {'$E_t[p_{t+1}]$','$E_t[Q_{t+1}^*]$','$\phi_t^R$','$\phi_t^S$'};

fontOptions = {'Interpreter','latex','FontSize',12};
fontOptions_Alt = {'FontSize',6};

figure
for aa = 1:4

    subplot(2,2,aa)
    
    plot(phi_R_inv_space,yVals{aa}(:,1),'b','LineWidth',2)
    
    hold
    
    plot(phi_R_inv_space,yVals{aa}(:,2),'r:','LineWidth',2)
    
    yLim = ylim;
    hl3 = line([min(phi_R_inv_space) max(phi_R_inv_space)],[0 0],'Color','k');
    
    xlim([min(phi_R_inv_space) max(phi_R_inv_space)])
    ylim(yLim)
    
    ylabel(yLabels{aa},fontOptions{:})
    if strcmp(opt.reg,'retail') == 1
        xlabel('$$1/\bar{\phi}^R_t$$',fontOptions{:})
    elseif strcmp(opt.reg,'shadow') == 1
        xlabel('$$1/\bar{\phi}^S_t$$',fontOptions{:})
    end
    title(yNames{aa},fontOptions{:})
end

hl = legend('Recession (Low Z)','Expansion (High Z)');
if strcmp(opt.reg,'retail') == 1
    set(hl,'Location','SouthWest',fontOptions_Alt{:})
elseif strcmp(opt.reg,'shadow') == 1
    set(hl,'Location','West',fontOptions_Alt{:})
end

vRim = 0.25;
hRim = 0.25;

set(gcf,'Units','centimeters')
if strcmp(figureVersion,'slides') == 1
    paperDim = scalingFactor*[12.8 9.6];
else
    paperDim = [21 29.7/2];
end
set(gcf,'PaperSize',paperDim)
set(gcf,'PaperPosition',[hRim,vRim,paperDim(1)-2*hRim,paperDim(2)-2*vRim])

figureName = strcat('./Figures/p_against_phi_R_',opt.reg);
print(figureName,'-dpdf')
saveas(gcf,strcat(figureName,'.fig'))

