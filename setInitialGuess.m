function [pols,pols_run,vals,vals_run] = setInitialGuess(p,steady,grd,states_no_run,states_run)

vec_K = states_no_run(:,3);
vec_logZ = states_no_run(:,4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Check if a previous guess exists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if exist('initialGuess.mat','file') == 2
    
    load('initialGuess.mat','pols','pols_run','vals','vals_run','funs','funs_run')
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % interpolate on new grid
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    run = 0;
    pols = interpolate_on_new_grid( pols,funs,states_no_run,grd,run );
    vals = interpolate_on_new_grid( vals,funs,states_no_run,grd,run );
    
    run = 1;
    pols_run = interpolate_on_new_grid( pols_run,funs_run,states_run,grd,run );
    vals_run = interpolate_on_new_grid( vals_run,funs_run,states_run,grd,run );
    
else

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Initial Guess, No Run
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % use the steady state policy functions as initial guess
    
    % policy functions
    pols.K_H_prime = steady.K_H*ones(grd.n_total,1);
    pols.K_R_prime = steady.K_R*ones(grd.n_total,1);
    pols.K_S_prime = steady.K_S*ones(grd.n_total,1);           
    pols.phi_R     = steady.phi_R*ones(grd.n_total,1);
    pols.phi_S     = steady.phi_S*ones(grd.n_total,1);
    pols.C_H       = steady.C_H*ones(grd.n_total,1);
    pols.B_prime   = steady.B*ones(grd.n_total,1);
    pols.D_prime   = steady.D*ones(grd.n_total,1);
    pols.Q         = ones(grd.n_total,1);
    
    pols.R_D_prime = steady.R_D*ones(grd.n_total,1);
    pols.R_B_prime = steady.R_B*ones(grd.n_total,1);
    pols.E_R_K_prime = steady.R_K*ones(grd.n_total,1);

    % LoMs
    pols.N_R_prime_n2n = steady.N_R*ones(grd.n_total,grd.n_Z*grd.n_Xi);
    pols.N_R_prime_n2r = steady.N_R*ones(grd.n_total,grd.n_Z*grd.n_Xi);
    pols.N_S_prime_n2n = steady.N_S*ones(grd.n_total,grd.n_Z*grd.n_Xi);
    pols.N_S_prime_n2r = steady.N_S*ones(grd.n_total,grd.n_Z*grd.n_Xi);
    pols.x_prime = ones(grd.n_total,grd.n_Z*grd.n_Xi);
    pols.p_prime = zeros(grd.n_total,grd.n_Z*grd.n_Xi);
    pols.K_prime = steady.K*ones(grd.n_total,1);
    pols.I = pols.K_prime - (1-p.delta)*vec_K;
    
    % Value Functions
    vals = struct;
    vals.C_H = pols.C_H;
    vals.V_R = steady.V_R*ones(grd.n_total,1);
    vals.V_S = steady.V_S*ones(grd.n_total,1);  
    vals.Q   = ones(grd.n_total,1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Initial Guess, Run
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % use the steady state policy functions as initial guess

    pols_run = struct;
    pols_run.K_H_prime      = steady.K_H*ones(grd.n_total_run,1);
    pols_run.K_R_prime      = steady.K_R*ones(grd.n_total_run,1);
    pols_run.K_prime        = steady.K*ones(grd.n_total_run,1);
    pols_run.I              = p.delta*steady.K*ones(grd.n_total_run,1);
    pols_run.phi_R          = steady.phi_R*ones(grd.n_total_run,1);
    pols_run.R_D_prime      = steady.R_D*ones(grd.n_total_run,1);
    pols_run.R_B_prime      = steady.R_B*ones(grd.n_total_run,1);
    pols_run.D_prime        = steady.D*ones(grd.n_total_run,1);
    pols_run.N_R_prime_r2n  = steady.N_R*ones(grd.n_total_run,grd.n_Z);
    pols_run.N_R_prime_r2r  = steady.N_R*ones(grd.n_total_run,grd.n_Z);
    pols_run.N_S_prime      = p.exoInc*steady.K*ones(grd.n_total_run,grd.n_Z);
    pols_run.C_H            = steady.C_H*ones(grd.n_total_run,1);
    pols_run.Q              = ones(grd.n_total_run,1);

    % additional policies to ensure backwards compatibility of the code
    pols_run.N_R_prime      = steady.N_R*ones(grd.n_total_run,grd.n_Z);
    pols_run.N_R_prime_star = steady.N_R*ones(grd.n_total_run,grd.n_Z);
    
    vals_run = struct;
    vals_run.C_H            = steady.C_H*ones(grd.n_total_run,1);
    vals_run.V_R            = steady.V_R*ones(grd.n_total_run,1);
    vals_run.Q              = ones(grd.n_total_run,1);
end