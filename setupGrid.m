function [grd,states_no_run,states_run,funs,funs_run] = setupGrid(p,steady)

grd = struct;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

scale_N_R_minus = 0.5;
scale_N_R_plus = 0.75;

scale_N_S_minus = 1 - p.exoInc*steady.K/steady.N_S;
scale_N_S_plus = 1 + p.exoInc*steady.K/steady.N_S;

scale_K_minus = 0.25;
scale_K_plus = 0.15;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid Bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

grd.K_l = (1-scale_K_minus) * steady.K;
grd.K_u = (1+scale_K_plus) * steady.K;

grd.N_R_l = (1-scale_N_R_minus)*steady.N_R;
grd.N_R_u = (1+scale_N_R_plus)*steady.N_R;

grd.N_S_l = (1-scale_N_S_minus)*steady.N_S;
grd.N_S_u = (1+scale_N_S_plus)*steady.N_S;  % /////

p.uncondStd = sqrt(p.sigma_Z^2/(1-p.rho_Z^2));
grd.Z_l = p.mu_Z - 3*p.uncondStd;
grd.Z_u = p.mu_Z + 3*p.uncondStd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% No Run Grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
funs = struct;
funs.orig_min = [grd.N_R_l grd.N_S_l grd.K_l grd.Z_l];
funs.orig_max = [grd.N_R_u grd.N_S_u grd.K_u grd.Z_u];

funs.unit_min = -ones(size(funs.orig_min));
funs.unit_max = ones(size(funs.orig_max));

% 1 Smolyak approximation level
funs.approx_level = [5 5 5 5];
funs.max_approx_level = max(funs.approx_level);
funs.nDims = length(funs.approx_level);

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(funs.nDims,funs.max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,funs.approx_level);
funs.elem = elem_aniso;

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(funs.nDims,funs.max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
funs.base_poly = Smolyak_Polynomial(unit_grid,funs.nDims,funs.max_approx_level,elem_aniso);

% original smolyak grid
states_no_run = Rescale(unit_grid,funs.orig_min,funs.orig_max,funs.unit_min,funs.unit_max);
grd.n_total = size(states_no_run,1);
vec_N_R = states_no_run(:,1);
vec_N_S = states_no_run(:,2);
vec_K = states_no_run(:,3);

grd.n_Z = 9;
grd.n_Xi = 9;
[grd.eps_Z,grd.Z_prob] = qnwnorm(grd.n_Z,0,1);
[grd.eps_Xi,grd.Xi_prob] = qnwnorm(grd.n_Xi,0,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run Grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
funs_run = struct;

funs_run.unit_min = funs.unit_min([1 3 4]);
funs_run.unit_max = funs.unit_max([1 3 4]);
funs_run.orig_min = funs.orig_min([1 3 4]);
funs_run.orig_max = funs.orig_max([1 3 4]);

% 1 Smolyak approximation level
funs_run.approx_level = [4 4 3];
funs_run.max_approx_level = max(funs_run.approx_level);
funs_run.nDims = length(funs_run.approx_level);

% 2 isotropic elements
elem_iso = Smolyak_Elem_Isotrop(funs_run.nDims,funs_run.max_approx_level);

% 3 anisotropic elements
elem_aniso = Smolyak_Elem_Anisotrop(elem_iso,funs_run.approx_level);
funs_run.elem = elem_aniso;

% 4 unit smolyak grid
unit_grid = Smolyak_Grid(funs_run.nDims,funs_run.max_approx_level,elem_aniso);

% 5 unit smolyak polynomials
funs_run.base_poly = Smolyak_Polynomial(unit_grid, ...
    funs_run.nDims,funs_run.max_approx_level,elem_aniso);

% original smolyak grid
states_run = Rescale(unit_grid,funs_run.orig_min, ...
    funs_run.orig_max,funs_run.unit_min,funs_run.unit_max);
grd.n_total_run = size(states_run,1);
vec_N_R_star = states_run(:,1);
vec_K_star = states_run(:,2);